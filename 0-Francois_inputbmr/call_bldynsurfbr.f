c***********************************************************************
      PROGRAM call_bldyn
c***********************************************************************
      IMPLICIT NONE
      
      INTEGER           nfit,i,
     +                  cyc1,ncyc,nrep,vb,bmrtype(3,4),
     +                  bldseed(4),bldseedd(4),datseed                  !(Francois)
      REAL              tfitf(2),xfit(100),xfitmn(100),xfitmx(100),f
      REAL              dur
      CHARACTER         datetime*14,simid*8,simname*40 !franky
      CHARACTER*250     bmrfile,magfile,inrfile,inrsurf
      
      REAL, EXTERNAL :: bldynfit
c-----------------------------------------------------------------------
      
c---- Read input
      WRITE(*,*) 'Enter time (YYYYmmddHHMMSS):'
      READ (*,'(a14)') datetime
      WRITE(*,'(a14)') datetime
      WRITE(*,*) 'Enter cyc1,ncyc,nrep,tfitf(2):'
      READ (*,*)               cyc1,ncyc,nrep,tfitf
      WRITE(*,'(3i10,2f10.3)') cyc1,ncyc,nrep,tfitf
      WRITE(*,*) 'Enter bmrtype(3,4),bldseed(4),bldseedd(4),datseed:'   !(Francois)
      READ (*,*)         bmrtype,bldseed,bldseedd,datseed               !(Francois)
      WRITE(*,'(21i10)') bmrtype,bldseed,bldseedd,datseed               !(Francois)
      WRITE(*,*) 'inrfile, inrsurf, bmrfile, magfile, simname, simid:'
      READ (*,'(a250)') inrfile
      WRITE(*,'(a250)') inrfile
      READ (*,'(a250)') inrsurf
      WRITE(*,'(a250)') inrsurf
      READ (*,'(a250)') bmrfile
      WRITE(*,'(a250)') bmrfile
      READ (*,'(a250)') magfile
      WRITE(*,'(a250)') magfile
      READ (*,'(a40)') simname
      WRITE(*,'(a40)') simname
      READ (*,'(a8)') simid
      WRITE(*,'(a8)') simid
      
c---- Read parameters
      WRITE(*,*) 'Enter nfit, xfit, xfitmn, xfitmx:'
      READ (*,*) nfit
      WRITE(*,'(i10)') nfit
      READ (*,*) xfit(1:nfit)
      WRITE(*,15) '      xfit = [',(xfit(i),i=1,nfit)
      READ (*,*) xfitmn(1:nfit)
      WRITE(*,15) '      xfitmn = [',(xfitmn(i),i=1,nfit)
      READ (*,*) xfitmx(1:nfit)
      WRITE(*,15) '      xfitmx = [',(xfitmx(i),i=1,nfit)
   15 FORMAT(a14,64f8.5)
      
      WRITE(*,'(/)')
      
c     nfit=8
c     xfit=[
c    +  0.43580,
c    +  0.06324,
c    +  0.50470,
c    +  0.26631,
c    +  0.62123,0.64669,0.98014,0.80042]
c     fstcyc=19
c     ncyc=5
c     nrep=1
c     incrfile='./xesults/bldyn0302_180x5_64x96_Rm 99_ybl0.072_etac8.1_'
c    +       //'etat11.9_u11.1_rbt0.60_m0.47_p9.78_w4.00_eta276_fit0.11'
c    +       //'_synth2_18x9.0_m.rst'
c     datafile='../0-surfbr/1-data/arnaud/synth_wolf_1.5xbmr_1_'
c    +       //'cycle19a23'
c     simname='synth1_19a23_nn3s1x'
c     simid='0'
      vb=2
      
c---- Call bldyn
      f=bldynfit(nfit,xfit(1:nfit),
     +                                    xfitmn(1:nfit),xfitmx(1:nfit),
     +    cyc1,ncyc,nrep,tfitf,inrfile,bmrfile,magfile,simname,simid,vb,
     +                bmrtype,bldseed,bldseedd,datseed,inrsurf,datetime)!(Francois)
     
      END PROGRAM
c=======================================================================

     
c***********************************************************************
c     This is the numerical problem to optimize:
c     The main function returns the fitness required by PIKAIA
c***********************************************************************
      INCLUDE 
     +'bldynsurfbr.f'
c=======================================================================
