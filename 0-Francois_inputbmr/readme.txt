./call_bldynsurfbr.bash
  |
  |–>./call_bldynsurfbr.f
  |    |
  |    |–>./bldynsurfbr.f
  |         |
  |         |–>./surfbr2.sub.f
  |         |–>./bldyn_includefiles/*
  |         |–>./0blds0302_sample-initial-condition_*.rst
  |
  |–>./bldyn_anim.pro
  |
  |–>./bldyn_bfly.pro

outputs:
./xesults/*