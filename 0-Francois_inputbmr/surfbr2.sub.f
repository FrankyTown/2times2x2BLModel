c***********************************************************************
      MODULE surfbr2_param
c***********************************************************************
      DOUBLE PRECISION, PARAMETER :: pi=3.141592653589793238462643383d0,
     +                               pi180 = pi/180.d0
      
c --> The following are number of elements in x- and y-direction
c     (mesh numbering assumes nesx <= nesy)
      INTEGER, PARAMETER ::          nelx=64,
     +                               nesxvrai=256, nesyvrai=128
      
c --> Number of fantom cells to add to the left and right boundaries to
c     enforce longitudinal periodicity
c     and to the top and bottom boundary to enforce null flux
c     (careful, adding fantom cells outside tht=[0,pi] means
c     deltaxx=deltacos(tht) will change sign...)
      INTEGER, PARAMETER ::          nxfant=nesxvrai/8,          !fantom
     +                               nyfant=0,                   !fantom
     +                               nesx=nesxvrai+2*nxfant,
     +                               nesy=nesyvrai+2*nyfant
c     IF nxfant=0, set avbc parameter to 1 for periodicity
      INTEGER, PARAMETER ::          avbc=0
      
c --> The following are number of time steps
      INTEGER, PARAMETER ::          nt=100, nstep=10,
     +                               nts=1, nsteps=1000
      
c --> Finite elements parameters
      INTEGER, PARAMETER ::          ntype = 23
      DOUBLE PRECISION, PARAMETER :: theta = 2./3.
      
c-----------------------------------------------------------------------
c     Parametres:
c     nnsx--> number of x nodes (depends on ntype)
c     nnsy--> number of y nodes (depends on ntype)
c     nbcs--> maximum number of boundary conditions = perimeter of domain
c     nes --> maximum number of elements (nesx*nesy)
c     nns --> maximum number of nodes (nnsx*nnsy) (depends on ntype)
c     nbs --> bandwidth of system matrices with periodic B.C.
c            (4*nnsx-3 with periodic B.C.)
c            (2*nnsx+3 without periodic B.C.)
c     ngs--> number of gauss point per element  (4 for ntype=23)
      
      INTEGER, PARAMETER :: nnt=nt*nstep, nntp1=nnt+1,
     +                      nnts=nts*nsteps, nntsp1=nnts+1,
     +                      nndx=nelx+1,
     +                      nnsx=nesx+1, nnsxvrai=nesxvrai+1,
     +                      nnsy=nesy+1, nnsyvrai=nesyvrai+1,
     +                      nnyfant=nyfant*nnsx,
     +                      nbcs=2*nnsx+2*nnsy,
     +                      nes=nesx*nesy,
     +                      nns=nnsx*nnsy,
     +                      nesvrai=nesxvrai*nesyvrai,
     +                      nnsvrai=nnsxvrai*nnsyvrai,
     +                      nbs=2*nnsx+3,ngs=4
c    +                      nbs=4*nnsx-3,ngs=4 !perbc
c-----------------------------------------------------------------------
      END MODULE
c=======================================================================

c***********************************************************************
      SUBROUTINE surfbr2_init(tbmr,            !franky removed bldseedd
     +    uin,usin,qin,nin,vin,win,etasin,tdin,durin,ampin,expin,lerfin,
     +                 path,filename,bmrfilein,magfile,simname,simid,vb,
     +                                               bmrtypein,incrfile)
c***********************************************************************
c     Solves an equation of the form:                                  *
c                                                                      *
c         dA    d              dA      d               dA              *
c     rho -- = --[alpha_x(x,y) -- ] + --[ alpha_y(x,y) -- ]            *
c         dt   dx              dx     dy               dy              *
c                                                                      *
c                           dA                dA                       *
c            + gamma_x(x,y) -- + gamma_y(x,y) -- + beta(x,y) A         *
c                           dx                dy                       *
c                                                                      *
c     The functional form of the coefficient alpha, gamma, etc...      *
c     is set in subroutine "elem23s" or "elem24s"                      *
c***********************************************************************
c     MODIFICATIONS DISPONIBLES DANS LE CODE:
c     To enforce periodicity:
c     - By imposing a linear average to first and last column:
c        search for "avbc"
c     - By applying a constraint equation:
c        search for "perbc" !!! NE MARCHE PAS !!!!!!!!!!!!!!!!!!!!!!!!!!
c     - By adding fantom cells on sides of the domain:
c        search for "fantom"
c     - By forcing right boundary nodes to equal left boundary nodes:
c        see code "democode2d.perdbc.onespotmodif.f" (failed...)
c***********************************************************************
c     Version 1.0,         ...            Paul Charbonneau, HAO/NCAR
c     ...                  ...               ''
c     Nettoyage...         2011-...       Alex. Lemerle,    GRPS/UdeM
c     Périodicité (fantom) 2011-...          ''
c     Subroutines inputbmr 2011-...          ''
c     ...                  ...               ''
c     inputbmr: gaussienne 2012-03-02        ''
c     Version 1.5:
c     Double + Include...  2012-03-09        ''
c     Profil circ. mer.    2012-06-19        ''
c     Global param...      2012-06-21        ''
cc    Test cut circ.mer.   2012-08-06        ''
c     Top & bottom fantom  2012-08-07        ''
c     Version 2.0:
c     br->Br & mu->tht     2012-09-11        ''
c     Initial condition    2012-09-26        ''
c     ivrb                 2012-01-24        ''
c     calvecpot            2012-02-05        ''
c     exp. decay (taud,td) 2013-05-28        ''
c     iout                 2013-05-28        ''
c     subroutine surfbr2   2013-06-10        ''
c     Ajout qq             2014-04-09        ''
c     Ajout vv et nn       2014-05-01        ''
c     Subroutine surfbr2   2014-05-05        ''
c     Ajout us0, etas0     2014-09-23        ''
c     Self-generated BMRs  2015-01-01        ''
c     visible/hidden side  2017-06-20     Alex. Lemerle & Francois Labonville
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      
c      INTEGER          bldseedd(4)                                      !(Francois)
      REAL             tbmr,uin,usin,qin,nin,vin,win,etasin,tdin,durin, !(Francois)
     +                 ampin,expin,lerfin
      CHARACTER        simid*8,simname*40
      CHARACTER*100    home,scratch,path
      CHARACTER*250    filename,bmrfilein,magfile,bmrfile
      CHARACTER*250    logfile,outfile,latfile,incrfile,incwfile
      INTEGER          bmrtypein(3,4),vb,seed,strlen,its,itts
      DOUBLE PRECISION datawang(11),uuu,etass
      
c     INTEGER           nncyc
c     PARAMETER         (nncyc=29)
c     INTEGER           fstcyc,ncyc,nrep,dcyc,dmonth(12),year,month,
c    +                  icyc,fsticyc,tcyc(nncyc)
c     DOUBLE PRECISION  ycyc(nncyc),daycyc(nncyc)
c-----------------------------------------------------------------------
      
c-----------------------------------------------------------------------
c---- Global parameters ------------------------------------------------
      
c --> Solar parameters (CGS) -----
c     uu,us    Amplitude of the meriodional circulation (cm/s), north,south hemispheres
c     qq,vv,ww For latitudinal profile of the meriodional circulation
c     etas     Supergranular diffusivity (cm^2/s)
c     td       Exponential decay time (years)
c --------------------------------
      
      rsun  = 6.9599d10
      r2d21 = rsun*rsun/1.d21
      omeg0 = 2.894d-6
c     WRITE(*,*) 'Enter uu/100.(cm/s),etas/1.E10(cm^2/s),ww,td(years) :'
c     READ (*,*) uu,qq,vv,ww,etas,td
c     uu    = 1.5d3
c     uu    = uu*1.d2
      uu    = DBLE(uin)
      us    = DBLE(usin)
c     qq    = 1.d0
      qq    = DBLE(qin)
c     nn    = 1.d0
      nn    = DBLE(nin)
c     vv    = 1.d0
      vv    = DBLE(vin)
c     ww    = 1.d0
      ww    = DBLE(win)
c     etas  = 6.d12
c     etas  = etas*1.d10
      etas  = DBLE(etasin)
      td    = DBLE(tdin)
c     WRITE(*,*) 'uu,us,qq,vv,ww,etas,td = ',uu,us,qq,vv,ww,etas,td
      
      uuu   = uu
      etass = etas
      
c --> Values for dimensionless numbers
      rm    = uuu*rsun/etass
      ru    = uuu/omeg0/rsun
      
c --> xy (longitude-latitude) domain
      xxi   = 0.
      xxo   = 2.*pi
      yyi   = 0.                 !cmu -pi                !-pi+pi/(nesyvrai+2)
      yyo   = pi                 !cmu 0.                 !-pi/(nesyvrai+2)
      xomxi = xxo-xxi
      yomyi = yyo-yyi
      
c--------------
c     ycyc     : dates of activity minima (years)
c     dcyc     : shift between cycle number and position in ycyc
c     dmonth   : number of days in a year, before a given month
c     daycyc   : dates of activity minima (days)
c     dur      : duration of the simulation, in days
c                (anciens calculs avec: 11 y. = 4017 d., 44 y. = 16068 d., 88 y. = 32136., cycles 16-23 = 31141.)
c                (cycle19x1:  3836., cycle20x1:  4261., cycle21x1:  3744., cycle22x1:  3530., cycle23x1:  4597.)
c                (cycle18x5: 18560., cycle18x9:33408., cycle19x8: 30688., cycle20x8: 34088., cycle21x8: 29952., cycle22x8: 28240., cycle23x8: 36776.)
c                (cycles19-20x4: 32388., cycles 16-23: 31169., cycles 14-23: 39020., cycles 01-23: 92684.)
c     tau      : diffusion time = R^2/eta_T (in s)
c     to       : duration of simulation, in diffusion times
c--------------
c     ycyc= (/1701.       , 1713.       , 1724.       , 1732.       ,
c    +        1744.       , 1755.+ 2./12, 1766.+ 5./12, 1775.+ 5./12,
c    +        1784.+ 7./12, 1798.+ 4./12, 1810.+11./12, 1823.+ 4./12,
c    +        1833.+10./12, 1843.+ 6./12, 1855.+11./12, 1867.+ 2./12,
c    +        1878.+11./12, 1890.+ 2./12, 1902.+ 1./12, 1913.+ 7./12,
c    +        1923.+ 7./12, 1933.+ 8./12, 1944.+ 1./12, 1954.+ 3./12,
c    +        1964.+ 9./12, 1976.+ 5./12, 1986.+ 8./12, 1996.+ 4./12,
c    +        2008.+11./12/)
c     dcyc=5
c     
c     dmonth = (/ 0,31,59,90,120,151,181,212,243,273,304,334 /)
c     
c     DO icyc=1,nncyc
c        year = INT(ycyc(icyc))
c        month= INT((ycyc(icyc)-year)*12+1+0.5)
c        daycyc(icyc) = (year-1)*365.+INT((year-1)/4.)+dmonth(month)
c        IF (((year/4.-INT(year/4.)).EQ.0.).AND.(month.GE.3)) THEN
c           daycyc(icyc) = daycyc(icyc)+1
c        ENDIF
c     ENDDO
c     
c     WRITE(*,*) 'Enter fstcyc,ncyc,nrep :'
c     READ (*,*) fstcyc,ncyc,nrep
c     WRITE(*,*) 'fstcyc,ncyc,nrep = ',fstcyc,ncyc,nrep
c       
c     fsticyc=fstcyc+dcyc
c     dur=daycyc(fsticyc+ncyc)-daycyc(fsticyc)      
c     tcyc(1)=1
c     DO icyc=2,ncyc
c        tcyc(icyc)=
c    +          INT((daycyc(fsticyc+icyc-1)-daycyc(fsticyc))/dur*nnts)+1
c     ENDDO
c     WRITE(*,*) 'tcyc =',tcyc(1:ncyc)
      
c     dur=dur*nrep
c     WRITE(*,*) 'dur =',dur
      
c     (dur is the duration of the simulation, in days)
c     (anciens calculs avec: 11 y. = 4017 d., 44 y. = 16068 d., 88 y. = 32136., cycles 16-23 = 31141.)
c     (cycle19x1:  3836., cycle20x1:  4261., cycle21x1:  3744., cycle22x1:  3530., cycle23x1:  4597.)
c     (cycle18x5: 18560., cycle18x9: 33408., cycle19x8: 30688., cycle20x8: 34088., cycle21x8: 29952., cycle22x8: 28240., cycle23x8: 36776.)
c     (cycles19-20x4: 32388., cycles 16-23: 31169., cycles 14-23: 39020., cycles 01-23: 92684.)
c     dur   = 18560.
c     WRITE(*,*) 'Duration of the simulation (days):'
c     READ (*,*) dur
      dur   = DBLE(durin)
c     WRITE(*,*) dur
      
      tau   = rsun/uuu
      tomti = dur*24.*3600./tau
c     tomti = 10.
      taud  = td*365.25*24.*3600./tau
      
c--------------
c     Initial condition:
c     ampi     : if incr=0, amplitude of initial field distribution (G)
c     expi     : if incr=0, exponent of the cos(theta)
c     faci     : if incr=1, multiplication factor for rst file
c--------------
c     ampi = -10.      !11.5 (positif pour cycles impairs, pas considéré si incr=1)
c     WRITE(*,*) 'Amplitude (positive) of initial condition (G):'
c     READ (*,*) ampi
      ampi = DBLE(ampin)
c     IF ((fstcyc/2.-fstcyc/2).EQ.0.) ampi=-ampi
c     WRITE(*,*) 'ampi = ',ampi
      expi = DBLE(expin)
      lerfi= DBLE(lerfin)
      faci = 1.           !franky multiplication factor
      
c --> Types of BMR emergence (3x4 table):
c     4 rows for 4 half-hemispheres (1VisibleNorthern, 2HiddenNorthern, 3VisibleSouthern, 4HiddenSouthern)
c     3 terms per row (0 to deactivate, 1 to activate:
c     (1) BMRs read from database (Wang&Sheeley1989 format)
c     (2) random BMRs ("nbmr"),
c     (3) self-generate BMRs from toroidal field,
c     ((0) 1-polarity MR ("onespot")))
      bmrtype=bmrtypein
c     Parameters for all emergences (width=6° Munoz2010)
      wdth  = 4.
c     Parameters for artificial emergences
c     (see subroutines "inputbmr2" and "inputbmr3")
      nfbmr    = 100                                                    !Number of random emergences in inputbmr2
      nbmrmax  = 1000
      flux0min = 20.       !21.4                                        !Log(Maxwell)
      flux0max = 22.       !21.4                                        !Log(Maxwell)
      flux0fit = 21.35                                                  !Log(Maxwell)
      flux0dev = 0.55                                                   !Log(Maxwell)
      awbmrmin = 5.        !1.                                          !Degrees
      awbmrmax = 5.        !10.                                         !Degrees
      awbmrlin0= 0.46                                                   !Log(degrees)
      awbmrlin1= 0.42                                                   !Log(degrees)/Log(Maxwell)
      awbmrdev = 0.16                                                   !Log(degrees)
      phi0min  = 0.                                                     !Degrees (min longitude of emergence)
      phi0max  = 360.                                                   !Degrees (max longitude of emergence)
      phimask  = [0.,180.]                                              !Degrees (interval of 'visible hemisphere' longitudes on the Sun)
      phimaskv = 0.                                                     !Degrees/day (speed of rotation of the heliocentric longitude)
      tht0min  = 55.       !90.                                         !Degrees
      tht0max  = 125.      !90.                                         !Degrees
      thtmask  = [0.,90.]                                               !Degrees (interval of 'northern hemisphere' colatitudes on the Sun)
      tiltjoy  = 0.50
      tiltdev0 = 8.5                                                    !Degrees
      tiltdev1 = 12.                                                    !Degrees
      tiltdev2 = 0.8                                                    !Log(Maxwell)
c---- Tilt-quenching against bfit amplitude:
c     (see the use of DYNAMO NUMBER 'bfitfac' in main program)
      bfitqch  = 1.5       !franky
      
c --> Parameters for vector potential
      aphi0    = 0.        !Value at the poles
      lerf     = 0.2       !Width of the error function...
      
c --> Verbosity: 0 for no log, 1 for file.log, 2 for file.log and shell
c     ivrb=0
      IF (vb.EQ.2) THEN
         ivrb=2
      ELSE
         ivrb=0
      ENDIF
      
c --> Select output: 0 for no output, 1 for full output only (*.i3e), 2 for axisymetric vector potential only (*.alat), 3 for both
c     iout=0
      IF (vb.GE.1) THEN
         iout=3
      ELSE
         iout=0
      ENDIF
      
c-----------------------------------------------------------------------
c----- Title and files ---------------------------------------------
      
c --> Header
      WRITE(title,17) nts,nsteps,nesxvrai,nesyvrai,nxfant,nyfant,
     +                rm,ru,bmrtype
   17 FORMAT('B-L mech. ',i3,'x',i5,'steps ',i3,'x',i3,' nxfant=',i2,
     +       ' nyfant=',i2,' Rm=',f5.1,' Ru=',f6.3,' bmrtype=',12i1)
      
c---- All output files go to directory [path]
c     all    : output  file [outfile] solution at each time step
c     incr=1 : restart file [incrfile] being used as initial condition
c     incw=1 : restart file [incwfile] being written
      IF (vb.GE.1) THEN
         incw=1
      ELSE
         incw=0
      ENDIF
c     incrfile='0'
c     WRITE(*,*) 'Enter incrfile:'
c     READ (*,'(a250)') incrfile
c     WRITE(*,*) 'incrfile = ',incrfile
      IF (incrfile(1:strlen(incrfile)).EQ.'0') THEN
         incr=0
      ELSE
         incr=1
      ENDIF
      
c --> Unit 2: Long output
c     CALL GETENV('HOME',home)
c     CALL GETENV('SCRATCH',scratch)
c     scratch = home(1:strlen(home)) // '/Documents/scratch'
      
c     path='./'
c     path=scratch(1:strlen(scratch)) // '/0-surfbr/xesults/'
c     WRITE(*,*) 'Enter path'
c     READ (*,'(a250)') path
c     WRITE(*,*) 'path = ',path
      
      filename=filename(1:strlen(filename))//'.s'
      
      logfile  = path(1:strlen(path))//filename(1:strlen(filename))
     +                                                          //'.log'
      outfile  = path(1:strlen(path))//filename(1:strlen(filename))
     +                                                          //'.i3e'
      latfile  = path(1:strlen(path))//filename(1:strlen(filename))
     +                                                         //'.alat'
      incwfile = path(1:strlen(path))//filename(1:strlen(filename))
     +                                                          //'.rst'
      bmrfile  = path(1:strlen(path))//filename(1:strlen(filename))
     +                                                          //'.dat'
      
c     IF(ivrb.GE.1)             OPEN(11,FILE=logfile)
      IF(iout.EQ.1.OR.iout.EQ.3)OPEN(18,FILE=outfile,FORM='UNFORMATTED')
      IF(iout.EQ.2.OR.iout.EQ.3)OPEN(14,FILE=latfile,FORM='UNFORMATTED')
      IF(incw.EQ.1)            OPEN(19,FILE=incwfile,FORM='UNFORMATTED')
      IF(incr.EQ.1)            OPEN(17,FILE=incrfile,FORM='UNFORMATTED')
      OPEN(34,FILE=bmrfile)
      WRITE(34,'(a46)')
     +                  'BLDYN self-generated data      Duration (days)'
      WRITE(34,'(f46.4)') dur
      WRITE(34,'(a46,a48)')
     +                  '   MBR# Cycle Sign      Date & UT   Time(days)'
     +               ,' Flux(10^21Mx) Sign1 Colat1  Long1 Colat2  Long2'
      
      IF(ivrb.GE.1) THEN
         
         WRITE(10,'(a250)') bmrfile
         CLOSE(10)
         
         WRITE(20,'(a250)') filename
         WRITE(20,'(a40)') simname
         WRITE(20,'(a250)') magfile
         WRITE(20,'(8f9.2)') uu/100.,qq,vv,ww,etas/1.e10,td,ampi,expi
         CLOSE(20)
         
      ENDIF
      
c --> Set parameters for the emergence of BMR
      nbmr=0
      DO itts=1,nntsp1
         nnbmr(itts)=0
         nnbmrin(itts)=0
      ENDDO
      
c     SELECT CASE (bmrtype)
      
c --- inputbmr0: 1-polarity MR
c     CASE (0)
c        
c        seed=1999
c        CALL urands_init(seed)                                         !Resets random seed
c        
c        path=path(1:strlen(path))//'onespot/'
c        WRITE(filename,18) nts,nsteps,nesxvrai,nesyvrai,nxfant,nyfant,
c    +                INT(rm+0.5),ru,INT(wdth),INT(tht0min),INT(phi0min)
c  18    FORMAT('surfbr2_',i3,'x',i5,'_',i3,'x',i2,'_',i2,'x',i1,'fant',
c    +              '_Rm',i2,'_Ru',f5.3,'_wdth',i1,'_colat',i2,'lon',i3)
         
c --- inputbmr1: Read BMRs from database (Wang&Sheeley1989 format)
c     CASE (1)
      IF ((bmrtype(1,1).EQ.1.).OR.(bmrtype(1,2).EQ.1.).OR.
     +    (bmrtype(1,3).EQ.1.).OR.(bmrtype(1,4).EQ.1.)) THEN
         
c        bmrfilein='.../data/arnaud/synth_wolf_1.5xbmr_0_cycle18x5.dat'
c        bmrfilein='.../data/wang/wang_8cycles.dat'
c        WRITE(*,*) 'Name of data file:'
c        READ (*,'(a250)') bmrfilein
c        WRITE(*,*) 'bmrfilein = ',bmrfilein
         
         OPEN(33,FILE=bmrfilein)
         READ(33,*) ! Skip header (3 lines)
         READ(33,*) ! Skip header (3 lines)
         READ(33,*) ! Skip header (3 lines)
         DO WHILE(.TRUE.)
            READ(33,*,END=271) datawang
            itts=INT(datawang(5)/dur*nnts+0.5)+1
            nnbmrin(itts)=nnbmrin(itts)+1
         ENDDO
  271    CLOSE(33)
         OPEN(33,FILE=bmrfilein) ! Re-open file at the beginning
         READ(33,*) ! Skip header (3 lines)
         READ(33,*) ! Skip header (3 lines)
         READ(33,*) ! Skip header (3 lines)
         
c        simname='wang8'
c        simname='synth0_18x5'
c        WRITE(*,*) 'Name of simulation:'
c        READ (*,'(a20)') simname
c        WRITE(*,*) 'simname = ',simname
         
c        WRITE(filename,20) nts,nsteps,nesxvrai,nesyvrai,nxfant,nyfant,
c    +                     uu/100.,qq,vv,ww,INT(etas/1.d10+0.5),INT(td),
c    +                                INT(DABS(ampi)+0.5),INT(expi+0.5),
c    +                                  INT(wdth+0.5),simname,incr,simid
c  20    FORMAT('surfbr2s_',i3,'x',i5,'_',i3,'x',i3,'_',i2,'x',i1,'fant'
c    +          ,'_u',f4.1,'q',f4.1,'v',f3.1,'w',f4.1,'eta',i4,'tau',i2,
c    +                                          'a',i2,'n',i2,'wdth',i1,
c    +                                            '_',a12,'.',i1,'_',a4)
cc   +                                             '_1.5xsynth0_20.',i1)
cc   +                                      '_i8G_1.5xsynth4_14-23.',i1)
cc   +                                                '_i20G_wang8.',i1)
         
      ELSE
         bmrfilein=bmrfile(1:strlen(bmrfile))
      ENDIF
      
c --- inputbmr2: Set probability for a BMR to emerge at each time step
c     CASE (2)
      IF ((bmrtype(2,1).EQ.1.).OR.(bmrtype(2,2).EQ.1.).OR.
     +    (bmrtype(2,3).EQ.1.).OR.(bmrtype(2,4).EQ.1.)) THEN
         
c ------ Defines an emergence probability proportional to the desired
c        number of BMRs (such that the final number of BMRs approximately= nfbmr)
         ifbmr=nfbmr/DBLE(nnts)
c        ifbmr=(nfbmr-1)/DBLE(nnts)                                     !Set this if want to impose emergence at first timestep
         IF (ivrb.GE.1) THEN
            WRITE(1,*) 'inputbmr2: probability of emergence = ',ifbmr
            IF (ivrb.GE.2) THEN
            WRITE(*,*) 'inputbmr2: probability of emergence = ',ifbmr
            ENDIF
         ENDIF
c        CALL SYSTEM_CLOCK(COUNT=seed)        
c        seed=1999
c        CALL urands_init(seed)                                         !Resets random seed
c        CALL RANDOM_SEED(PUT=seed)                  !Resets random seed
         
c        path=path(1:strlen(path))//'nbmr/'
c        WRITE(filename,19) nts,nsteps,nesxvrai,nesyvrai,nxfant,nyfant,
c    +                              INT(rm+0.5),ru,INT(wdth),nfbmr,theta
c  19    FORMAT('surfbr2_',i3,'x',i5,'_',i3,'x',i3,'_',i2,'x',i1,'fant',
c    +             '_Rm',i2,'_Ru',f5.3,'_wdth',i1,'_',i3,'bmr_tht',f3.1)
         
      ENDIF
      
c --- inputbmr3: Self-generated emergences
c     CASE (3)
      IF ((bmrtype(3,1).EQ.1.).OR.(bmrtype(3,2).EQ.1.).OR.
     +    (bmrtype(3,3).EQ.1.).OR.(bmrtype(3,4).EQ.1.)) THEN
         
c        simname='auto00'
c        WRITE(*,*) 'Name of simulation:'
c        READ (*,'(a20)') simname
c        WRITE(*,*) 'simname = ',simname
         
c        WRITE(filename,21) nts,nsteps,nesxvrai,nesyvrai,nxfant,nyfant,
c    +                     uu/100.,qq,vv,ww,INT(etas/1.d10+0.5),INT(td),
c    +                                INT(DABS(ampi)+0.5),INT(expi+0.5),
c    +                    INT(wdth+0.5),tbmr,bldseedd,simname,incr,simid
c  21    FORMAT('surfbr2s_',i3,'x',i5,'_',i3,'x',i3,'_',i2,'x',i1,'fant'
c    +          ,'_u',f4.1,'q',f4.1,'v',f3.1,'w',f4.1,'eta',i4,'tau',i2,
c    +                     'a',i2,'n',i2,'wdth',i1,'_tbmr',f4.2,'sd',i2,
c    +                                            '_',a12,'.',i1,'_',a4)
         
c        nnbmr(2) = nnbmr(1)+nnbmr(2)
         nnbmr(1) = 0
      ENDIF
      
c     CASE DEFAULT
c     STOP '!! Error in the choice of BMR type !!'  
c     END SELECT
      
c-----------------------------------------------------------------------
      
c---> Output some header info
      IF (ivrb.GE.1) THEN
         CALL headers(logfile,outfile,latfile,incrfile,incwfile,bmrfile)
      ENDIF
      
c---> Generation du maillage spatial et calcul des coefficients
c     de l'equation de diffusion aux points de gauss
      CALL mesh2ds
      
      its=0
      istep=1
      itts=1
      ctime=0.d0
      IF (ivrb.GE.1) THEN
         write(1,221) its,istep,itts,ctime,nnbmrin(itts)
         IF (ivrb.GE.2) THEN
         WRITE(*,221)  its,istep,itts,ctime,nnbmrin(itts)
         ENDIF
      ENDIF
      
c---> Calculated longitudinaly averaged vector potential
      CALL calvecpot
      
c---> Output first time step
      CALL writouts(its,itts)
      
      onemth=1.d0-theta
      rns=DBLE(nsteps)
      
c-----------------------------------------------------------------------
  221 FORMAT('INITIAL: Time step #',i4,'.',i5,'=',i6', ctime =',f7.3,
     +       ' (',i5,' new BMRs)')
      
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE surfbr2_solve(         !franky added bldseed
     +        it,itbmr,bldseed,bldseedd,its,uu0in,us0in,
     +        qin,nin,vin,win,etas0in)
c     SUBROUTINE calfuns
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
      INTEGER          it,its,itbmr,bldseed(4),bldseedd(4)    !franky
      REAL             uu0in,us0in,qin,nin,vin,win,etas0in
c --- Local:
      DOUBLE PRECISION keff(nbs,nns),feff(nns),fns(nns),an(nns)
      equivalence      (keff,k),(fns,f),(an,a)
      INTEGER          iit,itts,i,j,jpmb,jshift,ieb,i1,i2,jj,ijleft,
     +                 ijright,ijbot,ijtop,ihem
      DOUBLE PRECISION rn,cka,av
c-----------------------------------------------------------------------
      
c---> MAIN-TIMESTEPS LOOP --------------------------------------------------
c     (its = 1 to nts, runs every time dynamo main-timestep it = multiple of (ntcall=nt/nts))
      IF (FLOAT(it-1)/ntcall.EQ.FLOAT((it-1)/ntcall)) THEN
         its=its+1
         
         uu0  = DBLE(uu0in)
         us0  = DBLE(us0in)
         qq   = DBLE(qin)
         qqm1 = qq-1.d0
         nn   = DBLE(nin)
         nnm1 = nn-1.d0
         vv   = DBLE(vin)
         vvpi = 2.d0/SQRT(pi)*vv
         ww   = DBLE(win)
         wwpi = 2.d0/SQRT(pi)*ww
         etas0= DBLE(etas0in)
         
c------- Set up time stepping parameters
         CALL tsteps(its)
         
c------- Assemble system matrices
         CALL forms(its)
         
c------- Apply boundary conditions
         CALL aplybcs
         
c------- Surface solver (calculates Aphi(R,theta))
c******* IMPLICIT INITIAL-BOUNDARY-VALUE PROBLEM
         ctime=times(its)
         
c------- Save load vector and function at start of interval
         DO 410 i=1,numnp
            f0(i)=fns(i)
            fnm1(i)=f0(i)
            anm1(i)=a(i)
  410    CONTINUE
         
c------- Forward reduction of effective stiffness matrix (keff)
         CALL tribus(numnp,mband,mfbw,k)
         
c------- Restart small-steps counter
         istep=0
         
      ENDIF
      
c---> SOLVE RECURRENCE RELATION OVER ALL SMALL-TIMESTEPS ---------------
c     (nstepscall= nsteps/ntcall = nsteps/(nt/nts) iterations at a time)
      DO 480 iit=1,nstepscall
c------  Small-timesteps counter, istep = 1 to nsteps
         istep=istep+1
         rn=DBLE(istep)
         
c------  All-timesteps counter: itts = 2 to nntsp1=nts*nsteps+1
         itts=(its-1)*nsteps+istep+1
         ctime=ctime+dt
         
c------- Calculate nat+int loads at step istep by ramping
         DO 420 i=1,numnp
            fn(i)=f0(i)+(rn/rns)*(fns(i)-f0(i)) !(cca  ) = 0
  420    CONTINUE
         
c------- Calculate effective load vector (feff) at step istep
         DO 440 j=1,numnp
            cka=0.d0
            jpmb=j+mband
            DO 430 i=MAX(1,jpmb-numnp),MIN(mfbw,jpmb-1)
               jshift=jpmb-i
               cka=cka+ck(i,jshift)*anm1(jshift)
  430       CONTINUE
            feff(j)=onemth*fnm1(j)+theta*fn(j)+cka
  440    CONTINUE
  
c------- Modify effective load vector for ramped essential bc
c --> Insertion du "IF" pour eviter keffu0 indefini si numebc=0  7/4/88
c        IF(numebc.GT.0) THEN
         DO 445 i=1,numnp
            feff(i)=feff(i)-keffu0(i)-rn*keffdu(i)
  445    CONTINUE
         DO 450 ieb=1,numebc
            i=iuebc(ieb)
            feff(i)=anm1(i)+delu(i)
  450    CONTINUE
         
c MOFIFS POUR BC PERIODIQUES (perbc) EN x, 29/5/06
c        DO ieb=1,numnp,nesx+1
c           feff(ieb)=0.d0
c        ENDDO
         
c        END IF
         
c------- Reduction of load vector: backsubstitution
         CALL rhsbus(numnp,mband,mfbw,keff,feff,an)
         
c------- Check for solution blowup
c        IF(DABS(a(numnp/2)).GE.1.d10
c    +   .or.DABS(a(numnp/2-1)).GE.1.d10)THEN
c           WRITE(*,491) its,istep
c           write(1,491) its,istep
c           STOP 'blowup'
c        END IF
         
c ------ EMERGENCE of new BMRs
c        (Selection of bmrtype is done inside the subroutines after running 
c        for all 4 half-hemispheres to ensure coherent random sequences)
c
c ------ inputbmr1: Read BMRs from database
         CALL inputbmr1(it,itbmr,itts,bldseedd)
c ------ inputbmr2: Random BMRs at each time step
         CALL inputbmr2(it,itbmr,itts,bldseed,bldseedd)
c ------ inputbmr3: Self-generated BMRs
         CALL inputbmr3(it,itbmr,itts,bldseed,bldseedd)
         
c------- Impose periodic BCs in x (avbc only)
         IF (avbc.EQ.1) THEN
            DO 455 i=1,numnp,nnsx
               i1=i
               i2=i+nesx
               av=0.5d0*(an(i1+1)+an(i2-1))
               an(i1)=av
               an(i2)=av
  455       CONTINUE
         ENDIF
         
c------- Enforce periodicity in right and left fantom columns
c        (A.Lemerle: doublechecked 2012-03-16 => OK)
         IF (nxfant.NE.0) THEN
            DO j=1,nnsy
               jj=(j-1)*nnsx
               DO i=1,nxfant
                  ijleft=jj+i
                  ijright=jj+nnsx+1-i
                  an(ijleft) =an(ijleft +nesxvrai)
                  an(ijright)=an(ijright-nesxvrai)
               ENDDO
            ENDDO
         ENDIF
c------- Copy values of top and bottom BC in top and bottom fantom rows
c        IF (nyfant.NE.0) THEN
c           DO j=1,nyfant
c              jj=(j-1)*nnsx
c              DO i=1,nnsx
c                 ijbot=jj+i
c                 ijtop=numnp+1-ijbot
c                 an(ijbot)=an(nnyfant+i)
c                 an(ijtop)=an(numnp-nnyfant+1-i)
c              ENDDO
c           ENDDO
c        ENDIF
c------- Copy -1.*values near top and bottom BC in top and bottom fantom rows,
c        to ensure that no flux crosses the boundary
         IF (nyfant.NE.0) THEN
c           WRITE(*,*) (an((j-1)*nnsx+1),j=1,nnsy)
c           WRITE(*,*)
            DO j=1,nyfant
               jj=j*nnsx
               DO i=1,nnsx
                  ijbot=nnyfant+i
                  ijtop=numnp+1-nnyfant-i
                  an(ijbot-jj) = -1.*an(ijbot+jj)
                  an(ijtop+jj) = -1.*an(ijtop-jj)
               ENDDO
            ENDDO
c           WRITE(*,*) (an((j-1)*nnsx+1),j=1,nnsy)
c           READ(*,*)
         ENDIF
         
c------- Apply (time varying) essential boundary conditions, IF any
c        none...
         
c------- Calculate longitudinally averaged vector potential
         CALL calvecpot
         
c------- Print step IF making video
         CALL writouts(its,itts)
         
c------- Save values for next step
         DO 460 i=1,numnp
            anm1(i)=an(i)
            fnm1(i)=fn(i)
  460    CONTINUE         
         
         IF (ivrb.GE.1) THEN
            write(1,221) its,istep,itts,ctime,nnbmr(itts)
            IF (ivrb.GE.2) THEN
            WRITE(*,221)  its,istep,itts,ctime,nnbmr(itts)
            ENDIF
         ENDIF
         
  480 CONTINUE
c     END OF BIG LOOP OVER TIME STEPS ! --------------------------------      
      
      IF (istep.EQ.nsteps) THEN
c------- Save nat+int loads at END of interval
         DO 490 i=1,numnp
            f0(i) = fns(i)
            u0(i) = an(i)
  490    CONTINUE
      ENDIF
      
      RETURN
c-------------------------------------
  221 FORMAT('CALFUN:  Time step #',i4,'.',i5,'=',i6', ctime =',f7.3,
     +       ' (',i5,' new BMRs)')
  491 FORMAT(//,'***** solution blowup, step #',i2,5x,
     +          'substep #',i3)
      END SUBROUTINE
c=======================================================================
      
c***********************************************************************
      SUBROUTINE surfbr2_end
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
c-----------------------------------------------------------------------
      IF(incw.EQ.1) CALL stlasts
      
      IF (ivrb.GE.1) THEN
         write(1,231) nbmr
         IF (ivrb.GE.2) THEN
         WRITE(*,231)  nbmr
         ENDIF
  231    FORMAT(/,72('*'),/,'Total number of BMRs:',i7)
      ENDIF
      
c     CLOSE(11)
      CLOSE(18)
      CLOSE(14)
      CLOSE(17)
      CLOSE(19)
      CLOSE(33)
      
c-----------------------------------------------------------------------
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE
     +        headers(logfile,outfile,latfile,incrfile,incwfile,bmrfile)
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      
      CHARACTER*200  logfile,outfile,latfile,incrfile,incwfile,bmrfile
      CHARACTER      labdate*8,labclock*10
      INTEGER        strlen
c-----------------------------------------------------------------------
      CALL DATE_AND_TIME(DATE=labdate,TIME=labclock)
      write(1,1)title,labdate,labclock
      IF (ivrb.GE.2) THEN
      WRITE(*,1) title,labdate,labclock
      ENDIF

      IF(incr.NE.1) THEN
         write(1,2)
         IF (ivrb.GE.2) THEN
         WRITE(*,2)
         ENDIF
      ELSE
         write(1,3)incrfile
         IF (ivrb.GE.2) THEN
         WRITE(*,3) incrfile
         ENDIF
      ENDIF
      write(1,5)outfile
      write(1,6)logfile
      write(1,8)latfile
      write(1,11)bmrfile
      IF (ivrb.GE.2) THEN
      WRITE(*,5) outfile
      WRITE(*,6) logfile
      WRITE(*,8) latfile
      WRITE(*,11)bmrfile
      ENDIF
      
      IF(incw.EQ.1) THEN
      	write(1,4)incwfile
         IF (ivrb.GE.2) THEN
      	WRITE(*,4) incwfile
         ENDIF
   	ENDIF
      
      write(1,7)nesx,nesxvrai,nxfant,nesy,nesyvrai,nyfant,
     +           nes,nesvrai,nns,nnsvrai,ntype,
     +           nts,nsteps,nntsp1,tomti,dur,theta
      IF (ivrb.GE.2) THEN
      WRITE(*,7) nesx,nesxvrai,nxfant,nesy,nesyvrai,nyfant,
     +           nes,nesvrai,nns,nnsvrai,ntype,
     +           nts,nsteps,nntsp1,tomti,dur,theta
      ENDIF
      
      write(1,9)
      IF (ivrb.GE.2) THEN
      WRITE(*,9)
c     WRITE(*,*) '(Press enter to CONTINUE)'
c     READ (*,*)
      ENDIF
      
      CALL SLEEP(2)
      
      RETURN
c-----------------------------------------------------------------------
    1 FORMAT(72('*'),/,
     +       '*',10x,'BABCOCK-LEIGHTON MECHANISM',/,
     +       '*',10x,'LINEAR TRANSPORT EQUATION SOLVER',/,
     +       72('*'),/,
     +       'Run : ',a200,/,
     +       'Date: ',a8,1x,a10)
    2 FORMAT('Starting from t=0 initial condition')
    3 FORMAT('Extending earlier run. Restart file: ',/,a220)
    5 FORMAT('Output  file: ',/,a220)
    6 FORMAT('Log file:     ',/,a220)
    8 FORMAT('lat-time file:',/,a220)
    4 FORMAT('Restart file: ',/,a220)
   11 FORMAT('BMR outfile:  ',/,a220)
    7 FORMAT(/,'Mesh parameters:',/,
     +      5x,'nesx       =',i12,' (=',i5,' +',i5,'x2 fantom cells)',/,
     +      5x,'nesy       =',i12,' (=',i5,' +',i5,'x2 fantom cells)',/,
     +      5x,'nes        =',i12,' (=',i5,' + fantom cells)',/,
     +      5x,'nns        =',i12,' (=',i5,' + fantom cells)',/,
     +      5x,'elem. type =',i12,/,
     +      5x,'nts        =',i12,/,
     +      5x,'nsteps     =',i12,/,
     +      5x,'nntsp1     =',i12,/,
     +      5x,'duration   =',e12.4,' (',f9.2,' days)',/,
     +      5x,'theta      =',e12.4)
    9 FORMAT(72('*'),/)
    
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE mesh2ds
c**********************************************************************
c         ===================================================         *
c                    generation du reseau d'element                   *
c         et calcul des coefficients de l'equation a resoudre         *
c         ===================================================         *
c**********************************************************************
c     --> partie spatiale traitee par elements finis lineaires,       *
c         quadratiques ou cubiques (au choix)                         *
c                                                                     *
c     --> quadrature gaussienne: elements lineaires --> 2 point       *
c                                      quadratiques --> 3 points      *
c                                          cubiques --> 4 points      *
c**********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      INTEGER           i,iwarn,ii,numnpr,numelr,ntyper,ntr,nstepsr
      DOUBLE PRECISION  xmin,xmax,ymin,ymax,
     +                  del,costht,
     +                 thetar,timeinr,xxr(nns-nes),yyr(nns-nes),ar(nns),
     +                  tht,sin1,cos1,fctsin,fctcos,fct,uumax,thtmax
      CHARACTER         label*10,titler*200
c-----------------------------------------------------------------------
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "mesh2ds" subroutine      *****')
      
c---> Construct cartesian grid x,y.
cmu   (NOT ANYMORE: y --> Attention: code solves for mu=COS(theta))
      
c---> Modify domain by adding "nyfant" fantom cells to the top and
c     bottom boundaries
      ymin=yyi-yomyi*nyfant/DBLE(nesyvrai)                       !fantom
      ymax=yyo+yomyi*nyfant/DBLE(nesyvrai)                       !fantom
      
      DO 90 i=1,nnsy
         yy(i)=(ymax-ymin)*((DBLE(i-1)/(nnsy-1))**1.0)+ymin             !theta in increasing order
cmu      yy(i)=DCOS(yy(i))
c        CALL angvels(yy(i),omg)
         IF (ivrb.GE.1) THEN
c           write(1,*) i,yy(i),omg/ru
            IF (ivrb.GE.2) THEN
c           WRITE(*,*) i,yy(i),omg/ru
            ENDIF
         ENDIF
   90 CONTINUE
c     STOP ' *TMP*'

c---> Modify domain by adding "nxfant" fantom cells to the left and
c     right boundaries, to enforce periodicity
      xmin=xxi-xomxi*nxfant/DBLE(nesxvrai)                       !fantom
      xmax=xxo+xomxi*nxfant/DBLE(nesxvrai)                       !fantom
      
      DO 102 i=1,nnsx
         xx(i)=(xmax-xmin)*((DBLE(i-1)/(nnsx-1))**1.0)+xmin
  102 CONTINUE
      
c---> Calcul du maximum de la fonction d'écoulement méridien
cvv   IF (vv.LE.1. .AND. ww.LE.1.) THEN                                 !cvv
c     Profile with sin^qq*cos^nn (analytical position of maximum)
cvv      uu0 = (qq/nn)**(-0.5*qq)*(qq/nn+1.)**(0.5*(qq+nn))             !cvv
cvv      thtmax = 90.-ATAN(SQRT(qq/nn))/pi*180.                         !cvv
cvv   ELSE                                                              !cvv
c     Profile with erf(sin/vv)^qq*erf(cos/ww)^nn
c        uumax = 0.
c        DO i=1,10001
c           tht=(pi-0.)*DBLE(i-1)/10000+0.
c           cos1 = DCOS(tht)
c           sin1 = DSIN(tht)
cvv         IF (vv.LE.1.) THEN                                          !cvv
cvv            fctsin = sin1                                            !cvv
cvv         ELSE                                                        !cvv
c              fctsin = DERF(sin1*vv)
cvv         ENDIF                                                       !cvv
cvv         IF (ww.LE.1.) THEN                                          !cvv
cvv            fctcos = cos1                                            !cvv
cvv         ELSE                                                        !cvv
c              fctcos = DERF(cos1*ww)
cvv         ENDIF                                                       !cvv
c           fct = fctsin**qq * fctcos**nn
c           IF(fct.GT.uumax) THEN
c              uumax = fct
c              thtmax = 90.-tht/pi*180.
c           ENDIF
c        ENDDO
c        uu0 = uu/uuu/uumax
c        us0 = us/uuu/uumax
cvv   ENDIF                                                             !cvv
c     IF (ivrb.GE.1) THEN
c        write(1,237) thtmax
c        IF (ivrb.GE.2) THEN
c        WRITE(*,237) thtmax
c        ENDIF
c     ENDIF
c 237 FORMAT('MERIDIONAL CIRCULATION PROFILE:',
c    +       ' Peak latitude =',f6.2,' deg')
      
c---> Mesh quality control
      IF (ivrb.GE.1) THEN
         write(1,1) nesy
         write(1,2) 1,yy(1)
         IF (ivrb.GE.2) THEN
         WRITE(*,1) nesy
         WRITE(*,2) 1,yy(1)
         ENDIF
      ENDIF
      label='          '
      del=1.d0
      iwarn=0
      DO 110 i=2,nesy
         del=(yy(i)-yy(i-1))/(yy(i+1)-yy(i))
         IF(del.GE.3..or.del.LE.0.3333)THEN
            label='*CAUTION* '
            IF(del.GE.10..or.del.LE.0.1) iwarn=1
         ENDIF
         IF (ivrb.GE.1) THEN
            write(1,2) i,yy(i),del,label
cvrb        IF (ivrb.GE.2) THEN
cvrb        WRITE(*,2) i,yy(i),del,label
cvrb        ENDIF
         ENDIF
         label='          '
  110 CONTINUE
      IF (ivrb.GE.1) THEN
         write(1,2) nnsy,yy(nnsy)
         IF (ivrb.GE.2) THEN
         WRITE(*,2) nnsy,yy(nnsy)
         ENDIF
      ENDIF
      
      IF (ivrb.GE.1) THEN
         write(1,3) nesx
         write(1,2) 1,xx(1)
         IF (ivrb.GE.2) THEN
         WRITE(*,3) nesx
         WRITE(*,2) 1,xx(1)
         ENDIF
      ENDIF
      del=1.d0
      DO 111 i=2,nesx
         del=(xx(i)-xx(i-1))/(xx(i+1)-xx(i))
         IF(del.GE.3..or.del.LE.0.3333)THEN
            label='*CAUTION* '
            IF(del.GE.10..or.del.LE.0.1) iwarn=1
         ENDIF
         IF (ivrb.GE.1) THEN
            write(1,2) i,xx(i),del,label
cvrb        IF (ivrb.GE.2) THEN
cvrb        WRITE(*,2) i,xx(i),del,label
cvrb        ENDIF
         ENDIF
         label='          '
  111 CONTINUE
      IF (ivrb.GE.1) THEN
         write(1,2) nnsx,xx(nnsx)
         IF (ivrb.GE.2) THEN
         WRITE(*,2) nnsx,xx(nnsx)
         ENDIF
      ENDIF
      IF(iwarn.EQ.1)THEN
         IF (ivrb.GE.1) THEN
            write(1,4)
            IF (ivrb.GE.2) THEN
            WRITE(*,4)
            ENDIF
         ENDIF
      ENDIF
    1 FORMAT(/,'***** SPATIAL MESH:',//,'Latitudinal zones: ',i3,
     +       /,10x,'y_i',9x,'dy_i/dy_i-1')
    2 FORMAT(i5,2x,f10.5,3x,f8.4,2x,a10,2x,a20)
    3 FORMAT(/,'Longitudinal zones: ',i3,
     +       /,10x,'x_i',9x,'dx_i/dx_i-1')
    4 FORMAT(/,
     + '**** WARNING : SEVERELY DISTORTED MESH *****',
     + ' PROCEED WITH CAUTION *****')

c---> Construct 2-D grid and connectivity matrix
      IF(ntype.EQ.23.or.ntype.EQ.25)THEN
         nnd=4
         CALL grid1s
      ELSE
         STOP 'BAD ntype'
         nnd=8
         CALL grid3s
      END IF
      
c---> Choix du nombre de points de gauss
      IF(ntype.EQ.23) ngp=2
      IF(ntype.EQ.24) ngp=3
      IF(ngp.GT.ngs)THEN
         IF (ivrb.GE.1) THEN
            write(1,5) ngp,ngs
            IF (ivrb.GE.2) THEN
            WRITE(*,5) ngp,ngs
            ENDIF
         ENDIF
         STOP ' *MESH*'
      ENDIF
    5 FORMAT(/,'***** ERROR: ngp= ',i5,5x,' ng= ',i5)

c---> Initialisations [ internal to FEM ]
      CALL initdts

c---> Compute half bandwidth and full bandwidth of system matrices
      IF(ntype.EQ.23) mband=(  nnsx+2)
      IF(ntype.EQ.24) mband=(3*nnsx+3)
c     With periodic B.C.:
c     IF(ntype.EQ.23) mband=(2*nnsx-1) !perbc
c     IF(ntype.EQ.24) mband=(5*nnsx-1) !perbc
      mfbw=2*mband-1
      IF(mfbw.GT.nbs)THEN
         IF (ivrb.GE.1) THEN
            write(1,6) mfbw,nbs
            IF (ivrb.GE.2) THEN
            WRITE(*,6) mfbw,nbs
            ENDIF
         ENDIF
         STOP ' *MESH*'
      ENDIF
    6 FORMAT(/,'***** ERROR: mfbw= ',i5,5x,' nbs= ',i5)
      IF (ivrb.GE.1) THEN
c        write(1,*) mband,mfbw
         IF (ivrb.GE.2) THEN
c        WRITE(*,*) mband,mfbw
         ENDIF
      ENDIF
c     STOP ' *TMP*'

c---> Compute gauss points position, PDE coefficients
c     at gauss points, and upwinding parameters (IF any)
      CALL gaussps

c---> INITIAL CONDITION (polar cap = activity minimum) -----------------
      sfluxin=0.d0
      usfluxin=0.d0
      
c     IF incr=0 --> begin run: initialize field
      IF(incr.NE.1)THEN
         DO i=1,numnp
            costht=DCOS(y(i))
c           a(i)=ampi*ERF(costht*DABS(costht)**(expi-1.)/lerfi)
            a(i)=ampi*costht*DABS(costht)**(expi-1.)
c           a(i)=ampi*costht*DABS(costht)**7
cmu         a(i)=ampi*DABS(y(i))*y(i)**7 * DSQRT(1.d0-y(i)**2)
         ENDDO
         timein=0.d0
         IF (ivrb.GE.1) THEN
            write(1,1009) 'ampi*COS(theta)^expi',timein
            IF (ivrb.GE.2) THEN
            WRITE(*,1009) 'ampi*COS(theta)^expi',timein
            ENDIF
         ENDIF
c     IF incr=1 --> restart run
      ELSE
         READ(17) titler
         READ(17) numnpr,numelr,ntyper
         IF (numnpr.NE.numnp) THEN
            WRITE(1,*) 'ERROR: numnpr.NE.numnp'
            STOP       'ERROR: numnpr.NE.numnp'
         ENDIF
         IF (numelr.NE.numel) THEN
            WRITE(1,*) 'ERROR: numelr.NE.numel'
            STOP       'ERROR: numelr.NE.numel'
         ENDIF
         IF (ntyper.NE.ntype) THEN
            WRITE(1,*) 'ERROR: ntyper.NE.ntype'
            STOP       'ERROR: ntyper.NE.ntype'
         ENDIF
         READ(17) ntr,nstepsr,thetar,timeinr,timein
         IF (ivrb.GE.1) THEN
            write(1,1010) titler,ntr,nstepsr,timein
            IF (ivrb.GE.2) THEN
            WRITE(*,1010) titler,ntr,nstepsr,timein
            ENDIF
         ENDIF
cca      IF(to.LE.timein)THEN
            IF (ivrb.GE.1) THEN
cca            write(1,1011) timein,timeout
               IF (ivrb.GE.2) THEN
cca            WRITE(*,1011) timein,timeout
               ENDIF
            ENDIF
cca         STOP 'initial0'
cca      END IF
         READ(17) xxr
         READ(17) yyr
         READ(17) ar
         DO i=1,numnp
            a(i) = 0.924385*ar(i)
         ENDDO
      END IF
      timeout=timein+tomti
      
c --- EMERGENCE of new BMRs at first time step
c     (Selection of bmrtype is done inside the subroutines after running 
c     for all 4 half-hemispheres to ensure coherent random sequences)
c     
c --- inputbmr1: Read BMRs from database
      CALL inputbmr1(1,1,1,[1,1,1,1])
c---- inputbmr2: Random BMRs at each time step
      CALL inputbmr2(1,1,1,[1,1,1,1],[1,1,1,1])
c --- inputbmr3: Self-generated BMRs: no emergence at first timestep !
c     CALL inputbmr3(1,1,1,[1,1,1,1])
      
c---> CONDITIONS LIMITES -----------------------------------------------
c     1. Conditions de Dirichlet à mu = 0. et 1. => b = 0.
      ii=0
c     Première ligne (nyfant lignes exclues):                    !fantom
c     DO i=1,nnsx
cmu   DO i=nnyfant+1,nnyfant+nnsx
c     Premières lignes (nyfant+1):                               !fantom
c     DO i=1,(nyfant+1)*nnsx
cmu      ii=ii+1
cmu      iuebc(ii)=i
cmu      uebc(ii) =0.d0
cmu   ENDDO
c     Dernière ligne (nyfant lignes exclues):                    !fantom
c     DO i=(numnp-nnsx+1),numnp
cmu   DO i=numnp-nnyfant-nesx,numnp-nnyfant
c     Dernières lignes (nyfant+1):                               !fantom
c     DO i=(numnp-(nyfant+1)*nnsx+1),numnp
cmu      ii=ii+1
cmu      iuebc(ii)=i
cmu      uebc(ii) =0.d0
cmu   ENDDO
      numebc=ii
      IF (ivrb.GE.1) THEN
         write(1,976) numebc
         IF (ivrb.GE.2) THEN
         WRITE(*,976) numebc
         ENDIF
      ENDIF
  976 FORMAT(/,'Number of Boundary Conditions (Dirichlet) =',i6)
      
c     2. Périodicité entre x = 0. et pi/2.
cperbc:
c     ii=0
c     DO i=nnsx,numnp,nnsx
c        ii=ii+1
c        iupbc0(ii)=i
c        iupbc1(ii)=i-nnsx+1
c     ENDDO
c     numpbc=ii
      IF (ivrb.GE.1) THEN
c        write(1,977) numpbc
         IF (ivrb.GE.2) THEN
c        WRITE(*,977) numpbc
         ENDIF
      ENDIF
c 977 FORMAT('Number of Periodic Boundary Conditions=',i6)
      
c---> "LARGE" TEMPORAL MESH
      DO i=1,nts+1
         times(i)=tomti*((DBLE(i-1)/nts))+timein
         IF (ivrb.GE.1) THEN
            write(1,308) i,times(i)
cvrb        IF (ivrb.GE.2) THEN
cvrb        WRITE(*,308) i,times(i)
cvrb        ENDIF
         ENDIF
      ENDDO
  308 FORMAT(5x,i5,1pe14.5)
      
      RETURN
 1009 FORMAT(/,'INITIAL CONDITION:',/,
     +         'Starting from scratch.     ',/,
     +         ' latitudinal distribution: ',a19,/,
     +         '          simulation time:' ,e9.2)
 1010 FORMAT(/,'INITIAL CONDITION:',/,
     +         'Reading from restart file: ',a200,/,
c    +         '             generated on: ',a10,' at ',a10,/,
     +         '                    after:' ,i9,'x',i9,' iterations',/,
     +         '          simulation time:' ,e9.2)
cca 1011 FORMAT(/,' INVALID TIME INTERVAL',
cca  +       /,10x,'ti (yrs) = ',e12.4,
cca  +       /,10x,'to (yrs) = ',e12.4,
cca  +       //,' RUN ABORTED')
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE grid1s
c***********************************************************************
c     Genere une grid d'elements quadrilateraux bilineaires
c
c --> Calcul des coordonnees des noeuds et de la matrice de connectivite
c*********************************************************************** 
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      
      INTEGER nx,ny,i,j,i2,iel
c-----------------------------------------------------------------------
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "grid1s" subroutine       *****')
      
      nx=nnsx
      ny=nnsy
 
c---- 1. Calcul du "fond"
      DO 10 i=1,nx
         x(i)=xx(i)
         y(i)=yy(1)
   10 CONTINUE
 
      DO 13 j=1,nesy
         i2=1
         DO 14 i=(j-1)*nx+nx+1,j*nx+nx
            y(i)=yy(j+1)
            x(i)=xx(i2)
            i2=i2+1
   14    CONTINUE
   13 CONTINUE
cca971 FORMAT(i5,1x,1pe10.3,1x,1pe10.3)

      DO 15 i=1,nesy
         DO 16 j=1,nesx
            iel=(i-1)*nesx+j
            icon(iel,1)=(i-1)*nx+j
            icon(iel,2)=(i-1)*nx+j+1
            icon(iel,3)=i*nx+j+1
            icon(iel,4)=i*nx+j
   16    CONTINUE
   15 CONTINUE
 
      numnp=nx*ny
      numel=nesx*nesy

      IF (ivrb.GE.1) THEN
         write(1,1) nesx,nesy,numel,numnp
         IF (ivrb.GE.2) THEN
         WRITE(*,1) nesx,nesy,numel,numnp
         ENDIF
      ENDIF
    1 FORMAT(/,5x,'* GRID:',5x,i3,' X ',i3,' Bilinear Elements',/,
     +         7x,'Total Elements:     ',i6,/,
     +         7x,'Total Nodes:        ',i6)
      IF(numel.GT.nes)THEN
         IF (ivrb.GE.1) THEN
            write(1,2) numel,nes
            IF (ivrb.GE.2) THEN
            WRITE(*,2) numel,nes
            ENDIF
         ENDIF
         STOP ' *GRID*'
      ENDIF
      IF(numnp.GT.nns)THEN
         IF (ivrb.GE.1) THEN
            write(1,3) numnp,nns
            IF (ivrb.GE.2) THEN
            WRITE(*,3) numnp,nns
            ENDIF
         ENDIF
         STOP ' *GRID*'
      ENDIF
    2 FORMAT(/,'***** ERROR: numel= ',i5,5x,' nes= ',i5)
    3 FORMAT(/,'***** ERROR: numnp= ',i5,5x,' nns= ',i5)

      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE grid3s
c*********************************************************************** 
c     Genere une grid d'elements quadrilateraux quadratiques 8-noeuds
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      
      DOUBLE PRECISION  xxx(2*nesx+1)
      INTEGER nx,nnx,ny,i,j,i1,i2,n6,ij,jj,ii
c----------------------------------------------------------------------- 
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "grid3s " subroutine      *****')
      
c---------- calcul du maillage
c
c           les maillage de base est calcule sur les l0g[dm/m]
c
      DO 102 i=1,nnsx
         xxx(2*i-1)=xx(i)
  102 CONTINUE
      DO 1002 i=1,nesx
         xxx(2*i)=(xx(i+1)+xx(i))/2.d0
 1002 CONTINUE
      nx=2*nesx+1
      nnx=3*nesx+2
      ny=nesy+1 
 
c---------- 1. calcul du "fond"
 
      DO 10 i=1,nx
         y(i)=yy(1)
         x(i)=xxx(i)
   10 CONTINUE
c---------- 2. couches d'elements jusque sous la z.c.
c              (nesy elements quadrilateraux quadratiques par couche)
 
      DO 13 j=1,nesy
         i1=1
         i2=1
         DO 14 i=(j-1)*nnx+nx+1,j*nnx+nx
            IF(i1.LE.nx)THEN
               y(i)=(yy(j+1)+yy(j))/2.d0
               x(i)=xxx(i1)
               i1=i1+2
            ELSE
               y(i)=yy(j+1)
               x(i)=xxx(i2)
               i2=i2+1
            END IF
   14    CONTINUE
   13 CONTINUE

      n6=0
      DO 15 i=1,nesx*nesy
         DO 16 j=1,nesy
            IF(i.EQ.(j-1)*nesx+1)THEN
               ij=(j-1)*nnx
               jj=nx+1+(j-1)*nnx
               n6=n6+nesx
            END IF
   16    CONTINUE
         ii=2*(i-n6+nesx)-1
         icon(i,1)=ii+ij
         icon(i,2)=ii+2+ij
         icon(i,3)=ii+nnx+2+ij
         icon(i,4)=ii+nnx+ij
         icon(i,5)=ii+1+ij
         icon(i,6)=jj+1
         icon(i,7)=ii+nnx+1+ij
         icon(i,8)=jj
         jj=jj+1
   15 CONTINUE
 
      numnp=nnx*nesy+nx
      numel=nesx*nesy

      IF (ivrb.GE.1) THEN
         write(1,1) nesx,nesy,numel,numnp
         IF (ivrb.GE.2) THEN
         WRITE(*,1) nesx,nesy,numel,numnp
         ENDIF
      ENDIF
    1 FORMAT(/,'***** GRID:',5x,i3,' X ',i3,
     +         ' 8-node quadratic Elements',/,
     +         7x,'Total Elements:     ',i6,/,
     +         7x,'Total Nodes:        ',i6)
      IF(numel.GT.nes)THEN
         IF (ivrb.GE.1) THEN
            write(1,2) numel,nes
            IF (ivrb.GE.2) THEN
            WRITE(*,2) numel,nes
            ENDIF
         ENDIF
         STOP ' *GRID*'
      ENDIF
      IF(numnp.GT.nns)THEN
         IF (ivrb.GE.1) THEN
            write(1,3) numnp,nns
            IF (ivrb.GE.2) THEN
            WRITE(*,3) numnp,nns
            ENDIF
         ENDIF
         STOP ' *GRID*'
      ENDIF
    2 FORMAT(/,'***** ERROR: numel= ',i5,5x,' nes= ',i5)
    3 FORMAT(/,'***** ERROR: numnp= ',i5,5x,' nns= ',i5)

      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE initdts
c***********************************************************************
c     [ internal to FEM ]
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      INTEGER i,n,l
c----------------------------------------------------------------------- 
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "initdts" subroutine      *****')

      DO 10 i=1,30
         nnodes(i)=0
         intdef(i)=0
         ioptau(i)=0
   10 CONTINUE
      nnodes(21)=3
      nnodes(22)=6
      nnodes(23)=4
      nnodes(24)=8
      nnodes(25)=4
      nnodes(26)=4
      intdef(21)=0
      intdef(22)=4
      intdef(23)=2
c      intdef(23)=1
      intdef(24)=3
      intdef(25)=2
      intdef(26)=2
      ioptau(21)=1
      ioptau(22)=4
      ioptau(23)=1
      ioptau(24)=4
      ioptau(25)=1
      ioptau(26)=1
 
c---------- gauss (integration) points and weights for gauss-legendre
c           quadrature
      DO 50 n=1,5
      DO 50 l=1,5
         gp(n,l)=0.d0
         wt(n,l)=0.d0
   50 CONTINUE
c---------- for one gauss point
      gp(1,1)=0.d0
      wt(1,1)=2.d0
c---------- for two gauss points
      gp(2,1)=-1.d0/DSQRT(3.d0)
      gp(2,2)=-gp(2,1)
      wt(2,1)=1.d0
      wt(2,2)=1.d0
c---------- for three gauss points
      gp(3,1)=-DSQRT(3.d0/5.d0)
      gp(3,2)=0.d0
      gp(3,3)=-gp(3,1)
      wt(3,1)=5.d0/9.d0
      wt(3,2)=8.d0/9.d0
      wt(3,3)=wt(3,1)
c---------- for four gauss points
      gp(4,1)=-DSQRT((15.d0+2.d0*DSQRT(30.d0))/35.d0)
      gp(4,2)=-DSQRT((15.d0-2.d0*DSQRT(30.d0))/35.d0)
      gp(4,3)=-gp(4,2)
      gp(4,4)=-gp(4,1)
      wt(4,1)=49.d0/(6.d0*(18.d0+DSQRT(30.d0)))
      wt(4,2)=49.d0/(6.d0*(18.d0-DSQRT(30.d0)))
      wt(4,3)=wt(4,2)
      wt(4,4)=wt(4,1)

      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE gaussps
c***********************************************************************
c  calcule la position des points de gauss, ainsi que les coefficients
c  de l'equation differentielle en ces points
c*********************************************************************** 
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      
      DOUBLE PRECISION  sqr13,sqr35,ymid,xmid,xl,yl
      INTEGER           i
c----------------------------------------------------------------------- 
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "gaussps" subroutine      *****')
      
      IF (ivrb.GE.1) THEN
         write(1,28) ngp
         IF (ivrb.GE.2) THEN
         WRITE(*,28) ngp 
         ENDIF
      ENDIF
   28 FORMAT(/,'GAUSSP: ngp = ',i3)
      
      sqr13=DSQRT(1.d0/3.d0)
      sqr35=DSQRT(3.d0/5.d0)
      DO 1 i=1,numel
         ngpint(i)=ngp
c  1 point de gauss
         IF(ngp.EQ.1)THEN
            ymid=(y(icon(i,4))+y(icon(i,1)))/2.d0
            xmid=(x(icon(i,2))+x(icon(i,1)))/2.d0
            ygp(i,1)=ymid
            xgp(i,1)=xmid
         END IF
c  2x2 points de gauss
         IF(ngp.EQ.2)THEN
            ymid=(y(icon(i,4))+y(icon(i,1)))/2.d0
            xmid=(x(icon(i,2))+x(icon(i,1)))/2.d0
            xl=(x(icon(i,2))-x(icon(i,1)))/2.d0
            yl=(y(icon(i,4))-y(icon(i,1)))/2.d0
            xgp(i,1)=xmid-xl*sqr13
            xgp(i,2)=xmid+xl*sqr13
            xgp(i,3)=xmid-xl*sqr13
            xgp(i,4)=xmid+xl*sqr13
            ygp(i,1)=ymid-yl*sqr13
            ygp(i,2)=ymid-yl*sqr13
            ygp(i,3)=ymid+yl*sqr13
            ygp(i,4)=ymid+yl*sqr13
         END IF
c  3x3 points de gauss
         IF(ngp.EQ.3)THEN
            ymid=(y(icon(i,4))+y(icon(i,1)))/2.d0
            xmid=(x(icon(i,2))+x(icon(i,1)))/2.d0
            xl=(x(icon(i,2))-x(icon(i,1)))/2.d0
            yl=(y(icon(i,4))-y(icon(i,1)))/2.d0
            xgp(i,1)=xmid-xl*sqr35
            xgp(i,2)=xmid
            xgp(i,3)=xmid+xl*sqr35
            xgp(i,4)=xmid-xl*sqr35
c           xgp(i,5)=xmid
c           xgp(i,6)=xmid+xl*sqr35
c           xgp(i,7)=xmid-xl*sqr35
c           xgp(i,8)=xmid
c           xgp(i,9)=xmid+xl*sqr35
            ygp(i,1)=ymid-yl*sqr35
            ygp(i,2)=ymid-yl*sqr35
            ygp(i,3)=ymid-yl*sqr35
            ygp(i,4)=ymid
c           ygp(i,5)=ymid
c           ygp(i,6)=ymid
c           ygp(i,7)=ymid+yl*sqr35
c           ygp(i,8)=ymid+yl*sqr35
c           ygp(i,9)=ymid+yl*sqr35
         END IF
    1 CONTINUE

      RETURN
      END SUBROUTINE
c=======================================================================

C***********************************************************************
C     SUBROUTINE inputbmr0
C***********************************************************************
C     Input of magnetic region ("onespot")
C     - IF bmrtype=0 => One gaussian of positive polarity
C***********************************************************************
C     USE surfbr2_param
C     IMPLICIT NONE
C     INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
C     INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
C     INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
C     
C     DOUBLE PRECISION flux0,awbmr,tht0deg,phi0deg,sinth0,wdth2,
C    +                 amp,tht,sinth,ph,dth2,tt,ttm,ttp,
C    +                 sin2dth,sin2dph,spot,anew(nns)
C     INTEGER          i
C     REAL             urands2
C     EXTERNAL         urands2
C-----------------------------------------------------------------------
C
Cmuc**** REMEMBER: we're solving for b*SIN(theta) !!!
C
C---> Input of parameters of individual active region
C     flux0  = 10.**(urands2()*(flux0max-flux0min)+flux0min-21.)
C     awbmr  = urands2()*(awbmrmax-awbmrmin)+awbmrmin
C     phi0deg= urands2()*(phi0max-phi0min)+phi0min
C     phi0   = phi0deg*pi180
C     tht0deg= urands2()*(tht0max-tht0min)+tht0min
C     tht0   = tht0deg*pi180
C     
C     wdth2  = (wdth*pi180)**2
C     
C     Amplitude ajustée proportionnellement au flux mesuré
C     (autre possibilité: ajuster wdth...)
C     (Rappel: flux = intégrale 2D d'une gaussienne = amp0*wdth^2*pi)
C     amp0   = flux0/r2d21/pi/wdth2
C
C     IF (ivrb.GE.1) THEN
C        write(1,28) tht0deg,phi0deg,amp0,awbmr,wdth
C        IF (ivrb.GE.2) THEN
C        WRITE(*,28) tht0deg,phi0deg,amp0,awbmr,wdth
C        ENDIF
C     ENDIF
C  28 FORMAT(/,' *Subroutine "inputbmr0":',/,'  New active region at',
C    +       '   colatitude (deg):',f5.1,' ,  longitude (deg):',f5.1,/,
C    + '  (amplitude=',f5.1,', spacing=',f5.1,'deg, scale=',f5.1,'deg)')
C     
C     sinth0=DSIN(tht0)
C     amp= 1.d0*amp0
C     
C---> Initialize new input
C     DO i=1,numnp
C        anew(i)=0.d0
C     ENDDO
C     
C---> Add onespot, except to boundary layers
C     DO i=nnsx+1,numnp-nnsx
C     DO i=nnyfant+nnsx+1,numnp-nnyfant-nnsx                     !fantom
C        ph=x(i)
C        tht=y(i)
Cmu      tht=DACOS(y(i))
C        sinth=DSIN(tht)
C        dth2=(tht-tht0)**2
C        sin2dth =(DSIN((tht-tht0)/2.d0))**2
C        sin2dph =(DSIN((ph -phi0)/2.d0))**2
C        
C        tt= dth2 + (ph-phi0)**2
C------- Pour la périodicité:
C        ttm=dth2 + (ph-xomxi-phi0)**2
C        ttp=dth2 + (ph+xomxi-phi0)**2
C
C        tt= dth2 + sinth0*sinth*(ph-phi0)**2
C------- Pour la périodicité:
C        ttm=dth2 + sinth0*sinth*(ph-xomxi-phi0)**2
C        ttp=dth2 + sinth0*sinth*(ph+xomxi-phi0)**2
C
C        tt =(2.d0*DASIN(DSQRT(sin2dth + sinth0*sinth*sin2dph )))**2
C
C        spot=amp*(DEXP(-tt/wdth2)+DEXP(-ttm/wdth2)+DEXP(-ttp/wdth2))
C        spot=amp*(DEXP(-tt/wdth2))
C        anew(i)= spot
Cmu      anew(i)= spot*sinth
C     ENDDO
C     
C---> Add new input to evolving solution
C     DO i=1,numnp
C        a(i)= a(i) + anew(i)
C     ENDDO
C     
C---> Calculate new flux added
C     CALL calflux(anew)
C     
C     RETURN
C     END SUBROUTINE
C=======================================================================

c***********************************************************************
      SUBROUTINE inputbmr1(it,itbmr,itts,bldseedd)
c***********************************************************************
c     Input of magnetic regions
c     - IF bmrtype=1 => Two gaussians of opposite polarities at the
c                       positions and amplitudes given by the database
c                       of Wang and Sheeley 1989
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      
      INTEGER          it,itbmr,itts,bldseedd(4)
      
      INTEGER          ibmrtype,ibmr,ihem,signe
      DOUBLE PRECISION datawang(11),flux0,tht1,tht2,phi1,phi2,
     +                 wdth2,sinth0,tht0deg,phi0deg,amp1,amp2
c-----------------------------------------------------------------------

cmuc**** REMEMBER: we're solving for b*SIN(theta) !!!
      
      ibmrtype=1
      
c --- inputbmr1: Read nnbmrin(itts) BMRs from database (only if nnbmrin(itts).GT.0)
      DO ibmr=1,nnbmrin(itts)
         
         READ(33,*) datawang
         flux0=datawang(6)
c         write(*,*) 'flux0=',flux0
         signe=INT(datawang(7))
         tht1 =datawang(8) *pi180
         phi1 =datawang(9) *pi180
         tht2 =datawang(10)*pi180
         phi2 =datawang(11)*pi180
         
         wdth2=(wdth*pi180)**2                                             != 2*sigma^2
         
c        Amplitude ajustée proportionnellement au flux mesuré
c        (autre possibilité: ajuster wdth...)
c        (Rappel: flux = intégrale 2D d'une gaussienne = amp0*wdth^2*pi)
         amp0=signe*flux0/r2d21/pi/wdth2
         
         tht0   =(tht1+tht2)/2.d0
         tht0deg=tht0/pi180
         phi0   =(phi1+phi2)/2.d0
         phi0deg=phi0/pi180
c        sinth0=DSIN(tht0)
c        amp1= 1.d0*amp0*sinth0/sinth1
c        amp2=-1.d0*amp0*sinth0/sinth2
         amp1= 1.d0*amp0
         amp2=-1.d0*amp0
         
c ------ Loop over 4 half-hemispheres to check if the current bmrtype is active
c        IF yes, then check if the BMR is in the selected half-hemisphere
         DO ihem=1,4                                                    !(Francois)
c --------- If before the cut at tbmr
c           OR if a new seed is activated after tbmr
c           IF ((it.LE.itbmr).OR.(bldseedd(ihem).NE.0)) THEN            !Franky from here to line 1851 (Francois)              
c --------- If the bmrtype of this subroutine is active
            IF (bmrtype(ibmrtype,ihem).EQ.1) THEN                    !(Francois)  
               CALL calcbmr(ibmrtype,itts,ihem,flux0,signe,
     +           phi0deg,tht0deg,tht1,tht2,phi1,phi2,amp1,amp2,wdth2)
            ENDIF
c           ENDIF
         ENDDO
         
      ENDDO
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE inputbmr2(it,itbmr,itts,bldseed,bldseedd)
c***********************************************************************
c     Input of magnetic regions ("nbmr")
c     - IF bmrtype=2 => Two gaussians of opposite polarities at
c                       theta_0+/- epst, phi_0+/-epsp
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      
      INTEGER          it,itbmr,itts,bldseed(4),bldseedd(4)
      
      INTEGER          ibmrtype,ihem,signe
      DOUBLE PRECISION flux0,awbmr,tht0deg,phi0deg,wdth2,
     +                 atilt,tht1,tht2,phi1,phi2,sinth0,
     +                 amp1,amp2
      REAL             urands2,urandvec,gssrandvec
      EXTERNAL         urands2,urandvec,gssrandvec
c-----------------------------------------------------------------------
      
cmuc**** REMEMBER: we're solving for b*SIN(theta) !!!
      
      ibmrtype=2
      
c --> Probability of emergence set
      IF (urands2().LE.ifbmr) THEN
      
c --- Loop over 4 half-hemispheres to check if the current bmrtype is active
c     (and run all the random sequences to ensure coherence even when inactive)
c     IF yes, then check if the BMR is in the selected half-hemisphere
      DO ihem=1,4                                                    !(Francois)
         
c------> Input of random parameters of individual active region
         flux0  = 10.**(urandvec(ihem)*(flux0max-flux0min)+flux0min-21.)
         awbmr  = urandvec(ihem)*(awbmrmax-awbmrmin)+awbmrmin
         awbmr  = awbmr*pi180
         phi0deg= urandvec(ihem)*(phi0max-phi0min)+phi0min
         phi0   = phi0deg*pi180
         tht0deg= urandvec(ihem)*(tht0max-tht0min)+tht0min
         tht0   = tht0deg*pi180
         
         IF (tht0.LE.pi/2.d0) THEN
            signe = 1
         ELSE
            signe =-1
         ENDIF
         
         wdth2  = (wdth*pi180)**2
         
c        Amplitude ajustée proportionnellement au flux mesuré
c        (autre possibilité: ajuster wdth...)
c        (Rappel: flux = intégrale 2D d'une gaussienne = amp0*wdth^2*pi)
         amp0   = signe*flux0/r2d21/pi/wdth2
         
c        IF (ivrb.GE.1) THEN
c           write(1,28) tht0deg,phi0deg,amp0,awbmr,wdth
c           IF (ivrb.GE.2) THEN
c           WRITE(*,28) tht0deg,phi0deg,amp0,awbmr,wdth
c           ENDIF
c        ENDIF
c     28 FORMAT(/,' *Subroutine "inputbmr2":',/,'  New active region at',
c       +       '   colatitude (deg):',f5.1,' ,  longitude (deg):',f5.1,/,
c       + '  (amplitude=',f5.1,', spacing=',f5.1,'deg, scale=',f5.1,'deg)')
         
c------> First compute tilt angle via Joy's law
c        atilt=pi/2.
         atilt=DASIN( 0.5d0 *DCOS(tht0) )
         epst=awbmr*DSIN(atilt)/2.d0
         epsp=awbmr*DCOS(atilt)/2.d0/DSIN(tht0)
         tht1=tht0+epst
         tht2=tht0-epst
         phi1=phi0+epsp
         phi2=phi0-epsp
c        sinth0=DSIN(tht0)
c        amp1= 1.d0*amp0*sinth0/sinth1
c        amp2=-1.d0*amp0*sinth0/sinth2
         amp1= 1.d0*amp0
         amp2=-1.d0*amp0
         
c ------ If before the cut at tbmr
c        OR if a new seed is activated after tbmr
         IF (((it.LE.itbmr).AND.(bldseed(ihem).NE.0)).OR.(
     &(it.GT.itbmr).AND.(bldseedd(ihem).NE.0))) THEN               !franky (Francois)              
c --------- If the bmrtype of this subroutine is active
            IF (bmrtype(ibmrtype,ihem).EQ.1) THEN                       !(Francois)  
               CALL calcbmr(ibmrtype,itts,ihem,flux0,signe,
     +              phi0deg,tht0deg,tht1,tht2,phi1,phi2,amp1,amp2,wdth2)
            ENDIF
         ENDIF
         
      ENDDO
      
      ENDIF
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE inputbmr3(it,itbmr,itts,bldseed,bldseedd)
c***********************************************************************
c     Input of magnetic regions
c     - IF bmrtype=3 => Two gaussians of opposite polarities at a
c                       positions and amplitudes self-generated from the
c                       toroidal field at the base of the convection
c                       zone (from code bldyn.f)
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
      INTEGER          it,itbmr,itts,bldseed(4),bldseedd(4)
      
      DOUBLE PRECISION bcumul(nndx),deviate,logflux,
     +                 datawang(11),flux0,awbmr,tht0deg,phi0deg,wdth2,
     +                 atilt,tht1,tht2,phi1,phi2,sinth0,
     +                 sinth1,sinth2,amp1,amp2
      INTEGER          ibmrtype,iidx,ibmr,signe,ihem,i
      REAL             nbmremerg
      REAL             urandvec,gssrandvec
      EXTERNAL         urandvec,gssrandvec
c-----------------------------------------------------------------------
      
cmuc**** REMEMBER: we're solving for b*SIN(theta) !!!
      
      ibmrtype=3
      
c     READ(33,*) datawang
c     flux0=datawang(6) *1.d21
c     signe=datawang(7)
      
c --- Loop over 4 half-hemispheres to check if the current bmrtype is active
c     (and run all the random sequences to ensure coherence even when inactive)
c     IF yes, then check if the BMR is in the selected half-hemisphere
      DO ihem=1,4                                                    !(Francois)
         
c------- Calculate cumulative bfit distribution (at given timestep)
         bcumul(1) = DABS(bfit(itt,1)+0.D0)
         DO iidx=2,nndx
            bcumul(iidx) = bcumul(iidx-1) + DABS(bfit(itt,iidx)+0.D0)
         ENDDO
c------- Number of emergences at surface timestep itts (as compared to
c------- dynamo timestep itt)
         nbmremerg = bcumul(nndx)/(nnts/nnt)
         
c------- Decide how many BMRs will emerge
c        IF (bcumul(nndx).GE.1.E-6) THEN
c           nnbmr(itts) = INT(nbmremerg+0.5)
c           IF (nnbmr(itts).EQ.0) nnbmr(itts) = 1
c        ENDIF
c        DO ibmr=1,nbmremerg                                               !non-stochastic
         IF (nbmremerg.GT.nbmrmax) THEN                                    !Kill the dynamo if it explodes (nbrmemerg > 1000)
            nbmrmax=0
            DO i=1,numnp
               a(i)= 0.
            ENDDO
         ENDIF
         DO ibmr=1,nbmrmax                                                 !stochastic
         IF (urandvec(ihem)*nbmrmax.LE.nbmremerg) THEN                     !stochastic    !Double-checked 2015-06-09 (A.Lemerle)
            
c---------- Extract random latitude from bfit distribution
            deviate= urandvec(ihem)*bcumul(nndx)                           !stochastic
            iidx=1
            DO WHILE(deviate.GT.bcumul(iidx))
               iidx=iidx+1
            ENDDO
            tht0 = DACOS(mutht(iidx)+0.D0)
cwrong      tht0 = DACOS((iidx-1.D0)/nelx*2.d0-1.d0) !WRONG !!! The bldyn mesh is regular in theta, not in cos(theta)
            tht0deg = tht0/pi180
            
c---------- Sign of BMR corresponds to sign of bfit at chosen latitude
            IF (bfit(itt,iidx).GE.0.) THEN
               signe = 1                                                   !urandvec(ihem)
            ELSE
               signe =-1
            ENDIF
            
c---------> Random flux extracted from gaussian distribution
            logflux= -21.d0 + flux0fit + gssrandvec(ihem)*flux0dev         !stochastic
c            write(*,*)'logflux=',logflux
c  69        FORMAT (/,' logflux=',i10)
c            IF (logflux.GE.22.727136) THEN  !franky cutoff
c                logflux = 22.727136
c            ENDIF
            flux0  = 10.**logflux
c            write(*,*) 'flux0=',flux0
            IF (flux0.GT.73.47) THEN
                flux0 = 73.47
            ENDIF
            
c           Amplitude ajustée proportionnellement au flux mesuré
c           (autre possibilité: ajuster wdth...)
c           (Rappel: flux = intégrale(B*dA) d'une gaussienne = amp0*wdth^2*pi)
            wdth2  = (wdth*pi180)**2
            amp0   = signe*flux0/r2d21/pi/wdth2
            
c---------> Random longitude
            phi0deg= urandvec(ihem)*(phi0max-phi0min)+phi0min
            phi0   = phi0deg*pi180
            
c---------> Random separation extracted from gaussian distribution, with
c           dependence on flux
c           awbmr = urandvec(ihem)*(awbmrmax-awbmrmin)+awbmrmin
            awbmr = awbmrlin0+awbmrlin1*logflux + gssrandvec(ihem)*awbmrdev   !stochastic
            IF (awbmr.LT.-0.5) awbmr=-0.5d0
            IF (awbmr.GT. 1.5) awbmr= 1.5d0
            awbmr  = 10**awbmr*pi180
            
c---------> Tilt angle via Joy's law, with gaussian spread with dependance
c           on flux
c           atilt  = pi/2.
c           atilt  = DASIN(0.5d0*DCOS(tht0))                               ! + 0.3*DCOS(tht0)/DABS(DCOS(tht0)))
            atilt  = tiltjoy*(pi/2.-tht0) + gssrandvec(ihem)            !non-stochastic
     +                *pi180*(tiltdev0+tiltdev1*DEXP(-logflux/tiltdev2))!stochastic
            
c---------> Tilt quenching with respect to bfit(itt,iidx)
            IF (bfitqch.NE.0.) THEN
               atilt  = atilt/(1.d0+(bfit(itt,iidx)/bfitqch)**2)
c              atilt  = atilt
c    +               *0.5*(1.-DERF((ABS(bfit(itt,iidx))-bfitqch)/0.2d0))
            ENDIF
            
c---------> Splitting into two opposite polarity patches, separated by awbmr (deg)
            epst=awbmr*DSIN(atilt)/2.d0
            epsp=awbmr*DCOS(atilt)/2.d0/DSIN(tht0)
            tht1=tht0+epst
            tht2=tht0-epst
            phi1=phi0+epsp
            phi2=phi0-epsp
c           sinth0=DSIN(tht0)
c           amp1= 1.d0*amp0*sinth0/sinth1
c           amp2=-1.d0*amp0*sinth0/sinth2
            amp1= 1.d0*amp0
            amp2=-1.d0*amp0
            
c --------- If before the cut at tbmr
c           OR if a new seed is activated after tbmr
            IF (((it.LE.itbmr).AND.(bldseed(ihem).NE.0)).OR.(
     &(it.GT.itbmr).AND.(bldseedd(ihem).NE.0))) THEN            !franky (Francois)              
c ------------ If the bmrtype of this subroutine is active
               IF (bmrtype(ibmrtype,ihem).EQ.1) THEN                    !(Francois)  
                  CALL calcbmr(ibmrtype,itts,ihem,flux0,signe,
     +              phi0deg,tht0deg,tht1,tht2,phi1,phi2,amp1,amp2,wdth2)
               ENDIF
            ENDIF
            
         ENDIF
         ENDDO
         
      ENDDO
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE calcbmr(ibmrtype,itts,ihem,flux0,signe,
     +              phi0deg,tht0deg,tht1,tht2,phi1,phi2,amp1,amp2,wdth2)
c***********************************************************************
c     Calculate flux distribution of a magnetic region
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
      INTEGER          ibmrtype,itts,ihem,signe
      DOUBLE PRECISION flux0,phi0deg,tht0deg,
     +                               tht1,tht2,phi1,phi2,amp1,amp2,wdth2
      
      DOUBLE PRECISION phimaskmin,phimaskk(2),
     +                 sinth1,sinth2,ph,tht,sinth,
c    +                 dth21,dth22,tt1m,tt2m,tt1p,tt2p,
     +                 sin2dth1,sin2dth2,sin2dph1,sin2dph2,
     +                 tt1,tt2,spot1,spot2,anew(nns)
      LOGICAL          ifphi0deg
      INTEGER          i
c-----------------------------------------------------------------------
      
c---> Add new input only if on selected half-hemisphere of the Sun
      phimaskmin=itts*dur/nnts*phimaskv                              !(Francois)
      SELECT CASE (ihem)                                             !(Francois)
      CASE (1)
c------> 1='visible-northern half-hemisphere' (including equator)
         phimaskk(1)= MOD(phimaskmin+phimask(1),360.d0)
         phimaskk(2)= MOD(phimaskmin+phimask(2),360.d0)
         ifphi0deg  = (tht0deg.GE.thtmask(1))
     +           .AND.(tht0deg.LE.thtmask(2))
      CASE (2)
c------> 2='hidden-northern half-hemisphere' (including equator)
         phimaskk(1)= MOD(phimaskmin+phimask(2),360.d0)
         phimaskk(2)= MOD(phimaskmin+phimask(1),360.d0)
         ifphi0deg  = (tht0deg.GE.thtmask(1))
     +           .AND.(tht0deg.LE.thtmask(2))
      CASE (3)
c------> 3='visible-southern half-hemisphere'
         phimaskk(1)= MOD(phimaskmin+phimask(1),360.d0)
         phimaskk(2)= MOD(phimaskmin+phimask(2),360.d0)
         ifphi0deg  = (tht0deg.GT.thtmask(2))
     +           .AND.(tht0deg.LE.(180.-thtmask(1)))
      CASE (4)
c------> 4='hidden-southern half-hemisphere'
         phimaskk(1)= MOD(phimaskmin+phimask(2),360.d0)
         phimaskk(2)= MOD(phimaskmin+phimask(1),360.d0)
         ifphi0deg  = (tht0deg.GT.thtmask(2))
     +           .AND.(tht0deg.LE.(180.-thtmask(1)))
      CASE DEFAULT
         STOP '!! Error in the choice of half-hemisphere !!'  
      END SELECT
      
      IF (phimaskk(1).LT.0.) phimaskk(1)=phimaskk(1)+360.
      DO i=1,2 
         IF (phimaskk(2).LT.phimaskk(1)) phimaskk(2)=phimaskk(2)+360.
      ENDDO
      
      IF (ifphi0deg .AND.(((phi0deg.GE.phimaskk(1))
     +              .AND.  (phi0deg.LT.phimaskk(2)))
     +              .OR.  ((phi0deg.GE.phimaskk(1)-360.)
     +              .AND.  (phi0deg.LT.phimaskk(2)-360.)))) THEN        !(Francois)
         
c------> Count new input and write to database
         nbmr=nbmr+1
         nnbmr(itts)=nnbmr(itts)+1
         
         WRITE(34,'(i7,i6,i5,a14,i1,f13.4,f14.2,i6,4f7.1)') 
     +                               nbmr,1,0,'   99990101000',ibmrtype,
     +                         (ctime-timein)*tau/3600./24.,flux0,signe,
     +                       tht1/pi180,phi1/pi180,tht2/pi180,phi2/pi180
         
c------> Initialize new input
         DO i=1,numnp
            anew(i)=0.d0
         ENDDO
         
         sinth1=DSIN(tht1)
         sinth2=DSIN(tht2)
         
c------> Add BMR (two opposite gaussians), except to boundary layers
c        DO i=nnsx+1,numnp-nnsx
         DO i=nnyfant+nnsx+1,numnp-nnyfant-nnsx                         !fantom
            ph=x(i)
            tht=y(i)
cmu         tht=DACOS(y(i))
            sinth=DSIN(tht)
c           dth21=(tht-tht1)**2
c           dth22=(tht-tht2)**2
c --------- Avec le SIN, la périodicité se fait toute seule !
            sin2dth1 =(DSIN((tht-tht1)/2.d0))**2
            sin2dth2 =(DSIN((tht-tht2)/2.d0))**2
            sin2dph1 =(DSIN((ph -phi1)/2.d0))**2
            sin2dph2 =(DSIN((ph -phi2)/2.d0))**2
            
c           tt1 = (x(i)-(phi0-epsp))**2 
c           tt2 = (x(i)-(phi0+epsp))**2
            
c           tt1 = dth21 + (ph-phi1)**2
c           tt2 = dth22 + (ph-phi2)**2
c --------- Pour la périodicité:
c           tt1m= dth21 + (ph-xomxi-phi1)**2
c           tt2m= dth22 + (ph-xomxi-phi2)**2
c           tt1p= dth21 + (ph+xomxi-phi1)**2
c           tt2p= dth22 + (ph+xomxi-phi2)**2
            
c           tt1 = dth21 + sinth1*sinth*(ph-phi1)**2
c           tt2 = dth22 + sinth2*sinth*(ph-phi2)**2
c --------- Pour la périodicité:
c           tt1m= dth21 + sinth1*sinth*(ph-xomxi-phi1)**2
c           tt2m= dth22 + sinth2*sinth*(ph-xomxi-phi2)**2
c           tt1p= dth21 + sinth1*sinth*(ph+xomxi-phi1)**2
c           tt2p= dth22 + sinth2*sinth*(ph+xomxi-phi2)**2
            
            tt1 =(2.d0*DASIN(DSQRT(sin2dth1 +sinth1*sinth*sin2dph1)))**2!DOUBLE-CHECKED 2015-04-19 (A. Lemerle)
            tt2 =(2.d0*DASIN(DSQRT(sin2dth2 +sinth2*sinth*sin2dph2)))**2
            
c      spot1=amp1*(DEXP(-tt1/wdth2)+DEXP(-tt1m/wdth2)+DEXP(-tt1p/wdth2))
c      spot2=amp2*(DEXP(-tt2/wdth2)+DEXP(-tt2m/wdth2)+DEXP(-tt2p/wdth2))
            spot1=amp1*(DEXP(-tt1/wdth2))
            spot2=amp2*(DEXP(-tt2/wdth2))
            anew(i)= (spot1+spot2)                                      !Resulting signe flux should be flux-flux=0, while resulting unsigned flux can be between 0 (if spot1 and spot2 at the same position = irrealistic) and 2*flux (if spot1 and spot2 far enough).
cmu         anew(i)= (spot1+spot2)*sinth
         ENDDO
         
c------> Add new input to evolving solution
         DO i=1,numnp
            a(i)= a(i) + anew(i)
         ENDDO
         
c------> Calculate new flux added
         CALL calflux(anew)
         
      ENDIF
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      REAL FUNCTION urandvec(iseries)
c***********************************************************************
c     Returns a uniform random number from one of the predefined series
c***********************************************************************
      IMPLICIT NONE
      
      INTEGER          iseries
      
      REAL             urand12,urand22,urand32,urand42
      EXTERNAL         urand12,urand22,urand32,urand42
c-----------------------------------------------------------------------
      
      SELECT CASE (iseries)
      CASE (1)
         urandvec = urand12()
      CASE (2)
         urandvec = urand22()
      CASE (3)
         urandvec = urand32()
      CASE (4)
         urandvec = urand42()
      CASE DEFAULT
         STOP '!! Error in the choice of random series !!'  
      END SELECT
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      REAL FUNCTION gssrandvec(iseries)
c***********************************************************************
c     Returns a uniform random number from one of the predefined series
c***********************************************************************
      IMPLICIT NONE
      
      INTEGER          iseries
      
      REAL             gssrand1,gssrand2,gssrand3,gssrand4
      EXTERNAL         gssrand1,gssrand2,gssrand3,gssrand4
c-----------------------------------------------------------------------
      
      SELECT CASE (iseries)
      CASE (1)
         gssrandvec = gssrand1()
      CASE (2)
         gssrandvec = gssrand2()
      CASE (3)
         gssrandvec = gssrand3()
      CASE (4)
         gssrandvec = gssrand4()
      CASE DEFAULT
         STOP '!! Error in the choice of random series !!'  
      END SELECT
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE calflux(anew)
c***********************************************************************
c     Integrate the function "anew" over the entire domain to obtain the
c     associated flux
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      
      INTEGER           j,jj,i,jjp1,jjm1
      DOUBLE PRECISION  sinthj,sinthjp1,anew(nns),delx(nesx),dely(nesy),
     +                  usflux,sflux,ussum,ssum,amoy,delxy,dflux,
     +                  fluxef,fluxefin
c-----------------------------------------------------------------------
cmuc---- Reintroduce SIN(theta) factor to get real B_r
c     DO j=2,nnsy-1
c        oneovsinth=1.d0/DSQRT(1.d0-yy(j)**2)
c        jj=(j-1)*nnsx
c        DO i=1,nnsx
c           anew(jj+i)=anew(jj+i)*oneovsinth
c        ENDDO
c     ENDDO
cmu   DO j=1,nnsy
cmu      sinth=DSQRT(1.d0-yy(j)**2)
cmu      jj=(j-1)*nnsx
cmu      IF(sinth.NE.0.)THEN
cmu         DO i=1,nnsx
cmu            anew(jj+i)=anew(jj+i)/sinth
cmu         ENDDO
cmu      ENDIF
cmu   ENDDO
c---- Patch for SIN(theta)=0. at poles
c     DO i=1,nnsx
c        anew(i)=anew(nnsx+i)
c        anew(numnp-nnsx+i)=anew(numnp-nnsx-nnsx+i)
c     ENDDO
cmu   DO j=1,nnsy
cmu      sinth=DSIN(yy(j))
cmu      jj=(j-1)*nnsx
cmu      jjp1=jj+nnsx
cmu      jjm1=jj-nnsx
cmu      IF(jjm1.LT.0)          jjm1=jjp1
cmu      IF(jjp1.GT.numnp-nnsx) jjp1=jjm1
cmu      IF(sinth.EQ.0.)THEN
cmu         DO i=1,nnsx
cmu            anew(jj+i)=(anew(jjp1+i)+anew(jjm1+i))/2.d0
cmu         ENDDO
cmu      ENDIF
cmu   ENDDO
      
c---- Calculate grid intervals and integrate to get de flux
      DO i=1,nnsx-1
         delx(i)=xx(i+1)-xx(i)
      ENDDO
      DO j=1,nnsy-1
         dely(j)=yy(j+1)-yy(j)
cmu      delmuy(j)=yy(j+1)-yy(j)
      ENDDO
      
      usflux=0.d0
      sflux=0.d0
c---- DO not count fantom cells !
c     DO j=1,nesy
      DO j=nyfant+1,nesy-nyfant
         sinthj  =DSIN(yy(j))
         sinthjp1=DSIN(yy(j+1))
         jj=(j-1)*nnsx
         jjp1=jj+nnsx
         ussum=0.d0
         ssum=0.d0
c        DO i=1,nesx
         DO i=nxfant+1,nesx-nxfant
            amoy=((anew(jj+i)  + anew(jj+i+1))  *sinthj +
     +            (anew(jjp1+i)+ anew(jjp1+i+1))*sinthjp1)/4.d0
cmu         amoy=(anew(jj+i)  + anew(jj+i+1) +
cmu  +            anew(jjp1+i)+ anew(jjp1+i+1))/4.d0
            delxy=dely(j)*delx(i)
cmu         delxy=delmuy(j)*delx(i)
            dflux=amoy*delxy
            ussum=ussum+DABS(dflux)
            ssum=ssum+dflux
         ENDDO
         usflux=usflux+ussum
         sflux=sflux+ssum
      ENDDO
      usflux=usflux*rsun*rsun                                           !*r2d21
      sflux = sflux*rsun*rsun                                           !*r2d21
      
      fluxef=sflux/(usflux/2.d0)
      
      sfluxin = sfluxin+ sflux
      usfluxin=usfluxin+usflux
      fluxefin= sfluxin/(usfluxin/2.d0)
      
      IF (ivrb.GE.1) THEN
         write(1,325) nbmr,sflux,sfluxin,usflux,usfluxin,fluxef,fluxefin
cvrb     IF (ivrb.GE.2) THEN
cvrb     WRITE(*,325) nbmr,sflux,sfluxin,usflux,usfluxin,fluxef,fluxefin
cvrb     ENDIF
      ENDIF
  325 FORMAT('         BMR',i5,', sflux=',e9.2,' (total',e9.2,
     +                       '), usflux=',e9.2,' (total',e9.2,
     +            '), sflux/(usflux/2.)=',e9.2,' (total',e9.2,')')
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE calvecpot
c***********************************************************************
c     Calculates Aphi, the vector potential associated with the Br field
c
c  => Br = [rot x Aphi]_r = 1/(r*SIN(theta))*d(Aphi*SIN(theta))/d(theta)
c     
c  => a  = 1/r * d(aa)/d(theta)
c
c  => f  = d(aa)/d(theta) = r * a              with Aphi = aa/SIN(theta)
c
c     Note that because Aphi should not depend on phi, "a" must be
c     averaged over the phi direction.
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
      DOUBLE PRECISION bmoy(nnsyvrai),sintht(nnsyvrai),ff(nnsyvrai),
     +                 erfj(nnsyvrai),hh(nesyvrai),
     +                 aphi1(nnsyvrai),aphi2(nnsyvrai)
      INTEGER          j,jv,ii,i
c-----------------------------------------------------------------------
      
c---- Average of Br over the phi direction -----
      DO j=nyfant+1,nnsy-nyfant
         jv=j-nyfant
         bmoy(jv)=0.d0
         ii=(j-1)*nnsx
         DO i=nxfant+1,nnsx-nxfant-1                             !fantom
            bmoy(jv)=bmoy(jv)+(a(ii+i)+a(ii+i+1))/2.d0
         ENDDO
         bmoy(jv)=bmoy(jv)/nesxvrai
         sintht(jv)=DSIN(yy(j))
         ff(jv)=bmoy(jv)*sintht(jv)
         erfj(jv)=DERF(DCOS(yy(j))/lerf)
         IF (jv.LT.nnsyvrai) THEN
            hh(jv)=yy(j+1)-yy(j)
         ENDIF
      ENDDO
      
c---- Calculate Aphi -----
c --> From north pole:   
      aphi1(1)=aphi0
      j=1
c     aphi1(j+1)=aphi1(j)+hh(j)*ff(j)                                   !Euler
      aphi1(j+1)=aphi1(j)+hh(j)/2.*(ff(j)+ff(j+1))                      !Adams-Moulton AM2
      DO j=2,nesyvrai
c        aphi1(j+1)=aphi1(j)+hh(j)*ff(j)                                !Euler
c        aphi1(j+1)=aphi1(j)+hh(j)/2.*(ff(j)+ff(j+1))                   !Adams-Moulton AM2
         aphi1(j+1)=aphi1(j)+hh(j)/12.*(5.*ff(j+1)+8.*ff(j)-ff(j-1))    !AM3
         aphi1(j)=aphi1(j)/sintht(j)
      ENDDO
      aphi1(nnsyvrai)=aphi0
      
c --> From south pole:
      aphi2(nnsyvrai)=aphi0
      j=nnsyvrai
c     aphi2(j-1)=aphi2(j)-hh(j-1)*ff(j)                                 !Euler
      aphi2(j-1)=aphi2(j)-hh(j-1)/2.*(ff(j)+ff(j-1))                    !Adams-Moulton AM2
      DO j=nesyvrai,2,-1
c        aphi2(j-1)=aphi2(j)-hh(j-1)*ff(j)                              !Euler
c        aphi2(j-1)=aphi2(j)-hh(j-1)/2.*(ff(j)+ff(j-1))                 !Adams-Moulton AM2
         aphi2(j-1)=aphi2(j)-hh(j-1)/12.*(5.*ff(j-1)+8.*ff(j)-ff(j+1))  !AM3
         aphi2(j)=aphi2(j)/sintht(j)
      ENDDO
      aphi2(1)=aphi0
      
c --> Join the two functions
      DO j=1,nnsyvrai
         aphi(j)=0.5*(aphi1(j)*(1.+erfj(j))+aphi2(j)*(1.-erfj(j)))
      ENDDO
      
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE writouts(its,itts)
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
      INTEGER           its,itts
c     Local:
      DOUBLE PRECISION  avrai(nnsvrai)
      INTEGER           j,iivrai,jj,i,ii,jvrai
c-----------------------------------------------------------------------
      IF(its.EQ.0) THEN
         IF (iout.EQ.1.OR.iout.EQ.3) THEN
            WRITE(18) title
cca         WRITE(18) numnp,numel,nesx,nesy,nts+1,nsteps
            WRITE(18) nnsvrai,nesvrai,nesxvrai,nesyvrai,nts+1,nsteps    !fantom
            WRITE(18) theta,timein,timeout,rm,ru
cca         WRITE(18) tht0,phi0,awbmr,tht0deg
cca         WRITE(18) tht0,phi0,epst,epsp
            WRITE(18) tht0,phi0,amp0,wdth
cca         WRITE(18) (xx(j),j=1,nnsx)
            WRITE(18) (xx(j),j=nxfant+1,nnsx-nxfant)                 !fantom
cca         WRITE(18) (yy(j),j=1,nnsy)
            WRITE(18) (yy(j),j=nyfant+1,nnsy-nyfant)                 !fantom
         ENDIF
         IF (iout.EQ.2.OR.iout.EQ.3) THEN
            WRITE(14) nntsp1,nnsyvrai                      !fantom
            WRITE(14) timein,timeout
            WRITE(14) (yy(j),j=nyfant+1,nnsy-nyfant)                 !fantom
         ENDIF
      END IF
      
      IF (iout.EQ.1.OR.iout.EQ.3) THEN
         WRITE(18) its+1
         WRITE(18) ctime,times(its+1)
         
cmuc --> La solution : Br*SIN(theta)
         
cca      WRITE(18) (a(ii),ii=1,numnp)
         
c------- Output only real domain, removing fantom cells:
         iivrai=0
         DO j=nyfant+1,nnsy-nyfant
            jj=(j-1)*nnsx
            DO i=nxfant+1,nnsx-nxfant
               ii=jj+i
               iivrai=iivrai+1
               avrai(iivrai)=a(ii)
            ENDDO
         ENDDO
         IF(iivrai.NE.nnsvrai) STOP 'Erreur de décompte pour iivrai'
         WRITE(18) (avrai(iivrai),iivrai=1,nnsvrai)
         
         WRITE(18) sfluxin*1.d21,usfluxin*1.d21
      ENDIF
      
      IF (iout.EQ.2.OR.iout.EQ.3) THEN
         WRITE(14) (aphi(jvrai),jvrai=1,nnsyvrai)                  !fantom
      ENDIF
      
      RETURN
cca10 FORMAT(i6)
cca11 FORMAT(6i6)
cca12 FORMAT(e10.4,1x,e10.4,1x,1pe10.4)
cca13 FORMAT(e10.4)
cca14 FORMAT(i6,1x,1pe10.4)
cca15 FORMAT(1pe10.4)
cca16 FORMAT(1x,1p5e13.5)
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE tsteps(its)
c***********************************************************************
c     Sets up some parameters relating to time-stepping
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      
      DOUBLE PRECISION  dttot
      INTEGER           its,i
c-----------------------------------------------------------------------
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "tsteps" subroutine       *****')
      
      dt              =(times(its+1)-times(its))/nsteps
      dttot           = times(its+1)-times(its)

      DO 19 i=1,numnp
         f0(i)=0.d0
   19 CONTINUE

c --> This is where you will eventually want to update the flow field
cca   ??

      IF (ivrb.GE.1) THEN
         write(1,1) title,its,dttot,times(its+1),nsteps,dt,theta
         IF (ivrb.GE.2) THEN
         WRITE(*,1) title,its,dttot,times(its+1),nsteps,dt,theta
         ENDIF
      ENDIF

      RETURN
c-----------------------------------------------------------------------
    1 FORMAT(/,2x,a80,//,
     +           6x,'Time Step group          =',i12,/,
     +           6x,'Total time interval      =',e12.4,/,
     +           6x,'Cumulative time          =',e12.4,/,
     +           6x,'Number of sub-timesteps  =',i12,/,
     +           6x,'Time interval (substeps) =',e12.4,/,
     +           6x,'Theta (time stepping)    =',f12.6)
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE forms(ijac)
c***********************************************************************
c     Form the system matrices from element matrices contributions     *
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      INTEGER           ijac,i,j,ii,jj,ish
c-----------------------------------------------------------------------
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "forms" subroutine        *****')
      
c---- Initialize arrays for the system matrices
      DO 80 i=1,numnp
         f(i)=0.d0
   80 CONTINUE
      DO 60 j=1,numnp
         DO 61 i=1,mfbw
            k(i,j)=0.d0
            ck(i,j)=0.d0
   61    CONTINUE
   60 CONTINUE
   
c**** Loop over all elements
      nnd=nnodes(ntype) !cca   ntype=23 => nnd=4
      DO 500 ielno=1,numel
c------- Form element matrices
         IF(ntype.EQ.23) CALL elem23s(ijac)
         IF(ntype.EQ.24) CALL elem24s(ijac)
c------- Assemble the system matrices from the element matrices
         DO 440 i=1,nnd
            ii=icon(ielno,i)
            f(ii)=f(ii)+fe(i) !cca   fe(i) = 0
            DO 420 j=1,nnd
               jj=icon(ielno,j)
               ish=ii+mband-jj
               k(ish,jj) = k(ish,jj)+ce(i,j)/dt +theta *ke(i,j)
               ck(ish,jj)=ck(ish,jj)+ce(i,j)/dt -onemth*ke(i,j)
  420       CONTINUE
  440    CONTINUE
  500 CONTINUE
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE elem23s(ijac)
c***********************************************************************
c  [ internal to FEM ]
c  master routine for element 23, a 4-node 2-d bilinear quadrilateral
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      DOUBLE PRECISION wgtjac,cos1,sin2,sin1,cot,tht,
     +                 thtc,cos1c,sin2c,sin1c,uufac,
     +                 fctsinqqm1,fctcosnnm1,sinvv,cosww,
     +                 fctsin,fctcos,dfctsin,dfctcos,
     +                 utht,dutht,omg,ax,ay,bb,gx,gy,rho
      INTEGER          ijac,i,j,nint,igp,intx,inty
c-----------------------------------------------------------------------
c********** form elements matrices and vectors
      IF (ivrb.GE.1) THEN
c        write(1,*) ru,rm,uu0
         IF (ivrb.GE.2) THEN
c        WRITE(*,*) ru,rm,uu0
         ENDIF
      ENDIF
c     STOP ' *TMP*'
      DO i=1,4
         j=icon(ielno,i)
         xxx(i)=x(j)
         yyy(i)=y(j)
         fe(i)=0.d0
         DO j=1,4
           ke(j,i)=0.d0
           ce(j,i)=0.d0
         ENDDO
      ENDDO
      nint=ngpint(ielno)
c---------- integrate terms
      igp=0
      DO intx=1,nint
         xi=gp(nint,intx)
         DO inty=1,nint
            eta=gp(nint,inty)
            igp=igp+1
            CALL shap23s(ijac,igp)
            wgtjac=wt(nint,intx)*wt(nint,inty)*jac(ielno,igp)

c --> first compute some geometrical factors
            tht =ygp(ielno,igp)
            cos1=DCOS(tht)
cmu         cos1=ygp(ielno,igp)
cmu         tht =DACOS(cos1)
            sin2=1.d0-cos1*cos1
            sin1=DSQRT(sin2)
            IF(sin1.EQ.0.) THEN
               STOP 'Problem with cotan -> infty'
            ELSE
               cot=cos1/sin1
            ENDIF
            
c --> Get meriodional circulation and latitudinal derivative at point

c     Test: cut meridional circulation at poles (lat > 60deg)
c           thtc=(pi/2.-tht)/(2./3.)
c           IF(thtc.GE. pi/2.) thtc= pi/2.
c           IF(thtc.LE.-pi/2.) thtc=-pi/2.
c           thtc=pi/2.-thtc
c           cos1c=DCOS(thtc)
            cos1c=cos1
            sin2c=1.d0-cos1c*cos1c
            sin1c=DSQRT(sin2c)
cvv         IF (vv.LE.1.) THEN                                          !cvv
cvv            fctsin = sin1c                                           !cvv
cvv            dfctsin= cos1c                                           !cvv
cvv         ELSE                                                        !cvv
               sinvv  = sin1c*vv
               fctsin = DERF(sinvv)
               dfctsin= cos1c*vvpi*EXP(-sinvv*sinvv)
cvv         ENDIF                                                       !cvv
            fctsinqqm1 = fctsin**qqm1
cvv         IF (ww.LE.1.) THEN                                          !cvv
cvv            fctcos = cos1c                                           !cvv
cvv            dfctcos=-sin1c                                           !cvv
cvv         ELSE                                                        !cvv
               cosww  = cos1c*ww
               fctcos = DERF(cosww)
               dfctcos=-sin1c*wwpi*EXP(-cosww*cosww)
cvv         ENDIF                                                       !cvv
            fctcosnnm1 = fctcos**nnm1
            
            IF (cos1.GT.0.) THEN
               uufac=uu0
            ELSEIF (cos1.LT.0.) THEN
               uufac=us0
            ELSE
               uufac=0.
            ENDIF
            
            utht = -uufac * fctsin*fctsinqqm1*fctcos*fctcosnnm1
            dutht= -uufac * fctsinqqm1*fctcosnnm1 *
     +                           (qq*dfctsin*fctcos + nn*fctsin*dfctcos)
            
c --> get angular velocity and latitudinal derivative at point
            CALL angvels(cos1,omg)
            
            ax = etas0/rm/sin2
            ay = etas0/rm
            gx = -omg/ru
            gy = -utht + cot*etas0/rm
            bb = -cot*utht - dutht                  !- 2 to impose div(B) = 0 (but with condition dBr/dr=0)
c           Add exponential decay
            IF (taud.NE.0.) bb = bb - 1./taud
            
cmu         ax = 1.d0/sin2/rm
cmu         ay = sin2/rm
cmu         gx = -omg/ru
cmu         gy = uu0*sin2c*DERF(musw) + 2.d0*mu/rm
cmu       bb =uu0*(2.d0/SQRT(pi)/ww*sin2c*EXP(-musw*musw)-muc*DERF(musw))
cmu  +           +1.d0/rm/sin2
cca         bb = uu0*(1.d0-2.d0*mu**2)+1.d0/rm/sin2-2.d0/rm
            
c           IF(ielno.EQ.1000) THEN
C              IF (ivrb.GE.1) THEN
c                 write(1,*) mu,sin1,sin2,rm,ru,uu0,omg
c                 write(1,*) ax,ay,bb,gx,gy
C                 IF (ivrb.GE.2) THEN
c                 WRITE(*,*) mu,sin1,sin2,rm,ru,uu0,omg
c                 WRITE(*,*) ax,ay,bb,gx,gy
C                 ENDIF
C              ENDIF
c              STOP ' *TMP*'
c           ENDIF
            rho= 1.d0
            
            DO i=1,4
               DO j=1,4
                  ke(i,j)=ke(i,j)+wgtjac*(
     +                            dphdx(i)*ax*dphdx(j)
     +                           +dphdy(i)*ay*dphdy(j)
     +                           -  phi(i)*bb*  phi(j)
     +                           -dphdx(j)*gx*  phi(i)
     +                           -dphdy(j)*gy*  phi(i))
                  ce(i,j)=ce(i,j)+wgtjac*phi(i)*rho*phi(j)
               ENDDO
            ENDDO
         ENDDO
      ENDDO
      IF (ivrb.GE.1) THEN
c        write(1,*) ielno,1,ke(1,2),ke(2,1),ke(1,4),ke(2,3)
c        write(1,*) ielno,2,ke(3,2),ke(4,1),ke(3,4),ke(4,3)
c        write(1,*) ielno,3,ke(1,1),ke(2,2),ke(3,3),ke(4,4)
         IF (ivrb.GE.2) THEN
c        WRITE(*,*) ielno,1,ke(1,2),ke(2,1),ke(1,4),ke(2,3)
c        WRITE(*,*) ielno,2,ke(3,2),ke(4,1),ke(3,4),ke(4,3)
c        WRITE(*,*) ielno,3,ke(1,1),ke(2,2),ke(3,3),ke(4,4)
         ENDIF
      ENDIF
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE elem24s(ijac)
c***********************************************************************
c  [ internal to FEM ]
c  master routine for element 24, a 8-node 2-d quadratic quadrilateral
c***********************************************************************
      USE surfbr2_param
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      DOUBLE PRECISION wgtjac,cos1,sin2,sin1,cot,tht,
     +                 thtc,cos1c,sin2c,sin1c,uufac,
     +                 fctsinqqm1,fctcosnnm1,sinvv,cosww,
     +                 fctsin,fctcos,dfctsin,dfctcos,
     +                 utht,dutht,omg,ax,ay,bb,gx,gy,rho
      INTEGER          ijac,i,j,nint,igp,intx,inty
c-----------------------------------------------------------------------
c********** form elements matrices and vectors
      DO i=1,8
         j=icon(ielno,i)
         xxx(i)=x(j)
         yyy(i)=y(j)
         fe(i)=0.d0
         DO j=1,8
            ke(j,i)=0.d0
            ce(j,i)=0.d0
         ENDDO
      ENDDO
      nint=ngpint(ielno)
c---------- integrate terms
      igp=0
      DO intx=1,nint
         xi=gp(nint,intx)
         DO inty=1,nint
            igp=igp+1
            eta=gp(nint,inty)
            CALL shap24s(ijac,igp)
            wgtjac=wt(nint,intx)*wt(nint,inty)*jac(ielno,igp)

c --> first compute some geometrical factors
            tht =ygp(ielno,igp)
            cos1=DCOS(tht)
cmu         cos1=ygp(ielno,igp)
cmu         tht =DACOS(cos1)
            sin2=1.d0-cos1*cos1
            sin1=DSQRT(sin2)
            IF(sin1.EQ.0.) THEN
               STOP 'Problem with cotan -> infty'
            ELSE
               cot=cos1/sin1
            ENDIF
            
c --> Get meriodional circulation and latitudinal derivative at point

c     Test: cut meridional circulation at poles (lat > 60deg)
c           thtc=(pi/2.-tht)/(2./3.)
c           IF(thtc.GE. pi/2.) thtc= pi/2.
c           IF(thtc.LE.-pi/2.) thtc=-pi/2.
c           thtc=pi/2.-thtc
c           cos1c=DCOS(thtc)
            cos1c=cos1
            sin2c=1.d0-cos1c*cos1c
            sin1c=DSQRT(sin2c)
cvv         IF (vv.LE.1.) THEN                                          !cvv
cvv            fctsin = sin1c                                           !cvv
cvv            dfctsin= cos1c                                           !cvv
cvv         ELSE                                                        !cvv
               sinvv  = sin1c*vv
               fctsin = DERF(sinvv)
               dfctsin= cos1c*vvpi*EXP(-sinvv*sinvv)
cvv         ENDIF                                                       !cvv
            fctsinqqm1 = fctsin**qqm1
cvv         IF (ww.LE.1.) THEN                                          !cvv
cvv            fctcos = cos1c                                           !cvv
cvv            dfctcos=-sin1c                                           !cvv
cvv         ELSE                                                        !cvv
               cosww  = cos1c*ww
               fctcos = DERF(cosww)
               dfctcos=-sin1c*wwpi*EXP(-cosww*cosww)
cvv         ENDIF                                                       !cvv
            fctcosnnm1 = fctcos**nnm1
            
            IF (cos1.GT.0.) THEN
               uufac=uu0
            ELSEIF (cos1.LT.0.) THEN
               uufac=us0
            ELSE
               uufac=0.
            ENDIF
            
            utht = -uufac * fctsin*fctsinqqm1*fctcos*fctcosnnm1
            dutht= -uufac * fctsinqqm1*fctcosnnm1 *
     +                           (qq*dfctsin*fctcos + nn*fctsin*dfctcos)
            
c --> get angular velocity and latitudinal derivative at point
            CALL angvels(cos1,omg)
            
            ax = etas0/rm/sin2
            ay = etas0/rm
            gx = -omg/ru
            gy = -utht + cot*etas0/rm
            bb = -cot*utht - dutht
c           Add exponential decay
            IF (taud.NE.0.) bb = bb - 1./taud
            
cmu         ax = 1.d0/sin2/rm
cmu         ay = sin2/rm
cmu         gx = -omg/ru
cmu         gy = uu0*sin2*DERF(musw)+2.d0*mu/rm
cmu         bb =uu0*(2.d0/SQRT(pi)/ww*sin1*EXP(-musw*musw)-mu*DERF(musw))
cmu  +           +1.d0/rm/sin2
cca         bb = uu0*(1.d0-2.d0*mu**2)+1.d0/rm/sin2-2.d0/rm
            
            rho= 1.d0
            
            DO i=1,8
               fe(i)=fe(i)+fint(ielno,igp)*phi(i)*wgtjac
               DO j=1,8
                  ke(i,j)=ke(i,j)+wgtjac*(
     +                             dphdx(i)*ax*dphdx(j)
     +                            +dphdy(i)*ay*dphdy(j)
     +                            +  phi(j)*bb*  phi(i)
     +                            +dphdx(j)*gx*  phi(i)
     +                            +dphdy(j)*gy*  phi(i))
                  ce(i,j)=ce(i,j)+wgtjac*phi(i)*rho*phi(j)
               ENDDO
            ENDDO
         ENDDO
      ENDDO
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE shap23s(ijac,igp)
c***********************************************************************
c  [ internal to FEM ]
c  evaluates shape function and derivatives at points (xi,eta)
c  for 4-node quadrilateral
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      DOUBLE PRECISION  xip,xim,etp,etm,dxdxi,dydet,avg
      INTEGER           ijac,igp,i
c-----------------------------------------------------------------------
c---------- constants
cca908 FORMAT('xi =',1pe10.3,' eta =',1pe10.3)
      xip=1.d0+xi
      xim=1.d0-xi
      etp=1.d0+eta
      etm=1.d0-eta
c******************** shape functions
      phi(1)=xim*etm/4.d0
      phi(2)=xip*etm/4.d0
      phi(3)=xip*etp/4.d0
      phi(4)=xim*etp/4.d0
c******************** derivatives of the shape function
c---------- derivatives wrt xi at (xi,eta)
      dphdxi(1)=-etm/4.d0
      dphdxi(2)=etm/4.d0
      dphdxi(3)=etp/4.d0
      dphdxi(4)=-etp/4.d0
c---------- derivatives wrt eta at (xi,eta)
      dphdet(1)=-xim/4.d0
      dphdet(2)=-xip/4.d0
      dphdet(3)=xip/4.d0
      dphdet(4)=xim/4.d0
c---------- DO jacobian calculations
      IF(ijac.EQ.1)THEN
c---------- compute jacobian at (xi,eta)
         dxdxi=0.d0
         dydet=0.d0
         DO 210 i=1,4
            dxdxi=dxdxi+dphdxi(i)*xxx(i)
            dydet=dydet+dphdet(i)*yyy(i)
  210    CONTINUE
         jac(ielno,igp)=dxdxi*dydet
c---------- check positiveness of jacobian
         avg=(DABS(dxdxi)+DABS(dydet))/4.d0
         IF(jac(ielno,igp).LT.avg*1.d-8)THEN
            IF (ivrb.GE.1) THEN
               write(1,*) ielno,avg,jac(ielno,igp),dxdxi,dydet
               write(1,*) xxx(1),xxx(2),xxx(3),xxx(4)
               write(1,*) yyy(1),yyy(2),yyy(3),yyy(4)
               IF (ivrb.GE.2) THEN
               WRITE(*,*) ielno,avg,jac(ielno,igp),dxdxi,dydet
               WRITE(*,*) xxx(1),xxx(2),xxx(3),xxx(4)
               WRITE(*,*) yyy(1),yyy(2),yyy(3),yyy(4)
               ENDIF
            ENDIF
c           STOP '1 jacobi'
         END IF
c---------- compute inverse of the jacobian at (xi,theta)
         jacinv(ielno,igp,1)=dydet/jac(ielno,igp)
         jacinv(ielno,igp,2)=dxdxi/jac(ielno,igp)
      END IF
c---------- derivatives wrt x and y at (xi,eta)
      DO 10 i=1,4
         dphdx(i)=jacinv(ielno,igp,1)*dphdxi(i)
         dphdy(i)=jacinv(ielno,igp,2)*dphdet(i)
   10 CONTINUE
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE shap24s(ijac,igp)
c*********************************************************************** 
c  [ internal to FEM ]
c  evaluates shape function and derivatives at points (xi,eta)
c  for 8-node quadrilateral
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      DOUBLE PRECISION  xip,xim,etp,etm,xi2,et2,dxdxi,dydet,avg
      INTEGER           ijac,igp,i
c-----------------------------------------------------------------------
c---------- constants
      xip=1.d0+xi
      xim=1.d0-xi
      etp=1.d0+eta
      etm=1.d0-eta
      xi2=xip*xim
      et2=etp*etm
c******************** shape functions
      phi(1)=xim*etm*(-xi-eta-1.d0)/4.d0
      phi(2)=xip*etm*( xi-eta-1.d0)/4.d0
      phi(3)=xip*etp*( xi+eta-1.d0)/4.d0
      phi(4)=xim*etp*(-xi+eta-1.d0)/4.d0
      phi(5)=xi2*etm/2.d0
      phi(6)=xip*et2/2.d0
      phi(7)=xi2*etp/2.d0
      phi(8)=xim*et2/2.d0
c******************** derivatives of the shape function
c---------- derivatives wrt xi at (xi,eta)
      dphdxi(1)=etm*( 2.d0*xi+eta)/4.d0
      dphdxi(2)=etm*( 2.d0*xi-eta)/4.d0
      dphdxi(3)=etp*( 2.d0*xi+eta)/4.d0
      dphdxi(4)=etp*( 2.d0*xi-eta)/4.d0
      dphdxi(5)=-xi*etm
      dphdxi(6)=et2/2.d0
      dphdxi(7)=-xi*etp
      dphdxi(8)=-et2/2.d0
c---------- derivatives wrt eta at (xi,eta)
      dphdet(1)=xim*( xi+2.d0*eta)/4.d0
      dphdet(2)=xip*(-xi+2.d0*eta)/4.d0
      dphdet(3)=xip*( xi+2.d0*eta)/4.d0
      dphdet(4)=xim*(-xi+2.d0*eta)/4.d0
      dphdet(5)=-xi2/2.d0
      dphdet(6)=-xip*eta
      dphdet(7)= xi2/2.d0
      dphdet(8)=-xim*eta
c---------- DO jacobian calculations
      IF(ijac.EQ.1)THEN
c---------- compute jacobian at (xi,eta)
      dxdxi=0.d0
      dydet=0.d0
      DO 210 i=1,8
         dxdxi=dxdxi+dphdxi(i)*xxx(i)
         dydet=dydet+dphdet(i)*yyy(i)
  210 CONTINUE
      jac(ielno,igp)=dxdxi*dydet
c---------- check positiveness of jacobian
      avg=(DABS(dxdxi)+DABS(dydet))/4.d0
      IF(jac(ielno,igp).LT.avg*1.d-8)THEN
         IF (ivrb.GE.1) THEN
            write(1,*) ielno,jac(ielno,igp)
            IF (ivrb.GE.2) THEN
            WRITE(*,*) ielno,jac(ielno,igp)
            ENDIF
         ENDIF
         STOP '1 jacobi'
      END IF
c---------- compute inverse of the jacobian at (xi,theta)
      jacinv(ielno,igp,1)=dydet/jac(ielno,igp)
      jacinv(ielno,igp,2)=dxdxi/jac(ielno,igp)
      END IF
c---------- derivatives wrt x and y at (xi,eta)
      DO 50 i=1,8
         dphdx(i)=jacinv(ielno,igp,1)*dphdxi(i)
         dphdy(i)=jacinv(ielno,igp,2)*dphdet(i)
   50 CONTINUE
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE angvels(xx,omg)
c***********************************************************************
c     Computes a solar-like surface angular velocity
c***********************************************************************
      IMPLICIT NONE
      DOUBLE PRECISION  xx,omg,a0,a2,a4
c     data              a0/1.00d0/, a2/-0.1264d0/, a4/-0.1591d0/
c     data              a0/0.0588044d0/, a2/-0.1264d0/, a4/-0.1591d0/
      data              a0/0.04d0/, a2/-0.1264d0/, a4/-0.1591d0/
c-----------------------------------------------------------------------
      omg = a0 + a2*xx**2 + a4*xx**4
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE aplybcs
c***********************************************************************
c  applies the natural and essential boundary conditions to
c  the system matrices
c*********************************************************************** 
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femlocal.f'
      
      INTEGER           i,ieb,j,jpmb,jshift,ibc,jj,ishp,ishm,jrp,jrm
      DOUBLE PRECISION  sum1,sum2
c-----------------------------------------------------------------------
c**** APPLY THE ESSENTIAL BOUNDARY CONDITIONS, IF ANY, BY IMPOSING
c     CONSTRAINT EQUATIONS
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "aplybcs" subroutine      *****')

  400 IF(numebc.LE.0) RETURN

c---- Calculate step change in essential boundary conditions
c     (for ramping in calfuns)
      DO 410 i=1,numnp
         u0(i)=0.d0
         delu(i)=0.d0
         keffdu(i)=0.d0
         keffu0(i)=0.d0
  410 CONTINUE
      DO 420 ieb=1,numebc !(cca  ) numebc = 258
cca  --- Première et dernière ligne:
         i=iuebc(ieb)                                       !(cca  ) iuebc donne la position i de l'élément
         u0(i)=a(i)                                         !(cca  ) a(i)= 0 pour la première et dernière ligne
c        For time varying boundary conditions:
         delu(i)=(uebc(ieb)-a(i))/rns                       !(cca  ) uebc = 0
  420 CONTINUE
  
c**** ENFORCE DIRICHLET B.C. by applying constraint equations to system
c---- 1. Calculate modified load vector [see Burnett, eq.(11.116)]    
      DO 450 j=1,numnp
         sum1=0.d0
         sum2=0.d0
         jpmb=j+mband
         DO 440 i=MAX(1,jpmb-numnp),MIN(mfbw,jpmb-1)
            jshift=jpmb-i
            sum1=sum1+k(i,jshift)*delu(jshift)              !(cca  ) = 0
            sum2=sum2+k(i,jshift)*  u0(jshift)              !(cca  ) = 0
  440    CONTINUE
         keffdu(j)=sum1                                     !(cca  ) = 0
         keffu0(j)=sum2                                     !(cca  ) = 0
  450 CONTINUE

c---- 2. Modify system matrix for Dirichlet BC
      DO 470 ibc=1,numebc                                   !(cca  ) numebc = 258
         jj=iuebc(ibc)                                      !(cca  ) iuebc donne la position i de l'élément
c --> Zero matrix column
         DO 465 i=1,mfbw
            k(i,jj)=0.d0
  465    CONTINUE
c --> Zero matrix "line" (shifted version)
         DO 460 i=2,mband
            ishp=mband-i+1
            ishm=mband+i-1
            jrp=jj+i-1
            jrm=jj-i+1
            IF(jrm.GE.1)     k(ishm,jrm)=0.d0
            IF(jrp.LE.numnp) k(ishp,jrp)=0.d0
  460    CONTINUE
c --> Set corresponding diagonal element of system matrix to one
         k(mband,jj)=1.d0
  470 CONTINUE

c**** ENFORCE PERIODIC B.C.(perbc) by applying constraint a_right=a_left
c     DO ibc=1,numpbc
c        jj0=iupbc0(ibc) !Right boundary (see mesh2ds)
c        jj1=iupbc1(ibc) !Left boundary (see mesh2ds)
c        djj=djj0-djj1
c---- 1. Calculate modified load vector and
c        f(jj1)=f(jj1)+f(jj0)
c        f(jj0)=0.d0
c---- 2. Modify system matrix for periodic BC (A.Lemerle, Juin 2011)
c --> Add column jj0 to column jj1 and zero column jj0 (shifted version)
c        DO i=1,mfbw-djj
c           ish=i+djj
c           k(ish,jj1)=k(ish,jj1)+k(i,jj0)
c           k(i,jj0)=0.d0
c        ENDDO
c --> Add "line" jj0 to "line" jj1 and zero "line" jj0 (shifted version)
c        DO i=2,mband
c           ishp=mband-i+1
c           ishp0=ishp-djj
c           ishm=mband+i-1
c           ishm0=ishm-djj
c           jrp=jj0+i-1
c           jrm=jj0-i+1
c           IF(jrm.GE.1) THEN
c              k(ishm0,jrm)=k(ishm0,jrm)+k(ishm,jrm)
c              k(ishm,jrm)=0.d0
c           ENDIF
c           IF(jrp.LE.numnp) THEN
c              k(ishp0,jrp)=k(ishp0,jrp)+k(ishp,jrp)
c              k(ishp,jrp)=0.d0
c           ENDIF
c        ENDDO
c --> Set corresponding diagonal element of system matrix to one
c        k(mband,jj0)=1.d0
c --> Set element of corresponding periodic node to minus one
c        ish=mband+djj
c        k(ish,jj1)=-1.d0
c     ENDDO

      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      subroutine tribus(n,mhb,mb,gk)
c***********************************************************************
c     triangularized shifted matrix
c     (modified alg., after Golub & Van Loan, Matrix Computations, p. 152)
c
c     nbs is maximum matrix dimension
c     nns is maximum full bandwidth
c
c     INPUT:
c     n   is actual matrix dimension !(cca  ) = 129²
c     mhb is actual half bandwidth !(cca  ) = 131
c     mb  is actual full bandwidth !(cca  ) = 261
c     gk  is rectangular matrix of dimension (mb,n) !(cca  ) = k(261,129²)
c
c     OUTPUT:
c     gk  is triangularized form of input matrix
c==========================================
c     IF   (i,j)      is position in original squared matrix,
c     THEN (i+mb-j,j) is position in shifted matrix
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      
      DOUBLE PRECISION  gk(nbs,nns),c
      INTEGER           n,mhb,mb,k,i,j,kpj,imj
      
c-----------------------------------------------------------------------
      IF (ivrb.GE.1) THEN
         write(1,27) n,mhb,mb
         IF (ivrb.GE.2) THEN
         WRITE(*,27)  n,mhb,mb
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "tribus" subroutine       *****',/,
     +    '(',i6,' nodes, half-bandwidth=',i6,', full-bandwith=',i6,')')
      
c     iter=0
      DO 1 k=1,n-1
         IF(gk(mhb,k).EQ.0.) GOTO 99
         c=1.d0/gk(mhb,k)
         DO 2 i=mhb+1,mb
            gk(i,k)=c*gk(i,k)
    2    CONTINUE
cca 5    CONTINUE
         DO 3 j=1,MIN(mhb-1,n-k)
            kpj=k+j
            c=gk(mhb-j,kpj)
            IF(c.ne.0.) THEN
               DO 4 i=mhb+1,MIN(mb,n-k+mhb)
                  imj=i-j
                  gk(imj,kpj)=gk(imj,kpj)
     +                          -c*gk(i,k)
c                 iter=iter+1
    4          CONTINUE
            END IF
    3    CONTINUE
    1 CONTINUE
c     IF (ivrb.GE.1) THEN
c        write(1,28) iter
c        IF (ivrb.GE.2) THEN
c        WRITE(*,28)  iter
c        ENDIF
c     ENDIF
c  28 FORMAT('(',i12,' iterations)',/)
      
      RETURN
      
   99 CONTINUE
      IF (ivrb.GE.1) THEN
         write(1,100) k,gk(mhb,k)
         IF (ivrb.GE.2) THEN
         WRITE(*,100) k,gk(mhb,k)
         ENDIF
      ENDIF
      STOP 'tribus'
  100 FORMAT(//,'  set of equations may be singular..',/,
     +'   diagonal term of equation',i5,'  at tribus is equal to',
     +1pe15.8)
     
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE rhsbus(n,mhb,mb,gk,gf,u)
c***********************************************************************
c     forward reduction & backsubstitution
c     (modified alg., after Golub & Van Loan, p. 152)
c     nbs is maximum matrix dimension
c     nns is maximum full bandwidth
c
c     INPUT:
c     n   is actual matrix dimension
c     mhb is actual half bandwidth
c     mb  is actual full bandwidth
c     gk  is triangularized rectangular matrix of dimension (mb,n)
c     gf  is RHS vector
c
c     OUTPUT:
c     u   is solution vector     
c     gf  is modified on output
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      
      DOUBLE PRECISION  gk(nbs,nns),gf(nns),u(nns)
      INTEGER           n,mhb,mb,j,mhbmj,i,ishift
c-----------------------------------------------------------------------
c---------- forward reduction
      DO 1 j=1,n
         IF(gk(mhb,j).EQ.0.) GOTO 99
         mhbmj=mhb-j
         DO 2 i=j+1,MIN(n,j+mhb-1)
            ishift=i+mhbmj
            gf(i)=gf(i)-gk(ishift,j)*gf(j)
    2    CONTINUE
    1 CONTINUE

c---------- backsubstitution 

      DO 3 j=n,1,-1
         u(j)=gf(j)/gk(mhb,j)
         mhbmj=mhb-j
         DO 4 i=MAX(1,j-mhb+1),j-1
            ishift=i+mhbmj
            gf(i)=gf(i)-gk(ishift,j)*u(j)
    4    CONTINUE
    3 CONTINUE
      RETURN
   99 CONTINUE

      IF (ivrb.GE.1) THEN
         write(1,100) j,gk(mhb,j)
         IF (ivrb.GE.2) THEN
         WRITE(*,100) j,gk(mhb,j)
         ENDIF
      ENDIF
      STOP 'rhsbus'
  100 FORMAT(//,'  set of equations are singular.',/,
     +'   diagonal term of equation',i5,'  at tribus is equal to',
     +1pe15.8)
      END SUBROUTINE
c=======================================================================

c***********************************************************************
      SUBROUTINE stlasts
c***********************************************************************
c stores solutions of last step, for future usage as initial condition *
c***********************************************************************
      USE surfbr2_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/surfbr2.common.global.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.mesh.f'
      INCLUDE 'bldyn_includefiles/surfbr2.common.femglobal.f'
      
      INTEGER j
c-----------------------------------------------------------------------
      IF (ivrb.GE.1) THEN
         write(1,27)
         IF (ivrb.GE.2) THEN
         WRITE(*,27)
         ENDIF
      ENDIF
   27 FORMAT(/,'*****      Entering "stlasts" subroutine      *****')
      
c---- output grid values
cca   istp=ntype-10
c     cdate=date()
c     cclock=clock()
c     WRITE(19) cdate,cclock
      WRITE(19) title
      WRITE(19) numnp,numel,ntype
      WRITE(19) nts,nsteps,theta,timein,timeout
      WRITE(19) xx
      WRITE(19) yy
      
      WRITE(19) a
      
      RETURN
      END SUBROUTINE
c=======================================================================

c***********************************************************************
c     INCLUDE FILES
c
c     subs.derf.f     --> Numerical Recipes routines needed for error function (double precision)
c     fct.strlen.f   --> finds true length of a string (remove blanks)
c***********************************************************************
      
      include 'bldyn_includefiles/subs.derf.f'
c     include 'bldyn_includefiles/fct.strlen.f'
      INCLUDE './0-data/3-arnaud/synthese_wolf_bmr_alex.f'
      INCLUDE './0-data/3-arnaud/subs.randsom.f'
      
c=======================================================================

