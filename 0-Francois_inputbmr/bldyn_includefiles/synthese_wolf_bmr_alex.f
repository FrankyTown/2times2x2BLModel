c***********************************************************************
      SUBROUTINE synthese_wolf_bmr_alex(seed,fstcyci,lstcyci,filename)
c     PROGRAM synthese_wolf_bmr_alex
c***********************************************************************      
c     Fonction du programme: associer des latitudes et des aires
c     synthetiques aux taches solaires synthétisées à partir du nombre
c     Wolf.
c***********************************************************************      
c     Auteur: Arnaud Carignan-Dugas, été 2011
c     Partie magnétique: Alexandre Lemerle
c     Mise à jour: A. Lemerle 2015-04-07
c***********************************************************************      
      IMPLICIT NONE
      
c---- Inputs:      
      integer             seed                                          !Random seed: initialize with urands_init(seed), then call with urands2() and gssrands()
      integer             fstcyci,lstcyci
      character           filename*250
      
c---- Local:
      integer,parameter:: ivrb=0                                        !Print on-screen information (1) or not (0)
c     integer,parameter:: seed=12345                                    !(Base de donnée "0":221, "1":1234, "2":2345, "3":3456, "4":4567, "5":5678, "6":6789, "7":7890, "8":8901, "9":9012)
      
      character home*250,path*250,filessn*250,
     +          mjdortime*4,seedtext*5,cyclet*3,
     +          anmois*6,anmoisjour*8,anmoisjourhr*10,anmoisjourhrmin*12
      
      integer res                                                       !Résolution de base par cycle
      parameter (res=300)
      integer ncycles
      parameter(ncycles=29)                                             !Nombre de cycles
      integer dmincyc,fstcyc,lstcyc                                     !Positions du début du premier et de la fin du dernier cycle à utiliser
      integer ndatamax
      parameter(ndatamax=50000)                                         !Nb max de données à lire
      real x(res*2,ncycles),xx                                          !Position dans le cycle
      real f1(res*2,ncycles)                                            !Parametre de centre de gaussienne
      real f3(res*2,ncycles)                                            !Parametre d'ecart type de gaussienne
      real c1(ncycles)                                                  !Valeur definitive d'un c1 (gerant f1)
      real c2(ncycles)                                                  !Valeur definitive d'un c2 (gerant f1)
      real c3(ncycles)                                                  !Valeur definitive d'un c3 (gerant f1)
      real c4(ncycles)                                                  !Valeur de c dans la parabole gerant f3
      real c5(ncycles)                                                  !Valeur de b dans la parabole gerant f3
      real c6(ncycles)                                                  !Valeur de a dans la parabole gerant f3
      
      integer           b
      parameter         (b=30.)                                         !Subdivisions des cycles
      integer           narmax,nar                                      !Nbr max d'émergences générées
      parameter         (narmax=100000)
      
c---- Temps:
      DOUBLE PRECISION  time(narmax),timeday(narmax),tdayjul,
     +                  timeday0,timeday1,dur
      integer           cycle(narmax),year(narmax),
     +                  month(narmax),day(narmax),daysinmonth(12),
     +                  daymonth(12),year0,month0,dd0,hh0,mm0,year1,
     +                  month1,dd1,hh1,mm1,yrjul,mojul,ddjul,hhjul,mmjul
      real              rzar(narmax),
     +                  tod(narmax),jour(narmax),pourcent,randjour(100),
     +                  temp,sum_jr_mois,yrmincyc0,yrmincyc1
      INTEGER           signcyclei,heure,minute
      
      real              latday(narmax)                                  !Latitude exp.
      real              latsynth(narmax)                                !Latitude synthetique
      real              longsynth(narmax)                               !Longitude synthetque
      real              longday(narmax)                                 !Longitude
      real              areaday(narmax)                                 !Aire
      
      integer           i,j,k,l,m,n,flag,idebut                         !(variables itératives ou conditionnelles)
      integer           nbmr (ncycles)                                  !Nombre de bmr dans chaque cycle = somme(l)*nbmrnar
      real              somme(ncycles)                                  !Nombre de taches dans chaque cycle (intégrale si on veut)
      real              rzmax(ncycles)                                  !Maximum of Rz (wolf ssn) during each cycle
      integer           rescyc(ncycles)                                 !Résolution de chaque cycle avec cutmin et cutmax ajoutés
      real              fract(narmax)                                   !Fraction de la position temporelle dans un cycle
      real              f1synth(narmax),f3synth(narmax)                 !Moyenne et deviation standard de la dispersion latitudinale
      real              yrmincyc(ncycles+1)                             !Dates des minima des cycles (pour subdiviser)
      real              cutmin(ncycles),cutmax(ncycles)                 !variables pour l'overlap
      real              airesynth(narmax),loga(narmax),
     +                  loga0(ncycles),logasig(ncycles)                 !D'autres paramètres synthétiques (notamment pour l'aire synthétique)
      integer           intairesynth(narmax)
      integer           maximum                                         !maximum pour arreter une boucle potentiellement infinie
      parameter         (maximum=100000)
      
      REAL              rand,gauss,chance,chanceneg
      INTEGER           cyclel
      
c---- Variables pour lire les données de Wolf
      
      double precision  timerz(ndatamax)
      real              prob(ndatamax),spots(ndatamax),rzsmth(ndatamax),
     +                  rz(ndatamax)
      integer           yearrz(ndatamax),monthrz(ndatamax),
     +                  dom_wolf(ndatamax),compteurj,emerg,maxemerg,
     +                  nread,narinclus
      
c---- Variables pour conversion Active Regions (AR) en Bipolar magnetic regions (BMR)
      
      INTEGER           signe(narmax)
      REAL              flux(narmax),minflux,maxflux,
     +                  lat1(narmax),lon1(narmax),
     +                  lat2(narmax),lon2(narmax),
     +                  b0,minb0,maxb0,
     +                  minarea,maxarea
      
      real              nbmrnar,loga021,logasig21,logphi0,logphisig,
     +                  tiltjoy,tiltdev0,tiltdev1,tiltdev2,
     +                  deltalin0,deltalin1,deltadev
      real              tht,phi,logflux,sigtilt,tilt,delta,dtht,dphi
      real,parameter :: pi=3.141592654,rsun=6.9599e10                   !Rsun en cm
      
c---- PARAMETRES OPTIMISÉS dans analyse_rgo-wang.idl -------------------
      
      real,parameter :: minlat=0.,minaire=1.,maxaire=4000.,             !minaire=7.3905,maxaire=1930.624,
     +                  decoupagemin=0.935,decoupagemax=0.9172
c     real,parameter :: ajustf1=-1.44e-4,ajustf3=-2.26e-4,ajustc0synth=-3.083e-9,ajustc1synth=-5.3958e-10
      
c     REAL,PARAMETER :: flux0=0., b0=450., ajust=3000., ajust2=0.
c     REAL,PARAMETER :: tilt0=18., tilt1=90., tiltflux=8.e-2
c     REAL,PARAMETER :: delta1=-8.51, sigmadelta=0.17, delta2=0.427
      
c-----------------------------------------------------------------------
      
      INTEGER           strlen
      REAL              urands2,gssrands
      EXTERNAL          urands2,gssrands
      
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      
      CALL urands_init(seed)
      
c     Nbr de jours dans chaque mois et avant chaque mois
      daysinmonth =[31,28,31,30, 31, 30, 31, 31, 30, 31, 30, 31]
      daymonth    =[ 0,31,59,90,120,151,181,212,243,273,304,334]
      
c     Time conversion (Modified Julian days)
c     (Julian Date starts: January 1st,-4712 (4713 BC), 12:00)
c     (Modified Julian Date starts: November 17, 1858, 0:00)
c     mjd=2400000.5d0
      yrjul=1858
      mojul=11
      ddjul=17
      hhjul=0
      mmjul=0
      
      tdayjul = (yrjul-1)*365.d0                         + (yrjul-1)/4  
     +                                                   - (yrjul-1)/100
     +                                                   + (yrjul-1)/400
     +         + daymonth(mojul) + (ddjul-1) + hhjul/24. + mmjul/60./24.
      IF ((mojul.GT.2)                  .AND.(yrjul/4.  .EQ.yrjul/4  )
     +                                 .AND.((yrjul/100..NE.yrjul/100)
     +                                   .OR.(yrjul/400..EQ.yrjul/400)))
     +                                              tdayjul=tdayjul+1.d0
      IF (ivrb.EQ.1) WRITE(*,*) 'tdayjul=',tdayjul
      
c     Dates des minima de cycles
      yrmincyc =(/ 1701.       ,1713.       ,1724.       ,1732.       ,
     +             1744.       ,1755.+ 2./12,1766.+ 5./12,1775.+ 5./12,
     +             1784.+ 7./12,1798.+ 4./12,1810.+11./12,1823.+ 4./12,
     +             1833.+10./12,1843.+ 6./12,1855.+11./12,1867.+ 2./12,
     +             1878.+11./12,1890.+ 2./12,1902.+ 1./12,1913.+ 7./12,
     +             1923.+ 7./12,1933.+ 8./12,1944.+ 1./12,1954.+ 3./12,
     +             1964.+ 9./12,1976.+ 5./12,1986.+ 8./12,1996.+ 4./12,
     +             2008.+11./12,2020. /)
      dmincyc  = 5
      
c     fstcyci=21
c     lstcyci=21
      fstcyc =fstcyci+dmincyc                                           ! 5 pour début cycle 0 (1744...) et 26 pour début cycle 21 (1976...)
      lstcyc =lstcyci+dmincyc                                           ! 29 pour début du cycle 24 (2008...)
      
      CALL GETENV('HOME',home)
      path    =home(1:strlen(home))//
     +                          '/Documents/1-Doctorat/0-data/3-arnaud/'
      
c     filename=path(1:strlen(path))//'synth_wolf_bmr_1749a2014_'        !Output database (without seed and '.dat")
c     filename=path(1:strlen(path))//'synth_wolf_bmr_cycle21x01_'       !Output database (without seed and '.dat")
c     WRITE(seedtext,'(i5)') seed
c     filename=filename(1:strlen(filename))//seedtext//'.dat'
      
      filessn =path(1:strlen(path))//'donnees/SIDC/monthssn.dat'
      
      mjdortime= 'Time'                                                 !Print time in Modified Julian Days (' MJD') or in time relative to start date ('Time')
      
c     Date de début
      yrmincyc0= yrmincyc(fstcyc)
      year0    = INT(yrmincyc0)
c     month0   = INT((yrmincyc0-year0)*12+1+0.5)
c     dd0      = 0
c     hh0      = 0
c     mm0      = 0
      
      timeday0 = -tdayjul + (yrmincyc0-1)*365.d0         + (year0-1)/4  
     +                                                   - (year0-1)/100
     +                                                   + (year0-1)/400
c    +              + daymonth(month0) + (dd0-1) + hh0/24. + mm0/60./24.
      IF (                                   (year0/4.  .EQ.year0/4  )
     +                                 .AND.((year0/100..NE.year0/100)
     +                                   .OR.(year0/400..EQ.year0/400)))
     +                          timeday0=timeday0+(yrmincyc0-year0)*1.d0
c     IF ((month0.GT.2)                 .AND.(year0/4.  .EQ.year0/4  )
c    +                                 .AND.((year0/100..NE.year0/100)
c    +                                   .OR.(year0/400..EQ.year0/400)))
c    +                                            timeday0=timeday0+1.d0
      IF (ivrb.EQ.1) WRITE(*,*) 'timeday0=',timeday0
      
c     Date de fin
      yrmincyc1= yrmincyc(lstcyc+1)
      year1    = INT(yrmincyc1)
c     month1   = INT((yrmincyc1-year1)*12+1+0.5)
c     dd1      = 0
c     hh1      = 0
c     mm1      = 0
      
      timeday1 = -tdayjul + (yrmincyc1-1)*365.d0         + (year1-1)/4  
     +                                                   - (year1-1)/100
     +                                                   + (year1-1)/400
c    +              + daymonth(month1) + (dd1-1) + hh1/24. + mm1/60./24.
      IF (                                   (year1/4.  .EQ.year1/4  )
     +                                 .AND.((year1/100..NE.year1/100)
     +                                   .OR.(year1/400..EQ.year1/400)))
     +                          timeday1=timeday1+(yrmincyc1-year1)*1.d0
c     IF ((month1.GT.2)                 .AND.(year1/4.  .EQ.year1/4  )
c    +                                 .AND.((year1/100..NE.year1/100)
c    +                                   .OR.(year1/400..EQ.year1/400)))
c    +                                            timeday1=timeday1+1.d0
      IF (ivrb.EQ.1) WRITE(*,*) 'timeday1=',timeday1
      
c     Durée
      dur = timeday1-timeday0
      
c     ========================================EXECUTION======================================================================
c     ;1.1-----------Lecture de donnees
c     IF (ivrb.EQ.1) WRITE(*,*) 
c    +              'LECTURE DES DONNEES DES TACHES (le nombre de wolf)'
c     l=rand(seed1)  !dummy quelconque pour empecher de tourner en rond (il faut initialiser le générateur de nombre aléatoire)

c ICI ON LIT LES DONNES JOURNALIERES (trop de variations, allons pour les donnees mensuelles lissees)

c53    format(i4,2i2,2x,f8.3,a4)
c54    format(i4,2i2,2x,f8.3,i4)
c      open(73,file='donnees/dayssn.dat',status='old')
c
c      !Skip datas before 1874========================================
c      do i=1,20454 !Pour commencer en 1874
c      read(73,53,end=55)yearrz(1),monthrz(1),dom_wolf(1),timerz(1),unknowed
c      enddo
c      !Read after 1874
c
c      i=0
c      compteurj=0
c56    i=i+1
c      read(73,54,end=55)yearrz(i),monthrz(i),dom_wolf(i),timerz(i),rz(i)
c
c         prob(i)=(0.141705+0.545767*rz(i)+(-0.00105210)*rz(i)**2.)/30.
c                0.173912+0.0123116*rz(i)+(gssrands()*0.254225)!0.311673+0.00974497*rz(i)+(gssrands()*0.367609)
c
c         emerg=int(prob(i))

c         if (urands2().le.(prob(i)-int(prob(i)))) then emerg=emerg+1

c         if (emerg .gt. 0) then
c            do j=i+compteurj,i+compteurj+emerg
c            time(j)=timerz(i)
c            year(j)=yearrz(i)
c            month(j)=monthrz(i)
c            day(j)=dom_wolf(i)
c            tod(j)=0.
c            enddo
c         endif

c      compteurj=compteurj+emerg
c      goto 56
c55    close(73)


c      open(73,file='donnees/wolf_data.dat',status='old')
c      i=0
c      compteurj=0
c56    i=i+1
c      read(73,*,end=55)spots(i),rz(i),rzsmth(i),temps(i),timerz(i),yearrz(i),monthrz(i)


c ICI on lit les données mensuelles lissées-----------------------------------------------------------------------
      
      open(73,file=filessn,status='old')
      
      maxemerg=0
      
      i=0
      compteurj=0
56    i=i+1
         read(73,'(i4,i2,f10.3,f8.1,f8.1)',end=55)
     +                    yearrz(i),monthrz(i),timerz(i),rz(i),rzsmth(i)!assignation des données à des vecteurs
         IF (rzsmth(i).LT.0.) THEN                                      ! Éviter les mauvaises données
            IF (ivrb.EQ.1) WRITE(*,*) 
     +                            '! Donnee invalide: rzsmth=',rzsmth(i)
            i=i-1
            GOTO 56
         ENDIF
         IF (timerz(i).LT.yrmincyc(fstcyc)) THEN                      ! Éviter les données à l'extérieur des cycles utilises
c           IF (ivrb.EQ.1) WRITE(*,*) 
c    +                           '! Donnee avant ',yrmincyc(fstcyc),' !'
            i=i-1
            GOTO 56
         ENDIF
         IF (timerz(i).GE.yrmincyc(lstcyc+1)) THEN                    ! Éviter les données à l'extérieur des cycles utilises
c           IF (ivrb.EQ.1) WRITE(*,*) 
c    +                         '! Donnee apres ',yrmincyc(lstcyc+1),' !'
            i=i-1
            GOTO 56
         ENDIF
         IF ((i.GE.2).AND.(timerz(i).LT.timerz(i-1)))
     +        STOP '!! Probleme de chronologie dans les donnees lues !!'
         
c        Probabilité d'apparition d'une region active (par rapport au nbr de Wolf):
         prob(i)=0.141705+0.545767*rzsmth(i)+(-0.00105210)*rzsmth(i)**2.   
         if (prob(i) .lt. 0.) prob(i)=0.
         
c        On multiplie la probabilité d'émergence par nbmrnar (en théorie, on devrait avoir 1 BMR pour 1 region active, on trouve plutôt 0.67 pour correspondre a Wang&Sheeley...)
         nbmrnar=0.75                                                   !0.67
         prob(i)=prob(i)*nbmrnar
         
c        A partir de cette probabilité, donner un nombre d'émergences (en arrondissant a la baisse la probabilité)
         emerg=int(prob(i))
c        On gagne un émergence suplémentaire si le nombre aléatoire est inférieur aux décimales de la probabilité:
         if (urands2().le.(prob(i)-emerg)) emerg=emerg+1
                  
         IF (emerg.GT.maxemerg) maxemerg=emerg
         
c        On génère un vecteur de nombre aléatoires ordonnés pour définir le jour
         DO j=1,emerg
            randjour(j)=urands2()                                        !!!Verifier les premieres donnes avec different seed... Bizarrement, ca donne les memes valeurs...
         ENDDO
         DO j=2,emerg
            temp=randjour(j)
            DO k=j-1,1,-1
               IF (randjour(k).LE.temp) GOTO 27
               randjour(k+1)=randjour(k)
            ENDDO
	         k=0
   27       randjour(k+1)=temp
         ENDDO
         
c        A partir des émergences, associons, une date à chacune dans un mois
         if (emerg .gt. 0) then
c           do j=i+compteurj,i+compteurj+emerg
            do j=compteurj+1,compteurj+emerg
               
               rzar (j)= rzsmth(i)
               year (j)= yearrz(i)                                      !Association de l'année et du mois
               month(j)= monthrz(i)
               
               daysinmonth(2)=28
               IF (                      (year(j)/4.  .EQ.year(j)/4  )
     +                             .AND.((year(j)/100..NE.year(j)/100)
     +                               .OR.(year(j)/400..EQ.year(j)/400)))
     +                                                 daysinmonth(2)=29
               sum_jr_mois=0.
               DO k=1,month(j)-1
                  sum_jr_mois=sum_jr_mois+daysinmonth(k)
               ENDDO
               
               jour(j)= (1.*randjour(j-compteurj)*daysinmonth(month(j)))!Donne une journée entre 0 et le nombre de jour du mois
               time(j)= (sum_jr_mois+jour(j))/(1.d0*sum(daysinmonth))
     +                                                   + DBLE(year(j))!Calcul le temps en années (= (jour(j) + jours des mois précédents)/(nombre jour cette année) + année).
               tod(j) = 1.*jour(j)-int(jour(j))                         !Calcul le moment de la journée
               day(j) = int(jour(j))+1                                  !Donne une journée entre 1 et le jour du mois maximal+1 (ex:entre 0 et 31.999999 (périodique)
               
               ! Temps en Modified Julian Days:
               timeday(j)=-tdayjul + (year(j)-1)*365.d0+ (year(j)-1)/4  
     +                                                 - (year(j)-1)/100
     +                                                 + (year(j)-1)/400
     +                               + sum_jr_mois + (day(j)-1) + tod(j)
               IF (mjdortime.EQ.' MJD') THEN
                  timeday(j)=timeday(j)+0.
               ELSEIF (mjdortime.EQ.'Time') THEN
                  timeday(j)=timeday(j)-timeday0
               ELSE
                  STOP 'STOP: Wrong choice of mjdortime !'
               ENDIF
               
c              Condition importante, car dans l'écriture, on arrondit tod à quatre décimales.
c              Si cela arrondit à la hausse, il faut additionner une autre journée, et il faut
c              prévoir que cette journée peut permettre de sauter un mois, voire une année 
c              (Si on est le 31.99995 décembre 1880 par exemple, on saute au 1er janvier 1881 en arrondissant)
               if (tod(j) .ge. 0.99995)then
                  tod(j)=0.
                  day(j)=day(j)+1 
                  if (day(j) .gt. daysinmonth(month(j)))then
                     day(j)=1
                     month(j)=month(j)+1
                     if (month(j) .gt. 12) then
                        month(j)=1
                        year(j)=year(j)+1
                     endif
                  endif
               endif
               
            enddo
         endif
         compteurj=compteurj+emerg  !ce compteur permet d'attribuer des données à toutes les taches déterminées par le nombre de Wolf
      goto 56
55    close(73)
      nread=i-1
      
      IF (ivrb.EQ.1) WRITE(*,*) nread,' donnees lues (= nombre de mois)'
      nar=compteurj
      IF (ivrb.EQ.1) WRITE(*,*) nar,' emergences generees (max ',
     +                     maxemerg,' emergences par mois)'

      
c     ;---------2.1 Subdivision des cycles


c     ===============Boucle sur les 28 cycles pour calculer l'intensité d'un cycle========
      IF (ivrb.EQ.1) WRITE(*,*) 'ASSIGNATION DES PARAMÈTRES'

      DO l=1,ncycles
         nbmr (l)=0
         somme(l)=0.
         rzmax(l)=0.
      ENDDO
      
      flag=0
      idebut=nar
      narinclus=0
      DO i=1,nar                                                        !Boucle sur toutes les données i
         cycle(i)=99

         DO l=1,ncycles                                                 !Boucle pour choisir cycle l
            if ((time(i).ge.yrmincyc(l)).and.
     +          (time(i).lt.yrmincyc(l+1))) then                        !condition avec les largeurs de boites
               IF (flag.EQ.0) idebut=i
               flag=1
               cycle(i)=l-dmincyc
c              somme(l)=somme(l)+1.
               nbmr (l)=nbmr (l)+1
               somme(l)=somme(l)+1./nbmrnar
               IF (rzar(i).GT.rzmax(l)) rzmax(l) = rzar(i)
               narinclus=narinclus+1
            endif
         ENDDO                                                          !Fin boucle sur cycles l
         
         IF ((i.GT.idebut).AND.(cycle(i).LT.cycle(i-1)))
     +    STOP '!! Probleme de chronologie dans les donnees generees !!'
         
      ENDDO                                                             !Fin boucle sur données i
      
      DO l=1,ncycles
c        IF (l.GE.fstcyc .AND. l.LE.lstcyc .AND. somme(l).EQ.0) THEN
         IF (l.GE.fstcyc .AND. l.LE.lstcyc .AND. nbmr(l).EQ.0) THEN
            IF (ivrb.EQ.1) WRITE(*,*) 
     +                    '!! Pas d emergence au cycle ',l-dmincyc,' !!'
            STOP
         ENDIF
         IF (ivrb.EQ.1) WRITE(*,15) l-dmincyc,yrmincyc(l),yrmincyc(l+1),
     +                                         nbmr(l),somme(l),rzmax(l)
      ENDDO
      IF (ivrb.EQ.1) WRITE(*,*) narinclus,'/',nar,
     +                   ' emergences a l interieur des cycles utilises'
      nar=narinclus
      
      IF (ivrb.EQ.1) WRITE(*,*) 'Sur une duree totale de ',dur,' jours'
      
   15 FORMAT('Cycle ',i3,' (',f6.1,'-',f6.1,') : ',i7,' BMR emergees (',
     +                                       f6.1,' AR), Rz_max =',f6.1)
      
c==================================================================================================================================
c==================================================================================================================================
c=========Determiner les paramètres à partir de où on est dans le cycle (fixer les gaussiennes mobiles)

c Tous les paramètres proviennent des codes IDL 
c Il est à noter que partout il y a en commentaire des gauss*un_nombre, cela permettait à la base de vérifier la variabilité du modèle.
c gauss est une fonction qui donne une distributino gaussienne à partir de la méthode box-muller. Le nombre correspond au ki normalisé.

      cutmin(lstcyc)=0
      cutmax(fstcyc)=0
      
      do l=fstcyc,lstcyc
         
c     ===============================================Paramètres pour la latitude====================================================
         
         !somme(l) = 1893 (cyc 12), 2224 (cyc 14) to 4663 (cyc 19)
         
         !1.La moyenne des latitudes----------------------
         
         c1(l) = 14.6842 +( 0.00211349 )*somme(l)                       !+(gssrands()*1.43197)       !=> de 18.7 à 24.5 deg
         c2(l) = 0.498766+( 3.14934e-05)*somme(l)                       !+((0.0416158)*gssrands())   !=> de 0.558 à 0.646
         c3(l) = 2.83640 +(-1.13568e-05)*somme(l)                       !+((0.434625)*gssrands())    !=> de 2.81 à 2.78 deg
         
         !1.L'écart type des latitudes----------------------
         
         c4(l) = 0.979392+( 0.000538578)*somme(l)                       !+gssrands()*0.257910        !=> de 2.00 à 3.49 deg
         c5(l) = 17.6579 +(-5.22007e-05)*somme(l)                       !+gssrands()*0.460325        !=> de 17.56 à 17.41 deg
         c6(l) =-17.6929 +(-0.000173730)*somme(l)                       !+gssrands()*0.733518        !=> de -18,0 à -18.5 deg
         
c     ===============================================Paramètres pour le découpage (overlap)====================================================
         
         if (l .ne. lstcyc) then
            
            cutmin(l)  =( 4.35029e-09)*somme(l+1)**2.
     +                 +(-2.12830e-05)*somme(l+1)
     +                 +( 0.0411586  )                                  !+gssrands()*0.0211791     !=> de 0.016 à 0.037 
            cutmax(l+1)=( 7.21523e-08)*somme(l+1)**2.
     +                 +(-0.000501344)*somme(l+1)
     +                 +( 0.869126   )                                  !+gssrands()*0.0313298     !=> de 0.179 à 0.100
            
            !Assurons nous que le cut est toujours positif ou nul
            
            if (cutmax(l+1).lt. 0.) cutmax(l+1)=0.
            if (cutmin(l)  .lt. 0.) cutmin(l)=0.
         endif
         
         rescyc(l)=int(res*(1+cutmin(l)+cutmax(l)))

c     ===============================================Paramètres pour l'aire synthétique====================================================
c        Distribution log-normale
         loga0  (l) = 1.75                                              ! 2.97678+somme(l)*0.000126658+ajustc0synth*somme(l)**2.!+gssrands()*0.118883
         logasig(l) = 0.60 + 0.13*(rzmax(l)/200.)                       ! 2.77757+somme(l)*(-5.52692e-05)+ajustc1synth*somme(l)**2.!+gssrands()*0.126330
         
c        ASSIGNATION DES PARAMÈTRES PLUS SPÉCIFIQUES (latitude et aire estimés avec une incertitude)

         do j=1,rescyc(l)
            !Ce paramètre situe la position temporelle dans un cycle (avec la résolution):
            x(j,l)=(-res*cutmax(l)+(j-1))/res+l-1
         ENDDO
         x(rescyc(l)+1,l)=cutmin(l)+l
         
         DO j=1,rescyc(l)
            xx=(x(j,l)+x(j+1,l))/2.
            !Moyenne des latitudes:
            f1(j,l) = c1(l)*exp(-(xx-l+1)/c2(l)) + c3(l)                !+somme(l)*ajustf1!+(gssrands()*0.696470)
            !Ecart-type des latitudes:
            f3(j,l) = c4(l) + c5(l)*(xx-l+1) + c6(l)*(xx-l+1)**2.       !+somme(l)*ajustf3!+(0.780106*gssrands())
         enddo
         
      ENDDO !POUR LES L
      
c     MAINTENANT, ON UTILISE LES VALEURS synthETIQUES DES GAUSSIENNES POUR CRÉER DES LATITUDES synthETIQUES=======================================
c==============================================================================================================================================

c     Determiner ou on est dans le cycle pour choisir un gaussienne appropriee
      do l=fstcyc,lstcyc                                            !Je fais cycle par cycle pcq les x changent, les gaussiennes aussi, selon l'intensite du cycle
         IF (ivrb.EQ.1) WRITE(*,*) 
     +             'CREATION DE DONNEES SYNTHETIQUES, cycle:',l-dmincyc,
     +                                    ', Nombre de taches:',somme(l)
         
         do i=1,nar  !Je balais les donnees de chq tache pour leur associe une latitude synthetique
            
            if ((time(i).gt.(yrmincyc(l))).AND.
     +          (time(i).le.(yrmincyc(l+1)))) THEN                      !condition avec les largeurs des cycles pour choisir le cycle
               
               fract(i)=
     +       (SNGL(time(i))-yrmincyc(l))/(yrmincyc(l+1)-yrmincyc(l))+l-1!La fraction donne ou on est dans le cycle
                                                                        !(valeur entre 0+l et 1+l)
                                                                        !Cela permet de choisir une gaussienne appropriee pour la latitude
               
               do j=1,rescyc(l)                                         !Ici on associe une latitude avec les parametres du cycle l
                  
                  if ((fract(i).gt.x(j,l)).and.
     +                (fract(i).le.x(j+1,l))) then                      !On se sert de fraction pour se trouver dans le cycle

c         LATITUDE SYNTHETIQUE=======================================================================================================
                     latsynth(i) = f1(j,l)+f3(j,l)*gssrands()        !Une fois trouvé, on assigne la latitude synthetique
                     do k=0,maximum 
                        ! on s'assure que la valeur donnée soit dans les limites instaurées:
                        if ((latsynth(i).gt.60.).or.
     +                      (latsynth(i).lt.minlat)) then
                           latsynth(i) = f1(j,l)+f3(j,l)*gssrands()
                        else
                           exit
                        endif
                     enddo
                     f1synth(i)=f1(j,l)
                     f3synth(i)=f3(j,l)

c         AIRE SYNTHETIQUE=======================================================================================================                 
c                    Correspond à une vraie distribution lognormale fA(A) = f0*1/A*exp(-0.5*(logA-logA0)^2/sigma^2) = f0'*exp(-0.5*(logA-(logA0-sigma^2))^2/sigma^2)
c                    En effet, ne pas corriger pour le (-sigma^2) semble être une erreur courante (e.g. Jiang et al.2011).
                     loga(i) = loga0(l)+logasig(l)*gssrands()
                     
                     do k=0,maximum
c                       on s'assure que la valeur donnée soit dans les limites instaurées:
                        if ((10**loga(i).gt.maxaire).or.
     +                      (10**loga(i) .lt. minaire)) then
                           loga(i) = loga0(l)+logasig(l)*gssrands()
                        else
                           exit
                        endif
                     enddo
                     airesynth(i)=10**loga(i)

                  endif
                                  
               Enddo                                                    !pour les j
    
c====================================Maintenant, on inclut les possibilités d'overlap.============================================

c       ======================================OVERLAP DU CYCLE PRÉCÉDENT==========================================================
               if (l .ne. fstcyc) then    
                  
                  do j=1,rescyc(l-1)                                    !On bride au croisement des cycles (pour le min)
                  
c            LATITUDES SYNTHETIQUES======================================================================================================= 
                     if ((fract(i).gt.x(j,l-1)).and.
     +                   (fract(i).lt.x(j+1,l-1))) then
c                       WRITE(*,*) x(j,l-1),fract(i),x(j+1,l-1)
                        chance=urands2()
                        if (chance .lt. decoupagemin) then              !La chance d'overlap dépend de decoupagemin
                           latsynth(i) = f1(j,l-1)+f3(j,l-1)*gssrands()
                           cycle(i)=cycle(i)-1
                           do k=0,maximum 
c                             on s'assure que la valeur donnée soit dans les limites instaurées:
                              if ((latsynth(i).gt.60.).or.
     +                            (latsynth(i).lt.minlat)) then
                           latsynth(i) = f1(j,l-1)+f3(j,l-1)*gssrands() 
c                                WRITE(*,*) latsynth(i),f1(j,l-1),f1(j,l-1),l,x(j,l-1)
                              else
                                 exit
                              endif
                           enddo
                           f1synth(i)=f1(j,l-1)
                           f3synth(i)=f3(j,l-1)
                           
c            AIRES SYNTHETIQUES=======================================================================================================                 
                           loga(i) = loga0(l-1)+logasig(l-1)*gssrands()
                           do k=0,maximum
c                             on s'assure que la valeur donnée soit dans les limites instaurées:
                              if ((10**loga(i).gt.maxaire).or.
     +                            (10**loga(i).lt.minaire)) then
                           loga(i) = loga0(l-1)+logasig(l-1)*gssrands()
                              else
                                 exit
                              endif
                           enddo
                           airesynth(i)=10**loga(i)
                           
                        endif
                     endif
                  enddo             !pour les j
                  
               endif

c       ======================================OVERLAP DU CYCLE SUIVANT==========================================================
               if (l .ne. lstcyc) then    
                  
                  do j=1,rescyc(l+1)     !On bride au croisement des cycles (pour le max)
                  
c            LATITUDES SYNTHETIQUES======================================================================================================= 
                     if ((fract(i).gt.x(j,l+1)).and.
     +                   (fract(i).lt.x(j+1,l+1))) then
                        chance=urands2()
                        if (chance .lt. decoupagemax) then              !La chance d'overlap dépend de decoupagemax
                           latsynth(i) = f1(j,l+1)+f3(j,l+1)*gssrands()
                           cycle(i)=cycle(i)+1
                           do k=0,maximum 
c                             on s'assure que la valeur donnée soit dans les limites instaurées:
                              if ((latsynth(i).gt.60.).or.
     +                            (latsynth(i).lt.minlat)) then
                           latsynth(i) = f1(j,l+1)+f3(j,l+1)*gssrands() 
                              else
                                 exit
                              endif
                           enddo
                           f1synth(i)=f1(j,l+1)
                           f3synth(i)=f3(j,l+1)
                           
c            AIRES SYNTHETIQUES=======================================================================================================                 
                           loga(i) = loga0(l+1)+logasig(l+1)*gssrands()
                           do k=0,maximum
c                             on s'assure que la valeur donnée soit dans les limites instaurées:
                              if ((10**loga(i).gt.maxaire).or.
     +                            (10**loga(i).lt.minaire)) then
                           loga(i) = loga0(l+1)+logasig(l+1)*gssrands()
                              else
                                 exit
                              endif
                           enddo
                           airesynth(i)=10**loga(i)
                           
                        endif
                     endif
                  enddo             !pour les j
               endif
               
c              if (l .gt. ncycles-3) then                               ! Donne des valeurs discretes aux cycle 9 ,10 et 11.
c                 latsynth(i)=int(latsynth(i))                          ! Arrondi a la hausse
c              endif
               
               chanceneg=urands2()                                       ! Une chance sur deux que la latitude change de signe
               if (chanceneg .lt. 0.5) then
                  latsynth(i)=-1.*latsynth(i)
               endif
               
               longsynth(i)=-90.+180.*urands2()                          !Assignation de la longitude
               
            endif                                                       !pour balayer les i sans revenir dessus
            
         enddo                                                          !pour les i
         
      enddo                                                             !pour les l
      
c=============================================================================================
c==== Construction d'une Bipolar Magnetic Emergence (BMR) pour chaque Active Region (AR) =====
      
      loga021  = 1.75                                                   !=loga0  (21+dmincyc) Log(microHem)
      logasig21= 0.70                                                   !=logasig(21+dmincyc) Log(microHem)
      logphi0  = 21.3                                                   !Log(Maxwell)
      logphisig= 0.5                                                    !Log(Maxwell)
      tiltjoy  = 0.5
      tiltdev0 = 8.5                                                    !Degrees
      tiltdev1 = 12.                                                    !Degrees
      tiltdev2 = 0.8                                                    !Log(Maxwell)
      deltalin0= 0.46                                                   !Log(degrees)
      deltalin1= 0.42                                                   !Log(degrees)/Log(Maxwell)
      deltadev = 0.16                                                   !Log(degrees)
      
      minarea=1.e20
      maxarea=0.
      minb0  =1.e20
      maxb0  =0.
      minflux=1.e20
      maxflux=0.
      do i=1,nar
         
         tht = 90.-latsynth(i)
         phi = 360.*urands2()                                            ! Attention, longsynth sont données en coordonnées Stonyhurst, entre -90 et 90. On veut coordonnées Carrington...
         
c------- Average radial surface magnetic field inside the active region
         b0 = (10**logphi0)/(10**loga021*1.e-6*2*pi*rsun*rsun)*
     +              (airesynth(i)/10**loga021)**(logphisig/logasig21-1.)
         
c------- Corresponding magnetic flux
         flux(i) = b0 * airesynth(i)*1.e-6*2.*pi*(rsun*rsun/1.e21)
         logflux = LOG10(flux(i))
c        airecm2 = airesynth(i)*1.e-6*2.*pi*(rsun*rsun/1.e21)
c        b0ajust = b0 + ajust*airecm2 + ajust2/airecm2
c        IF (b0ajust.GT.b0ajustmax) b0ajustmax=b0ajust
c        IF (b0ajust.LT.b0ajustmin) b0ajustmin=b0ajust         
c        flux(i) = flux0 + b0ajust*airecm2
         
c------- Signe de la tache ouest typique pour un cycle "positif" (i.e. toroidal prograde)
         IF (tht.LE.90.) THEN
            signe(i)=-1
         ELSE
            signe(i)= 1
         ENDIF
         
c------- Magnetic bipole tilt angle relative to east-west direction
         sigtilt = tiltdev0 + tiltdev1*EXP(-logflux/tiltdev2)
         tilt    = tiltjoy*latsynth(i) + sigtilt*gssrands()
c        sigtilt = tilt0 + tilt1*EXP(-flux(i)/tiltflux)
c        tilt    = 180./pi*ASIN(0.5*COS(pi/180.*tht)) + sigtilt*gssrands()
         IF (tilt.GT.180.) THEN
            tilt=tilt-360.
         ENDIF
         IF (tilt.LT.-180.) THEN
            tilt=tilt+360.
         ENDIF
         IF (tilt.GT.90.) THEN
            tilt=tilt-180.
            signe(i)=-signe(i)
         ENDIF
         IF (tilt.LT.-90.)THEN
            tilt=tilt+180.
            signe(i)=-signe(i)
         ENDIF
c        kappa=1.                                                       !Constante de proportionalité
c        Bzero=1.                                                       !Champ magnétique initial
c        flux=1.                                                        !Flux magnétique
c        tilt=kappa*sin(latsynth(i))*flux**(1./4.)*Bzero**(-5./4.)      !tilt est une variable dependante de plusieurs facteurs...
         
c------- Magnetic bipole angular separation
         delta  = deltalin0 + deltalin1*logflux + deltadev*gssrands()
         IF (delta.LT.-0.5) delta=-0.5d0
         IF (delta.GT. 1.5) delta= 1.5d0
         delta  = 10**delta
c        expdelta = delta1 + sigmadelta*gssrands()
c        delta= 10**(expdelta) * (flux(i)*1.e21)**delta2        
c        delta=2.                                                       !Arnaud: une correction peut etrre apportee pour eloigner les taches les une des autres...
c        rayon=sqrt(aire_dbl(k)*(4*pi)/(10e6))*delta
         
         dtht = delta*SIN(pi/180.*tilt)
         IF (ABS(tilt).EQ.90.) THEN
            dphi=0.
         ELSE
            dphi = delta*COS(pi/180.*tilt)/SIN(pi/180.*tht)
         ENDIF
         IF (dphi.LT.0.) THEN
            WRITE(*,*) 'tht,tilt,delta,dphi=',tht,tilt,delta,dphi
            STOP '! Probleme: dphi negatif !'
         ENDIF
         
         lat1(i)=tht+dtht/2.
         lat2(i)=tht-dtht/2.
         lon1(i)=phi+dphi/2.
         IF (lon1(i).GT.360.) lon1(i)=lon1(i)-360.
         lon2(i)=phi-dphi/2.
         IF (lon2(i).LT.0.)   lon2(i)=lon2(i)+360.
         
         IF(airesynth(i).LT.minarea) minarea=airesynth(i)
         IF(airesynth(i).GT.maxarea) maxarea=airesynth(i)
         IF(b0.LT.minb0) minb0=b0
         IF(b0.GT.maxb0) maxb0=b0
         IF(flux(i).LT.minflux) minflux=flux(i)
         IF(flux(i).GT.maxflux) maxflux=flux(i)
      ENDDO
      
      IF (ivrb.EQ.1) WRITE(*,*) 'min(area),max(area)=',minarea,maxarea
      IF (ivrb.EQ.1) WRITE(*,*) 'min(b0),max(b0)=',minb0,maxb0
      IF (ivrb.EQ.1) WRITE(*,*) 'min(flux),max(flux)=',minflux,maxflux
      
c======================================================================================================================================
c Determination de la date (annee, mois, jour...)================================================================
      
      open(98,file=filename)
      
      IF (mjdortime.EQ.' MJD') THEN
         WRITE(98,'(a46,a7,i7)')
     +                                      filename(1:strlen(filename))
     +                                                   ,', seed=',seed
         WRITE(98,'(a1)') ' '
         WRITE(98,'(a46,a48)')
     +                  '   MBR# Cycle Sign      Date & UT    MJD(days)'
     +               ,' Flux(10^21Mx) Sign1 Colat1  Long1 Colat2  Long2'
      ELSEIF (mjdortime.EQ.'Time') THEN
         WRITE(98,'(a7,i2,a1,i2,a5,14x,a15,a7,i7)') 'Cycles ',
     +   fstcyc-dmincyc,'-',lstcyc-dmincyc,' x  1','Duration (days)'
     +                                                   ,', seed=',seed
         WRITE(98,'(f46.4)') dur
         WRITE(98,'(a46,a48)')
     +                  '   MBR# Cycle Sign      Date & UT   Time(days)'
     +               ,' Flux(10^21Mx) Sign1 Colat1  Long1 Colat2  Long2'
      ELSE
         STOP 'STOP: Wrong choice of mjdortime !'
      ENDIF
      
      i=0
      DO l=fstcyc,lstcyc
         cyclel=l-dmincyc
         
         DO j=1,nbmr(l)
            i=i+1
            
            pourcent=1.*i/nar*100.
            IF (ivrb.EQ.1 .AND. mod(pourcent,5.).lt.1.e-3) WRITE(*,*) 
     +                                                      pourcent,'%' 
            
            IF (cycle(i)/2.EQ.cycle(i)/2.) THEN
               signcyclei=1
            ELSE
               signcyclei=-1
            ENDIF
            signe(i)=signcyclei*signe(i)
            
            IF (month(i).GE.10) THEN
               WRITE(anmois,'(i4,   i2)') year(i),    month(i)
            ELSE
               WRITE(anmois,'(i4,a1,i1)') year(i),'0',month(i)
            ENDIF
            IF (day  (i).GE.10) THEN
               WRITE(anmoisjour,'(a6,   i2)') anmois,    day(i)
            ELSE
               WRITE(anmoisjour,'(a6,a1,i1)') anmois,'0',day(i)
            ENDIF
            heure=INT(tod(i)*24)
            IF (heure   .GE.10) THEN
               WRITE(anmoisjourhr,'(a8,   i2)') anmoisjour,    heure
            ELSE
               WRITE(anmoisjourhr,'(a8,a1,i1)') anmoisjour,'0',heure
            ENDIF
            minute=INT((tod(i)*24-heure)*60)
            IF (minute  .GE.10) THEN
            WRITE(anmoisjourhrmin,'(a10,   i2)') anmoisjourhr,    minute
            ELSE
            WRITE(anmoisjourhrmin,'(a10,a1,i1)') anmoisjourhr,'0',minute
            ENDIF
            
           WRITE(98,'(i7,i6,i5,a15,f13.4,f14.1,i6,f7.1,f7.1,f7.1,f7.1)')
     +                 i,cycle(i),signcyclei,anmoisjourhrmin,timeday(i),
     +                  flux(i),signe(i),lat1(i),lon1(i),lat2(i),lon2(i)
            
         ENDDO
	      
      ENDDO
      
      CLOSE(98)
      
      RETURN
      END SUBROUTINE
c     END
      
c***********************************************************************
cc    INCLUDE 'gauss.f'      
c     INCLUDE 'subs.randsom.f'
c     INCLUDE 'fct.strlen.f'
c***********************************************************************

