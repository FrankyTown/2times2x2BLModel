
c Arrays and variables needed by GLOBAL FEM machinery
c---------------------------------------------------------------------------
      real           ck,anm1,fnm1,fn,f0,delu,u0,keffdu,keffu0
      common/iveqs/  ck(mfb,nm),anm1(nm),fnm1(nm),fn(nm),f0(nm),
     +               delu(nm),u0(nm),keffdu(nm),keffu0(nm)
      real           k,f,a
      common/syseqs/ k(mfb,nm),f(nm),a(nm)
      integer        iuebc,numebc,iia
      real           uebc
      common/essbc/  iuebc(nbc),uebc(nbc),numebc,iia(nndx)

