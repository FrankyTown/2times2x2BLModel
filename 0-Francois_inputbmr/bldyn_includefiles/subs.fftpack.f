
c                       FFTPACK
c 
c * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
c 
c                   version 4  april 1985
c 
c      a package of fortran subprograms for the fast fourier
c       transform of periodic and other symmetric sequences
c 
c                          by
c 
c                   paul n swarztrauber
c 
c   national center for atmospheric research  boulder,colorado 80307
c 
c    which is sponsored by the national science foundation
c 
c * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
c 
c 
c this package consists of programs which perform fast fourier
c transforms for both complex and real periodic sequences and
c certain other symmetric sequences that are listed below.
c 
c 1.   rffti     initialize  rfftf and rfftb
c 2.   rfftf     forward transform of a real periodic sequence
c 3.   rfftb     backward transform of a real coefficient array
c 
c 4.   ezffti    initialize ezfftf and ezfftb
c 5.   ezfftf    a simplified real periodic forward transform
c 6.   ezfftb    a simplified real periodic backward transform
c 
c 7.   sinti     initialize sint
c 8.   sint      sine transform of a real odd sequence
c 
c 9.   costi     initialize cost
c 10.  cost      cosine transform of a real even sequence
c 
c 11.  sinqi     initialize sinqf and sinqb
c 12.  sinqf     forward sine transform with odd wave numbers
c 13.  sinqb     unnormalized inverse of sinqf
c 
c 14.  cosqi     initialize cosqf and cosqb
c 15.  cosqf     forward cosine transform with odd wave numbers
c 16.  cosqb     unnormalized inverse of cosqf
c 
c 17.  cffti     initialize cfftf and cfftb
c 18.  cfftf     forward transform of a complex periodic sequence
c 19.  cfftb     unnormalized inverse of cfftf
c 
c 
c **********************************************************************
c 
c subroutine rffti(n,wsave)
c 
c **********************************************************************
c 
c subroutine rffti initializes the array wsave which is used in
c both rfftf and rfftb. the prime factorization of n together with
c a tabulation of the trigonometric functions are computed and
c stored in wsave.
c 
c input parameter
c 
c n       the length of the sequence to be transformed.
c 
c output parameter
c 
c wsave   a work array which must be dimensioned at least 2*n+15.
c         the same work array can be used for both rfftf and rfftb
c         as long as n remains unchanged. different wsave arrays
c         are required for different values of n. the contents of
c         wsave must not be changed between calls of rfftf or rfftb.
c 
c **********************************************************************
c 
c subroutine rfftf(n,r,wsave)
c 
c **********************************************************************
c 
c subroutine rfftf computes the fourier coefficients of a real
c perodic sequence (fourier analysis). the transform is defined
c below at output parameter r.
c 
c input parameters
c 
c n       the length of the array r to be transformed.  the method
c         is most efficient when n is a product of small primes.
c         n may change so long as different work arrays are provided
c 
c r       a real array of length n which contains the sequence
c         to be transformed
c 
c wsave   a work array which must be dimensioned at least 2*n+15.
c         in the program that calls rfftf. the wsave array must be
c         initialized by calling subroutine rffti(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c         the same wsave array can be used by rfftf and rfftb.
c 
c 
c output parameters
c 
c r       r(1) = the sum from i=1 to i=n of r(i)
c 
c         if n is even set l =n/2   , if n is odd set l = (n+1)/2
c 
c           then for k = 2,...,l
c 
c              r(2*k-2) = the sum from i = 1 to i = n of
c 
c                   r(i)*cos((k-1)*(i-1)*2*pi/n)
c 
c              r(2*k-1) = the sum from i = 1 to i = n of
c 
c                  -r(i)*sin((k-1)*(i-1)*2*pi/n)
c 
c         if n is even
c 
c              r(n) = the sum from i = 1 to i = n of
c 
c                   (-1)**(i-1)*r(i)
c 
c  *****  note
c              this transform is unnormalized since a call of rfftf
c              followed by a call of rfftb will multiply the input
c              sequence by n.
c 
c wsave   contains results which must not be destroyed between
c         calls of rfftf or rfftb.
c 
c 
c **********************************************************************
c 
c subroutine rfftb(n,r,wsave)
c 
c **********************************************************************
c 
c subroutine rfftb computes the real perodic sequence from its
c fourier coefficients (fourier synthesis). the transform is defined
c below at output parameter r.
c 
c input parameters
c 
c n       the length of the array r to be transformed.  the method
c         is most efficient when n is a product of small primes.
c         n may change so long as different work arrays are provided
c 
c r       a real array of length n which contains the sequence
c         to be transformed
c 
c wsave   a work array which must be dimensioned at least 2*n+15.
c         in the program that calls rfftb. the wsave array must be
c         initialized by calling subroutine rffti(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c         the same wsave array can be used by rfftf and rfftb.
c 
c 
c output parameters
c 
c r       for n even and for i = 1,...,n
c 
c              r(i) = r(1)+(-1)**(i-1)*r(n)
c 
c                   plus the sum from k=2 to k=n/2 of
c 
c                    2.*r(2*k-2)*cos((k-1)*(i-1)*2*pi/n)
c 
c                   -2.*r(2*k-1)*sin((k-1)*(i-1)*2*pi/n)
c 
c         for n odd and for i = 1,...,n
c 
c              r(i) = r(1) plus the sum from k=2 to k=(n+1)/2 of
c 
c                   2.*r(2*k-2)*cos((k-1)*(i-1)*2*pi/n)
c 
c                  -2.*r(2*k-1)*sin((k-1)*(i-1)*2*pi/n)
c 
c  *****  note
c              this transform is unnormalized since a call of rfftf
c              followed by a call of rfftb will multiply the input
c              sequence by n.
c 
c wsave   contains results which must not be destroyed between
c         calls of rfftb or rfftf.
c 
c 
c **********************************************************************
c 
c subroutine ezffti(n,wsave)
c 
c **********************************************************************
c 
c subroutine ezffti initializes the array wsave which is used in
c both ezfftf and ezfftb. the prime factorization of n together with
c a tabulation of the trigonometric functions are computed and
c stored in wsave.
c 
c input parameter
c 
c n       the length of the sequence to be transformed.
c 
c output parameter
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         the same work array can be used for both ezfftf and ezfftb
c         as long as n remains unchanged. different wsave arrays
c         are required for different values of n.
c 
c 
c **********************************************************************
c 
c subroutine ezfftf(n,r,azero,a,b,wsave)
c 
c **********************************************************************
c 
c subroutine ezfftf computes the fourier coefficients of a real
c perodic sequence (fourier analysis). the transform is defined
c below at output parameters azero,a and b. ezfftf is a simplified
c but slower version of rfftf.
c 
c input parameters
c 
c n       the length of the array r to be transformed.  the method
c         is must efficient when n is the product of small primes.
c 
c r       a real array of length n which contains the sequence
c         to be transformed. r is not destroyed.
c 
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         in the program that calls ezfftf. the wsave array must be
c         initialized by calling subroutine ezffti(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c         the same wsave array can be used by ezfftf and ezfftb.
c 
c output parameters
c 
c azero   the sum from i=1 to i=n of r(i)/n
c 
c a,b     for n even b(n/2)=0. and a(n/2) is the sum from i=1 to
c         i=n of (-1)**(i-1)*r(i)/n
c 
c         for n even define kmax=n/2-1
c         for n odd  define kmax=(n-1)/2
c 
c         then for  k=1,...,kmax
c 
c              a(k) equals the sum from i=1 to i=n of
c 
c                   2./n*r(i)*cos(k*(i-1)*2*pi/n)
c 
c              b(k) equals the sum from i=1 to i=n of
c 
c                   2./n*r(i)*sin(k*(i-1)*2*pi/n)
c 
c 
c **********************************************************************
c 
c subroutine ezfftb(n,r,azero,a,b,wsave)
c 
c **********************************************************************
c 
c subroutine ezfftb computes a real perodic sequence from its
c fourier coefficients (fourier synthesis). the transform is
c defined below at output parameter r. ezfftb is a simplified
c but slower version of rfftb.
c 
c input parameters
c 
c n       the length of the output array r.  the method is most
c         efficient when n is the product of small primes.
c 
c azero   the constant fourier coefficient
c 
c a,b     arrays which contain the remaining fourier coefficients
c         these arrays are not destroyed.
c 
c         the length of these arrays depends on whether n is even or
c         odd.
c 
c         if n is even n/2    locations are required
c         if n is odd (n-1)/2 locations are required
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         in the program that calls ezfftb. the wsave array must be
c         initialized by calling subroutine ezffti(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c         the same wsave array can be used by ezfftf and ezfftb.
c 
c 
c output parameters
c 
c r       if n is even define kmax=n/2
c         if n is odd  define kmax=(n-1)/2
c 
c         then for i=1,...,n
c 
c              r(i)=azero plus the sum from k=1 to k=kmax of
c 
c              a(k)*cos(k*(i-1)*2*pi/n)+b(k)*sin(k*(i-1)*2*pi/n)
c 
c ************************* complex notation **************************
c 
c         for j=1,...,n
c 
c         r(j) equals the sum from k=-kmax to k=kmax of
c 
c              c(k)*exp(i*k*(j-1)*2*pi/n)
c 
c         where
c 
c              c(k) = .5*cmplx(a(k),-b(k))   for k=1,...,kmax
c 
c              c(-k) = conjg(c(k))
c 
c              c(0) = azero
c 
c                   and i=sqrt(-1)
c 
c ******************* amplitude - phase notation ***********************
c 
c         for i=1,...,n
c 
c         r(i) equals azero plus the sum from k=1 to k=kmax of
c 
c              alpha(k)*cos(k*(i-1)*2*pi/n+beta(k))
c 
c         where
c 
c              alpha(k) = sqrt(a(k)*a(k)+b(k)*b(k))
c 
c              cos(beta(k))=a(k)/alpha(k)
c 
c              sin(beta(k))=-b(k)/alpha(k)
c 
c **********************************************************************
c 
c subroutine sinti(n,wsave)
c 
c **********************************************************************
c 
c subroutine sinti initializes the array wsave which is used in
c subroutine sint. the prime factorization of n together with
c a tabulation of the trigonometric functions are computed and
c stored in wsave.
c 
c input parameter
c 
c n       the length of the sequence to be transformed.  the method
c         is most efficient when n+1 is a product of small primes.
c 
c output parameter
c 
c wsave   a work array with at least int(2.5*n+15) locations.
c         different wsave arrays are required for different values
c         of n. the contents of wsave must not be changed between
c         calls of sint.
c 
c **********************************************************************
c 
c subroutine sint(n,x,wsave)
c 
c **********************************************************************
c 
c subroutine sint computes the discrete fourier sine transform
c of an odd sequence x(i). the transform is defined below at
c output parameter x.
c 
c sint is the unnormalized inverse of itself since a call of sint
c followed by another call of sint will multiply the input sequence
c x by 2*(n+1).
c 
c the array wsave which is used by subroutine sint must be
c initialized by calling subroutine sinti(n,wsave).
c 
c input parameters
c 
c n       the length of the sequence to be transformed.  the method
c         is most efficient when n+1 is the product of small primes.
c 
c x       an array which contains the sequence to be transformed
c 
c 
c wsave   a work array with dimension at least int(2.5*n+15)
c         in the program that calls sint. the wsave array must be
c         initialized by calling subroutine sinti(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c 
c output parameters
c 
c x       for i=1,...,n
c 
c              x(i)= the sum from k=1 to k=n
c 
c                   2*x(k)*sin(k*i*pi/(n+1))
c 
c              a call of sint followed by another call of
c              sint will multiply the sequence x by 2*(n+1).
c              hence sint is the unnormalized inverse
c              of itself.
c 
c wsave   contains initialization calculations which must not be
c         destroyed between calls of sint.
c 
c **********************************************************************
c 
c subroutine costi(n,wsave)
c 
c **********************************************************************
c 
c subroutine costi initializes the array wsave which is used in
c subroutine cost. the prime factorization of n together with
c a tabulation of the trigonometric functions are computed and
c stored in wsave.
c 
c input parameter
c 
c n       the length of the sequence to be transformed.  the method
c         is most efficient when n-1 is a product of small primes.
c 
c output parameter
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         different wsave arrays are required for different values
c         of n. the contents of wsave must not be changed between
c         calls of cost.
c 
c **********************************************************************
c 
c subroutine cost(n,x,wsave)
c 
c **********************************************************************
c 
c subroutine cost computes the discrete fourier cosine transform
c of an even sequence x(i). the transform is defined below at output
c parameter x.
c 
c cost is the unnormalized inverse of itself since a call of cost
c followed by another call of cost will multiply the input sequence
c x by 2*(n-1). the transform is defined below at output parameter x
c 
c the array wsave which is used by subroutine cost must be
c initialized by calling subroutine costi(n,wsave).
c 
c input parameters
c 
c n       the length of the sequence x. n must be greater than 1.
c         the method is most efficient when n-1 is a product of
c         small primes.
c 
c x       an array which contains the sequence to be transformed
c 
c wsave   a work array which must be dimensioned at least 3*n+15
c         in the program that calls cost. the wsave array must be
c         initialized by calling subroutine costi(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c 
c output parameters
c 
c x       for i=1,...,n
c 
c             x(i) = x(1)+(-1)**(i-1)*x(n)
c 
c              + the sum from k=2 to k=n-1
c 
c                  2*x(k)*cos((k-1)*(i-1)*pi/(n-1))
c 
c              a call of cost followed by another call of
c              cost will multiply the sequence x by 2*(n-1)
c              hence cost is the unnormalized inverse
c              of itself.
c 
c wsave   contains initialization calculations which must not be
c         destroyed between calls of cost.
c 
c **********************************************************************
c 
c subroutine sinqi(n,wsave)
c 
c **********************************************************************
c 
c subroutine sinqi initializes the array wsave which is used in
c both sinqf and sinqb. the prime factorization of n together with
c a tabulation of the trigonometric functions are computed and
c stored in wsave.
c 
c input parameter
c 
c n       the length of the sequence to be transformed. the method
c         is most efficient when n is a product of small primes.
c 
c output parameter
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         the same work array can be used for both sinqf and sinqb
c         as long as n remains unchanged. different wsave arrays
c         are required for different values of n. the contents of
c         wsave must not be changed between calls of sinqf or sinqb.
c 
c **********************************************************************
c 
c subroutine sinqf(n,x,wsave)
c 
c **********************************************************************
c 
c subroutine sinqf computes the fast fourier transform of quarter
c wave data. that is , sinqf computes the coefficients in a sine
c series representation with only odd wave numbers. the transform
c is defined below at output parameter x.
c 
c sinqb is the unnormalized inverse of sinqf since a call of sinqf
c followed by a call of sinqb will multiply the input sequence x
c by 4*n.
c 
c the array wsave which is used by subroutine sinqf must be
c initialized by calling subroutine sinqi(n,wsave).
c 
c 
c input parameters
c 
c n       the length of the array x to be transformed.  the method
c         is most efficient when n is a product of small primes.
c 
c x       an array which contains the sequence to be transformed
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         in the program that calls sinqf. the wsave array must be
c         initialized by calling subroutine sinqi(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c 
c output parameters
c 
c x       for i=1,...,n
c 
c              x(i) = (-1)**(i-1)*x(n)
c 
c                 + the sum from k=1 to k=n-1 of
c 
c                 2*x(k)*sin((2*i-1)*k*pi/(2*n))
c 
c              a call of sinqf followed by a call of
c              sinqb will multiply the sequence x by 4*n.
c              therefore sinqb is the unnormalized inverse
c              of sinqf.
c 
c wsave   contains initialization calculations which must not
c         be destroyed between calls of sinqf or sinqb.
c 
c **********************************************************************
c 
c subroutine sinqb(n,x,wsave)
c 
c **********************************************************************
c 
c subroutine sinqb computes the fast fourier transform of quarter
c wave data. that is , sinqb computes a sequence from its
c representation in terms of a sine series with odd wave numbers.
c the transform is defined below at output parameter x.
c 
c sinqf is the unnormalized inverse of sinqb since a call of sinqb
c followed by a call of sinqf will multiply the input sequence x
c by 4*n.
c 
c the array wsave which is used by subroutine sinqb must be
c initialized by calling subroutine sinqi(n,wsave).
c 
c 
c input parameters
c 
c n       the length of the array x to be transformed.  the method
c         is most efficient when n is a product of small primes.
c 
c x       an array which contains the sequence to be transformed
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         in the program that calls sinqb. the wsave array must be
c         initialized by calling subroutine sinqi(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c 
c output parameters
c 
c x       for i=1,...,n
c 
c              x(i)= the sum from k=1 to k=n of
c 
c                4*x(k)*sin((2k-1)*i*pi/(2*n))
c 
c              a call of sinqb followed by a call of
c              sinqf will multiply the sequence x by 4*n.
c              therefore sinqf is the unnormalized inverse
c              of sinqb.
c 
c wsave   contains initialization calculations which must not
c         be destroyed between calls of sinqb or sinqf.
c 
c **********************************************************************
c 
c subroutine cosqi(n,wsave)
c 
c **********************************************************************
c 
c subroutine cosqi initializes the array wsave which is used in
c both cosqf and cosqb. the prime factorization of n together with
c a tabulation of the trigonometric functions are computed and
c stored in wsave.
c 
c input parameter
c 
c n       the length of the array to be transformed.  the method
c         is most efficient when n is a product of small primes.
c 
c output parameter
c 
c wsave   a work array which must be dimensioned at least 3*n+15.
c         the same work array can be used for both cosqf and cosqb
c         as long as n remains unchanged. different wsave arrays
c         are required for different values of n. the contents of
c         wsave must not be changed between calls of cosqf or cosqb.
c 
c **********************************************************************
c 
c subroutine cosqf(n,x,wsave)
c 
c **********************************************************************
c 
c subroutine cosqf computes the fast fourier transform of quarter
c wave data. that is , cosqf computes the coefficients in a cosine
c series representation with only odd wave numbers. the transform
c is defined below at output parameter x
c 
c cosqf is the unnormalized inverse of cosqb since a call of cosqf
c followed by a call of cosqb will multiply the input sequence x
c by 4*n.
c 
c the array wsave which is used by subroutine cosqf must be
c initialized by calling subroutine cosqi(n,wsave).
c 
c 
c input parameters
c 
c n       the length of the array x to be transformed.  the method
c         is most efficient when n is a product of small primes.
c 
c x       an array which contains the sequence to be transformed
c 
c wsave   a work array which must be dimensioned at least 3*n+15
c         in the program that calls cosqf. the wsave array must be
c         initialized by calling subroutine cosqi(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c 
c output parameters
c 
c x       for i=1,...,n
c 
c              x(i) = x(1) plus the sum from k=2 to k=n of
c 
c                 2*x(k)*cos((2*i-1)*(k-1)*pi/(2*n))
c 
c              a call of cosqf followed by a call of
c              cosqb will multiply the sequence x by 4*n.
c              therefore cosqb is the unnormalized inverse
c              of cosqf.
c 
c wsave   contains initialization calculations which must not
c         be destroyed between calls of cosqf or cosqb.
c 
c **********************************************************************
c 
c subroutine cosqb(n,x,wsave)
c 
c **********************************************************************
c 
c subroutine cosqb computes the fast fourier transform of quarter
c wave data. that is , cosqb computes a sequence from its
c representation in terms of a cosine series with odd wave numbers.
c the transform is defined below at output parameter x.
c 
c cosqb is the unnormalized inverse of cosqf since a call of cosqb
c followed by a call of cosqf will multiply the input sequence x
c by 4*n.
c 
c the array wsave which is used by subroutine cosqb must be
c initialized by calling subroutine cosqi(n,wsave).
c 
c 
c input parameters
c 
c n       the length of the array x to be transformed.  the method
c         is most efficient when n is a product of small primes.
c 
c x       an array which contains the sequence to be transformed
c 
c wsave   a work array that must be dimensioned at least 3*n+15
c         in the program that calls cosqb. the wsave array must be
c         initialized by calling subroutine cosqi(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c 
c output parameters
c 
c x       for i=1,...,n
c 
c              x(i)= the sum from k=1 to k=n of
c 
c                4*x(k)*cos((2*k-1)*(i-1)*pi/(2*n))
c 
c              a call of cosqb followed by a call of
c              cosqf will multiply the sequence x by 4*n.
c              therefore cosqf is the unnormalized inverse
c              of cosqb.
c 
c wsave   contains initialization calculations which must not
c         be destroyed between calls of cosqb or cosqf.
c 
c **********************************************************************
c 
c subroutine cffti(n,wsave)
c 
c **********************************************************************
c 
c subroutine cffti initializes the array wsave which is used in
c both cfftf and cfftb. the prime factorization of n together with
c a tabulation of the trigonometric functions are computed and
c stored in wsave.
c 
c input parameter
c 
c n       the length of the sequence to be transformed
c 
c output parameter
c 
c wsave   a work array which must be dimensioned at least 4*n+15
c         the same work array can be used for both cfftf and cfftb
c         as long as n remains unchanged. different wsave arrays
c         are required for different values of n. the contents of
c         wsave must not be changed between calls of cfftf or cfftb.
c 
      SUBROUTINE CFFTI (N,WSAVE)
      DIMENSION       WSAVE(1)
      IF (N .EQ. 1) RETURN
      IW1 = N+N+1
      IW2 = IW1+N+N
      CALL CFFTI1 (N,WSAVE(IW1),WSAVE(IW2))
      RETURN
      END
c **********************************************************************
      SUBROUTINE CFFTI1 (N,WA,IFAC)
      DIMENSION       WA(1)      ,IFAC(1)    ,NTRYH(4)
      DATA NTRYH(1),NTRYH(2),NTRYH(3),NTRYH(4)/3,4,2,5/
      NL = N
      NF = 0
      J = 0
  101 J = J+1
      IF (J-4) 102,102,103
  102 NTRY = NTRYH(J)
      GO TO 104
  103 NTRY = NTRY+2
  104 NQ = NL/NTRY
      NR = NL-NTRY*NQ
      IF (NR) 101,105,101
  105 NF = NF+1
      IFAC(NF+2) = NTRY
      NL = NQ
      IF (NTRY .NE. 2) GO TO 107
      IF (NF .EQ. 1) GO TO 107
      DO 106 I=2,NF
         IB = NF-I+2
         IFAC(IB+2) = IFAC(IB+1)
  106 CONTINUE
      IFAC(3) = 2
  107 IF (NL .NE. 1) GO TO 104
      IFAC(1) = N
      IFAC(2) = NF
      TPI = 6.28318530717959
      ARGH = TPI/FLOAT(N)
      I = 2
      L1 = 1
      DO 110 K1=1,NF
         IP = IFAC(K1+2)
         LD = 0
         L2 = L1*IP
         IDO = N/L2
         IDOT = IDO+IDO+2
         IPM = IP-1
         DO 109 J=1,IPM
            I1 = I
            WA(I-1) = 1.
            WA(I) = 0.
            LD = LD+L1
            FI = 0.
            ARGLD = FLOAT(LD)*ARGH
            DO 108 II=4,IDOT,2
               I = I+2
               FI = FI+1.
               ARG = FI*ARGLD
               WA(I-1) = COS(ARG)
               WA(I) = SIN(ARG)
  108       CONTINUE
            IF (IP .LE. 5) GO TO 109
            WA(I1-1) = WA(I-1)
            WA(I1) = WA(I)
  109    CONTINUE
         L1 = L2
  110 CONTINUE
      RETURN
      END
c **********************************************************************
c 
c subroutine cfftf(n,c,wsave)
c 
c **********************************************************************
c 
c subroutine cfftf computes the forward complex discrete fourier
c transform (the fourier analysis). equivalently , cfftf computes
c the fourier coefficients of a complex periodic sequence.
c the transform is defined below at output parameter c.
c 
c the transform is not normalized. to obtain a normalized transform
c the output must be divided by n. otherwise a call of cfftf
c followed by a call of cfftb will multiply the sequence by n.
c 
c the array wsave which is used by subroutine cfftf must be
c initialized by calling subroutine cffti(n,wsave).
c 
c input parameters
c 
c 
c n      the length of the complex sequence c. the method is
c        more efficient when n is the product of small primes. n
c 
c c      a complex array of length n which contains the sequence
c 
c wsave   a real work array which must be dimensioned at least 4n+15
c         in the program that calls cfftf. the wsave array must be
c         initialized by calling subroutine cffti(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c         the same wsave array can be used by cfftf and cfftb.
c 
c output parameters
c 
c c      for j=1,...,n
c 
c            c(j)=the sum from k=1,...,n of
c 
c                  c(k)*exp(-i*(j-1)*(k-1)*2*pi/n)
c 
c                        where i=sqrt(-1)
c 
c wsave   contains initialization calculations which must not be
c         destroyed between calls of subroutine cfftf or cfftb
c 
      SUBROUTINE CFFTF (N,C,WSAVE)
      DIMENSION       C(1)       ,WSAVE(1)
      IF (N .EQ. 1) RETURN
      IW1 = N+N+1
      IW2 = IW1+N+N
      CALL CFFTF1 (N,C,WSAVE,WSAVE(IW1),WSAVE(IW2))
      RETURN
      END
c **********************************************************************
      SUBROUTINE CFFTF1 (N,C,CH,WA,IFAC)
      DIMENSION       CH(1)      ,C(1)       ,WA(1)      ,IFAC(1)
      NF = IFAC(2)
      NA = 0
      L1 = 1
      IW = 1
      DO 116 K1=1,NF
         IP = IFAC(K1+2)
         L2 = IP*L1
         IDO = N/L2
         IDOT = IDO+IDO
         IDL1 = IDOT*L1
         IF (IP .NE. 4) GO TO 103
         IX2 = IW+IDOT
         IX3 = IX2+IDOT
         IF (NA .NE. 0) GO TO 101
         CALL PASSF4 (IDOT,L1,C,CH,WA(IW),WA(IX2),WA(IX3))
         GO TO 102
  101    CALL PASSF4 (IDOT,L1,CH,C,WA(IW),WA(IX2),WA(IX3))
  102    NA = 1-NA
         GO TO 115
  103    IF (IP .NE. 2) GO TO 106
         IF (NA .NE. 0) GO TO 104
         CALL PASSF2 (IDOT,L1,C,CH,WA(IW))
         GO TO 105
  104    CALL PASSF2 (IDOT,L1,CH,C,WA(IW))
  105    NA = 1-NA
         GO TO 115
  106    IF (IP .NE. 3) GO TO 109
         IX2 = IW+IDOT
         IF (NA .NE. 0) GO TO 107
         CALL PASSF3 (IDOT,L1,C,CH,WA(IW),WA(IX2))
         GO TO 108
  107    CALL PASSF3 (IDOT,L1,CH,C,WA(IW),WA(IX2))
  108    NA = 1-NA
         GO TO 115
  109    IF (IP .NE. 5) GO TO 112
         IX2 = IW+IDOT
         IX3 = IX2+IDOT
         IX4 = IX3+IDOT
         IF (NA .NE. 0) GO TO 110
         CALL PASSF5 (IDOT,L1,C,CH,WA(IW),WA(IX2),WA(IX3),WA(IX4))
         GO TO 111
  110    CALL PASSF5 (IDOT,L1,CH,C,WA(IW),WA(IX2),WA(IX3),WA(IX4))
  111    NA = 1-NA
         GO TO 115
  112    IF (NA .NE. 0) GO TO 113
         CALL PASSF (NAC,IDOT,IP,L1,IDL1,C,C,C,CH,CH,WA(IW))
         GO TO 114
  113    CALL PASSF (NAC,IDOT,IP,L1,IDL1,CH,CH,CH,C,C,WA(IW))
  114    IF (NAC .NE. 0) NA = 1-NA
  115    L1 = L2
         IW = IW+(IP-1)*IDOT
  116 CONTINUE
      IF (NA .EQ. 0) RETURN
      N2 = N+N
      DO 117 I=1,N2
         C(I) = CH(I)
  117 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSF (NAC,IDO,IP,L1,IDL1,CC,C1,C2,CH,CH2,WA)
      DIMENSION       CH(IDO,L1,IP)          ,CC(IDO,IP,L1)          ,
     1                C1(IDO,L1,IP)          ,WA(1)      ,C2(IDL1,IP),
     2                CH2(IDL1,IP)
      IDOT = IDO/2
      NT = IP*IDL1
      IPP2 = IP+2
      IPPH = (IP+1)/2
      IDP = IP*IDO
C
      IF (IDO .LT. L1) GO TO 106
      DO 103 J=2,IPPH
         JC = IPP2-J
         DO 102 K=1,L1
            DO 101 I=1,IDO
               CH(I,K,J) = CC(I,J,K)+CC(I,JC,K)
               CH(I,K,JC) = CC(I,J,K)-CC(I,JC,K)
  101       CONTINUE
  102    CONTINUE
  103 CONTINUE
      DO 105 K=1,L1
         DO 104 I=1,IDO
            CH(I,K,1) = CC(I,1,K)
  104    CONTINUE
  105 CONTINUE
      GO TO 112
  106 DO 109 J=2,IPPH
         JC = IPP2-J
         DO 108 I=1,IDO
            DO 107 K=1,L1
               CH(I,K,J) = CC(I,J,K)+CC(I,JC,K)
               CH(I,K,JC) = CC(I,J,K)-CC(I,JC,K)
  107       CONTINUE
  108    CONTINUE
  109 CONTINUE
      DO 111 I=1,IDO
         DO 110 K=1,L1
            CH(I,K,1) = CC(I,1,K)
  110    CONTINUE
  111 CONTINUE
  112 IDL = 2-IDO
      INC = 0
      DO 116 L=2,IPPH
         LC = IPP2-L
         IDL = IDL+IDO
         DO 113 IK=1,IDL1
            C2(IK,L) = CH2(IK,1)+WA(IDL-1)*CH2(IK,2)
            C2(IK,LC) = -WA(IDL)*CH2(IK,IP)
  113    CONTINUE
         IDLJ = IDL
         INC = INC+IDO
         DO 115 J=3,IPPH
            JC = IPP2-J
            IDLJ = IDLJ+INC
            IF (IDLJ .GT. IDP) IDLJ = IDLJ-IDP
            WAR = WA(IDLJ-1)
            WAI = WA(IDLJ)
            DO 114 IK=1,IDL1
               C2(IK,L) = C2(IK,L)+WAR*CH2(IK,J)
               C2(IK,LC) = C2(IK,LC)-WAI*CH2(IK,JC)
  114       CONTINUE
  115    CONTINUE
  116 CONTINUE
      DO 118 J=2,IPPH
         DO 117 IK=1,IDL1
            CH2(IK,1) = CH2(IK,1)+CH2(IK,J)
  117    CONTINUE
  118 CONTINUE
      DO 120 J=2,IPPH
         JC = IPP2-J
         DO 119 IK=2,IDL1,2
            CH2(IK-1,J) = C2(IK-1,J)-C2(IK,JC)
            CH2(IK-1,JC) = C2(IK-1,J)+C2(IK,JC)
            CH2(IK,J) = C2(IK,J)+C2(IK-1,JC)
            CH2(IK,JC) = C2(IK,J)-C2(IK-1,JC)
  119    CONTINUE
  120 CONTINUE
      NAC = 1
      IF (IDO .EQ. 2) RETURN
      NAC = 0
      DO 121 IK=1,IDL1
         C2(IK,1) = CH2(IK,1)
  121 CONTINUE
      DO 123 J=2,IP
         DO 122 K=1,L1
            C1(1,K,J) = CH(1,K,J)
            C1(2,K,J) = CH(2,K,J)
  122    CONTINUE
  123 CONTINUE
      IF (IDOT .GT. L1) GO TO 127
      IDIJ = 0
      DO 126 J=2,IP
         IDIJ = IDIJ+2
         DO 125 I=4,IDO,2
            IDIJ = IDIJ+2
            DO 124 K=1,L1
               C1(I-1,K,J) = WA(IDIJ-1)*CH(I-1,K,J)+WA(IDIJ)*CH(I,K,J)
               C1(I,K,J) = WA(IDIJ-1)*CH(I,K,J)-WA(IDIJ)*CH(I-1,K,J)
  124       CONTINUE
  125    CONTINUE
  126 CONTINUE
      RETURN
  127 IDJ = 2-IDO
      DO 130 J=2,IP
         IDJ = IDJ+IDO
         DO 129 K=1,L1
            IDIJ = IDJ
            DO 128 I=4,IDO,2
               IDIJ = IDIJ+2
               C1(I-1,K,J) = WA(IDIJ-1)*CH(I-1,K,J)+WA(IDIJ)*CH(I,K,J)
               C1(I,K,J) = WA(IDIJ-1)*CH(I,K,J)-WA(IDIJ)*CH(I-1,K,J)
  128       CONTINUE
  129    CONTINUE
  130 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSF2 (IDO,L1,CC,CH,WA1)
      DIMENSION       CC(IDO,2,L1)           ,CH(IDO,L1,2)           ,
     1                WA1(1)
      IF (IDO .GT. 2) GO TO 102
      DO 101 K=1,L1
         CH(1,K,1) = CC(1,1,K)+CC(1,2,K)
         CH(1,K,2) = CC(1,1,K)-CC(1,2,K)
         CH(2,K,1) = CC(2,1,K)+CC(2,2,K)
         CH(2,K,2) = CC(2,1,K)-CC(2,2,K)
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            CH(I-1,K,1) = CC(I-1,1,K)+CC(I-1,2,K)
            TR2 = CC(I-1,1,K)-CC(I-1,2,K)
            CH(I,K,1) = CC(I,1,K)+CC(I,2,K)
            TI2 = CC(I,1,K)-CC(I,2,K)
            CH(I,K,2) = WA1(I-1)*TI2-WA1(I)*TR2
            CH(I-1,K,2) = WA1(I-1)*TR2+WA1(I)*TI2
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSF3 (IDO,L1,CC,CH,WA1,WA2)
      DIMENSION       CC(IDO,3,L1)           ,CH(IDO,L1,3)           ,
     1                WA1(1)     ,WA2(1)
      DATA TAUR,TAUI /-.5,-.866025403784439/
      IF (IDO .NE. 2) GO TO 102
      DO 101 K=1,L1
         TR2 = CC(1,2,K)+CC(1,3,K)
         CR2 = CC(1,1,K)+TAUR*TR2
         CH(1,K,1) = CC(1,1,K)+TR2
         TI2 = CC(2,2,K)+CC(2,3,K)
         CI2 = CC(2,1,K)+TAUR*TI2
         CH(2,K,1) = CC(2,1,K)+TI2
         CR3 = TAUI*(CC(1,2,K)-CC(1,3,K))
         CI3 = TAUI*(CC(2,2,K)-CC(2,3,K))
         CH(1,K,2) = CR2-CI3
         CH(1,K,3) = CR2+CI3
         CH(2,K,2) = CI2+CR3
         CH(2,K,3) = CI2-CR3
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            TR2 = CC(I-1,2,K)+CC(I-1,3,K)
            CR2 = CC(I-1,1,K)+TAUR*TR2
            CH(I-1,K,1) = CC(I-1,1,K)+TR2
            TI2 = CC(I,2,K)+CC(I,3,K)
            CI2 = CC(I,1,K)+TAUR*TI2
            CH(I,K,1) = CC(I,1,K)+TI2
            CR3 = TAUI*(CC(I-1,2,K)-CC(I-1,3,K))
            CI3 = TAUI*(CC(I,2,K)-CC(I,3,K))
            DR2 = CR2-CI3
            DR3 = CR2+CI3
            DI2 = CI2+CR3
            DI3 = CI2-CR3
            CH(I,K,2) = WA1(I-1)*DI2-WA1(I)*DR2
            CH(I-1,K,2) = WA1(I-1)*DR2+WA1(I)*DI2
            CH(I,K,3) = WA2(I-1)*DI3-WA2(I)*DR3
            CH(I-1,K,3) = WA2(I-1)*DR3+WA2(I)*DI3
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSF4 (IDO,L1,CC,CH,WA1,WA2,WA3)
      DIMENSION       CC(IDO,4,L1)           ,CH(IDO,L1,4)           ,
     1                WA1(1)     ,WA2(1)     ,WA3(1)
      IF (IDO .NE. 2) GO TO 102
      DO 101 K=1,L1
         TI1 = CC(2,1,K)-CC(2,3,K)
         TI2 = CC(2,1,K)+CC(2,3,K)
         TR4 = CC(2,2,K)-CC(2,4,K)
         TI3 = CC(2,2,K)+CC(2,4,K)
         TR1 = CC(1,1,K)-CC(1,3,K)
         TR2 = CC(1,1,K)+CC(1,3,K)
         TI4 = CC(1,4,K)-CC(1,2,K)
         TR3 = CC(1,2,K)+CC(1,4,K)
         CH(1,K,1) = TR2+TR3
         CH(1,K,3) = TR2-TR3
         CH(2,K,1) = TI2+TI3
         CH(2,K,3) = TI2-TI3
         CH(1,K,2) = TR1+TR4
         CH(1,K,4) = TR1-TR4
         CH(2,K,2) = TI1+TI4
         CH(2,K,4) = TI1-TI4
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            TI1 = CC(I,1,K)-CC(I,3,K)
            TI2 = CC(I,1,K)+CC(I,3,K)
            TI3 = CC(I,2,K)+CC(I,4,K)
            TR4 = CC(I,2,K)-CC(I,4,K)
            TR1 = CC(I-1,1,K)-CC(I-1,3,K)
            TR2 = CC(I-1,1,K)+CC(I-1,3,K)
            TI4 = CC(I-1,4,K)-CC(I-1,2,K)
            TR3 = CC(I-1,2,K)+CC(I-1,4,K)
            CH(I-1,K,1) = TR2+TR3
            CR3 = TR2-TR3
            CH(I,K,1) = TI2+TI3
            CI3 = TI2-TI3
            CR2 = TR1+TR4
            CR4 = TR1-TR4
            CI2 = TI1+TI4
            CI4 = TI1-TI4
            CH(I-1,K,2) = WA1(I-1)*CR2+WA1(I)*CI2
            CH(I,K,2) = WA1(I-1)*CI2-WA1(I)*CR2
            CH(I-1,K,3) = WA2(I-1)*CR3+WA2(I)*CI3
            CH(I,K,3) = WA2(I-1)*CI3-WA2(I)*CR3
            CH(I-1,K,4) = WA3(I-1)*CR4+WA3(I)*CI4
            CH(I,K,4) = WA3(I-1)*CI4-WA3(I)*CR4
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSF5 (IDO,L1,CC,CH,WA1,WA2,WA3,WA4)
      DIMENSION       CC(IDO,5,L1)           ,CH(IDO,L1,5)           ,
     1                WA1(1)     ,WA2(1)     ,WA3(1)     ,WA4(1)
      DATA TR11,TI11,TR12,TI12 /.309016994374947,-.951056516295154,
     1-.809016994374947,-.587785252292473/
      IF (IDO .NE. 2) GO TO 102
      DO 101 K=1,L1
         TI5 = CC(2,2,K)-CC(2,5,K)
         TI2 = CC(2,2,K)+CC(2,5,K)
         TI4 = CC(2,3,K)-CC(2,4,K)
         TI3 = CC(2,3,K)+CC(2,4,K)
         TR5 = CC(1,2,K)-CC(1,5,K)
         TR2 = CC(1,2,K)+CC(1,5,K)
         TR4 = CC(1,3,K)-CC(1,4,K)
         TR3 = CC(1,3,K)+CC(1,4,K)
         CH(1,K,1) = CC(1,1,K)+TR2+TR3
         CH(2,K,1) = CC(2,1,K)+TI2+TI3
         CR2 = CC(1,1,K)+TR11*TR2+TR12*TR3
         CI2 = CC(2,1,K)+TR11*TI2+TR12*TI3
         CR3 = CC(1,1,K)+TR12*TR2+TR11*TR3
         CI3 = CC(2,1,K)+TR12*TI2+TR11*TI3
         CR5 = TI11*TR5+TI12*TR4
         CI5 = TI11*TI5+TI12*TI4
         CR4 = TI12*TR5-TI11*TR4
         CI4 = TI12*TI5-TI11*TI4
         CH(1,K,2) = CR2-CI5
         CH(1,K,5) = CR2+CI5
         CH(2,K,2) = CI2+CR5
         CH(2,K,3) = CI3+CR4
         CH(1,K,3) = CR3-CI4
         CH(1,K,4) = CR3+CI4
         CH(2,K,4) = CI3-CR4
         CH(2,K,5) = CI2-CR5
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            TI5 = CC(I,2,K)-CC(I,5,K)
            TI2 = CC(I,2,K)+CC(I,5,K)
            TI4 = CC(I,3,K)-CC(I,4,K)
            TI3 = CC(I,3,K)+CC(I,4,K)
            TR5 = CC(I-1,2,K)-CC(I-1,5,K)
            TR2 = CC(I-1,2,K)+CC(I-1,5,K)
            TR4 = CC(I-1,3,K)-CC(I-1,4,K)
            TR3 = CC(I-1,3,K)+CC(I-1,4,K)
            CH(I-1,K,1) = CC(I-1,1,K)+TR2+TR3
            CH(I,K,1) = CC(I,1,K)+TI2+TI3
            CR2 = CC(I-1,1,K)+TR11*TR2+TR12*TR3
            CI2 = CC(I,1,K)+TR11*TI2+TR12*TI3
            CR3 = CC(I-1,1,K)+TR12*TR2+TR11*TR3
            CI3 = CC(I,1,K)+TR12*TI2+TR11*TI3
            CR5 = TI11*TR5+TI12*TR4
            CI5 = TI11*TI5+TI12*TI4
            CR4 = TI12*TR5-TI11*TR4
            CI4 = TI12*TI5-TI11*TI4
            DR3 = CR3-CI4
            DR4 = CR3+CI4
            DI3 = CI3+CR4
            DI4 = CI3-CR4
            DR5 = CR2+CI5
            DR2 = CR2-CI5
            DI5 = CI2-CR5
            DI2 = CI2+CR5
            CH(I-1,K,2) = WA1(I-1)*DR2+WA1(I)*DI2
            CH(I,K,2) = WA1(I-1)*DI2-WA1(I)*DR2
            CH(I-1,K,3) = WA2(I-1)*DR3+WA2(I)*DI3
            CH(I,K,3) = WA2(I-1)*DI3-WA2(I)*DR3
            CH(I-1,K,4) = WA3(I-1)*DR4+WA3(I)*DI4
            CH(I,K,4) = WA3(I-1)*DI4-WA3(I)*DR4
            CH(I-1,K,5) = WA4(I-1)*DR5+WA4(I)*DI5
            CH(I,K,5) = WA4(I-1)*DI5-WA4(I)*DR5
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c **********************************************************************
c 
c subroutine cfftb(n,c,wsave)
c 
c **********************************************************************
c 
c subroutine cfftb computes the backward complex discrete fourier
c transform (the fourier synthesis). equivalently , cfftb computes
c a complex periodic sequence from its fourier coefficients.
c the transform is defined below at output parameter c.
c 
c a call of cfftf followed by a call of cfftb will multiply the
c sequence by n.
c 
c the array wsave which is used by subroutine cfftb must be
c initialized by calling subroutine cffti(n,wsave).
c 
c input parameters
c 
c 
c n      the length of the complex sequence c. the method is
c        more efficient when n is the product of small primes.
c 
c c      a complex array of length n which contains the sequence
c 
c wsave   a real work array which must be dimensioned at least 4n+15
c         in the program that calls cfftb. the wsave array must be
c         initialized by calling subroutine cffti(n,wsave) and a
c         different wsave array must be used for each different
c         value of n. this initialization does not have to be
c         repeated so long as n remains unchanged thus subsequent
c         transforms can be obtained faster than the first.
c         the same wsave array can be used by cfftf and cfftb.
c 
c output parameters
c 
c c      for j=1,...,n
c 
c            c(j)=the sum from k=1,...,n of
c 
c                  c(k)*exp(i*(j-1)*(k-1)*2*pi/n)
c 
c                        where i=sqrt(-1)
c 
c wsave   contains initialization calculations which must not be
c         destroyed between calls of subroutine cfftf or cfftb
c 
      SUBROUTINE CFFTB (N,C,WSAVE)
      DIMENSION       C(1)       ,WSAVE(1)
      IF (N .EQ. 1) RETURN
      IW1 = N+N+1
      IW2 = IW1+N+N
      CALL CFFTB1 (N,C,WSAVE,WSAVE(IW1),WSAVE(IW2))
      RETURN
      END
c **********************************************************************
      SUBROUTINE CFFTB1 (N,C,CH,WA,IFAC)
      DIMENSION       CH(1)      ,C(1)       ,WA(1)      ,IFAC(1)
      NF = IFAC(2)
      NA = 0
      L1 = 1
      IW = 1
      DO 116 K1=1,NF
         IP = IFAC(K1+2)
         L2 = IP*L1
         IDO = N/L2
         IDOT = IDO+IDO
         IDL1 = IDOT*L1
         IF (IP .NE. 4) GO TO 103
         IX2 = IW+IDOT
         IX3 = IX2+IDOT
         IF (NA .NE. 0) GO TO 101
         CALL PASSB4 (IDOT,L1,C,CH,WA(IW),WA(IX2),WA(IX3))
         GO TO 102
  101    CALL PASSB4 (IDOT,L1,CH,C,WA(IW),WA(IX2),WA(IX3))
  102    NA = 1-NA
         GO TO 115
  103    IF (IP .NE. 2) GO TO 106
         IF (NA .NE. 0) GO TO 104
         CALL PASSB2 (IDOT,L1,C,CH,WA(IW))
         GO TO 105
  104    CALL PASSB2 (IDOT,L1,CH,C,WA(IW))
  105    NA = 1-NA
         GO TO 115
  106    IF (IP .NE. 3) GO TO 109
         IX2 = IW+IDOT
         IF (NA .NE. 0) GO TO 107
         CALL PASSB3 (IDOT,L1,C,CH,WA(IW),WA(IX2))
         GO TO 108
  107    CALL PASSB3 (IDOT,L1,CH,C,WA(IW),WA(IX2))
  108    NA = 1-NA
         GO TO 115
  109    IF (IP .NE. 5) GO TO 112
         IX2 = IW+IDOT
         IX3 = IX2+IDOT
         IX4 = IX3+IDOT
         IF (NA .NE. 0) GO TO 110
         CALL PASSB5 (IDOT,L1,C,CH,WA(IW),WA(IX2),WA(IX3),WA(IX4))
         GO TO 111
  110    CALL PASSB5 (IDOT,L1,CH,C,WA(IW),WA(IX2),WA(IX3),WA(IX4))
  111    NA = 1-NA
         GO TO 115
  112    IF (NA .NE. 0) GO TO 113
         CALL PASSB (NAC,IDOT,IP,L1,IDL1,C,C,C,CH,CH,WA(IW))
         GO TO 114
  113    CALL PASSB (NAC,IDOT,IP,L1,IDL1,CH,CH,CH,C,C,WA(IW))
  114    IF (NAC .NE. 0) NA = 1-NA
  115    L1 = L2
         IW = IW+(IP-1)*IDOT
  116 CONTINUE
      IF (NA .EQ. 0) RETURN
      N2 = N+N
      DO 117 I=1,N2
         C(I) = CH(I)
  117 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSB (NAC,IDO,IP,L1,IDL1,CC,C1,C2,CH,CH2,WA)
      DIMENSION       CH(IDO,L1,IP)          ,CC(IDO,IP,L1)          ,
     1                C1(IDO,L1,IP)          ,WA(1)      ,C2(IDL1,IP),
     2                CH2(IDL1,IP)
      IDOT = IDO/2
      NT = IP*IDL1
      IPP2 = IP+2
      IPPH = (IP+1)/2
      IDP = IP*IDO
C
      IF (IDO .LT. L1) GO TO 106
      DO 103 J=2,IPPH
         JC = IPP2-J
         DO 102 K=1,L1
            DO 101 I=1,IDO
               CH(I,K,J) = CC(I,J,K)+CC(I,JC,K)
               CH(I,K,JC) = CC(I,J,K)-CC(I,JC,K)
  101       CONTINUE
  102    CONTINUE
  103 CONTINUE
      DO 105 K=1,L1
         DO 104 I=1,IDO
            CH(I,K,1) = CC(I,1,K)
  104    CONTINUE
  105 CONTINUE
      GO TO 112
  106 DO 109 J=2,IPPH
         JC = IPP2-J
         DO 108 I=1,IDO
            DO 107 K=1,L1
               CH(I,K,J) = CC(I,J,K)+CC(I,JC,K)
               CH(I,K,JC) = CC(I,J,K)-CC(I,JC,K)
  107       CONTINUE
  108    CONTINUE
  109 CONTINUE
      DO 111 I=1,IDO
         DO 110 K=1,L1
            CH(I,K,1) = CC(I,1,K)
  110    CONTINUE
  111 CONTINUE
  112 IDL = 2-IDO
      INC = 0
      DO 116 L=2,IPPH
         LC = IPP2-L
         IDL = IDL+IDO
         DO 113 IK=1,IDL1
            C2(IK,L) = CH2(IK,1)+WA(IDL-1)*CH2(IK,2)
            C2(IK,LC) = WA(IDL)*CH2(IK,IP)
  113    CONTINUE
         IDLJ = IDL
         INC = INC+IDO
         DO 115 J=3,IPPH
            JC = IPP2-J
            IDLJ = IDLJ+INC
            IF (IDLJ .GT. IDP) IDLJ = IDLJ-IDP
            WAR = WA(IDLJ-1)
            WAI = WA(IDLJ)
            DO 114 IK=1,IDL1
               C2(IK,L) = C2(IK,L)+WAR*CH2(IK,J)
               C2(IK,LC) = C2(IK,LC)+WAI*CH2(IK,JC)
  114       CONTINUE
  115    CONTINUE
  116 CONTINUE
      DO 118 J=2,IPPH
         DO 117 IK=1,IDL1
            CH2(IK,1) = CH2(IK,1)+CH2(IK,J)
  117    CONTINUE
  118 CONTINUE
      DO 120 J=2,IPPH
         JC = IPP2-J
         DO 119 IK=2,IDL1,2
            CH2(IK-1,J) = C2(IK-1,J)-C2(IK,JC)
            CH2(IK-1,JC) = C2(IK-1,J)+C2(IK,JC)
            CH2(IK,J) = C2(IK,J)+C2(IK-1,JC)
            CH2(IK,JC) = C2(IK,J)-C2(IK-1,JC)
  119    CONTINUE
  120 CONTINUE
      NAC = 1
      IF (IDO .EQ. 2) RETURN
      NAC = 0
      DO 121 IK=1,IDL1
         C2(IK,1) = CH2(IK,1)
  121 CONTINUE
      DO 123 J=2,IP
         DO 122 K=1,L1
            C1(1,K,J) = CH(1,K,J)
            C1(2,K,J) = CH(2,K,J)
  122    CONTINUE
  123 CONTINUE
      IF (IDOT .GT. L1) GO TO 127
      IDIJ = 0
      DO 126 J=2,IP
         IDIJ = IDIJ+2
         DO 125 I=4,IDO,2
            IDIJ = IDIJ+2
            DO 124 K=1,L1
               C1(I-1,K,J) = WA(IDIJ-1)*CH(I-1,K,J)-WA(IDIJ)*CH(I,K,J)
               C1(I,K,J) = WA(IDIJ-1)*CH(I,K,J)+WA(IDIJ)*CH(I-1,K,J)
  124       CONTINUE
  125    CONTINUE
  126 CONTINUE
      RETURN
  127 IDJ = 2-IDO
      DO 130 J=2,IP
         IDJ = IDJ+IDO
         DO 129 K=1,L1
            IDIJ = IDJ
            DO 128 I=4,IDO,2
               IDIJ = IDIJ+2
               C1(I-1,K,J) = WA(IDIJ-1)*CH(I-1,K,J)-WA(IDIJ)*CH(I,K,J)
               C1(I,K,J) = WA(IDIJ-1)*CH(I,K,J)+WA(IDIJ)*CH(I-1,K,J)
  128       CONTINUE
  129    CONTINUE
  130 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSB2 (IDO,L1,CC,CH,WA1)
      DIMENSION       CC(IDO,2,L1)           ,CH(IDO,L1,2)           ,
     1                WA1(1)
      IF (IDO .GT. 2) GO TO 102
      DO 101 K=1,L1
         CH(1,K,1) = CC(1,1,K)+CC(1,2,K)
         CH(1,K,2) = CC(1,1,K)-CC(1,2,K)
         CH(2,K,1) = CC(2,1,K)+CC(2,2,K)
         CH(2,K,2) = CC(2,1,K)-CC(2,2,K)
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            CH(I-1,K,1) = CC(I-1,1,K)+CC(I-1,2,K)
            TR2 = CC(I-1,1,K)-CC(I-1,2,K)
            CH(I,K,1) = CC(I,1,K)+CC(I,2,K)
            TI2 = CC(I,1,K)-CC(I,2,K)
            CH(I,K,2) = WA1(I-1)*TI2+WA1(I)*TR2
            CH(I-1,K,2) = WA1(I-1)*TR2-WA1(I)*TI2
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSB3 (IDO,L1,CC,CH,WA1,WA2)
      DIMENSION       CC(IDO,3,L1)           ,CH(IDO,L1,3)           ,
     1                WA1(1)     ,WA2(1)
      DATA TAUR,TAUI /-.5,.866025403784439/
      IF (IDO .NE. 2) GO TO 102
      DO 101 K=1,L1
         TR2 = CC(1,2,K)+CC(1,3,K)
         CR2 = CC(1,1,K)+TAUR*TR2
         CH(1,K,1) = CC(1,1,K)+TR2
         TI2 = CC(2,2,K)+CC(2,3,K)
         CI2 = CC(2,1,K)+TAUR*TI2
         CH(2,K,1) = CC(2,1,K)+TI2
         CR3 = TAUI*(CC(1,2,K)-CC(1,3,K))
         CI3 = TAUI*(CC(2,2,K)-CC(2,3,K))
         CH(1,K,2) = CR2-CI3
         CH(1,K,3) = CR2+CI3
         CH(2,K,2) = CI2+CR3
         CH(2,K,3) = CI2-CR3
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            TR2 = CC(I-1,2,K)+CC(I-1,3,K)
            CR2 = CC(I-1,1,K)+TAUR*TR2
            CH(I-1,K,1) = CC(I-1,1,K)+TR2
            TI2 = CC(I,2,K)+CC(I,3,K)
            CI2 = CC(I,1,K)+TAUR*TI2
            CH(I,K,1) = CC(I,1,K)+TI2
            CR3 = TAUI*(CC(I-1,2,K)-CC(I-1,3,K))
            CI3 = TAUI*(CC(I,2,K)-CC(I,3,K))
            DR2 = CR2-CI3
            DR3 = CR2+CI3
            DI2 = CI2+CR3
            DI3 = CI2-CR3
            CH(I,K,2) = WA1(I-1)*DI2+WA1(I)*DR2
            CH(I-1,K,2) = WA1(I-1)*DR2-WA1(I)*DI2
            CH(I,K,3) = WA2(I-1)*DI3+WA2(I)*DR3
            CH(I-1,K,3) = WA2(I-1)*DR3-WA2(I)*DI3
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c **********************************************************************
      SUBROUTINE PASSB4 (IDO,L1,CC,CH,WA1,WA2,WA3)
      DIMENSION       CC(IDO,4,L1)           ,CH(IDO,L1,4)           ,
     1                WA1(1)     ,WA2(1)     ,WA3(1)
      IF (IDO .NE. 2) GO TO 102
      DO 101 K=1,L1
         TI1 = CC(2,1,K)-CC(2,3,K)
         TI2 = CC(2,1,K)+CC(2,3,K)
         TR4 = CC(2,4,K)-CC(2,2,K)
         TI3 = CC(2,2,K)+CC(2,4,K)
         TR1 = CC(1,1,K)-CC(1,3,K)
         TR2 = CC(1,1,K)+CC(1,3,K)
         TI4 = CC(1,2,K)-CC(1,4,K)
         TR3 = CC(1,2,K)+CC(1,4,K)
         CH(1,K,1) = TR2+TR3
         CH(1,K,3) = TR2-TR3
         CH(2,K,1) = TI2+TI3
         CH(2,K,3) = TI2-TI3
         CH(1,K,2) = TR1+TR4
         CH(1,K,4) = TR1-TR4
         CH(2,K,2) = TI1+TI4
         CH(2,K,4) = TI1-TI4
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            TI1 = CC(I,1,K)-CC(I,3,K)
            TI2 = CC(I,1,K)+CC(I,3,K)
            TI3 = CC(I,2,K)+CC(I,4,K)
            TR4 = CC(I,4,K)-CC(I,2,K)
            TR1 = CC(I-1,1,K)-CC(I-1,3,K)
            TR2 = CC(I-1,1,K)+CC(I-1,3,K)
            TI4 = CC(I-1,2,K)-CC(I-1,4,K)
            TR3 = CC(I-1,2,K)+CC(I-1,4,K)
            CH(I-1,K,1) = TR2+TR3
            CR3 = TR2-TR3
            CH(I,K,1) = TI2+TI3
            CI3 = TI2-TI3
            CR2 = TR1+TR4
            CR4 = TR1-TR4
            CI2 = TI1+TI4
            CI4 = TI1-TI4
            CH(I-1,K,2) = WA1(I-1)*CR2-WA1(I)*CI2
            CH(I,K,2) = WA1(I-1)*CI2+WA1(I)*CR2
            CH(I-1,K,3) = WA2(I-1)*CR3-WA2(I)*CI3
            CH(I,K,3) = WA2(I-1)*CI3+WA2(I)*CR3
            CH(I-1,K,4) = WA3(I-1)*CR4-WA3(I)*CI4
            CH(I,K,4) = WA3(I-1)*CI4+WA3(I)*CR4
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c ********************************************************************** 
      SUBROUTINE PASSB5 (IDO,L1,CC,CH,WA1,WA2,WA3,WA4)
      DIMENSION       CC(IDO,5,L1)           ,CH(IDO,L1,5)           ,
     1                WA1(1)     ,WA2(1)     ,WA3(1)     ,WA4(1)
      DATA TR11,TI11,TR12,TI12 /.309016994374947,.951056516295154,
     1-.809016994374947,.587785252292473/
      IF (IDO .NE. 2) GO TO 102
      DO 101 K=1,L1
         TI5 = CC(2,2,K)-CC(2,5,K)
         TI2 = CC(2,2,K)+CC(2,5,K)
         TI4 = CC(2,3,K)-CC(2,4,K)
         TI3 = CC(2,3,K)+CC(2,4,K)
         TR5 = CC(1,2,K)-CC(1,5,K)
         TR2 = CC(1,2,K)+CC(1,5,K)
         TR4 = CC(1,3,K)-CC(1,4,K)
         TR3 = CC(1,3,K)+CC(1,4,K)
         CH(1,K,1) = CC(1,1,K)+TR2+TR3
         CH(2,K,1) = CC(2,1,K)+TI2+TI3
         CR2 = CC(1,1,K)+TR11*TR2+TR12*TR3
         CI2 = CC(2,1,K)+TR11*TI2+TR12*TI3
         CR3 = CC(1,1,K)+TR12*TR2+TR11*TR3
         CI3 = CC(2,1,K)+TR12*TI2+TR11*TI3
         CR5 = TI11*TR5+TI12*TR4
         CI5 = TI11*TI5+TI12*TI4
         CR4 = TI12*TR5-TI11*TR4
         CI4 = TI12*TI5-TI11*TI4
         CH(1,K,2) = CR2-CI5
         CH(1,K,5) = CR2+CI5
         CH(2,K,2) = CI2+CR5
         CH(2,K,3) = CI3+CR4
         CH(1,K,3) = CR3-CI4
         CH(1,K,4) = CR3+CI4
         CH(2,K,4) = CI3-CR4
         CH(2,K,5) = CI2-CR5
  101 CONTINUE
      RETURN
  102 DO 104 K=1,L1
         DO 103 I=2,IDO,2
            TI5 = CC(I,2,K)-CC(I,5,K)
            TI2 = CC(I,2,K)+CC(I,5,K)
            TI4 = CC(I,3,K)-CC(I,4,K)
            TI3 = CC(I,3,K)+CC(I,4,K)
            TR5 = CC(I-1,2,K)-CC(I-1,5,K)
            TR2 = CC(I-1,2,K)+CC(I-1,5,K)
            TR4 = CC(I-1,3,K)-CC(I-1,4,K)
            TR3 = CC(I-1,3,K)+CC(I-1,4,K)
            CH(I-1,K,1) = CC(I-1,1,K)+TR2+TR3
            CH(I,K,1) = CC(I,1,K)+TI2+TI3
            CR2 = CC(I-1,1,K)+TR11*TR2+TR12*TR3
            CI2 = CC(I,1,K)+TR11*TI2+TR12*TI3
            CR3 = CC(I-1,1,K)+TR12*TR2+TR11*TR3
            CI3 = CC(I,1,K)+TR12*TI2+TR11*TI3
            CR5 = TI11*TR5+TI12*TR4
            CI5 = TI11*TI5+TI12*TI4
            CR4 = TI12*TR5-TI11*TR4
            CI4 = TI12*TI5-TI11*TI4
            DR3 = CR3-CI4
            DR4 = CR3+CI4
            DI3 = CI3+CR4
            DI4 = CI3-CR4
            DR5 = CR2+CI5
            DR2 = CR2-CI5
            DI5 = CI2-CR5
            DI2 = CI2+CR5
            CH(I-1,K,2) = WA1(I-1)*DR2-WA1(I)*DI2
            CH(I,K,2) = WA1(I-1)*DI2+WA1(I)*DR2
            CH(I-1,K,3) = WA2(I-1)*DR3-WA2(I)*DI3
            CH(I,K,3) = WA2(I-1)*DI3+WA2(I)*DR3
            CH(I-1,K,4) = WA3(I-1)*DR4-WA3(I)*DI4
            CH(I,K,4) = WA3(I-1)*DI4+WA3(I)*DR4
            CH(I-1,K,5) = WA4(I-1)*DR5-WA4(I)*DI5
            CH(I,K,5) = WA4(I-1)*DI5+WA4(I)*DR5
  103    CONTINUE
  104 CONTINUE
      RETURN
      END
c **********************************************************************
c 
c ["send index for vfftpk" describes a vectorized version of fftpack]
c 
c 