c Arrays and variables needed for spatial and temporal mesh and time-stepping
c---------------------------------------------------------------------------
      real           xtht(nndx),yrad(nndy),x(nnd),y(nnd),
     +               xgp(nel,ng),ygp(nel,ng),rb
      integer        icon(nel,4),ielnoint
      common/meshx/  xtht,yrad,x,y,
     +               xgp,ygp,rb,
     +               icon,ielnoint
      real          time(nt+1),timein,timeout,tomti,dt,to,dur,yeari,dury
      common/mesht/  time,timein,timeout,tomti,dt,to,dur,yeari,dury
      real           ebt,ebtt,bavr,vol,b1max,b2max,
     +               ebtot(nntp1),ebttot(nntp1),bavrt(nntp1),volt(nntp1)
      common/post/   ebt,ebtt,bavr,vol,b1max,b2max,
     +               ebtot,ebttot,bavrt,volt
      REAL           bphit(nntp1,nndx),aquadt(nntp1,nndx),
     +               asurft(nntp1,nndx),brsurf(nntp1,nndx)
      INTEGER        itfit(2),ixfits(2),ixfitn(2),
     +               ntfit,nxfits,nxfitn,nnfit,
     +               nfft(2),nnfft,
     +               itbmr
      COMMON/fitns/  bphit,aquadt,
     +               asurft,brsurf,
     +               itfit,ixfits,ixfitn,
     +               ntfit,nxfits,nxfitn,nnfit,
     +               nfft,nnfft,
     +               itbmr
