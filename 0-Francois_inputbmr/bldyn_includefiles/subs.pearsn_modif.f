c***********************************************************************
      SUBROUTINE pearsn(x,y,n,r)
c     SUBROUTINE pearsn(x,y,n,r,prob,z)
c***********************************************************************
c     Given two arrays x(1:n) and y(1:n), this routine computes their
c     correlation coefficient r (returned as r), the significance level
c     at which the null hypothesis of zero correlation is disproved
c     (prob whose small value indicates a significant correlation), and
c     Fisher's z (returned as z), whose value can be used in further
c     statistical tests as described above.
c***********************************************************************
      INTEGER n
      REAL prob,r,z,x(n),y(n),TINY
      PARAMETER (TINY=1.e-20)
CU    USES betai
      INTEGER j
      REAL ax,ay,df,sxx,sxy,syy,t,xt,yt,betai
c-----------------------------------------------------------------------
      ax=0.
      ay=0.
      do 11 j=1,n
        ax=ax+x(j)
        ay=ay+y(j)
11    continue
      ax=ax/n
      ay=ay/n
      sxx=0.
      syy=0.
      sxy=0.
      do 12 j=1,n
        xt=x(j)-ax
        yt=y(j)-ay
        sxx=sxx+xt**2
        syy=syy+yt**2
        sxy=sxy+xt*yt
12    continue
      r=sxy/sqrt(sxx*syy)
      z=0.5*log(((1.+r)+TINY)/((1.-r)+TINY))
      df=n-2
      t=r*sqrt(df/(((1.-r)+TINY)*((1.+r)+TINY)))
c     prob=betai(0.5*df,0.5,df/(df+t**2))
C     prob=erfcc(abs(z*sqrt(n-1.))/1.4142136)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software Vs1&v%1jw#<?4210(9p#.
c=======================================================================
