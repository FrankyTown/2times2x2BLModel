c Arrays and variables needed throughout
c-----------------------------------------------------------------------

      CHARACTER        title*200
      common/probts/   title
      INTEGER          ivrb,iout,incr,incw
      common/cntrls/   ivrb,iout,incr,incw
      DOUBLE PRECISION rsun,r2d21,omeg0,etas,uu,us,qq,nn,vv,ww,taud,
     +                 uu0,us0,etas0,tau,rm,ru,
     +                 qqm1,nnm1,vvpi,wwpi,
     +                 xxi,xxo,yyi,yyo,xomxi,yomyi,
     +                 ampi,expi,lerfi,faci,
     +                 dur,td,
     +                 wdth,flux0min,flux0max,flux0fit,flux0dev,
     +                 awbmrmin,awbmrmax,awbmrlin0,awbmrlin1,awbmrdev,
     +                 tht0min,tht0max,phi0min,phi0max,
     +                 thtmask(2),phimask(2),phimaskv,
     +                 tiltjoy,tiltdev0,tiltdev1,tiltdev2,bfitqch,
     +                 ifbmr,
     +                 aphi0,lerf
      INTEGER          bmrtype(3,4),nfbmr,nbmrmax,nbmr,nbmrin,
     +                 nnbmr(nntsp1),nnbmrin(nntsp1)
      common/input/    rsun,r2d21,omeg0,etas,uu,us,qq,nn,vv,ww,taud,
     +                 uu0,us0,etas0,tau,rm,ru,
     +                 qqm1,nnm1,vvpi,wwpi,
     +                 xxi,xxo,yyi,yyo,xomxi,yomyi,
     +                 ampi,expi,lerfi,faci,
     +                 dur,td,
     +                 wdth,flux0min,flux0max,flux0fit,flux0dev,
     +                 awbmrmin,awbmrmax,awbmrlin0,awbmrlin1,awbmrdev,
     +                 tht0min,tht0max,phi0min,phi0max,
     +                 thtmask,phimask,phimaskv,
     +                 tiltjoy,tiltdev0,tiltdev1,tiltdev2,bfitqch,
     +                 ifbmr,
     +                 aphi0,lerf,
     +                 bmrtype,nfbmr,nbmrmax,nbmr,nbmrin,
     +                 nnbmr,nnbmrin
      DOUBLE PRECISION sfluxin,usfluxin
      common/influx/   sfluxin,usfluxin
      DOUBLE PRECISION tht0,phi0,epst,epsp,amp0
      common/initc/    tht0,phi0,epst,epsp,amp0
