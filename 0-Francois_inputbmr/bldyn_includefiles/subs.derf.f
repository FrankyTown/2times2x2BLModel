c***********************************************************************
c    The following are Numerical Recipes Routines for the Error function
c    (Double precision)
c***********************************************************************

c***********************************************************************
      FUNCTION dgammp(a,x)
c***********************************************************************
      DOUBLE PRECISION a,dgammp,x
CU    USES dgcf,dgser
      DOUBLE PRECISION gammcf,gamser,gln
c-----------------------------------------------------------------------
      IF(x.LT.0..or.a.LE.0.) STOP 'bad arguments in dgammp'
      IF(x.LT.a+1.)THEN
        CALL dgser(gamser,a,x,gln)
        dgammp=gamser
      ELSE
        CALL dgcf(gammcf,a,x,gln)
        dgammp=1.-gammcf
      ENDIF
      RETURN
      END
C  (C) Copr. 1986-92 Numerical Recipes Software Vs1&v%1jw#<?4210(9p#.
c=======================================================================

c***********************************************************************
      FUNCTION derf(x)
c***********************************************************************
      DOUBLE PRECISION derf,x
CU    USES dgammp
      DOUBLE PRECISION dgammp
c-----------------------------------------------------------------------
      IF(x.LT.0.)THEN
         derf=-dgammp(.5d0,x**2)
      ELSE
         derf=dgammp(.5d0,x**2)
      ENDIF
      RETURN
      END
C  (C) Copr. 1986-92 Numerical Recipes Software Vs1&v%1jw#<?4210(9p#.
c=======================================================================

c***********************************************************************
      SUBROUTINE dgcf(gammcf,a,x,gln)
c***********************************************************************
      INTEGER ITMAX
      DOUBLE PRECISION a,gammcf,gln,x,EPS,FPMIN
      PARAMETER (ITMAX=100,EPS=3.d-7,FPMIN=1.d-30)
CU    USES dgammln
      INTEGER i
      DOUBLE PRECISION an,b,c,d,del,h,dgammln
c-----------------------------------------------------------------------
      gln=dgammln(a)
      b=x+1.-a
      c=1./FPMIN
      d=1./b
      h=d
      DO 11 i=1,ITMAX
        an=-i*(i-a)
        b=b+2.
        d=an*d+b
        IF(DABS(d).LT.FPMIN)d=FPMIN
        c=b+an/c
        IF(DABS(c).LT.FPMIN)c=FPMIN
        d=1./d
        del=d*c
        h=h*del
        IF(DABS(del-1.).LT.EPS)GOTO 1
11    CONTINUE
      STOP 'a too large, ITMAX too small in dgcf'
1     gammcf=DEXP(-x+a*DLOG(x)-gln)*h
      RETURN
      END
C  (C) Copr. 1986-92 Numerical Recipes Software Vs1&v%1jw#<?4210(9p#.
c=======================================================================

c***********************************************************************
      SUBROUTINE dgser(gamser,a,x,gln)
c***********************************************************************
      INTEGER ITMAX
      DOUBLE PRECISION a,gamser,gln,x,EPS
      PARAMETER (ITMAX=100,EPS=3.d-7)
CU    USES dgammln
      INTEGER n
      DOUBLE PRECISION ap,del,sum,dgammln
c-----------------------------------------------------------------------
      gln=dgammln(a)
      IF(x.LE.0.)THEN
        IF(x.LT.0.) STOP 'x < 0 in dgser'
        gamser=0.
        RETURN
      ENDIF
      ap=a
      sum=1./a
      del=sum
      DO 11 n=1,ITMAX
        ap=ap+1.
        del=del*x/ap
        sum=sum+del
        IF(DABS(del).LT.DABS(sum)*EPS)GOTO 1
11    CONTINUE
      STOP 'a too large, ITMAX too small in dgser'
1     gamser=sum*DEXP(-x+a*DLOG(x)-gln)
      RETURN
      END
C  (C) Copr. 1986-92 Numerical Recipes Software Vs1&v%1jw#<?4210(9p#.
c=======================================================================

c***********************************************************************
      FUNCTION dgammln(xx)
c***********************************************************************
      DOUBLE PRECISION dgammln,xx
      INTEGER j
      DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
      SAVE cof,stp
      DATA cof,stp/76.18009172947146d0,-86.50532032941677d0,
     *24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2,
     *-.5395239384953d-5,2.5066282746310005d0/
c-----------------------------------------------------------------------
      x=xx
      y=x
      tmp=x+5.5d0
      tmp=(x+0.5d0)*DLOG(tmp)-tmp
      ser=1.000000000190015d0
      DO 11 j=1,6
        y=y+1.d0
        ser=ser+cof(j)/y
11    CONTINUE
      dgammln=tmp+DLOG(stp*ser/x)
      RETURN
      END
C  (C) Copr. 1986-92 Numerical Recipes Software Vs1&v%1jw#<?4210(9p#.
c=======================================================================

