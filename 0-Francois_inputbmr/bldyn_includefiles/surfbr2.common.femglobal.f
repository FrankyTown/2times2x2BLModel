c Arrays and variables needed by GLOBAL FEM machinery
c---------------------------------------------------------------------------
      DOUBLE PRECISION  ck,anm1,fnm1,fn,f0,delu,u0,keffdu,keffu0
      common/iveqss/    ck(nbs,nns),anm1(nns),fnm1(nns),fn(nns),f0(nns),
     +                  delu(nns),u0(nns),keffdu(nns),keffu0(nns)
      DOUBLE PRECISION  k,f,a
      common/syseqss/   k(nbs,nns),f(nns),a(nns)
      INTEGER           iuebc,numebc
      DOUBLE PRECISION  uebc
      common/essbcs/    iuebc(nbcs),uebc(nbcs),numebc
      INTEGER           nnodes,intdef,ioptau
      common/elspec/    nnodes(30),intdef(30),ioptau(30)
      INTEGER           mband,mfbw
      common/genrl/     mband,mfbw
      INTEGER           iupbc0,iupbc1,numpbc
      common/perbc/     iupbc0(nbcs),iupbc1(nbcs),numpbc
