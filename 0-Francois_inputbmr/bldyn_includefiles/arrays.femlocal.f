c Arrays and variables needed by LOCAL FEM machinery
c---------------------------------------------------------------------------
      real           ke,fe,ce
      common/elstf/  ke(8,8),fe(8),ce(8,8)
      real           gp,wt
      common/glint/  gp(5,5),wt(5,5)
      real           xi,eta,jac,jacinv,xx,yy,phi,dphdx,dphdy,
     +               dphdxi,dphdet
      common/elvar/  xi,eta,jac(nel,ng),jacinv(nel,ng,4),
     +               xx(4),yy(4),phi(4),
     +               dphdx(4),dphdy(4),dphdxi(4),dphdet(4)

