c Arrays and variables needed by LOCAL FEM machinery
c---------------------------------------------------------------------------
      DOUBLE PRECISION  ke,fe,ce
      common/elstfs/    ke(8,8),fe(8),ce(8,8)
      DOUBLE PRECISION  gp,wt
      common/glints/    gp(5,5),wt(5,5)
      DOUBLE PRECISION  xi,eta,jac,xxx,yyy,phi,
     +                  dphdx,dphdy,dphdxi,dphdet,jacinv
      INTEGER           ielno,nnd
      common/elvars/    xi,eta,jac(nes,ngs),xxx(8),yyy(8),phi(8),
     +                  dphdx(8),dphdy(8),dphdxi(8),dphdet(8),ielno,nnd,
     +                  jacinv(nes,ngs,2)
      DOUBLE PRECISION  fint
      common/intld/     fint(nes,ngs)             !Used only in elem24... but undefined !
      
