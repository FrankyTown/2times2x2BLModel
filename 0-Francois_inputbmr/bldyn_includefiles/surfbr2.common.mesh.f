c Arrays and variables needed for spatial and temporal mesh and time-stepping
c---------------------------------------------------------------------------

      DOUBLE PRECISION  x,y,xx,yy,xgp,ygp
      INTEGER           icon,ngpint,numel,numnp
      common/meshs/   x(nns),y(nns),xx(nns-nes),yy(nns-nes),icon(nes,9),
     +                  ngpint(nes),xgp(nes,ngs),ygp(nes,ngs),
     +                  numel,numnp
      DOUBLE PRECISION  times,dt,timein,timeout,tomti
      INTEGER           istep
      common/meshts/    times(nts+1),dt,timein,timeout,tomti,istep
      INTEGER           ngp
      common/phys/      ngp
      INTEGER           ivid
      common/video/     ivid
      DOUBLE PRECISION  ctime,onemth,rns
      common/calf/      ctime,onemth,rns
