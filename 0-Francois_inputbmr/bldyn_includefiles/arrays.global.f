c Arrays and variables needed throughout
c-----------------------------------------------------------------------

      character      title*200
      common/probt/  title
      integer        ijac,ipar,isrc,ialp,iloss,icnd,istc,incr,incw,ivrb
      common/cntrl/  ijac,ipar,isrc,ialp,iloss,icnd,istc,incr,incw,ivrb
      real           alpsfac,alpsfac2,addnamp
      common/stoch/  alpsfac,alpsfac2,addnamp
      INTEGER        iybphi(2),iybr,
     +               nsmth,datseed
      real           faca,calpha,calpha2,comega,reynolds,rmag,
     +               etadf,ybl,rzc,gamfl,rsun,
     +               a0,a2,a4,omgc,
     +               uu,us,rbot,mm,pp,qq,nn,vv,ww,etas,taud,
     +               ampi,expi,lerfi,faci,
     +               uu0,us0,etat0,etas0,
     +               xib,c1,c2,cc0,cc1,cc2,
     +               qqm1,nnm1,vvpi,wwpi,
     +               bbb,aaa,bcrit,dbcrit,ccc,
     +               xcrit0,xcrit,dxcrit,bfitfac,expfly,
     +               region(6),
     +               iffit1,iffit2,iffit3
c    +               uuf,qqf,vvf,wwf,etf,tauf,
      common/param1/ iybphi,iybr,
     +               nsmth,datseed
      common/param2/ faca,calpha,calpha2,comega,reynolds,rmag,
     +               etadf,ybl,rzc,gamfl,rsun,
     +               a0,a2,a4,omgc,
     +               uu,us,rbot,mm,pp,qq,nn,vv,ww,etas,taud,
     +               ampi,expi,lerfi,faci,
     +               uu0,us0,etat0,etas0,
     +               xib,c1,c2,cc0,cc1,cc2,
     +               qqm1,nnm1,vvpi,wwpi,
     +               bbb,aaa,bcrit,dbcrit,ccc,
     +               xcrit0,xcrit,dxcrit,bfitfac,expfly,
     +               region,
     +               iffit1,iffit2,iffit3
c    +               uuf,qqf,vvf,wwf,etf,tauf,

