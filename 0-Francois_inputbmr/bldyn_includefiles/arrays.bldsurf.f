c Surface vector potential
c-----------------------------------------------------------------------

      DOUBLE PRECISION aphi(nnsyvrai)
      REAL             bfit(nntp1,nndx),mutht(nndx)
      INTEGER          ntcall,nstepscall,itt
      common/bldsurf/  aphi,
     +                 bfit,mutht,
     +                 ntcall,nstepscall,itt
