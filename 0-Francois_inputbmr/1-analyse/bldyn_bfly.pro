;= IDL CODE ============================================================
PRO bldyn_bfly
;=======================================================================
; 2x2D BL DYNAMO, butterfly diagrams
;-----------------------------------------------------------------------
; (APAS7500 Fall 1997  Problem II.X.Y see Section X.Y of notes, and Appendix X)
; 
; Updated:
;  2012-01-31  A. Lemerle
;  2013-04-18  A. Lemerle
;  2015...     A. Lemerle
;-----------------------------------------------------------------------
; This codes produces butterfly diagrams of B_r and
; B_phi at depths rbr and rbp, as set by the user (see below), from a
; solution produced by bldyn.f
;-----------------------------------------------------------------------
; COMMAND LINE INPUTS:
;  - ...
;  - name of input folder
;  - name of input file
;  - name of data file (emergences statistics)
;  - depth of toroidal field cut (two elements vector)
;  - fraction of time lapse to use for fitness
;  - ...
;=======================================================================

pi180 = !PI/180.
rsun = 6.9599d10                                                        ;Solar radius (cm)

args=COMMAND_LINE_ARGS(COUNT=nargs)
IF (nargs GT 1) THEN STOP,'Error, wrong number of arguments !'

tbfitfacv = ['0']
;tbfitfacv = ['020','025','030','035','040','045','050']
nbfitfac = N_ELEMENTS(tbfitfacv)

tbldseedv = ['0']
;tbldseedv = ['0','1','2']
;tbldseedv = ['0','1','2','3','4','5','6','7','8','9']
nbldseed = N_ELEMENTS(tbldseedv)

dafft0amp=0.
ebfft0amp=0.
ebfft1amp=0.
ebfft2amp=0.
unfft0amp=0.
snfft0amp=0.
ssnfft0amp=0.
sssfft0amp=0.
FOREACH tbfitfac, tbfitfacv, ibfitfac DO BEGIN
growthrate=[]
frequency=[]
FOREACH tbldseed, tbldseedv, ibldseed DO BEGIN

idlfile=args
;idlfile='bldyn_bfly.in'
;idlfile='bldyn_wangal_21x08_optimal_0.in'
;idlfile='bldyn_wangal_21x08_dEeRpqvwW21uetf1_0.in'
;idlfile='bldyn_bldf070q15_21x32_100.in'
;idlfile='bldyn_bldf040q2_21x32_'+tbldseed+'.in'
;idlfile='bldyn_bldf'+tbfitfac+'q0_21x08_'+tbldseed+'.in'
CLOSE,5 & OPENR,5,idlfile

;path=GETENV('HOME')+'/Documents/'
path='../'

;pathin = path +    'scratch/5-bldsforecast/xesults/'
pathin = path + 'xesults/'
;pathout= path + '1-Doctorat/5-bldsforecast/xesults/'
pathout= path + 'xesults/'

simname=''
;PRINT,'Enter simname (ex: synth0_18x9):'
READF,5,simname   &   simname = STRTRIM(simname,2)

filename=''
;PRINT,'Enter file name (ex: bldyn0320+_180x5_64x96_synth0_18x9_0_fit0.1x33408d_Co14019_Rm118):'
READF,5,filename   &   filename = STRTRIM(filename,2)

file   = simname + '/' + filename
filein   = pathin  + file + '.i3e'
PRINT,'Using input file:'
PRINT,filein

SETENV,'FILE='+pathout+simname
SPAWN,'mkdir $FILE'

rbr = 1.0                              ; depth of radial field cut
rbp = [0.,0.]                          ;[0.68,0.70] depth of toroidal field cut (was 0.718)
;PRINT,'Enter depth of toroidal cut (two element vector):'
READF,5,rbp

fluxfac=1.e21
mbflymax=24.415                                                         ; maximum of synoptic magnetogram (G)

tfit=[0.1,0.0]                                                          ; fraction of the temporal mesh to use for fitness (from tfit to end)
latfit=[0.0,0.0]                                                        ; 0.666667 fraction of hemisphere (latitude) to use for fitness1 (from 0deg to +-latfit)
;PRINT,'Enter fraction of time lapse to use for fitness:'
READF,5,tfit
READF,5,latfit
;bycrit = 0.6
;bfitcrit = 0.6
;brcrit = 3.e-2

fpole=1./6.

smthcrit = 3/16.

cycdeb=0L                                                               ;Numéro du cycle réel
ncyc=0L                                                                 ;Nombre de cycles réels successifs
nrep=0L                                                                 ;Nombre de répétitions identiques du ou des cycles
;PRINT,'Enter fstcyc, ncyc, nrep:'
READF,5,cycdeb,ncyc,nrep     

yrminmodif=0      ;1976.7                                               ;Correction to yrmincyc vector... (0. to do nothing)

nfit=0L
READF,5,nfit
xfit=FLTARR(nfit) & xfitmin=FLTARR(nfit) & xfitmax=FLTARR(nfit)
READF,5,xfit
READF,5,xfitmin
READF,5,xfitmax

ncycpar=0L
READF,5,ncycpar
ybl=FLTARR(ncycpar) & yblmin=FLTARR(ncycpar) & yblmax=FLTARR(ncycpar)
READF,5,ybl
READF,5,yblmin
READF,5,yblmax
etac=FLTARR(ncycpar) & etacmin=FLTARR(ncycpar) & etacmax=FLTARR(ncycpar)
READF,5,etac
READF,5,etacmin
READF,5,etacmax
etat=FLTARR(ncycpar) & etatmin=FLTARR(ncycpar) & etatmax=FLTARR(ncycpar)
READF,5,etat
READF,5,etatmin
READF,5,etatmax
uu=FLTARR(ncycpar) & uumin=FLTARR(ncycpar) & uumax=FLTARR(ncycpar)
READF,5,uu    & uu   = uu/100.
READF,5,uumin & uumin= uumin/100.
READF,5,uumax & uumax= uumax/100.
us=FLTARR(ncycpar) & usmin=FLTARR(ncycpar) & usmax=FLTARR(ncycpar)
READF,5,us    & us   = us/100.
READF,5,usmin & usmin= usmin/100.
READF,5,usmax & usmax= usmax/100.
pp=FLTARR(ncycpar) & ppmin=FLTARR(ncycpar) & ppmax=FLTARR(ncycpar)
READF,5,pp
READF,5,ppmin
READF,5,ppmax
rbot=FLTARR(ncycpar) & rbotmin=FLTARR(ncycpar) & rbotmax=FLTARR(ncycpar)
READF,5,rbot
READF,5,rbotmin
READF,5,rbotmax
mm=FLTARR(ncycpar) & mmmin=FLTARR(ncycpar) & mmmax=FLTARR(ncycpar)
READF,5,mm
READF,5,mmmin
READF,5,mmmax
qq=FLTARR(ncycpar) & qqmin=FLTARR(ncycpar) & qqmax=FLTARR(ncycpar)
READF,5,qq
READF,5,qqmin
READF,5,qqmax
vv=FLTARR(ncycpar) & vvmin=FLTARR(ncycpar) & vvmax=FLTARR(ncycpar)
READF,5,vv
READF,5,vvmin
READF,5,vvmax
ww=FLTARR(ncycpar) & wwmin=FLTARR(ncycpar) & wwmax=FLTARR(ncycpar)
READF,5,ww
READF,5,wwmin
READF,5,wwmax
eta0=FLTARR(ncycpar) & eta0min=FLTARR(ncycpar) & eta0max=FLTARR(ncycpar)
READF,5,eta0
READF,5,eta0min
READF,5,eta0max
taud=0. & taudmin=0. & taudmax=0.
READF,5,taud
READF,5,taudmin
READF,5,taudmax
;amp=0. & ampmin=0. & ampmax=0.
;READF,5,amp
;READF,5,ampmin
;READF,5,ampmax

bb=0. & aa=0. & bcrit=0. & dbcrit=0. & cc=0. & x0crit=0. & xcrit=0. & dxcrit=0. & nsmth=0 & expfly=0.
;PRINT,'Enter fitness parameters:'
READF,5,bb,aa,bcrit,dbcrit,cc,x0crit,xcrit,dxcrit,bfitfac
READF,5,nsmth,expfly

;bfitfaccor = 4.08/5.60                                                  ; Correction factor Nbmr/bfit, from final optimization
bfitfaccor = 1.

datafile=''
;PRINT,'Enter datafile (ex: .../arnaud/synth_wolf_1.5xbmr_0_cycles14-23_1000x64):'
READF,5,datafile   &   datafile = STRTRIM(datafile,2)
PRINT,'Using datafile file for SSN:'
PRINT,datafile

bmrfile=''
;PRINT,'Enter bmrfile (ex: .../arnaud/synth_wolf_1.5xbmr_0_cycles14-23_1000x64):'
READF,5,bmrfile   &   bmrfile = STRTRIM(bmrfile,2)
PRINT,'Using bmrfile file:'
PRINT,bmrfile

;CLOSE,6 & OPENR,6,'surfbr'+STRMID(idlfile,STRLEN('bldyn'))
;
;nntsp1=0 & nnsxvrai=0 & nnsyvrai=0
;READF,6,nntsp1,nnsxvrai,nnsyvrai
;
;cycdebs=0 & ncycs=0 & nreps=0
;READF,6,cycdebs,ncycs,nreps
;IF ((cycdebs NE cycdeb) OR (ncycs NE ncyc) OR (nreps NE nrep)) THEN STOP,'Error: wrong cycdeb, ncyc, or nrep !'
;
;nntp1=0 & nndx=0
;READF,6,nntp1,nndx
;
;tfits=[0.,0.] & latfits=[0.,0.]
;READF,6,tfits
;READF,6,latfits
;
;regions=FLTARR(6)
;READF,6,regions
;
;filenames=''
;READF,6,filenames
;
;simnames=''
;READF,6,simnames
;
;magfile=''
;READF,6,magfile
;
;uu100s=0. & qqs=0. & vvs=0. & wws=0. & etase10s=0. & tds=0. & ampis=0. & expis=0.
;READF,6,uu100s,qqs,vvs,wws,etase10s,tds,ampis,expis

;---- Open simulation output and read header info ----------------------

!ORDER = 0
CLOSE,1 & OPENR,1,/F77_UNFORMATTED,filein

title = ''
READU,1,title
PRINT,'Run title:'
PRINT,title

idum=LONARR(6)
vdum=FLTARR(7)
READU,1,idum
numnp=idum(0) & numel=idum(1) & nelx=idum(2) & nely=idum(3) & nt0=idum(4) & nstep=idum(5)
nelybuf=nely/10-1
READU,1,vdum
comega=vdum(0) & calpha=vdum(1) & reynolds=vdum(2) & etadf=vdum(3) & ybll=vdum(4) & rzc=vdum(5) & to=vdum(6)

ti = 0.
nt=nt0*nstep

itfit = LONG(tfit*nt)
ntfit = itfit[1]-itfit[0]+1
ixfit = LONG(latfit*nelx)
nxfit = ixfit[1]-ixfit[0]+1
nnfit = ntfit*nxfit

xx = FLTARR(nelx+1)
yy = FLTARR(nely+1)
xxx=FLTARR(nelx+1,nely+1)
yyy=FLTARR(nelx+1,nely+1)
READU,1,xx                    ;Careful: xx=cos(theta) in increasing order (from south to north pole), but regular mesh in theta
READU,1,yy
FOR j=0,nely DO xxx(*,j) = xx
FOR i=0,nelx DO yyy(i,*) = yy
xxa  = ACOS(xx)
sinxx= SIN(xxa)
xxan = 90.-xxa/pi180

delmu=xx(1:nelx)-xx(0:nelx-1)
delr =yy(1:nely)-yy(0:nely-1)

jpole=LONG(fpole*nelx)

omg = FLTARR(nelx+1,nely+1)
ur  = FLTARR(nelx+1,nely+1)
uth = FLTARR(nelx+1,nely+1)
eta = FLTARR(nelx+1,nely+1)
READU,1,omg
READU,1,ur
READU,1,uth
READU,1,eta

intsouth = INT_TABULATED(xx(0:jpole)        ,REPLICATE(1.,jpole+1))
intnorth = INT_TABULATED(xx(nelx-jpole:nelx),REPLICATE(1.,jpole+1))
;intsouth = INT_TABULATED(REVERSE(xxa(0:jpole)        ),REVERSE(sinxx(0:jpole)        ))
;intnorth = INT_TABULATED(REVERSE(xxa(nelx-jpole:nelx)),REVERSE(sinxx(nelx-jpole:nelx)))

;---- Set real time vector -----
anmincyc0= [1701.       , 1713.       , 1724.       , 1732.       , 1744.       , 1755.+ 2./12, 1766.+ 5./12, 1775.+ 5./12, $
            1784.+ 7./12, 1798.+ 4./12, 1810.+11./12, 1823.+ 4./12, 1833.+10./12, 1843.+ 6./12, 1855.+11./12, 1867.+ 2./12, $
            1878.+11./12, 1890.+ 2./12, 1902.+ 1./12, 1913.+ 7./12, 1923.+ 7./12, 1933.+ 8./12, 1944.+ 1./12, 1954.+ 3./12, $
            1964.+ 9./12, 1976.+ 5./12, 1986.+ 8./12, 1996.+ 4./12, 2008.+11./12]
dmincyc  = 4

;---- Modif to anmincyc vector:
IF (yrminmodif NE 0.) THEN BEGIN
   anmincyc0(21+dmincyc) = yrminmodif
ENDIF

cycfin=cycdeb+ncyc-1

anmincyc = anmincyc0(cycdeb+dmincyc:cycfin+dmincyc+1)                   ;Dans le vecteur 'anmincyc' (cycle 16 => 20, cycle 19 => 23, cycle 21 => 25, etc.)
durcycle = anmincyc(ncyc) - anmincyc(0)
temp=anmincyc
FOR i=1,nrep-1 DO temp=[temp,anmincyc(1:ncyc)+i*durcycle]
anmincyc=temp

ncycles=N_ELEMENTS(anmincyc)-1
timei = anmincyc(0)
timeo = anmincyc(ncycles)
time  = FINDGEN(nt+1)/nt * (timeo-timei) + timei

;---- Set simulation time vector -----
tsim = FINDGEN(nt+1)/nt * (to-ti) + ti
simmincyc = (anmincyc-timei)/(timeo-timei) * (to-ti) + ti

;---- Find boundaries of layer and top -----

jbr=0
jbp=[0,0]
FOR j=0,nely-1 DO BEGIN
   IF (yy(j+1) GT rbr    AND yy(j) LE rbr)    THEN jbr=j
   IF (yy(j+1) GT rbp(0) AND yy(j) LE rbp(0)) THEN jbp(0)=j
   IF (yy(j+1) GT rbp(1) AND yy(j) LE rbp(1)) THEN jbp(1)=j
ENDFOR
njbp=jbp(1)-jbp(0)+1
njbr=1

;---- Sunspot number ---------------------------------------------------

;CLOSE,3 & OPENR,3,STRMID(datafile,0,STRLEN(datafile)-STRLEN('.dat'))+'.ssn'
CLOSE,3 & OPENR,3,'../0-data/3-arnaud/donnees/SIDC/monthssn_01a23.ssn'

nmax = 10000
ssnread = FLTARR(4)
ssntime = FLTARR(nmax)
ssnt    = FLTARR(nmax)
ssnsmth = FLTARR(nmax)
issn=0L
WHILE (NOT EOF(3)) DO BEGIN
   READF,3,ssnread
;  IF ((ssnread(1) GE timei) AND (ssnread(1) LE timeo)) THEN BEGIN
      ssntime(issn) = ssnread(1)
      ssnt   (issn) =(ssnread(1)-timei)/(timeo-timei)*nt
      ssnsmth(issn) = ssnread(3)
      issn=issn+1
; ENDIF
ENDWHILE
CLOSE,3
nssn=issn
PRINT,nssn,' SSN read'
ssntime=ssntime(0:nssn-1)
ssnt   =ssnt   (0:nssn-1)
ssnsmth=ssnsmth(0:nssn-1)

ssnsmthmax= MAX(ssnsmth)

;;---- Interpolation on temporal grid -----
;ssne    = FLTARR(nt+1)
;
;issn=0L
;FOR itt=0,nt DO BEGIN
;   WHILE ((issn+1 LT nssn-1) AND (itt GT ssnt(issn+1))) DO issn=issn+1
;   a = (ssnsmth(issn+1)-ssnsmth(issn))/(ssnt(issn+1)-ssnt(issn))
;   b = ssnsmth(issn)-a*ssnt(issn)
;   ssne(itt) = b + a*itt
;;  WHILE ((issn+1 LT nssn-1) AND (time(itt) GT ssntime(issn+1))) DO issn=issn+1
;;  a = (ssnsmth(issn+1)-ssnsmth(issn))/(ssntime(issn+1)-ssntime(issn))
;;  b = ssnsmth(issn)-a*ssntime(issn)
;;  ssne(itt) = b + a*time(itt)
;ENDFOR

;ssnemax   = MAX(ssne(itfit[0]:itfit[1]))

;---- FFT on SSN evolution ---------------------------------------------

ssnfft0 = FFT(ssnsmth/ssnsmthmax,-1)

nsample = nssn
dtsample= MEAN(ssntime[1:nssn-1]-ssntime[0:nssn-2])

ssnfreq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   ssnfreq = [0., ssnfreq, nsample/2, -nsample/2+ssnfreq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   ssnfreq = [0., ssnfreq, -(nsample/2+1)+ssnfreq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nssnfreq = nsample/2
ssnfreq = ssnfreq[0:nssnfreq]

ssnfft0amp = ssnfft0amp + 1./(nbfitfac*nbldseed) * 2*ABS(ssnfft0[0:nssnfreq])^2
ssnfft0phs = ATAN(IMAGINARY(ssnfft0[0:nssnfreq]),REAL_PART(ssnfft0[0:nssnfreq]))   ;ATAN(ssnfft0[0:nssnfreq],/PHASE)

ssnfft0ampmax = MAX(ssnfft0amp,issnfft0max)

;---- Gaussian fit -----

ifreqfit = WHERE(ssnfreq GE 0.,/NULL)

;ssnfft0amppar = [ssnfft0ampmax,ssnfreq[issnfft0max],0.001]
ssnfft0amppar = [1.e-2,0.05,0.001]
ssnfft0ampfit = GAUSSFIT(ssnfreq[ifreqfit], ssnfft0amp[ifreqfit], ssnfft0amppar, ESTIMATES=ssnfft0amppar, CHISQ=ssnfft0ampchi, NTERMS=3)
ssnfreqq = FINDGEN(100001)/100000.*(ssnfreq[nssnfreq]-ssnfreq[0])+ssnfreq[0]
ssnfft0ampfitt= ssnfft0amppar[0]*EXP(-(ssnfreqq-ssnfft0amppar[1])^2/(2.*ssnfft0amppar[2]^2))

;---- 'Signed' SSN (inverted every even cycle) -------------------------

ssssmth = ssnsmth
icyc=0
WHILE (ssntime[0] GT anmincyc0[icyc+1]) DO icyc=icyc+1
FOR issn=0,nssn-1 DO BEGIN
   IF ((icyc+1)/2 EQ (icyc+1)/2.) THEN ssssmth[issn]=-ssssmth[issn]
   IF (ssntime[issn] GT anmincyc0[icyc+1]) THEN icyc=icyc+1
ENDFOR

ssssmthmax= MAX(ABS(ssssmth))

;---- FFT on signed SSN evolution --------------------------------------

sssfft0 = FFT(ssssmth/ssssmthmax,-1)

nsample = nssn
dtsample= MEAN(ssntime[1:nssn-1]-ssntime[0:nssn-2])

ssnfreq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   ssnfreq = [0., ssnfreq, nsample/2, -nsample/2+ssnfreq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   ssnfreq = [0., ssnfreq, -(nsample/2+1)+ssnfreq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nssnfreq = nsample/2
ssnfreq = ssnfreq[0:nssnfreq]

sssfft0amp = sssfft0amp + 1./(nbfitfac*nbldseed) * 2*ABS(sssfft0[0:nssnfreq])^2
sssfft0phs = ATAN(IMAGINARY(sssfft0[0:nssnfreq]),REAL_PART(sssfft0[0:nssnfreq]))   ;ATAN(sssfft0[0:nssnfreq],/PHASE)

sssfft0ampmax = MAX(sssfft0amp,isssfft0max)

;---- Gaussian fit -----

ifreqfit = WHERE(ssnfreq GE 0.,/NULL)

;sssfft0amppar = [sssfft0ampmax,ssnfreq[isssfft0max],0.001]
sssfft0amppar = [1.e-2,0.05,0.001]
sssfft0ampfit = GAUSSFIT(ssnfreq[ifreqfit], sssfft0amp[ifreqfit], sssfft0amppar, ESTIMATES=sssfft0amppar, CHISQ=sssfft0ampchi, NTERMS=3)
ssnfreqq = FINDGEN(100001)/100000.*(ssnfreq[nssnfreq]-ssnfreq[0])+ssnfreq[0]
sssfft0ampfitt= sssfft0amppar[0]*EXP(-(ssnfreqq-sssfft0amppar[1])^2/(2.*sssfft0amppar[2]^2))

;---- Open and read database -------------------------------------------

CLOSE,2 & OPENR,2,'../'+bmrfile

text  = ''
nmax  = 9999999L
dur   = 0.
nr    = 0L & cycler=0 & signcycler=0 & yearr=0 & moisr=0 & jourr=0 & hrr=0 & minr=0 & tempsr=0.D0 & fluxr=0. & signr=0 & tht1r=0. & lon1r=0. & tht2r=0. & lon2r=0.
bfly0 = FLTARR(nt+1,nelx+1) + 0.
tday0 = FLTARR(nmax)
flux0 = FLTARR(nmax)
;sign0 = INTARR(nmax)
tht10 = FLTARR(nmax)
lon10 = FLTARR(nmax)
tht20 = FLTARR(nmax)
lon20 = FLTARR(nmax)

;---- Read header -----
READF,2,text
READF,2,dur
READF,2,text

;---- Read file -----
nbmr=0L
fluxtot=0.
nbmrnorth=0L
nbmrnorthwrong=0L
fluxnorth=0.
fluxnorthwrong=0.
nbmrsouth=0L
nbmrsouthwrong=0L
fluxsouth=0.
fluxsouthwrong=0.
WHILE ((nbmr LT nmax) AND (NOT EOF(2)))  DO BEGIN
;WHILE (NOT EOF(2)) DO BEGIN
   nbmr=nbmr+1
   
   READF,2,text
   nr        =   LONG(STRMID(text, 0, 7))
   cycler    =   LONG(STRMID(text, 7, 6))
   signcycler=   LONG(STRMID(text,13, 5))
   yearr     =   LONG(STRMID(text,18, 7))
   moisr     =   LONG(STRMID(text,25, 2))
   jourr     =   LONG(STRMID(text,27, 2))
   hrr       =   LONG(STRMID(text,29, 2))
   minr      =   LONG(STRMID(text,31, 2))
   tempsr    = DOUBLE(STRMID(text,33,13))
   fluxr     =  FLOAT(STRMID(text,46,14))
   signr     =   LONG(STRMID(text,60, 6))
   tht1r     =  FLOAT(STRMID(text,66, 7))
   lon1r     =  FLOAT(STRMID(text,73, 7))
   tht2r     =  FLOAT(STRMID(text,80, 7))
   lon2r     =  FLOAT(STRMID(text,87, 7))
   
   IF (nr NE nbmr) THEN PRINT,'* Missing BMR(s):',nbmr,' to',nr-1
   nbmr=nr
   
   tday0(nbmr-1) = tempsr
   flux0(nbmr-1) = fluxr
;  sign0(nbmr-1) = signer
   tht10(nbmr-1) = tht1r
   lon10(nbmr-1) = lon1r
   tht20(nbmr-1) = tht2r
   lon20(nbmr-1) = lon2r
   
   tht0=(tht1r+tht2r)/2.
   i =LONG((1.-tht0/180.)*nelx+0.5)             ;regular mesh for theta, in decreasing order (calculations with mu=cos(theta) in increasing order)
;  i =LONG((1.+COS(tht0*pi180))/2.*nelx+0.5)    ;WRONG: suppose a regular mesh for mu=cos(theta), in increasing order
   it=LONG(tempsr/dur*nt+0.5)
   
   fluxtot=fluxtot+fluxr
   
   IF (tht0 LE 90.) THEN BEGIN
      signbfly =-signcycler
      nbmrnorth=nbmrnorth+1
      fluxnorth=fluxnorth+fluxr
      IF (signbfly NE signr) THEN BEGIN
         nbmrnorthwrong=nbmrnorthwrong+1
         fluxnorthwrong=fluxnorthwrong+fluxr
      ENDIF
   ENDIF ELSE BEGIN
      signbfly = signcycler
      nbmrsouth=nbmrsouth+1
      fluxsouth=fluxsouth+fluxr
      IF (signbfly NE signr) THEN BEGIN
         nbmrsouthwrong=nbmrsouthwrong+1
         fluxsouthwrong=fluxsouthwrong+fluxr
      ENDIF
   ENDELSE
   
;  bfly0(it,i) = bfly0(it,i) + 1.*signbfly
   bfly0(it,i) = bfly0(it,i) + 1.*signr
   
;  IF (tempsr/365. GT 90.) THEN BEGIN
;     PRINT,'nbmr,tempsr/365.,fluxr,tht1r,lon1r,tht2r,lon2r=',nbmr,tempsr/365.,fluxr,tht1r,lon1r,tht2r,lon2r
;     READ,text
;  ENDIF
ENDWHILE
CLOSE,2

PRINT,nbmr,' BMR read'
PRINT,nbmrnorth,' BMR in north hemisphere'
PRINT,nbmrnorthwrong,' BMR in north hemisphere with wrong polarity'
PRINT,nbmrsouth,' BMR in south hemisphere'
PRINT,nbmrsouthwrong,' BMR in south hemisphere with wrong polarity'
PRINT,fluxtot       *fluxfac,' Mx'
PRINT,fluxnorth     *fluxfac,' Mx in north hemisphere'
PRINT,fluxnorthwrong*fluxfac,' Mx in north hemisphere with wrong polarity'
PRINT,fluxsouth     *fluxfac,' Mx in south hemisphere'
PRINT,fluxsouthwrong*fluxfac,' Mx in south hemisphere with wrong polarity'

tday0 = tday0[0:nbmr-1]
flux0 = flux0[0:nbmr-1]
;sign0 = sign0[0:nbmr-1]
tht10 = tht10[0:nbmr-1]
lon10 = lon10[0:nbmr-1]
tht20 = tht20[0:nbmr-1]
lon20 = lon20[0:nbmr-1]

logflux0= ALOG10(flux0*fluxfac)

sintht10= SIN(pi180*tht10)
sintht20= SIN(pi180*tht20)
tht00   = (tht10+tht20)/2.
sintht00= SIN(pi180*tht00)
;costht00= COS(pi180*tht00)
lat00   = 90.-tht00
abslat00= ABS(lat00)
;lon00   = (lon10+lon20)/2. MOD 360.

dtht0   = tht10-tht20
;dtht0x  = dtht0
;FOR n=0,nwang-1 DO BEGIN
;   IF (tht10(n) GT 90.) THEN dtht0x(n)=-dtht0x(n)
;ENDFOR
dlon0   = lon10-lon20
dlon0(WHERE(dlon0 GT 180.)) = dlon0(WHERE(dlon0 GT 180.)) - 360.
dlon0(WHERE(dlon0 LT-180.)) = dlon0(WHERE(dlon0 LT-180.)) + 360.
dlon0(WHERE(dlon0 EQ   0.)) = 0.01

;;delta0 = SQRT(dtht0^2+(sintht00*dlon0)^2)
delta0  = SQRT(dtht0^2+sintht10*sintht20*(dlon0)^2)
logdelta0=ALOG10(delta0)

;;tilt0  = 1./pi180*ASIN(dtht0/delta0)
;;tilt0  = 1./pi180*ACOS((dlon0*sintht00)/delta0) & tilt0(WHERE(dtht0 LT 0.)) = - tilt0(WHERE(dtht0 LT 0.))
tilt0   = 1./pi180*ATAN(dtht0/(sintht00*dlon0))
;sintilt0= SIN(pi180*tilt0)
abstilt0= tilt0
abstilt0[WHERE(lat00 LT 0.)] = -abstilt0[WHERE(lat00 LT 0.)]

flux0max    = MAX(flux0)
delta0max   = MAX(delta0)

;---- Smooth bfly -----

smthcrit = 3/16.                     ; delta_t = smthcrit * delta_x, used for smoothing... (must be < 1/4)

bfly=bfly0

FOR i=1,nsmth DO BEGIN
   obfly = bfly
   FOR itt=1,nt-1 DO BEGIN
      FOR j=1,nelx-1 DO BEGIN
         bfly(itt,j) = obfly(itt,j) + smthcrit*(-4*obfly(itt,j)+obfly(itt-1,j)+obfly(itt+1,j)+obfly(itt,j-1)+obfly(itt,j+1))
      ENDFOR
   ENDFOR
   FOR itt=1,nt-1 DO BEGIN
      bfly(itt,0)    = bfly(itt,1)
      bfly(itt,nelx) = bfly(itt,nelx-1)
   ENDFOR
   FOR j=0,nelx DO BEGIN
      bfly(0,j)      = bfly(1,j)
      bfly(nt,j)     = bfly(nt-1,j)
   ENDFOR
ENDFOR

;---- Bfly stats -----

bfly0max= MAX(ABS(bfly0))

bflyabs = ABS(bfly)
bflymax = MAX(bflyabs)

latmin=FLTARR(nt+1)
latmax=FLTARR(nt+1)
FOR it=0,nt DO BEGIN
   i=0
   WHILE (xxan(i) LT 0. AND bfly(it,i) EQ 0.) DO i=i+1
   latmin(it)=xxan(i)
   i=nelx
   WHILE (xxan(i) GT 0. AND bfly(it,i) EQ 0.) DO i=i-1
   latmax(it)=xxan(i)
ENDFOR

;---- Loop over surface timesteps: Read surfbr output ------------------
;...
;READU,6,...

;---- Loop over timesteps: Read simulation output, calculate magnetic --
;---- energy, axial dipole, etc. ---------------------------------------

ilstep=LONARR(1)
isstep=LONARR(1)
vdum  = FLTARR(5)
aquad = FLTARR(nelx+1,nely+1)
bphii = FLTARR(nelx+1,nely+1)

cost  = FLTARR(nt+1,nelx+1)
thtt  = FLTARR(nt+1,nelx+1)
delmut= FLTARR(nt+1,nelx)

brrr  = DBLARR(nt+1,nelx+1,nely+1)
btht  = DBLARR(nt+1,nelx+1,nely+1)
bphi  = FLTARR(nt+1,nelx+1,nely+1)
b_r   = DBLARR(nt+1,nelx+1)
b_y   = DBLARR(nt+1,nelx+1)
a_q   = DBLARR(nt+1,nelx+1)
ebtot = DBLARR(nt+1)                         ;Energy of total magnetic field (read from file)
ebttot= DBLARR(nt+1)                         ;Energy of toroidal component only (read from file)
ebto2 = DBLARR(nt+1)                         ;Energy of total magnetic field
ebtto2= DBLARR(nt+1)                         ;Energy of toroidal component only
twst  = DBLARR(nt+1)                         ;"Twist" = toroidal^2*aphi
ebuz  = DBLARR(nt+1)                         ;Energy of total magnetic field in optimization zone (r/R=[0.68,0.70])
ebtuz = DBLARR(nt+1)                         ;...
twstuz= DBLARR(nt+1)
bnorth= DBLARR(nt+1)
bsouth= DBLARR(nt+1)
daxial= DBLARR(nt+1)

ifr = -1
FOR it=0,nt DO BEGIN
   
   cost(it,*)   = xx
   thtt(it,*)   = xxa
   delmut(it,*) = delmu
   
   READU,1,ilstep,isstep
   READU,1,vdum & ebtot(it)=vdum(1) & ebttot(it)=vdum(2)        ; & time(it)=vdum(0)
   READU,1,aquad
   READU,1,bphii
   
   bphi(it,*,*) = bphii
   
;- Compute Br=-1/r*d(A*sin(theta))/dmu    ( mu=cos(theta) )
   FOR i=1,nelx-1 DO BEGIN
      sinthp = sinxx(i+1) & IF(sinthp LT 1.e-3) THEN sinthp=1.e-3
      sinthm = sinxx(i-1) & IF(sinthm LT 1.e-3) THEN sinthm=1.e-3
      FOR j=0,nely DO BEGIN
         brrr(it,i,j) = -1./yy(j)*(aquad(i+1,j)*sinthp-aquad(i-1,j)*sinthm)/(delmu(i-1)+delmu(i))
      ENDFOR
   ENDFOR
;- Patch for pole
   sinthp = sinxx(1)
   sinthm = sinxx(0) & IF(sinthm LT 1.e-3) THEN sinthm=1.e-3
   FOR j=0,nely DO BEGIN
      brrr(it,0,j) = -1./yy(j)*(aquad(1,j)*sinthp-aquad(0,j)*sinthm)/delmu(0)
   ENDFOR
;- Patch for equator / other pole
   sinthp = sinxx(nelx) & IF(sinthp LT 1.e-3) THEN sinthp=1.e-3
   sinthm = sinxx(nelx-1)
   FOR j=0,nely DO BEGIN
      brrr(it,nelx,j) = -1./yy(j)*(aquad(nelx,j)*sinthp-aquad(nelx-1,j)*sinthm)/delmu(nelx-1)
   ENDFOR
   
;- Compute Btht=-1/r*d(A*r)/dr
   FOR j=1,nely-1 DO BEGIN
      FOR i=0,nelx DO BEGIN
         btht(it,i,j) = -1./yy(j)*(aquad(i,j+1)*yy(j+1)-aquad(i,j-1)*yy(j-1))/(delr(j-1)+delr(j))
      ENDFOR
   ENDFOR
;- Patch for bottom and top layers
   FOR i=0,nelx DO BEGIN
      btht(it,i,0) = -1./yy(0)*(aquad(i,1)*yy(1)-aquad(i,0)*yy(0))/delr(0)
   ENDFOR
   FOR i=0,nelx DO BEGIN
      btht(it,i,nely) = -1./yy(nely)*(aquad(i,nely)*yy(nely)-aquad(i,nely-1)*yy(nely-1))/delr(nely-1)
   ENDFOR
   
;- Extract field components at desired depths -----
   FOR i=0,nelx DO BEGIN
      b_r(it,i) = brrr(it,i,jbr)
      FOR j=jbp(0),jbp(1) DO BEGIN
         b_y(it,i) = b_y(it,i) + bphi(it,i,j)/njbp
      ENDFOR
      FOR j=jbp(0),jbp(1) DO BEGIN
         a_q(it,i) = a_q(it,i) + aquad(i,j)/njbp
      ENDFOR
   ENDFOR
   
;- Magnetic energy density and "twist" -----
   
   deb  = REFORM(brrr(it,*,*)^2+btht(it,*,*)^2+bphi(it,*,*)^2)*yyy^2
   debt = (              REFORM(bphi(it,*,*))^2)*yyy^2
   dtwst= (ABS(aquad)*   REFORM(bphi(it,*,*))^2)*yyy^2
   
   ebto2 (it) = 1./4.*TOTAL((deb  (0:nelx-1,0:nely-1)+deb  (1:nelx,0:nely-1)+deb  (0:nelx-1,1:nely)+deb  (1:nelx,1:nely))/4.*(delmu#delr))
   ebtot (it) = ebtot (it)
   ebtto2(it) = 1./4.*TOTAL((debt (0:nelx-1,0:nely-1)+debt (1:nelx,0:nely-1)+debt (0:nelx-1,1:nely)+debt (1:nelx,1:nely))/4.*(delmu#delr))
   ebttot(it) = ebttot(it)
   twst  (it) = 1./4.*TOTAL((dtwst(0:nelx-1,0:nely-1)+dtwst(1:nelx,0:nely-1)+dtwst(0:nelx-1,1:nely)+dtwst(1:nelx,1:nely))/4.*(delmu#delr))
   
   ebuz  (it) = 1./4.*TOTAL((deb  (0:nelx-1,jbp(0):jbp(1))+deb  (1:nelx,jbp(0):jbp(1))+deb  (0:nelx-1,jbp(0)+1:jbp(1)+1)+deb  (1:nelx,jbp(0)+1:jbp(1)+1))/4.*(delmu#delr(jbp(0):jbp(1))))
   ebtuz (it) = 1./4.*TOTAL((debt (0:nelx-1,jbp(0):jbp(1))+debt (1:nelx,jbp(0):jbp(1))+debt (0:nelx-1,jbp(0)+1:jbp(1)+1)+debt (1:nelx,jbp(0)+1:jbp(1)+1))/4.*(delmu#delr(jbp(0):jbp(1))))
   twstuz(it) = 1./4.*TOTAL((dtwst(0:nelx-1,jbp(0):jbp(1))+dtwst(1:nelx,jbp(0):jbp(1))+dtwst(0:nelx-1,jbp(0)+1:jbp(1)+1)+dtwst(1:nelx,jbp(0)+1:jbp(1)+1))/4.*(delmu#delr(jbp(0):jbp(1))))
   
;- Surface flux -----
   bsouth(it) = 1./intsouth * INT_TABULATED(xx(0:jpole)        ,b_r(it,0:jpole)        )
   bnorth(it) = 1./intnorth * INT_TABULATED(xx(nelx-jpole:nelx),b_r(it,nelx-jpole:nelx))
;  bsouth(it) = 1./intsouth * INT_TABULATED(REVERSE(xxa(0:jpole)        ),REVERSE(b_r(it,0:jpole)        *sinxx(0:jpole)        ))
;  bnorth(it) = 1./intnorth * INT_TABULATED(REVERSE(xxa(nelx-jpole:nelx)),REVERSE(b_r(it,nelx-jpole:nelx)*sinxx(nelx-jpole:nelx)))
   
   daxial(it) =   3./4./!PI * INT_TABULATED(xx(0:nelx),2*!PI*b_r(it,0:nelx)*xx(0:nelx))
;  daxial(it) =   3./4./!PI * INT_TABULATED(REVERSE(xxa(0:nelx)),REVERSE(2*!PI*b_r(it,0:nelx)*xx(0:nelx)*sinxx(0:nelx)))

ENDFOR

CLOSE,1

brrrmax  = MAX(ABS(brrr[itfit[0]:itfit[1],*,*]))
bthtmax  = MAX(ABS(btht[itfit[0]:itfit[1],*,*]))
bphimax  = MAX(ABS(bphi[itfit[0]:itfit[1],*,*]))
brmax    = MAX(ABS(b_r(itfit[0]:itfit[1],*)))
bymax    = MAX(ABS(b_y(itfit[0]:itfit[1],*)))
aqmax    = MAX(ABS(a_q(itfit[0]:itfit[1],*)))
ebtotmax = MAX(ebtot(itfit[0]:itfit[1]))
ebto2max = MAX(ebto2(itfit[0]:itfit[1]))
ebttotmax= MAX(ebttot(itfit[0]:itfit[1]))
ebtto2max= MAX(ebtto2(itfit[0]:itfit[1]))
ebuzmax  = MAX(ebuz(itfit[0]:itfit[1]))
ebtuzmax = MAX(ebtuz(itfit[0]:itfit[1]))
twstmax  = MAX(twst(itfit[0]:itfit[1]))
twstuzmax= MAX(twstuz(itfit[0]:itfit[1]))

daxialmax= MAX(ABS(daxial(itfit[0]:itfit[1])))
bcapmax  = MAX(ABS([bnorth(itfit[0]:itfit[1]),bsouth(itfit[0]:itfit[1])]))
bpolemax = MAX(ABS([b_r(itfit[0]:itfit[1],0),b_r(itfit[0]:itfit[1],nelx)]))

;---- Fit to axial dipole moment trend ---------------------------------

;daxnsmthpcyc= 10
daxnsmth    = 21                                                        ;nt/ncyc/nrep/daxnsmthpcyc+1

daxialsmth = SMOOTH(daxial,daxnsmth,/EDGE_TRUNCATE)

;etc.

;---- FFT on axial dipole moment evolution -----------------------------

dafft0 = FFT(daxial/daxialmax,-1)

nsample = nt+1
dtsample= time[1]-time[0]                                               ;IF (time[2]-time[1] NE dtsample) THEN STOP,'! Irregular time steps in FFT...'

freq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   freq = [0., freq, nsample/2, -nsample/2+freq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   freq = [0., freq, -(nsample/2+1)+freq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nfreq = nsample/2
freq = freq[0:nfreq]

dafft0amp = dafft0amp + 1./(nbfitfac*nbldseed) * 2*ABS(dafft0[0:nfreq])^2
dafft0phs = ATAN(IMAGINARY(dafft0[0:nfreq]),REAL_PART(dafft0[0:nfreq]))   ;ATAN(dafft0[0:nfreq],/PHASE)

dafft0ampmax = MAX(dafft0amp,idafft0max)

;---- Gaussian fit -----

ifreqfit = WHERE(freq GE 0.,/NULL)

;dafft0amppar = [dafft0ampmax,freq[idafft0max],0.001]
dafft0amppar = [1.e-2,0.05,0.001]
dafft0ampfit = GAUSSFIT(freq[ifreqfit], dafft0amp[ifreqfit], dafft0amppar, ESTIMATES=dafft0amppar, CHISQ=dafft0ampchi, NTERMS=3)
freqq = FINDGEN(100001)/100000.*(freq[nfreq]-freq[0])+freq[0]
dafft0ampfitt= dafft0amppar[0]*EXP(-(freqq-dafft0amppar[1])^2/(2.*dafft0amppar[2]^2))

;---- Axial dipole moment minima and maxima ----------------------------

ncycmax     = 1000
itdmaxcyc   = FLTARR(ncycmax)
daxialmaxcyc= FLTARR(ncycmax)
itdmincyc   = FLTARR(ncycmax)
daxialmincyc= FLTARR(ncycmax)

icyc=0
itdmaxcyc   [icyc] = itfit[0]
daxialmaxcyc[icyc] = ABS(daxialsmth[itfit[0]])
itdmincyc   [icyc] = itfit[0]
daxialmincyc[icyc] = ABS(daxialsmth[itfit[0]])
FOR it=itfit[0]+1,itfit[1] DO BEGIN
   IF (ABS(daxialsmth[it]) GT daxialmaxcyc[icyc]) THEN BEGIN            ;Find maximum for cycle icyc
      itdmaxcyc   [icyc] = it
      daxialmaxcyc[icyc] = ABS(daxialsmth[it])
   ENDIF ELSE BEGIN
   IF ((daxialmaxcyc[icyc] EQ daxialmincyc[icyc]) $                     ;While still in the descending slope
   AND (ABS(daxialsmth[it]) LT daxialmincyc[icyc])) THEN BEGIN          ;Find minimum for cycle icyc
      itdmaxcyc   [icyc] = it
      daxialmaxcyc[icyc] = ABS(daxialsmth[it])
      itdmincyc   [icyc] = it
      daxialmincyc[icyc] = ABS(daxialsmth[it])
   ENDIF
   IF ((daxialmaxcyc[icyc] GT daxialmincyc[icyc]) $                     ;If a maximum has been found
   AND (ABS(daxialsmth[it]) LT 0.90*daxialmaxcyc[icyc])) THEN BEGIN     ;And if passing under 0.90*this max
      PRINT,'icyc,time[itdmaxcyc[icyc]]-timei,daxialmaxcyc=', $
             icyc,time[itdmaxcyc[icyc]]-timei,daxialmaxcyc[icyc]
      icyc = icyc + 1L                                                  ;Change cycle (and save preceding max and min)
      itdmaxcyc   [icyc] = it
      daxialmaxcyc[icyc] = ABS(daxialsmth[it])
      itdmincyc   [icyc] = it
      daxialmincyc[icyc] = ABS(daxialsmth[it])
   ENDIF
   ENDELSE
ENDFOR
ndmaxcyc = icyc

IF (ndmaxcyc GE 1) THEN BEGIN
   itdmaxcyc    = itdmaxcyc   [0:ndmaxcyc-1]
   daxialmaxcyc = daxialmaxcyc[0:ndmaxcyc-1]
   itdmincyc    = itdmincyc   [0:ndmaxcyc-1]
   daxialmincyc = daxialmincyc[0:ndmaxcyc-1]
ENDIF

;---- Fit to axial dipole maxima -----

IF (ndmaxcyc GE 2) THEN BEGIN
   daxialmaxparr= POLY_FIT((time[itdmaxcyc]-time(itfit[0])), ALOG(daxialmaxcyc), 1, YFIT=daxialmaxfitc) & daxialmaxfitc=EXP(daxialmaxfitc)
   ;daxialmaxfitt= EXP(daxialmaxparr[0] + daxialmaxparr[1]*(time-time(itfit[0])))
   daxialmaxpar = [EXP(daxialmaxparr[0]),-1./daxialmaxparr[1],0.]
   daxialmaxfit = daxialmaxpar[0]*EXP(-(time-time(itfit[0]))/daxialmaxpar[1]) + daxialmaxpar[2]
   daxialmaxchi = SQRT(TOTAL((daxialmaxcyc-daxialmaxfitc)^2)/ndmaxcyc)
ENDIF

;---- Axial dipole moment maxima distribution --------------------------

ndaxialmaxcycbin = 30
mindaxialmaxcyc  = 0.
maxdaxialmaxcyc  = 30.

daxialmaxcycbinn= FINDGEN(ndaxialmaxcycbin+1)/ndaxialmaxcycbin*(maxdaxialmaxcyc-mindaxialmaxcyc) + mindaxialmaxcyc
daxialmaxcycbin = (daxialmaxcycbinn(0:ndaxialmaxcycbin-1)+daxialmaxcycbinn(1:ndaxialmaxcycbin))/2.

histdaxialmaxcyc=HISTOGRAM(daxialmaxcyc,MIN=mindaxialmaxcyc,MAX=maxdaxialmaxcyc,NBINS=ndaxialmaxcycbin+1,LOCATIONS=locdaxialmaxcyc) & histdaxialmaxcyc=histdaxialmaxcyc[0:ndaxialmaxcycbin-1]

;histdaxialmaxcyc(WHERE(histdaxialmaxcyc EQ 0.)) = 1.e-2                   ; Éviter les valeurs de 0
;histdaxialmaxcyc=histdaxialmaxcyc/(10^(daxialmaxcycbin-mindaxialmaxcyc))  ; Ponderation contre l'augmentation de la largeur des bins en daxialmaxcyc
 histdaxialmaxcyc=histdaxialmaxcyc/TOTAL(histdaxialmaxcyc)                 ; Normalisation
;histdaxialmaxcyc=histdaxialmaxcyc*ndaxialmaxcycbin/20                     ; Normalisation to nbin=20
;histdaxialmaxcyc=ALOG10(histdaxialmaxcyc)                                 ; Échelle verticale log

;---- FFT on magnetic energy evolution (0th pass) ----------------------

ebfft0 = FFT(ebtot/ebtotmax*2.-1.,-1)

nsample = nt+1
dtsample= time[1]-time[0]                                               ;IF (time[2]-time[1] NE dtsample) THEN STOP,'! Irregular time steps in FFT...'

freq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   freq = [0., freq, nsample/2, -nsample/2+freq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   freq = [0., freq, -(nsample/2+1)+freq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nfreq = nsample/2
freq = freq[0:nfreq]

ebfft0amp = ebfft0amp + 1./(nbfitfac*nbldseed) * 2*ABS(ebfft0[0:nfreq])^2
ebfft0phs = ATAN(IMAGINARY(ebfft0[0:nfreq]),REAL_PART(ebfft0[0:nfreq]))   ;ATAN(ebfft0[0:nfreq],/PHASE)

ebfft0ampmax = MAX(ebfft0amp,iebfft0max)

;---- Gaussian fit -----

ifreqfit = WHERE(freq GE 0.08,/NULL)

;ebfft0amppar = [ebfft0ampmax,freq[iebfft0max],0.001]
ebfft0amppar = [1.e-2,0.10,0.001]
ebfft0ampfit = GAUSSFIT(freq[ifreqfit], ebfft0amp[ifreqfit], ebfft0amppar, ESTIMATES=ebfft0amppar, CHISQ=ebfft0ampchi, NTERMS=3)
freqq = FINDGEN(100001)/100000.*(freq[nfreq]-freq[0])+freq[0]
ebfft0ampfitt= ebfft0amppar[0]*EXP(-(freqq-ebfft0amppar[1])^2/(2.*ebfft0amppar[2]^2))

;---- Fit to energy trend (1st pass) -----------------------------------

;ebnsmthpcyc= 10
ebnsmth    = 21                                                         ;nt/ncyc/nrep/ebnsmthpcyc+1
ebtotsmth  = SMOOTH(ebtot,ebnsmth,/EDGE_TRUNCATE)

ebtotgen0parr= POLY_FIT((time-time[itfit[0]]), ALOG(ebtotsmth), 1, YFIT=ebtotgen0fitc) & ebtotgen0fitc=EXP(ebtotgen0fitc)
ebtotgen0par = [EXP(ebtotgen0parr[0]),1./ebtotgen0parr[1],0.]
ebtotgen0fit = ebtotgen0par[0]*EXP((time-time(itfit[0]))/ebtotgen0par[1]) + ebtotgen0par[2]
ebtotgen0chi = SQRT(TOTAL((ebtotsmth-ebtotgen0fitc)^2)/(nt+1))

ebdetrend0 = ebtotsmth/ebtotgen0fit

;---- FFT on detrended magnetic energy evolution (1st pass) ------------

ebfft1 = FFT(ebdetrend0-1.,-1)

nsample = nt+1
dtsample= time[1]-time[0]                                               ;IF (time[2]-time[1] NE dtsample) THEN STOP,'! Irregular time steps in FFT...'

freq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   freq = [0., freq, nsample/2, -nsample/2+freq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   freq = [0., freq, -(nsample/2+1)+freq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nfreq = nsample/2
freq = freq[0:nfreq]

ebfft1amp = ebfft1amp + 1./(nbfitfac*nbldseed) * 2*ABS(ebfft1[0:nfreq])^2
ebfft1phs = ATAN(IMAGINARY(ebfft1[0:nfreq]),REAL_PART(ebfft1[0:nfreq]))   ;ATAN(ebfft1[0:nfreq],/PHASE)

ebfft1ampmax = MAX(ebfft1amp,iebfft1max)

;---- Gaussian fit -----

ifreqfit = WHERE(freq GE 0.08,/NULL)

;;ebfft1amppar = [ebfft1ampmax,freq[iebfft1max],0.001]
ebfft1amppar = [1.e-2,0.10,0.001]
ebfft1ampfit = GAUSSFIT(freq[ifreqfit], ebfft1amp[ifreqfit], ebfft1amppar, ESTIMATES=ebfft1amppar, CHISQ=ebfft1ampchi, NTERMS=3)
;ebfft1amppar = [ebfft1ampmax,freq[iebfft1max],0.001,0.,0.,0.]
;ebfft1ampfit = CURVEFIT(freq[ifreqfit], ebfft1amp[ifreqfit], freq[ifreqfit]*0.+1., ebfft1amppar, CHISQ=ebfft1ampchi, FITA=[1,0,1,0,0,0], FUNCTION_NAME=FUNCT)
freqq = FINDGEN(100001)/100000.*(freq[nfreq]-freq[0])+freq[0]
ebfft1ampfitt= ebfft1amppar[0]*EXP(-(freqq-ebfft1amppar[1])^2/(2.*ebfft1amppar[2]^2))

;---- Energy minima and maxima -----------------------------------------

ncycmax    = 1000
itemaxcyc  = LONARR(ncycmax)
ebtotmaxcyc= FLTARR(ncycmax)
itemincyc  = LONARR(ncycmax)
ebtotmincyc= FLTARR(ncycmax)

icyc=0
itemaxcyc  [icyc] = itfit[0]
ebtotmaxcyc[icyc] = ebdetrend0[itfit[0]]
itemincyc  [icyc] = itfit[0]
ebtotmincyc[icyc] = ebdetrend0[itfit[0]]
FOR it=itfit[0]+1,itfit[1] DO BEGIN
   IF (ebdetrend0[it] GT ebtotmaxcyc[icyc]) THEN BEGIN                   ;Find maximum for cycle icyc
      itemaxcyc  [icyc] = it
      ebtotmaxcyc[icyc] = ebdetrend0[it]
   ENDIF ELSE BEGIN
   IF ((ebtotmaxcyc[icyc] EQ ebtotmincyc[icyc]) $                       ;While still in the descending slope
   AND (ebdetrend0[it] LT ebtotmincyc[icyc])) THEN BEGIN                 ;Find minimum for cycle icyc
      itemaxcyc  [icyc] = it
      ebtotmaxcyc[icyc] = ebdetrend0[it]
      itemincyc  [icyc] = it
      ebtotmincyc[icyc] = ebdetrend0[it]
   ENDIF
   IF ((ebtotmaxcyc[icyc] GT ebtotmincyc[icyc]) $                       ;If a maximum has been found
   AND (ebdetrend0[it] LT 0.90*ebtotmaxcyc[icyc])) THEN BEGIN            ;And if passing under 0.90*this max
      PRINT,'icyc,time[itemaxcyc[icyc]]-timei,ebtotmaxcyc=', $
             icyc,time[itemaxcyc[icyc]]-timei,ebtotmaxcyc[icyc]
      icyc = icyc + 1L                                                  ;Change cycle (and save preceding max and min)
      itemaxcyc  [icyc] = it
      ebtotmaxcyc[icyc] = ebdetrend0[it]
      itemincyc  [icyc] = it
      ebtotmincyc[icyc] = ebdetrend0[it]
   ENDIF
   ENDELSE
;  IF ((icyc GE 2) AND (time[itemaxcyc[icyc]] GT time[itemaxcyc[icyc-1]]+1.75*(time[itemaxcyc[icyc-1]]-time[itemaxcyc[icyc-2]]))) THEN BREAK
ENDFOR
nemaxcyc = icyc

IF (nemaxcyc GE 1) THEN BEGIN
   itemaxcyc   = itemaxcyc  [0:nemaxcyc-1]
   ebtotmaxcyc = ebtotmaxcyc[0:nemaxcyc-1]*ebtotgen0fit[itemaxcyc[0:nemaxcyc-1]]
   itemincyc   = itemincyc  [0:nemaxcyc-1]
   ebtotmincyc = ebtotmincyc[0:nemaxcyc-1]*ebtotgen0fit[itemincyc[0:nemaxcyc-1]]
ENDIF
IF (nemaxcyc GE 2) THEN BEGIN
   peremaxcyc = time[itemaxcyc[1:nemaxcyc-1]]-time[itemaxcyc[0:nemaxcyc-2]]
   peremincyc = time[itemincyc[1:nemaxcyc-1]]-time[itemincyc[0:nemaxcyc-2]]
ENDIF

;---- Fit to energy minima and maxima ----------------------------------

ebtotmaxpar = [0.,0.,0.]
ebtotmaxfit = FLTARR(nt+1)+1.
ebtotmaxchi = 0.
ebtotminpar = [0.,0.,0.]
ebtotminfit = FLTARR(nt+1)+1.
ebtotminchi = 0.

IF (nemaxcyc GE 2) THEN BEGIN
   
   ;ebtotmaxpar = [1.e3,100.,  0.] & IF (ebtotmaxcyc[1] GT ebtotmaxcyc[0]) THEN ebtotmaxpar[1]=-ebtotmaxpar[1]
   ;ebtotmaxpaa = [  1.,  1.,  0.]
   ;ebtotmaxfitc= CURVEFIT((time[itemaxcyc]-time(itfit[0])), ebtotmaxcyc, (time[itemaxcyc]*0.)+1., ebtotmaxpar, FITA=ebtotmaxpaa, ITMAX=50, ITER=niter, FUNCTION_NAME='gfunct_exp') & PRINT,'CURVEFIT:',niter,' iterations'
   ebtotmaxparr= POLY_FIT((time[itemaxcyc]-time(itfit[0])), ALOG(ebtotmaxcyc), 1, YFIT=ebtotmaxfitc) & ebtotmaxfitc=EXP(ebtotmaxfitc)
   ;ebtotmaxfitt= EXP(ebtotmaxparr[0] + ebtotmaxparr[1]*(time-time(itfit[0])))
   ebtotmaxpar = [EXP(ebtotmaxparr[0]),1./ebtotmaxparr[1],0.]
   ebtotmaxfit = ebtotmaxpar[0]*EXP((time-time(itfit[0]))/ebtotmaxpar[1]) + ebtotmaxpar[2]
   ebtotmaxchi = SQRT(TOTAL((ebtotmaxcyc-ebtotmaxfitc)^2)/nemaxcyc)
   
   ebtotminparr= POLY_FIT((time[itemincyc]-time(itfit[0])), ALOG(ebtotmincyc), 1, YFIT=ebtotminfitc) & ebtotminfitc=EXP(ebtotminfitc)
   ebtotminpar = [EXP(ebtotminparr[0]),1./ebtotminparr[1],0.]
   ebtotminfit = ebtotminpar[0]*EXP((time-time(itfit[0]))/ebtotminpar[1]) + ebtotminpar[2]
   ebtotminchi = SQRT(TOTAL((ebtotmincyc-ebtotminfitc)^2)/nemaxcyc)
   
ENDIF

;---- Fit to energy trend (2nd pass) -----------------------------------

;itwhere = WHERE((time GE time[itfit[0]]) AND (time LE time[itemaxcyc[nemaxcyc-1]]+(time[itemaxcyc[nemaxcyc-1]]-time[itemincyc[nemaxcyc-1]])/2.),ntwhere)
itwhere = INDGEN(MIN([nt,itemaxcyc[nemaxcyc-1]+(itemaxcyc[nemaxcyc-1]-itemincyc[nemaxcyc-1])/2])-itfit[0]+1)+itfit[0]
ntwhere = N_ELEMENTS(itwhere)

ebtotgenpar = [0.,0.,0.]
ebtotgenfit = FLTARR(nt+1)+1.
ebtotgenchi = 0.

IF (ntwhere GE 2 AND nemaxcyc GE 2) THEN BEGIN
   
   ebtotgenparr= POLY_FIT((time[itwhere]-time[itfit[0]]), ALOG(ebtotsmth[itwhere]), 1, YFIT=ebtotgenfitc) & ebtotgenfitc=EXP(ebtotgenfitc)
   ebtotgenpar = [EXP(ebtotgenparr[0]),1./ebtotgenparr[1],0.]
   ebtotgenfit = ebtotgenpar[0]*EXP((time-time(itfit[0]))/ebtotgenpar[1]) + ebtotgenpar[2]
   ebtotgenchi = SQRT(TOTAL((ebtotsmth[itwhere]-ebtotgenfitc)^2)/ntwhere)
   
ENDIF

ebdetrend = ebtotsmth/ebtotgenfit

;---- FFT on detrended magnetic energy evolution (2nd pass) ------------

;ebfft2  = FFT(ebdetrend[itwhere]-1.,-1)
;nsample = ntwhere
;dtsample= time[itwhere[1]]-time[itwhere[0]]                             ;IF (time[itwhere[2]]-time[itwhere[1]] NE dtsample) THEN STOP,'! Irregular time steps in FFT...'

ebfft2  = FFT(ebdetrend-1.,-1)
nsample = nt+1
dtsample= time[1]-time[0]                                               ;IF (time[2]-time[1] NE dtsample) THEN STOP,'! Irregular time steps in FFT...'

freq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   freq = [0., freq, nsample/2, -nsample/2+freq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   freq = [0., freq, -(nsample/2+1)+freq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nfreq = nsample/2
freq = freq[0:nfreq]

ebfft2amp = ebfft2amp + 1./(nbfitfac*nbldseed) * 2*ABS(ebfft2[0:nfreq])^2
ebfft2phs = ATAN(IMAGINARY(ebfft2[0:nfreq]),REAL_PART(ebfft2[0:nfreq]))   ;ATAN(ebfft2[0:nfreq],/PHASE)

ebfft2ampmax = MAX(ebfft2amp,iebfft2max)

;---- Gaussian fit -----

ifreqfit = WHERE(freq GE 0.08,/NULL)

;ebfft2amppar = [ebfft2ampmax,freq[iebfft2max],0.001]
ebfft2amppar = [1.e-2,0.10,0.001]
ebfft2ampfit = GAUSSFIT(freq[ifreqfit], ebfft2amp[ifreqfit], ebfft2amppar, ESTIMATES=ebfft2amppar, CHISQ=ebfft2ampchi, NTERMS=3)
freqq = FINDGEN(100001)/100000.*(freq[nfreq]-freq[0])+freq[0]
ebfft2ampfitt= ebfft2amppar[0]*EXP(-(freqq-ebfft2amppar[1])^2/(2.*ebfft2amppar[2]^2))

;---- Magnetic energy maxima distribution ------------------------------

nebtotmaxcycbin = 30
minebtotmaxcyc  = 35.
maxebtotmaxcyc  = 40.

ebtotmaxcycbinn= FINDGEN(nebtotmaxcycbin+1)/nebtotmaxcycbin*(maxebtotmaxcyc-minebtotmaxcyc) + minebtotmaxcyc
ebtotmaxcycbin = (ebtotmaxcycbinn(0:nebtotmaxcycbin-1)+ebtotmaxcycbinn(1:nebtotmaxcycbin))/2.

histebtotmaxcyc=HISTOGRAM(ALOG10(rsun^3*ebtotmaxcyc),MIN=minebtotmaxcyc,MAX=maxebtotmaxcyc,NBINS=nebtotmaxcycbin+1,LOCATIONS=locebtotmaxcyc) & histebtotmaxcyc=histebtotmaxcyc[0:nebtotmaxcycbin-1]

;histebtotmaxcyc(WHERE(histebtotmaxcyc EQ 0.)) = 1.e-2                  ; Éviter les valeurs de 0
;histebtotmaxcyc=histebtotmaxcyc/(10^(ebtotmaxcycbin-minebtotmaxcyc))   ; Ponderation contre l'augmentation de la largeur des bins en ebtotmaxcyc
 histebtotmaxcyc=histebtotmaxcyc/TOTAL(histebtotmaxcyc)                 ; Normalisation
;histebtotmaxcyc=histebtotmaxcyc*nebtotmaxcycbin/20                     ; Normalisation to nbin=20
;histebtotmaxcyc=ALOG10(histebtotmaxcyc)                                ; Échelle verticale log

;---- Calculate fitness factor for optimisation ------------------------

;---- Emergence function - regular magnetic and latitudinal mesh -------

nelxgrid = nelx
nbphigrid= 100

xxgrid = xx

costgrid= FLTARR(nbphigrid+1,nelxgrid+1)
FOR i=0,nbphigrid DO costgrid(i,*) = xxgrid

minbphigrid=-1000.
maxbphigrid= 1000.
bphigrid= FINDGEN(nbphigrid+1)/nbphigrid*(maxbphigrid-minbphigrid)+minbphigrid
b_ygrid = FLTARR(nbphigrid+1,nelxgrid+1)
FOR j=0,nelxgrid DO b_ygrid(*,j) = bphigrid

a_qgrid = FLTARR(nbphigrid+1,nelxgrid+1) + 1.

;---- Weighted magnetic component -----
bymaxx = 1000.
aqmaxx = 1.

bfitgrid    = -b_ygrid * ABS(b_ygrid/bymaxx)^(bb-1) * ABS(a_qgrid/aqmaxx)^aa
bfitgrid[WHERE(b_ygrid EQ 0.,countbfit)]=0. & IF (countbfit EQ 0) THEN PRINT,'ERROR in countbfit !!'

;---- Apply magnetic mask -----
bfittgrid   = bymaxx*ABS(bfitgrid/bymaxx)^cc * 0.5*(ERF((bfitgrid+bcrit)/dbcrit)+ERF((bfitgrid-bcrit)/dbcrit))          ;/ (ERF((bfitmax-bcrit)/dbcrit)+ERF((bfitmax+bcrit)/dbcrit)) * bymax

;---- Apply latitudinal mask -----
bfitsgrid   = bfittgrid * 0.5*(ERF((costgrid+xcrit)/dxcrit)-ERF((costgrid-xcrit)/dxcrit)) * ((1.-x0crit)*ABS(costgrid)+x0crit)

;---- Pseudo-normalize -----
bfitfgrid   = bfitsgrid/100. * bfitfac

;---- Emergence function - input magnetic component -----

;---- Weighted magnetic component -----
bymaxx = 1000.
aqmaxx = 1.

bfit    = -b_y * ABS(b_y/bymaxx)^(bb-1) * ABS(a_q/aqmaxx)^aa
bfit[WHERE(b_y EQ 0.,countbfit)]=0. & IF (countbfit EQ 0) THEN PRINT,'ERROR in countbfit !!'
bfitmax = MAX(ABS(bfit[itfit[0]:itfit[1],*]))
PRINT,'bfitmax=',bfitmax

;bcrit = bcrit *bymax          ;Absolute value ~1.0e3
;dbcrit= dbcrit*bymax          ;Absolute value ~0.1e3

;---- Apply magnetic mask -----
bfitt   = bymaxx*ABS(bfit/bymaxx)^cc * 0.5*(ERF((bfit+bcrit)/dbcrit)+ERF((bfit-bcrit)/dbcrit))          ;/ (ERF((bfitmax-bcrit)/dbcrit)+ERF((bfitmax+bcrit)/dbcrit)) * bymax
bfittmax= MAX(ABS(bfitt[itfit[0]:itfit[1],*]))
PRINT,'bfittmax=',bfittmax

;---- Apply latitudinal mask -----
;expsin= 1.
;bfits   = bfit*ABS(SIN(thtt))^expsin
;bfits   = bfitt*ABS(SIN(thtt))^expsin
bfits   = bfitt * 0.5*(ERF((cost+xcrit)/dxcrit)-ERF((cost-xcrit)/dxcrit)) * ((1.-x0crit)*ABS(cost)+x0crit)
bfitsmax=MAX(ABS(bfits[itfit[0]:itfit[1],*]))
PRINT,'bfitsmax=',bfitsmax

;bcrit = bcrit *bfitsmax
;dbcrit= dbcrit*bfitsmax
;bfitf = (ERF((bfits-bcrit)/dbcrit)+ERF((bfits+bcrit)/dbcrit)) * ABS(bfits/bfitsmax)^cc    ;/ (ERF((bfitsmax-bcrit)/dbcrit)+ERF((bfitsmax+bcrit)/dbcrit)) * bymax

;---- Pseudo-normalize -----
bfitf   = bfits/100. * bfitfac
bfitfmax=MAX(ABS(bfitf[itfit[0]:itfit[1],*]))
PRINT,'bfitfmax=',bfitfmax

;---- Latitudinal average -----
bfitcumul = FLTARR(nt+1)
FOR it=0,nt DO BEGIN
   bfitcumul(it) = TOTAL(ABS(bfitf(it,*)))
ENDFOR
bfitcumulmax = MAX(bfitcumul)

;---- Magnetic "energy" based on bfit -----

ebfit = 1./4.*(yy[jbp(1)]^3-yy[jbp(0)]^3)/3.*TOTAL((bfit [*,0:nelx-1]^2+bfit [*,1:nelx]^2)/2.*delmut,2)
ebfitt= 1./4.*(yy[jbp(1)]^3-yy[jbp(0)]^3)/3.*TOTAL((bfitt[*,0:nelx-1]^2+bfitt[*,1:nelx]^2)/2.*delmut,2)
ebfits= 1./4.*(yy[jbp(1)]^3-yy[jbp(0)]^3)/3.*TOTAL((bfits[*,0:nelx-1]^2+bfits[*,1:nelx]^2)/2.*delmut,2)
ebfitmax = MAX(ebfit [itfit[0]:itfit[1]])
ebfittmax= MAX(ebfitt[itfit[0]:itfit[1]])
ebfitsmax= MAX(ebfits[itfit[0]:itfit[1]])

;---- Butterfly diagram -----

bfly0fit   = bfly0 * ABS(bfly0)^(expfly-1.)
bfly0fitmax= MAX(ABS(bfly0fit))

bflyfit    = bfly * ABS(bfly)^(expfly-1.)
bflyfitmax = MAX(ABS(bflyfit))

;bflyfit1=bflyabs
;bflyfit2=bflyabs
;bflyfit1[WHERE(bfly EQ 0., countbfly)] = -MEAN(bflyabs)
;bflyfit2[WHERE(bfly EQ 0., countbfly)] = -2*MEAN(bflyabs)
;fit1 = TOTAL( byabs           *bflyfit1        )+10000.
;fit2 = TOTAL( byabs           *bflyfit2        )+10000.

;---- Pseudo-SSN ----- franky

bfly0cumul = FLTARR(nt+1)
bfly0scumul= FLTARR(nt+1)
bfly0ncumul= FLTARR(nt+1)
FOR it=0,nt DO BEGIN
   bfly0cumul (it) = TOTAL(ABS(bfly0fit(it,*)))
   bfly0scumul(it) = TOTAL(ABS(bfly0fit(it,0:nelx/2-1)   ))+0.5*ABS(bfly0fit(it,nelx/2))
   bfly0ncumul(it) = TOTAL(ABS(bfly0fit(it,nelx/2+1:nelx)))+0.5*ABS(bfly0fit(it,nelx/2))
ENDFOR
bfly0cumulmax = MAX(bfly0cumul)
bfly0scumulmax= MAX(bfly0scumul)
bfly0ncumulmax= MAX(bfly0ncumul)

bflycumul = FLTARR(nt+1)
bflyscumul= FLTARR(nt+1)
bflyncumul= FLTARR(nt+1)
FOR it=0,nt DO BEGIN
   bflycumul (it) = TOTAL(ABS(bflyfit(it,*)))
   bflyscumul(it) = TOTAL(ABS(bflyfit(it,0:nelx/2-1)   ))+0.5*ABS(bflyfit(it,nelx/2))
   bflyncumul(it) = TOTAL(ABS(bflyfit(it,nelx/2+1:nelx)))+0.5*ABS(bflyfit(it,nelx/2))
ENDFOR
bflycumulmax = MAX(bflycumul)
bflyscumulmax= MAX(bflyscumul)
bflyncumulmax= MAX(bflyncumul)

;---- Correlate -----

bcor    = FLTARR(nnfit)
bflycor = FLTARR(nnfit)
i=-1
FOR j=ixfit[0],ixfit[1] DO BEGIN
   FOR it=itfit[0],itfit[1] DO BEGIN
      i=i+1
      bcor(i)    = bfitf  (it,j)      
      bflycor(i) = bflyfit(it,j)
   ENDFOR
ENDFOR

cbfitbfly= CORRELATE(bcor,bflycor)                    ; CRITÈRE RETENU !
xbfitbfly= SQRT(TOTAL((bcor-bflycor)^2)/nnfit)

ebcor  = ebfits(itfit[0]:itfit[1])
;ssncor = ssne(itfit[0]:itfit[1])
;cebssn = CORRELATE(ebcor,ssncor)

;---- Pseudo-SSN smoothing --------------------------------------------- franky

;bflynsmthpcyc= 10
bflynsmth    = 13                                                       ;nt/ncyc/nrep/bflynsmthpcyc+1

bflycumulsmth = SMOOTH(bflycumul ,bflynsmth,/EDGE_TRUNCATE)
bflyscumulsmth= SMOOTH(bflyscumul,bflynsmth,/EDGE_TRUNCATE)
bflyncumulsmth= SMOOTH(bflyncumul,bflynsmth,/EDGE_TRUNCATE)

;---- Pseudo-SSN min and max -------------------------------------------

ncycmax    = 1000
itfmaxcyc  = FLTARR(ncycmax)
bflymaxcyc = FLTARR(ncycmax)
itfmincyc  = FLTARR(ncycmax)
bflymincyc = FLTARR(ncycmax)

icyc=0
itfmaxcyc  [icyc] = itfit[0]
bflymaxcyc [icyc] = bflycumulsmth[itfit[0]]
itfmincyc  [icyc] = itfit[0]
bflymincyc [icyc] = bflycumulsmth[itfit[0]]
FOR it=itfit[0]+1,itfit[1] DO BEGIN
   IF (bflycumulsmth[it] GT bflymaxcyc[icyc]) THEN BEGIN                   ;Find maximum for cycle icyc
      itfmaxcyc  [icyc] = it
      bflymaxcyc [icyc] = bflycumulsmth[it]
   ENDIF ELSE BEGIN
   IF ((bflymaxcyc[icyc] EQ bflymincyc[icyc]) $                       ;While still in the descending slope
   AND (bflycumulsmth[it] LT bflymincyc[icyc])) THEN BEGIN                 ;Find minimum for cycle icyc
      itfmaxcyc  [icyc] = it
      bflymaxcyc [icyc] = bflycumulsmth[it]
      itfmincyc  [icyc] = it
      bflymincyc [icyc] = bflycumulsmth[it]
   ENDIF
   IF ((bflymaxcyc[icyc] GT bflymincyc[icyc]) $                       ;If a maximum has been found
   AND (bflycumulsmth[it] LT 0.90*bflymaxcyc[icyc])) THEN BEGIN            ;And if passing under 0.90*this max
      PRINT,'icyc,time[itfmaxcyc[icyc]]-timei,bflymaxcyc=', $
             icyc,time[itfmaxcyc[icyc]]-timei,bflymaxcyc[icyc]
      icyc = icyc + 1L                                                  ;Change cycle (and save preceding max and min)
      itfmaxcyc  [icyc] = it
      bflymaxcyc [icyc] = bflycumulsmth[it]
      itfmincyc  [icyc] = it
      bflymincyc [icyc] = bflycumulsmth[it]
   ENDIF
   ENDELSE
ENDFOR
nfmaxcyc = icyc+1

IF (nfmaxcyc GE 1) THEN BEGIN
   itfmaxcyc  = itfmaxcyc  [0:nfmaxcyc-1]
   bflymaxcyc = bflymaxcyc [0:nfmaxcyc-1]
   itfmincyc  = itfmincyc  [0:nfmaxcyc-1]
   bflymincyc = bflymincyc [0:nfmaxcyc-1]
ENDIF
IF (nfmaxcyc GE 2) THEN BEGIN
   perfmaxcyc = time[itfmaxcyc[1:nfmaxcyc-1]]-time[itfmaxcyc[0:nfmaxcyc-2]]
   perfmincyc = time[itfmincyc[1:nfmaxcyc-1]]-time[itfmincyc[0:nfmaxcyc-2]]
ENDIF

;---- Pseudo-SSN count per cycle ---------------------------------------

bflycountcyc = FLTARR(nfmaxcyc)

bflycountcyc[0] = TOTAL(bfly0cumul[0:itfmincyc[0]-1])
FOR icyc=1,nfmaxcyc-1 DO BEGIN
   bflycountcyc[icyc] = TOTAL(bfly0cumul[itfmincyc[icyc-1]:itfmincyc[icyc]-1])
ENDFOR

;---- FFT on (unsigned) pseudo-SSN evolution ---------------------------

unfft0 = FFT(bflycumul(itfit[0]:itfit[1])/bflycumulmax,-1)

nsample = ntfit
dtsample= time[1]-time[0]                                               ;IF (time[2]-time[1] NE dtsample) THEN STOP,'! Irregular time steps in FFT...'

freq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   freq = [0., freq, nsample/2, -nsample/2+freq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   freq = [0., freq, -(nsample/2+1)+freq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nfreq = nsample/2
freq = freq[0:nfreq]

unfft0amp = unfft0amp + 1./(nbfitfac*nbldseed) * 2*ABS(unfft0[0:nfreq])^2
unfft0phs = ATAN(IMAGINARY(unfft0[0:nfreq]),REAL_PART(unfft0[0:nfreq]))   ;ATAN(unfft0[0:nfreq],/PHASE)

unfft0ampmax = MAX(unfft0amp,iunfft0max)

;---- Gaussian fit -----

ifreqfit = WHERE(freq GE 0.,/NULL)

;unfft0amppar = [unfft0ampmax,freq[iunfft0max],0.001]
unfft0amppar = [1.e-2,0.10,0.001]
unfft0ampfit = GAUSSFIT(freq[ifreqfit], unfft0amp[ifreqfit], unfft0amppar, ESTIMATES=unfft0amppar, CHISQ=unfft0ampchi, NTERMS=3)
freqq = FINDGEN(100001)/100000.*(freq[nfreq]-freq[0])+freq[0]
unfft0ampfitt= unfft0amppar[0]*EXP(-(freqq-unfft0amppar[1])^2/(2.*unfft0amppar[2]^2))

;---- 'Signed' pseudo-SSN (inverted every even cycle) ------------------

sbflycumul = bflycumul
icyc=0
FOR it=itfit[0]+1,itfit[1] DO BEGIN
   IF ((icyc+1)/2 EQ (icyc+1)/2.) THEN sbflycumul[it]=-sbflycumul[it]
   IF (icyc LT nfmaxcyc-1) THEN IF (time[it] GT time[itfmincyc[icyc+1]]) THEN icyc=icyc+1
ENDFOR

;---- FFT on 'signed' pseudo-SSN evolution -----------------------------

snfft0 = FFT(sbflycumul/bflycumulmax,-1)

nsample = nt+1
dtsample= time[1]-time[0]                                               ;IF (time[2]-time[1] NE dtsample) THEN STOP,'! Irregular time steps in FFT...'

freq = FINDGEN((nsample-1)/2) + 1
IF ((nsample MOD 2) EQ 0) THEN BEGIN
   freq = [0., freq, nsample/2, -nsample/2+freq]/(nsample*dtsample)
ENDIF ELSE BEGIN
   freq = [0., freq, -(nsample/2+1)+freq]/(nsample*dtsample)
ENDELSE

;---- Real case -----
nfreq = nsample/2
freq = freq[0:nfreq]

snfft0amp = snfft0amp + 1./(nbfitfac*nbldseed) * 2*ABS(snfft0[0:nfreq])^2
snfft0phs = ATAN(IMAGINARY(snfft0[0:nfreq]),REAL_PART(snfft0[0:nfreq]))   ;ATAN(snfft0[0:nfreq],/PHASE)

snfft0ampmax = MAX(snfft0amp,isnfft0max)

;---- Gaussian fit -----

ifreqfit = WHERE(freq GE 0.,/NULL)

;snfft0amppar = [snfft0ampmax,freq[isnfft0max],0.001]
snfft0amppar = [1.e-2,0.05,0.001]
snfft0ampfit = GAUSSFIT(freq[ifreqfit], snfft0amp[ifreqfit], snfft0amppar, ESTIMATES=snfft0amppar, CHISQ=snfft0ampchi, NTERMS=3)
freqq = FINDGEN(100001)/100000.*(freq[nfreq]-freq[0])+freq[0]
snfft0ampfitt= snfft0amppar[0]*EXP(-(freqq-snfft0amppar[1])^2/(2.*snfft0amppar[2]^2))

;---- Pseudo-SSN-south Min and max -------------------------------------

ncycmax     = 1000
bflysmaxcyc = FLTARR(ncycmax)
timefsmaxcyc= FLTARR(ncycmax)
bflysmincyc = FLTARR(ncycmax)
timefsmincyc= FLTARR(ncycmax)

icyc=0
bflysmaxcyc [icyc] = bflyscumulsmth[itfit[0]]
timefsmaxcyc[icyc] = time         [itfit[0]]
bflysmincyc [icyc] = bflyscumulsmth[itfit[0]]
timefsmincyc[icyc] = time         [itfit[0]]
FOR it=itfit[0]+1,itfit[1] DO BEGIN
   IF (bflyscumulsmth[it] GT bflysmaxcyc[icyc]) THEN BEGIN                   ;Find maximum for cycle icyc
      bflysmaxcyc [icyc] = bflyscumulsmth[it]
      timefsmaxcyc[icyc] = time         [it]
   ENDIF ELSE BEGIN
   IF ((bflysmaxcyc[icyc] EQ bflysmincyc[icyc]) $                       ;While still in the descending slope
   AND (bflyscumulsmth[it] LT bflysmincyc[icyc])) THEN BEGIN                 ;Find minimum for cycle icyc
      bflysmaxcyc [icyc] = bflyscumulsmth[it]
      timefsmaxcyc[icyc] = time         [it]
      bflysmincyc [icyc] = bflyscumulsmth[it]
      timefsmincyc[icyc] = time         [it]
   ENDIF
   IF ((bflysmaxcyc[icyc] GT bflysmincyc[icyc]) $                       ;If a maximum has been found
   AND (bflyscumulsmth[it] LT 0.90*bflysmaxcyc[icyc])) THEN BEGIN            ;And if passing under 0.90*this max
      PRINT,'icyc,timefsmaxcyc-timei,bflysmaxcyc=', $
             icyc,timefsmaxcyc[icyc]-timei,bflysmaxcyc[icyc]
      icyc = icyc + 1L                                                  ;Change cycle (and save preceding max and min)
      bflysmaxcyc [icyc] = bflyscumulsmth[it]
      timefsmaxcyc[icyc] = time         [it]
      bflysmincyc [icyc] = bflyscumulsmth[it]
      timefsmincyc[icyc] = time         [it]
   ENDIF
   ENDELSE
ENDFOR
nfsmaxcyc = icyc

IF (nfsmaxcyc GE 1) THEN BEGIN
   bflysmaxcyc = bflysmaxcyc [0:nfsmaxcyc-1]
   timefsmaxcyc= timefsmaxcyc[0:nfsmaxcyc-1]
   bflysmincyc = bflysmincyc [0:nfsmaxcyc-1]
   timefsmincyc= timefsmincyc[0:nfsmaxcyc-1]
ENDIF

;---- Pseudo-SSN-north Min and max -------------------------------------

ncycmax     = 1000
bflynmaxcyc = FLTARR(ncycmax)
timefnmaxcyc= FLTARR(ncycmax)
bflynmincyc = FLTARR(ncycmax)
timefnmincyc= FLTARR(ncycmax)

icyc=0
bflynmaxcyc [icyc] = bflyncumulsmth[itfit[0]]
timefnmaxcyc[icyc] = time         [itfit[0]]
bflynmincyc [icyc] = bflyncumulsmth[itfit[0]]
timefnmincyc[icyc] = time         [itfit[0]]
FOR it=itfit[0]+1,itfit[1] DO BEGIN
   IF (bflyncumulsmth[it] GT bflynmaxcyc[icyc]) THEN BEGIN                   ;Find maximum for cycle icyc
      bflynmaxcyc [icyc] = bflyncumulsmth[it]
      timefnmaxcyc[icyc] = time         [it]
   ENDIF ELSE BEGIN
   IF ((bflynmaxcyc[icyc] EQ bflynmincyc[icyc]) $                       ;While still in the descending slope
   AND (bflyncumulsmth[it] LT bflynmincyc[icyc])) THEN BEGIN                 ;Find minimum for cycle icyc
      bflynmaxcyc [icyc] = bflyncumulsmth[it]
      timefnmaxcyc[icyc] = time         [it]
      bflynmincyc [icyc] = bflyncumulsmth[it]
      timefnmincyc[icyc] = time         [it]
   ENDIF
   IF ((bflynmaxcyc[icyc] GT bflynmincyc[icyc]) $                       ;If a maximum has been found
   AND (bflyncumulsmth[it] LT 0.90*bflynmaxcyc[icyc])) THEN BEGIN            ;And if passing under 0.90*this max
      PRINT,'icyc,timefnmaxcyc-timei,bflynmaxcyc=', $
             icyc,timefnmaxcyc[icyc]-timei,bflynmaxcyc[icyc]
      icyc = icyc + 1L                                                  ;Change cycle (and save preceding max and min)
      bflynmaxcyc [icyc] = bflyncumulsmth[it]
      timefnmaxcyc[icyc] = time         [it]
      bflynmincyc [icyc] = bflyncumulsmth[it]
      timefnmincyc[icyc] = time         [it]
   ENDIF
   ENDELSE
ENDFOR
nfnmaxcyc = icyc

IF (nfnmaxcyc GE 1) THEN BEGIN
   bflynmaxcyc = bflynmaxcyc [0:nfnmaxcyc-1]
   timefnmaxcyc= timefnmaxcyc[0:nfnmaxcyc-1]
   bflynmincyc = bflynmincyc [0:nfnmaxcyc-1]
   timefnmincyc= timefnmincyc[0:nfnmaxcyc-1]
ENDIF

PRINT,'nfsmaxcyc,nfnmaxcyc =',nfsmaxcyc,nfnmaxcyc
IF (nfnmaxcyc GT nfsmaxcyc) THEN BEGIN
   IF (timefsmincyc[0]-timefnmincyc[0] GT 5.) THEN BEGIN
      bflynmaxcyc = bflynmaxcyc [1:nfnmaxcyc-1]
      timefnmaxcyc= timefnmaxcyc[1:nfnmaxcyc-1]
      bflynmincyc = bflynmincyc [1:nfnmaxcyc-1]
      timefnmincyc= timefnmincyc[1:nfnmaxcyc-1]
   ENDIF ELSE BEGIN
      bflynmaxcyc = bflynmaxcyc [0:nfnmaxcyc-2]
      timefnmaxcyc= timefnmaxcyc[0:nfnmaxcyc-2]
      bflynmincyc = bflynmincyc [0:nfnmaxcyc-2]
      timefnmincyc= timefnmincyc[0:nfnmaxcyc-2]
   ENDELSE
   nfnmaxcyc = nfnmaxcyc-1
ENDIF
IF (nfsmaxcyc GT nfnmaxcyc) THEN BEGIN
   IF (timefnmincyc[0]-timefsmincyc[0] GT 5.) THEN BEGIN
      bflysmaxcyc = bflysmaxcyc [1:nfsmaxcyc-1]
      timefsmaxcyc= timefsmaxcyc[1:nfsmaxcyc-1]
      bflysmincyc = bflysmincyc [1:nfsmaxcyc-1]
      timefsmincyc= timefsmincyc[1:nfsmaxcyc-1]
   ENDIF ELSE BEGIN
      bflysmaxcyc = bflysmaxcyc [0:nfsmaxcyc-2]
      timefsmaxcyc= timefsmaxcyc[0:nfsmaxcyc-2]
      bflysmincyc = bflysmincyc [0:nfsmaxcyc-2]
      timefsmincyc= timefsmincyc[0:nfsmaxcyc-2]
   ENDELSE
   nfsmaxcyc = nfsmaxcyc-1
ENDIF

IF (nfsmaxcyc GE 1) THEN BEGIN
   delfmincyc = timefnmincyc[0:nfnmaxcyc-1]-timefsmincyc[0:nfsmaxcyc-1]
ENDIF

;---- Fit to pseudo-SSN maxima -----------------------------------------

IF (nfmaxcyc GE 2) THEN BEGIN
   bflymaxparr= POLY_FIT((time[itfmaxcyc]-time(itfit[0])), ALOG(bflymaxcyc), 1, YFIT=bflymaxfitc) & bflymaxfitc=EXP(bflymaxfitc)
   ;bflymaxfitt= EXP(bflymaxparr[0] + bflymaxparr[1]*(time-time(itfit[0])))
   bflymaxpar = [EXP(bflymaxparr[0]),-1./bflymaxparr[1],0.]
   bflymaxfit = bflymaxpar[0]*EXP(-(time-time(itfit[0]))/bflymaxpar[1]) + bflymaxpar[2]
   bflymaxchi = SQRT(TOTAL((bflymaxcyc-bflymaxfitc)^2)/nfmaxcyc)
ENDIF

;---- Pseudo-SSN maxima distribution -----------------------------------

nbflymaxcycbin = 30
minbflymaxcyc  = 0.
maxbflymaxcyc  = 300.

bflymaxcycbinn= FINDGEN(nbflymaxcycbin+1)/nbflymaxcycbin*(maxbflymaxcyc-minbflymaxcyc) + minbflymaxcyc
bflymaxcycbin = (bflymaxcycbinn(0:nbflymaxcycbin-1)+bflymaxcycbinn(1:nbflymaxcycbin))/2.

histbflymaxcyc=HISTOGRAM(bflymaxcyc,MIN=minbflymaxcyc,MAX=maxbflymaxcyc,NBINS=nbflymaxcycbin+1,LOCATIONS=locbflymaxcyc) & histbflymaxcyc=histbflymaxcyc[0:nbflymaxcycbin-1]

;histbflymaxcyc(WHERE(histbflymaxcyc EQ 0.)) = 1.e-2                    ; Éviter les valeurs de 0
;histbflymaxcyc=histbflymaxcyc/(10^(bflymaxcycbin-minbflymaxcyc))       ; Ponderation contre l'augmentation de la largeur des bins en bflymaxcyc
 histbflymaxcyc=histbflymaxcyc/TOTAL(histbflymaxcyc)                    ; Normalisation
;histbflymaxcyc=histbflymaxcyc*nbflymaxcycbin/20                        ; Normalisation to nbin=20
;histbflymaxcyc=ALOG10(histbflymaxcyc)                                  ; Échelle verticale log

;---- Pseudo-SSN-south maxima distribution -----------------------------------

nbflysmaxcycbin = 30
minbflysmaxcyc  = 0.
maxbflysmaxcyc  = 150.

bflysmaxcycbinn= FINDGEN(nbflysmaxcycbin+1)/nbflysmaxcycbin*(maxbflysmaxcyc-minbflysmaxcyc) + minbflysmaxcyc
bflysmaxcycbin = (bflysmaxcycbinn(0:nbflysmaxcycbin-1)+bflysmaxcycbinn(1:nbflysmaxcycbin))/2.

histbflysmaxcyc=HISTOGRAM(bflysmaxcyc,MIN=minbflysmaxcyc,MAX=maxbflysmaxcyc,NBINS=nbflysmaxcycbin+1,LOCATIONS=locbflysmaxcyc) & histbflysmaxcyc=histbflysmaxcyc[0:nbflysmaxcycbin-1]

;histbflysmaxcyc(WHERE(histbflysmaxcyc EQ 0.)) = 1.e-2                  ; Éviter les valeurs de 0
;histbflysmaxcyc=histbflysmaxcyc/(10^(bflysmaxcycbin-minbflysmaxcyc))   ; Ponderation contre l'augmentation de la largeur des bins en bflysmaxcyc
 histbflysmaxcyc=histbflysmaxcyc/TOTAL(histbflysmaxcyc)                 ; Normalisation
;histbflysmaxcyc=histbflysmaxcyc*nbflysmaxcycbin/20                     ; Normalisation to nbin=20
;histbflysmaxcyc=ALOG10(histbflysmaxcyc)                                ; Échelle verticale log

;---- Pseudo-SSN-north maxima distribution -----------------------------

nbflynmaxcycbin = 30
minbflynmaxcyc  = 0.
maxbflynmaxcyc  = 150.

bflynmaxcycbinn= FINDGEN(nbflynmaxcycbin+1)/nbflynmaxcycbin*(maxbflynmaxcyc-minbflynmaxcyc) + minbflynmaxcyc
bflynmaxcycbin = (bflynmaxcycbinn(0:nbflynmaxcycbin-1)+bflynmaxcycbinn(1:nbflynmaxcycbin))/2.

histbflynmaxcyc=HISTOGRAM(bflynmaxcyc,MIN=minbflynmaxcyc,MAX=maxbflynmaxcyc,NBINS=nbflynmaxcycbin+1,LOCATIONS=locbflynmaxcyc) & histbflynmaxcyc=histbflynmaxcyc[0:nbflynmaxcycbin-1]

;histbflynmaxcyc(WHERE(histbflynmaxcyc EQ 0.)) = 1.e-2                  ; Éviter les valeurs de 0
;histbflynmaxcyc=histbflynmaxcyc/(10^(bflynmaxcycbin-minbflynmaxcyc))   ; Ponderation contre l'augmentation de la largeur des bins en bflynmaxcyc
 histbflynmaxcyc=histbflynmaxcyc/TOTAL(histbflynmaxcyc)                 ; Normalisation
;histbflynmaxcyc=histbflynmaxcyc*nbflynmaxcycbin/20                     ; Normalisation to nbin=20
;histbflynmaxcyc=ALOG10(histbflynmaxcyc)                                ; Échelle verticale log

;-----------------------------------------------------------------------
;---- Open plot file ---------------------------------------------------

nnplot       = [1,21]                                                    ;[horizontal,vertical] number of plots
;nnplot       = [1,6]                                                    ;[horizontal,vertical] number of plots
;nnplot       = [1,1]                                                    ;[horizontal,vertical] number of plots
plotdim      = [16.10,3.00]                                              ;[x,y] dimension of each plot (cm)
;plotdim      = [7.35,3.00]                                              ;[x,y] dimension of each plot (cm)
plotmin      = [1.30,0.80]                                              ;[left,bottom] margin before first plot (cm)
;plotmax      = [1.60,1.20]                                              ;[right,top] margin after last plot (cm)
plotmax      = [1.60,0.10]                                              ;[right,top] margin after last plot (cm)
;plotmax      = [0.35,0.10]                                              ;[right,top] margin after last plot (cm)
;plotspc      = [0.00,0.10]                                              ;[horizontal,vertical] space between plots (cm)
plotspc      = [0.00,0.80]                                              ;[horizontal,vertical] space between plots (cm)
plotleg      = [0.20,3.00]                                              ;[x,y] dimension of legend (cm)
plotslg      = [1.35,0.00]                                              ;[horizontal,vertical] space before legend (cm)
dim          = plotmin+nnplot*(plotdim+plotspc)-plotspc+plotmax         ;dimension of canevas (cm) (Letter 8.5x11 => [21.59,27.94]cm)

npos = nnplot(0)*nnplot(1)
pos  = FLTARR(4,npos)
pos0 = FLTARR(4,npos)
xpos = plotmin(0)
xpos0= xpos+plotdim(0)+plotslg(0)
ypos = dim(1)-plotmax(1)
ypos0= ypos
FOR ipos=0,npos-1 DO BEGIN
   pos (*,ipos) = [xpos /dim(0),(ypos -plotdim(1))/dim(1),(xpos +plotdim(0))/dim(0),ypos /dim(1)]
   pos0(*,ipos) = [xpos0/dim(0),(ypos0-plotleg(1))/dim(1),(xpos0+plotleg(0))/dim(0),ypos0/dim(1)]
   xpos = xpos+plotdim(0)+plotspc(0)
   xpos0= xpos+plotdim(0)+plotslg(0)
   IF ((ipos+1.)/nnplot(0)-(ipos+1)/nnplot(0) EQ 0.) THEN BEGIN
      xpos = plotmin(0)
      xpos0= xpos+plotdim(0)+plotslg(0)
      ypos = ypos-plotdim(1)-plotspc(1)
      ypos0= ypos
   ENDIF
ENDFOR

PRINT,'Dimension of canevas (cm) : ',dim
iposplot=-1

openans='n'
IF ((ibfitfac EQ 0) AND (ibldseed EQ 0)) THEN BEGIN
;  PRINT,'Open new output file ? (y/n)'
;  READ,openans
   openans='y'
ENDIF
IF (openans EQ 'y') THEN BEGIN
   fileplot = pathout + file
   PRINT,'Output file:'
   PRINT,fileplot
   
   SET_PLOT,'ps'
   DEVICE, FILENAME = fileplot+'.eps'
   DEVICE, /PORTRAIT, YOFFSET=5., XSIZE = dim(0), YSIZE = dim(1), /ENCAPSULATED
   DEVICE, SET_FONT='Times', FONT_SIZE=4, /TT_FONT, /COLOR, BITS_PER_PIXEL=8     ;, SCALE_FACTOR=0.5
   SET_PLOT,'ps'
ENDIF

;------------To .txt time, Emag Tot, Axial dipole, SSN(t)----------- franky
close,1 & openw,1,fileplot+'.txt',WIDTH=90
for k=0,nt do begin
   printf,1,time(k),ebtot(k),daxial(k),bflycumul(k),bflyscumul(k),bflyncumul(k)
endfor
close,1

;---- Black-Green-Turquoise-Blue-Grey-Red-Orange-Yellow-White color table -----

;lev = [0.001,0.003,0.01,0.02,0.05,0.1,0.2,0.5,1.0,1000.]
;lev = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
;lev = [0.05,0.10,0.15,0.20,0.25,0.30,0.35,0.40,0.50,0.60,0.70,0.80,0.90,1.00]
;levbr = [0.02,0.04,0.06,0.08,0.10,0.12,0.14,0.16,0.18,0.20]
;levvj = [0.30,0.40,0.50,0.60,0.70,0.80,0.90,10.0]
;levbr = [0.002,0.004,0.006,0.008,0.010,0.012,0.014,0.016]
;levvj = [0.031,0.063,0.125,0.250,0.500,1.000]
;levlog= [-1000.,REVERSE(-levvj),REVERSE(-levbr),levbr,levvj]
levbr = [0.04,0.12,0.20,0.28,0.36,0.44]
levto = [0.52,0.60,0.68]
levvj = [0.76,0.84,0.92]
levnb = [1.00,2.00,3.00,4.00,5.00,6.00,7.00,8.00,9.00,10.0]
;levnb = [1.00,2.00,3.00,4.00,5.00,6.00,7.00,8.00,9.00,100.]
;levnb = [1.00,2.00,3.00,4.00,5.00,6.00,7.00,8.00,9.00,10.0]*1.2
lev   = [-1000.,REVERSE(-levnb),REVERSE(-levvj),REVERSE(-levto),REVERSE(-levbr),levbr,levto,levvj,levnb]

nlevbr=N_ELEMENTS(levbr)                                                ; nombre de niveaux gris à bleu/rouge
nlevto=N_ELEMENTS(levto)                                                ; nombre de niveaux bleu/rouge à turquoise/orange
nlevvj=N_ELEMENTS(levvj)                                                ; nombre de niveaux turquoise/orange à vert/jaune
nlevnb=N_ELEMENTS(levnb)                                                ; nombre de niveaux vert/jaune à noir/blanc
nlev=N_ELEMENTS(lev)

grey=[150,150,150]
gris=[210,210,210]
bleu=[  0,  0,255]
turq=[  0,127,127]
vert=[  0,255,  0]
roug=[255,  0,  0]
oran=[255,127,  0]
jaun=[255,255,  0]
noir=[  0,  0,  0]
blan=[255,255,255]

cgris=(nlev-1)/2                                                        ; position de la couleur du milieu
cbleu=cgris-nlevbr
croug=cgris+nlevbr
cturq=cbleu-nlevto
coran=croug+nlevto
cvert=cturq-nlevvj
cjaun=coran+nlevvj
cnoir=0          & IF(cnoir NE cvert-nlevnb) THEN STOP,'Bad color cnoir'
cblan=nlev-1     & IF(cblan NE cjaun+nlevnb) THEN STOP,'Bad color cblan'
cgrey=nlev
colors=[cnoir,cvert,cturq,cbleu,cgris,croug,coran,cjaun,cblan]

rgbvec=BYTARR(3,nlev+1)

rgbvec(*,cgris)=gris                                                     ; la couleur du milieu est gris (210,210,210) (le contour gris correspond à la valeur -0.2, mais CONTOUR avec l'option /FILL remplira de -0.2 à 0.2)
FOR ilev=1,nlevbr-1 DO BEGIN
   rgbvec(*,cgris-ilev)=gris+ilev/FLOAT(nlevbr+1)*(bleu-gris)           ; interpolation linéaire de gris à bleu (en évitant la dernière teinte)
   rgbvec(*,cgris+ilev)=gris+ilev/FLOAT(nlevbr+1)*(roug-gris)           ; interpolation linéaire de gris à rouge (en évitant la dernière teinte)
ENDFOR
rgbvec(*,cbleu)=bleu                                                    ; bleu
rgbvec(*,croug)=roug                                                    ; rouge
FOR ilev=1,nlevto-1 DO BEGIN
   rgbvec(*,cbleu-ilev)=bleu+(ilev+1)/FLOAT(nlevto+1)*(turq-bleu)       ; interpolation linéaire de bleu à turquoise (en évitant la première teinte)
   rgbvec(*,croug+ilev)=roug+(ilev+1)/FLOAT(nlevto+1)*(oran-roug)       ; interpolation linéaire de rouge à orange (en évitant la première teinte)
ENDFOR
rgbvec(*,cturq)=turq                                                    ; turquoise
rgbvec(*,coran)=oran                                                    ; orange
FOR ilev=1,nlevvj-1 DO BEGIN
   rgbvec(*,cturq-ilev)=turq+ilev/FLOAT(nlevvj)*(vert-turq)             ; interpolation linéaire de turquoise à vert
   rgbvec(*,coran+ilev)=oran+ilev/FLOAT(nlevvj)*(jaun-oran)             ; interpolation linéaire de orange à jaune
ENDFOR
rgbvec(*,cvert)=vert                                                    ; vert
rgbvec(*,cjaun)=jaun                                                    ; jaune
FOR ilev=1,nlevnb-1 DO BEGIN
;  rgbvec(*,cvert-ilev)=vert+ilev/FLOAT(nlevnb)*(noir-vert)             ; interpolation linéaire de vert à noir
   rgbvec(*,cvert-ilev)=vert+ilev/FLOAT(nlevnb)*(blan-vert)             ; interpolation linéaire de vert à blanc
   rgbvec(*,cjaun+ilev)=jaun+ilev/FLOAT(nlevnb)*(blan-jaun)             ; interpolation linéaire de jaune à blanc
ENDFOR
rgbvec(*,cnoir)=noir                                                    ; noir pour le contour -1000. (donc remplissage de -1000. à -1.)
rgbvec(*,cblan)=blan                                                    ; blanc pour le contour +1. (donc remplissage +1. à ...)
rgbvec(*,cgrey)=grey                                                    ; couleur supplémentaire gris foncé en cas de besoin

LOADCT,0                                                                ; charge une table de couleur prédéfinie dans IDL (0 fait une échelle linéaire de noir à blanc)
TVLCT,red,gre,blu,/GET                                                  ; charge la table de couleur précédente dans trois vecteur (un pour R, un pour G, un pour B), ceci n'est pas vraiment nécessaire puisqu'on modifie tout par la suite !
red(0:nlev)=rgbvec(0,*)
gre(0:nlev)=rgbvec(1,*)
blu(0:nlev)=rgbvec(2,*)
TVLCT,red,gre,blu                                                       ; on réécrit les vecteurs R, G et B dans la table IDL (seulement les 20 premières valeurs ont été modifiées)

;cin=rgbvec                                                             ; avec IPLOT, ICONTOUR, etc., on aura besoin d'un vecteur 3x20 contenant les codes de couleur directement (donc pas besoin de la dernière commande TVLCT)
cin=INDGEN(nlev)                                                        ; avec PLOT, CONTOUR, etc., on lui donne un vecteur d'indices (0 à 19) pour qu'ils aillent lire ce qu'on vient de rentrer dans TVLCT

clst = [REPLICATE(2,cgris+1),REPLICATE(0,cgris+1)]

;---- Set up some graphics stuff ---------------------------------------

;nsx=800 & nsy=400
;WINDOW,2,RETAIN=2,XS=nsx,YS=nsy
;mimage=BYTARR(nsx,nsy,ntrep+1)

nasym=12
asym=2.*!PI*FINDGEN(nasym+1)/nasym
USERSYM,sin(asym),cos(asym),/FILL

!ORDER=1
!X.STYLE=1 & !X.THICK=3
!Y.STYLE=1 & !Y.THICK=3

rikiki = 0.0000000000001
gros = 2.0
gross= 3.0

youts0= 0.04
dyouts= 0.11

;---- Construct grid in real [x,y] space (half circle)
xc = FLTARR(nelx+1,nely+1)
yc = FLTARR(nelx+1,nely+1)

FOR j=0,nely DO BEGIN
  FOR i=0,nelx DO BEGIN
     xc(i,j) = yy(j)*sinxx(i)
     yc(i,j) = yy(j)*xx(i)
  ENDFOR
ENDFOR

;---- Color-filled contour for Bphi(r,theta,time[itsnap[0:nsnap-1]]) ---

title0a   = '!6' + STRMID(filename,  0,50)
title0b   = '!6' + STRMID(filename, 50,50)
title0c   = '!6' + STRMID(filename,100,50)

;title01  = '!17ybl =' + STRING(ybl(0) ,FORMAT='(f5.2)') + '(!S!D' + STRING(yblmin(0), FORMAT='(f5.2)') + '!R!U' + STRING(yblmax(0), FORMAT='(f5.2)') + '!N)'
;title02  = '!17etac=' + STRING(etac(0),FORMAT='(e9.2)') + '(!S!D' + STRING(etacmin(0),FORMAT='(e9.2)') + '!R!U' + STRING(etacmax(0),FORMAT='(e9.2)') + '!N)'
;title03  = '!17etat=' + STRING(etat(0),FORMAT='(e9.2)') + '(!S!D' + STRING(etatmin(0),FORMAT='(e9.2)') + '!R!U' + STRING(etatmax(0),FORMAT='(e9.2)') + '!N)'
;title04  = '!17uu  =' + STRING(uu(0),  FORMAT='(f5.2)') + '(!S!D' + STRING(uumin(0),  FORMAT='(f5.2)') + '!R!U' + STRING(uumax(0),  FORMAT='(f5.2)') + '!N)' ;+ ',' + STRING(uu(1),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(1),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(1),  FORMAT='(f5.2)') + '!N),' + STRING(uu(2),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(2),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(2),  FORMAT='(f5.2)') + '!N),' + STRING(uu(3),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(3),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(3),  FORMAT='(f5.2)') + '!N),' + STRING(uu(4),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(4),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(4),  FORMAT='(f5.2)') + '!N),' + STRING(uu(5),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(5),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(5),  FORMAT='(f5.2)') + '!N),' + STRING(uu(6),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(6),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(6),  FORMAT='(f5.2)') + '!N),' + STRING(uu(7),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(7),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(7),  FORMAT='(f5.2)') + '!N),' + STRING(uu(8),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(8),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(8),  FORMAT='(f5.2)') + '!N),' + STRING(uu(9),  FORMAT='(f5.2)') + '(!S!U' + STRING(uumin(9),  FORMAT='(f5.2)') + '!R!D' + STRING(uumax(9),  FORMAT='(f5.2)') + '!N)'
;title05  = '!17pp  =' + STRING(pp(0),  FORMAT='(f5.2)') + '(!S!D' + STRING(ppmin(0),  FORMAT='(f5.2)') + '!R!U' + STRING(ppmax(0),  FORMAT='(f5.2)') + '!N)'
;title06  = '!17rbot=' + STRING(rbot(0),FORMAT='(f5.2)') + '(!S!D' + STRING(rbotmin(0),FORMAT='(f5.2)') + '!R!U' + STRING(rbotmax(0),FORMAT='(f5.2)') + '!N)'
;title07  = '!17mm ='  + STRING(mm(0),  FORMAT='(f5.2)') + '(!S!D' + STRING(mmmin(0),  FORMAT='(f5.2)') + '!R!U' + STRING(mmmax(0),  FORMAT='(f5.2)') + '!N)'
;title08  = '!17qq  =' + STRING(qq(0),  FORMAT='(f5.2)')
;title09  = '!17vv  =' + STRING(vv(0),  FORMAT='(f5.2)')
;title010  = '!17ww  =' + STRING(ww(0),  FORMAT='(f5.2)')
;title011 = '!17eta0=' + STRING(eta0(0),FORMAT='(e9.2)') + '(!S!D' + STRING(eta0min(0),FORMAT='(e9.2)') + '!R!U' + STRING(eta0max(0),FORMAT='(e9.2)') + '!N)'
;title012 = '!17taud=' + STRING(taud,   FORMAT='(f5.2)') + '(!S!D' + STRING(taudmin,   FORMAT='(f5.2)') + '!R!U' + STRING(taudmax,   FORMAT='(f5.2)') + '!N)'
;title013 = '!17amp='  + STRING(amp,    FORMAT='(f5.2)')

nsnap=9
itsnaprange= [itfmincyc[nfmaxcyc-2],itfmincyc[nfmaxcyc-1]]
itsnap= FINDGEN(nsnap)/(nsnap-1)*(itsnaprange[1]-itsnaprange[0])+itsnaprange[0]

title1   = '!18B!D!7u!N!17(r,!7h!17,t=!6[!17' + STRING(time[itsnap],FORMAT='(100f6.3)') + '!6]!17), max =' + STRING(bphimax,format='(e8.2)') + 'G'

xr    = [time[itsnaprange[0]],time[itsnaprange[1]]]-timei
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr    = [-1.,1.]
yname = ['!17 ','!17 ','!170','!170.5!18R!17','!18R!17']
ytitle= ''

iposplot=iposplot+1
;PRINT, xr, yr, itsnaprange, itfmincyc, nfmaxcyc
;IF xr[0].EQ.xr[1] do something that makes it work franky
PLOT, xr, [yr[0],yr[0]], $
   /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=9, XTICKLEN=0.04, XTICKS=nsnap, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=9, YTICKLEN=0.01, YMINOR=1, YTICKS=4, YTICKNAME=yname, YTITLE=ytitle, YCHARSIZE=gros

;XYOUTS, xr(0), yr(1)+youts0*(yr(1)-yr(0)), title1, CHARSIZE=gros
;
;youts=0.15
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title0c,   CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title0b,   CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title0a,   CHARSIZE=gros

;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title013, CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title012, CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title011, CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title010, CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title09,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title08,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title07,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title06,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title05,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title04,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title03,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title02,  CHARSIZE=gros
;youts=youts+dyouts
;XYOUTS, xr(0), yr(1)+youts*(yr(1)-yr(0)), title01,  CHARSIZE=gros

xcr    = [ 0.,1.]
xctitle= ''

FOR isnap=0,nsnap-1 DO BEGIN
   
   fac = 4424.         ;bphimax       ;300.
   levfac=715./4424.   ;bymax/bphimax ;
   fctplot=REFORM(bphi[itsnap[isnap],*,*])/fac
   levv=lev*levfac
   
   xpos0snap=pos[0,iposplot]+isnap*(pos[2,iposplot]-pos[0,iposplot])/nsnap
   ypos0snap=pos[1,iposplot]
   ypos1snap=pos[3,iposplot]
   xpos1snap=xpos0snap+(ypos1snap-ypos0snap)/2.*dim(1)/dim(0)
   
   CONTOUR, fctplot[*,0:nely-nelybuf], xc[*,0:nely-nelybuf], yc[*,0:nely-nelybuf], $
      /NOERASE, POS=[xpos0snap,ypos0snap,xpos1snap,ypos1snap], LEVELS=levv, C_COLOR=cin, C_LINESTYLE=clst, /FILL, $
      XRANGE=xcr, XSTYLE=5, $
      YRANGE=yr, YSTYLE=5
   
   OPLOT, yy(jbp(0))*sinxx, yy(jbp(0))*xx, $
      LINESTYLE=2, THICK=3, COLOR=cnoir
   
ENDFOR

;---- Color bar -----

;ytbar= '!18B!D!7u!N!17(r,!7h!17,t) (!9X!17'+STRING(fac,FORMAT='(f5.0)')+'G)'
ytbar= '!18B!D!7u!N!17(r,!7h!17,t)/'+STRING(fac,FORMAT='(f5.0)')+'G'
yrbar= [-1.,1.]
;yrbar= [-levfac,levfac]

cb = REPLICATE(1.,2) # levv
cbx = FINDGEN(2)

CONTOUR, cb, cbx, levv, $
   /NOERASE, POS = pos0(*,iposplot), $
   LEVELS = levv, C_COLOR = cin, /FILL, $
   XSTYLE=1, XTICKS=1, XCHARSIZE=rikiki, $
   YRANGE=yrbar, YTICKS=4, YMINOR=0, YTICKLEN=rikiki, YTITLE=ytbar, YCHARSIZE = gros

;---- Color-filled contours for B-phi ----------------------------------

;title2   = '!18B!D!7u!17!A' + STRING(bb,FORMAT='(f4.2)') + '!N!18A!D!7u!17!A' + STRING(aa,FORMAT='(f4.2)') + '!N(!6[!17' + string(yy(jbp(0)),format='(f4.2)') + ',' + STRING(yy(jbp(1)),format='(f4.2)') + '!6],!7h!17,!18t!17)!17, max =' + STRING(bfitmax,format='(e8.2)')
title2   = '!18B!D!7u!17!A' + STRING(1.,FORMAT='(f4.2)') + '!N!18A!D!7u!17!A' + STRING(0.,FORMAT='(f4.2)') + '!N(!6[!17' + string(yy(jbp(0)),format='(f4.2)') + ',' + STRING(yy(jbp(1)),format='(f4.2)') + '!6],!7h!17,!18t!17)!17, max =' + STRING(bymax,format='(e8.2)') + 'G'

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr    = [-90.,90.]
yname = ['!1790!9%!17S','45!9%!17S','EQ','45!9%!17N','90!9%!17N']
ytitle= '!17Latitude'

fac = 715.         ;bymax          ;300.
levfac=1.0
fctplot= b_y/fac                                                        ;-bfit/fac
levv=lev*levfac

iposplot=iposplot+1
CONTOUR, fctplot, time-timei, xxan, $
   /NOERASE, POS=pos(*,iposplot), LEVELS=levv, C_COLOR=cin, C_LINESTYLE=clst, /FILL, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, YTICKLEN=0.01, YMINOR=3, YTICKS=N_ELEMENTS(yname)-1, YTICKNAME=yname, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

;FOR isnap=0,nsnap-1 DO BEGIN
;   OPLOT, [time(itsnap[isnap]),time(itsnap[isnap])]-timei, yr, LINESTYLE=2, THICK=3
;ENDFOR

;XYOUTS, xr(0)+0.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title2, CHARSIZE=gros

;---- Color bar -----

;ytbar= '!18B!17!D!7u!17!N(!18r*!17,!7h!17,!18t!17) (!9X!17'+STRING(fac,FORMAT='(f5.0)')+'G)'
ytbar= '!18B!17!D!7u!17!N(!18r*!17,!7h!17,!18t!17)/'+STRING(fac,FORMAT='(f5.0)')+'G'
;yrbar= [-1,1]
yrbar= [-levfac,levfac]

cb = REPLICATE(1.,2) # levv
cbx = FINDGEN(2)

CONTOUR, cb, cbx, levv, $
   /NOERASE, POS = pos0(*,iposplot), $
   LEVELS = levv, C_COLOR = cin, /FILL, $
   XSTYLE=1, XTICKS=1, XCHARSIZE=rikiki, $
   YRANGE=yrbar, YTICKS=4, YMINOR=0, YTICKLEN=rikiki, YTITLE=ytbar, YCHARSIZE = gros

;---- Temporal evolution of magnetic energy ----------------------------

title5  = '!18E!S!DB!17!R!UMAX!N=' + STRING(rsun^3*ebtotmax,format='(e9.2)') + ', !18E!S!DB!17uz!R!UMAX!N=' + STRING(rsun^3*ebuzmax,format='(e9.2)') + ', !18E!S!DF!IB!17!R!UMAX!N=' + STRING(rsun^3*ebfitsmax,format='(e9.2)')

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Simulation time (yr)'
xtitle= '!17Simulation time/years'
xstyle = 5
IF (openans EQ 'y') THEN xstyle=1
;yr = [0.,3.]
;yr = [0.,1.]*MAX([MAX(ebtot)/ebtotmax,MAX(ebuz)/ebuzmax,MAX(ebfits)/ebfitsmax])
yr = [36.6,37.8]
;yr = [20.,ALOG10(MAX([MAX(rsun^3*ebtot),MAX(rsun^3*ebuz),MAX(rsun^3*ebfits)]))]
;yr = [ALOG10(MIN([MIN(rsun^3*ebtot),MIN(rsun^3*ebuz),MIN(rsun^3*ebfits)])),ALOG10(MAX([MAX(rsun^3*ebtot),MAX(rsun^3*ebuz),MAX(rsun^3*ebfits)]))]
yticks = 3
;ytitle = '!18E!DB!N!17 (erg)'
;ytitle = '!17Log !18E!DB!N!17 (erg)'
ytitle = '!17Log(!18E!DB!N!17/erg)'
ystyle = 5
IF (openans EQ 'y') THEN ystyle=1

iposplot=iposplot+1
PLOT, xr, [yr[0],yr[0]], $
   /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=xstyle, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $      ;, XTICKS=4
   YRANGE=yr, YSTYLE=ystyle, YTICKLEN=0.01, YTICKS=yticks, YTITLE=ytitle, YCHARSIZE=gros

IF (openans EQ 'y') THEN BEGIN
   
   OPLOT, xr, [ALOG10(rsun^3*ebtot[0]),ALOG10(rsun^3*ebtot[0])], $
;  OPLOT, xr, [ebtot[0],ebtot[0]]/ebtotmax, $
;  OPLOT, xr, [ebtot[0],ebtot[0]]/ebtotgen0fit[0], $
      LINESTYLE=2, THICK=3, COLOR=cnoir
   
;  OPLOT, xr, [ALOG10(rsun^3*ebtotmax),ALOG10(rsun^3*ebtotmax)], $
;  OPLOT, xr, [ebtotmax,ebtotmax]/ebtotmax, $
;     LINESTYLE=2, THICK=3, COLOR=cnoir

OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

ENDIF

;;---- Magnetic energy read from simulation output, smoothed -----
;OPLOT, time-timei, ALOG10(rsun^3*ebtotsmth), $
;;OPLOT, time-timei, ebtotsmth/ebtotmax, $
;   LINESTYLE=0, THICK=5, COLOR=cgrey
;
;;---- Calculated magnetic energy ----
;OPLOT, time-timei, ALOG10(rsun^3*ebto2), $
;;OPLOT, time-timei, ebto2/ebtotmax, $
;   LINESTYLE=2, THICK=3, COLOR=cnoir

;---- Magnetic energy read from simulation output -----
OPLOT, time[       0:itfit[0]]-timei, ALOG10(rsun^3*ebtot [       0:itfit[0]]), $
;OPLOT, time[       0:itfit[0]]-timei, ebtot [       0:itfit[0]]/ebtotmax, $
;OPLOT, time[       0:itfit[0]]-timei, ebtot [       0:itfit[0]]/ebtotgen0fit, $
   LINESTYLE=2, THICK=3, COLOR=cnoir
OPLOT, time[itfit[0]:itfit[1]]-timei, ALOG10(rsun^3*ebtot [itfit[0]:itfit[1]]), $
;OPLOT, time[itfit[0]:itfit[1]]-timei, ebtot [itfit[0]:itfit[1]]/ebtotmax, $
;OPLOT, time[itfit[0]:itfit[1]]-timei, ebtot [itfit[0]:itfit[1]]/ebtotgen0fit, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, time[itfit[1]:      nt]-timei, ALOG10(rsun^3*ebtot [itfit[1]:      nt]), $
;OPLOT, time[itfit[1]:      nt]-timei, ebtot [itfit[1]:      nt]/ebtotmax, $
;OPLOT, time[itfit[1]:      nt]-timei, ebtot [itfit[1]:      nt]/ebtotgen0fit, $
   LINESTYLE=2, THICK=3, COLOR=cnoir

;;---- Calculated magnetic energy, optimization zone ----
;OPLOT, time[       0:itfit[0]]-timei, ALOG10(rsun^3*ebuz  [       0:itfit[0]]), $
;;OPLOT, time[       0:itfit[0]]-timei, ebuz  [       0:itfit[0]]/ebuzmax, $
;   LINESTYLE=2, THICK=3, COLOR=cvert
;OPLOT, time[itfit[0]:itfit[1]]-timei, ALOG10(rsun^3*ebuz  [itfit[0]:itfit[1]]), $
;;OPLOT, time[itfit[0]:itfit[1]]-timei, ebuz  [itfit[0]:itfit[1]]/ebuzmax, $
;   LINESTYLE=0, THICK=3, COLOR=cvert
;OPLOT, time[itfit[1]:      nt]-timei, ALOG10(rsun^3*ebuz  [itfit[1]:      nt]), $
;;OPLOT, time[itfit[1]:      nt]-timei, ebuz  [itfit[1]:      nt]/ebuzmax, $
;   LINESTYLE=2, THICK=3, COLOR=cvert
;
;;---- Calculated 'magnetic energy', from emergence function bfit ----
;OPLOT, time[       0:itfit[0]]-timei, ALOG10(rsun^3*ebfits[       0:itfit[0]]), $
;;OPLOT, time[       0:itfit[0]]-timei, ebfits[       0:itfit[0]]/ebfitsmax, $
;   LINESTYLE=2, THICK=3, COLOR=croug
;OPLOT, time[itfit[0]:itfit[1]]-timei, ALOG10(rsun^3*ebfits[itfit[0]:itfit[1]]), $
;;OPLOT, time[itfit[0]:itfit[1]]-timei, ebfits[itfit[0]:itfit[1]]/ebfitsmax, $
;   LINESTYLE=0, THICK=3, COLOR=croug
;OPLOT, time[itfit[1]:      nt]-timei, ALOG10(rsun^3*ebfits[itfit[1]:      nt]), $
;;OPLOT, time[itfit[1]:      nt]-timei, ebfits[itfit[1]:      nt]/ebfitsmax, $
;   LINESTYLE=2, THICK=3, COLOR=croug

;OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
;OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

;;FOR icycle=0,ncycles DO BEGIN
;;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

IF (nemaxcyc GE 1) THEN BEGIN
   OPLOT, time[itemaxcyc]-timei, ALOG10(rsun^3*ebtotmaxcyc), $
;  OPLOT, time[itemaxcyc]-timei, ebtotmaxcyc/ebtotmax, $
;  OPLOT, time[itemaxcyc]-timei, ebtotmaxcyc/ebtotgen0fit[itemaxcyc], $
      psym=8, SYMSIZE=2.0, THICK=5, COLOR=cnoir
   OPLOT, time[itemincyc]-timei, ALOG10(rsun^3*ebtotmincyc), $
;  OPLOT, time[itemincyc]-timei, ebtotmincyc/ebtotmax, $
;  OPLOT, time[itemincyc]-timei, ebtotmincyc/ebtotgen0fit[itemincyc], $
      psym=6, SYMSIZE=0.8, THICK=5, COLOR=cnoir
ENDIF

XYOUTS, xr[0]+1.00*(xr[1]-xr[0]), yr[1]+youts0*(yr[1]-yr[0]), STRING(nemaxcyc,FORMAT='(i5)')+' cycles', CHARSIZE=gros, ALIGNMENT=1.0

;IF (nemaxcyc GE 2) THEN BEGIN
;
;   OPLOT, time-timei, ALOG10(rsun^3*ebtotgen0fit), $
;;  OPLOT, time-timei, ebtotgen0fit/ebtotmax, $
;;  OPLOT, time-timei, ebtotgen0fit/ebtotgen0fit, $
;      LINESTYLE=0, THICK=3, COLOR=croug
;   
;   OPLOT, time-timei, ALOG10(rsun^3*ebtotmaxfit), $
;;  OPLOT, time-timei, ebtotmaxfit/ebtotmax, $
;;  OPLOT, time-timei, ebtotmaxfit/ebtotgen0fit, $
;      LINESTYLE=2, THICK=3, COLOR=cbleu
;   
;   OPLOT, time-timei, ALOG10(rsun^3*ebtotminfit), $
;;  OPLOT, time-timei, ebtotminfit/ebtotmax, $
;;  OPLOT, time-timei, ebtotminfit/ebtotgen0fit, $
;      LINESTYLE=1, THICK=3, COLOR=cbleu
;   
;   OPLOT, time-timei, ALOG10(rsun^3*ebtotgenfit), $
;;  OPLOT, time-timei, ebtotgenfit/ebtotmax, $
;;  OPLOT, time-timei, ebtotgenfit/ebtotgen0fit, $
;      LINESTYLE=0, THICK=3, COLOR=cbleu
;   
;   title5a = '!7s!17 =' + STRING([ebtotgen0par[1],ebtotmaxpar[1],ebtotminpar[1],ebtotgenpar[1]],FORMAT='(4f7.1)') + 'yr, !7v!17 =' + STRING([ebtotmaxchi,ebtotminchi,ebtotgenchi]/ebtotmax,FORMAT='(3e9.2)')
;   XYOUTS, xr(0)+1.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title5a, CHARSIZE=gros, ALIGNMENT=1.0
;ENDIF
;
;XYOUTS, xr(0)+0.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title5 , CHARSIZE=gros

;---- Plot FFT on magnetic energy evolution ----------------------------

xr     = [0,0.2]
;xtitle = '!17Frequency (yr!U-1!N)'
xtitle = '!17Frequency/yr!U-1!N'
yr     = [1.e-4,1.e0]
;yr     = [0.,ebfft1ampmax]
ytitle = '!17FT power (energy)'

IF ((ibfitfac EQ nbfitfac-1) AND (ibldseed EQ nbldseed-1)) THEN BEGIN

iposplot=iposplot+1
PLOT, xr, [yr[0],yr[0]], $
;  /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=gros, $           ;, XTICKS=4
   /YLOG, YRANGE=yr, YTICKLEN=0.01, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, freq, ebfft1amp, $
;  PSYM=8, SYMSIZE=1.0, COLOR=cnoir
   LINESTYLE=0, THICK=3, COLOR=cnoir

OPLOT, [freq[iebfft1max],freq[iebfft1max]], yr, $
   LINESTYLE=2, THICK=3, COLOR=cnoir

XYOUTS, freq[iebfft1max], yr[0]+0.90*(yr[1]-yr[0]), STRING(freq[iebfft1max],FORMAT='(f6.3)')+'yr', CHARSIZE=gros, ALIGNMENT=0.0

OPLOT, freqq, ebfft1ampfitt, $
   LINESTYLE=0, THICK=3, COLOR=cbleu

XYOUTS, xr[0]+0.00*(xr[1]-xr[0]), yr[0]+0.90*(yr[1]-yr[0]), '!18P!17 ='+STRING(ebfft1amppar[0],FORMAT='(f9.6)')+'exp(-(!18f!17-'+STRING(ebfft1amppar[1],FORMAT='(f9.6)')+')!U2!N/(2*'+STRING(ebfft1amppar[2],FORMAT='(f9.6)')+'!U2!N))', CHARSIZE=gros, ALIGNMENT=0.0

ENDIF

;---- Plot Magnetic energy maxima distribution -------------------------

xr    = [minebtotmaxcyc,maxebtotmaxcyc]
;xtitle= '!17Log !18E!DB!N!17 (erg)'
xtitle= '!17Log(!18E!DB!N!17/erg)'
yr    = [0.0,0.3]
;ytitle= '!18f!D!17Log!18E!DB!N!17!N (erg!U-1!N)'
ytitle= '!18f!D!17Log(!18E!DB!N!17!N/erg)'

order=[]
FOR ibin=0,nebtotmaxcycbin-1 DO order=[order,ibin,ibin+nebtotmaxcycbin]
ebtotmaxcycbinplot = [ebtotmaxcycbinn(0:nebtotmaxcycbin-1),ebtotmaxcycbinn(1:nebtotmaxcycbin)]
ebtotmaxcycbinplot = ebtotmaxcycbinplot[order]
histebtotmaxcycplot= [histebtotmaxcyc,histebtotmaxcyc]
histebtotmaxcycplot= histebtotmaxcycplot[order]

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, ebtotmaxcycbinplot, histebtotmaxcycplot, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

;histebtotmaxcycpar=[0.17,21.35,0.54]
;histebtotmaxcycfit=histebtotmaxcycpar[0]*EXP(-0.5*(ebtotmaxcycbin-histebtotmaxcycpar[1])^2/histebtotmaxcycpar[2]^2)

;OPLOT, ebtotmaxcycbin, histebtotmaxcycfit, $
;   LINESTYLE=0, THICK=3, COLOR=cbleu

;---- Growth/decay rate and frequency vs Dynamo number -------------

;---- Growth rate -----

xr     = [0.2,0.8]
xtitle = '!17Dynamo number !18K!17'
xstyle = 5
IF (openans EQ 'y') THEN xstyle=1
yr     = [-0.2,0.2]
;ytitle = '!17Growth rate (yr!U-1!N)'
ytitle = '!17Growth rate/yr!U-1!N'
ystyle = 5
IF (openans EQ 'y') THEN ystyle=1

iposplot=iposplot+1
PLOT, xr, [yr[0],yr[0]], $
   /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.42*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;  /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=xstyle, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=gros, $     ;, XTICKS=4
   YRANGE=yr, YSTYLE=ystyle, YTICKLEN=0.02, YTITLE=ytitle, YCHARSIZE=gros

IF (openans EQ 'y') THEN BEGIN
   OPLOT, xr, [0.,0.], $
      LINESTYLE=2, THICK=3, COLOR=cnoir
ENDIF

IF (nemaxcyc GE 2) THEN BEGIN
   OPLOT, [1.,1.]*bfitfac/bfitfaccor, [MIN([1./ebtotminpar[1],1./ebtotmaxpar[1]]),MAX([1./ebtotminpar[1],1./ebtotmaxpar[1]])], $
      LINESTYLE=0, THICK=3, COLOR=cgrey
;     PSYM=8, SYMSIZE=0.5, COLOR=cgrey
ENDIF

IF (ntwhere GE 2 AND nemaxcyc GE 2) THEN BEGIN
  OPLOT, [bfitfac/bfitfaccor], [1./ebtotgenpar[1]], $
     PSYM=8, SYMSIZE=1.0, COLOR=cgrey
   
;  growthrate=[growthrate,1./ebtotgenpar[1]]
;  IF (ibldseed EQ N_ELEMENTS(tbldseedv)-1) THEN BEGIN
;     OPLOT, [1.,1.]*bfitfac/bfitfaccor, [MIN(growthrate),MAX(growthrate)], $
;        LINESTYLE=0, THICK=9, COLOR=cgrey
;     OPLOT, [1.]*bfitfac/bfitfaccor, [MEAN(growthrate)], $
;        PSYM=8, SYMSIZE=2.0, COLOR=cgrey
;  ENDIF
   
ENDIF

;---- Frequency -----

xr     = [0.2,0.8]
xtitle = '!17Dynamo number !18K!17'
xstyle = 5
IF (openans EQ 'y') THEN xstyle=1
yr     = [0.,0.2]
;ytitle = '!17Frequency (yr!U-1!N)'
ytitle = '!17Frequency/yr!U-1!N'
ystyle = 5
IF (openans EQ 'y') THEN ystyle=1

;iposplot=iposplot+1
PLOT, xr, [yr[0],yr[0]], $
   /NOERASE, POS=[pos[0,iposplot]+0.58*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;  /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=xstyle, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=gros, $     ;, XTICKS=4
   YRANGE=yr, YSTYLE=ystyle, yTICKLEN=0.02, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, [bfitfac/bfitfaccor], [freq[iebfft0max]], $
   PSYM=8, SYMSIZE=1.0, COLOR=cbleu

IF (nemaxcyc GE 3) THEN BEGIN
;;  OPLOT, REPLICATE([bfitfac/bfitfaccor],nfmaxcyc-1), [1./peremaxcyc], $
;;     PSYM=8, SYMSIZE=0.5, COLOR=cgrey
   OPLOT, [1.,1.]*bfitfac/bfitfaccor, [MIN(1./peremaxcyc),MAX(1./peremaxcyc)], $
      LINESTYLE=0, THICK=3, COLOR=cgrey
;     PSYM=8, SYMSIZE=0.5, COLOR=cgrey
   OPLOT, [bfitfac/bfitfaccor], [MEAN(1./peremaxcyc)], $
      PSYM=8, SYMSIZE=1.0, COLOR=cgrey
   
;  frequency=[frequency,MEAN(1./peremaxcyc)]
;  IF (ibldseed EQ N_ELEMENTS(tbldseedv)-1) THEN BEGIN
;     OPLOT, [1.,1.]*bfitfac/bfitfaccor, [MIN(frequency),MAX(frequency)], $
;        LINESTYLE=0, THICK=9, COLOR=cgrey
;     OPLOT, [1.]*bfitfac/bfitfaccor, [MEAN(frequency)], $
;        PSYM=8, SYMSIZE=2.0, COLOR=cgrey
;  ENDIF
   
ENDIF

;---- Stability diagram for Bfit ---------------------------------------

;---- Black-White-Grey-Black color table -----

levgrey = [-1000.,0.,0.03125,0.0625,0.125,0.25,0.50,1.00]
;levgrey = [-1000.,0.,0.04,0.12,0.20,0.28,0.36,0.44,0.52,0.60,0.68,0.76,0.84,0.92,1.00]

nlevgrey=N_ELEMENTS(levgrey)

LOADCT,0,NCOLORS=nlevgrey
TVLCT,red,gre,blu,/GET
red[1:nlevgrey]=REVERSE(red[0:nlevgrey-1])
gre[1:nlevgrey]=REVERSE(gre[0:nlevgrey-1])
blu[1:nlevgrey]=REVERSE(blu[0:nlevgrey-1])
TVLCT,red,gre,blu

cingrey=INDGEN(nlevgrey)
clstgrey = INDGEN(nlevgrey)

;---- Plot ----- franky emergence function start

xr    = [0.,MAX(b_ygrid)]
;xtitle= '!18B!17!D!7u!17!N(!18r*!17,!7h!17) (G)'
xtitle= '!18B!17!D!7u!17!N(!18r*!17,!7h!17)/G'
yr    = [0.,90.]
yname = ['EQ','30!9%!17N','60!9%!17N','90!9%!17N']
ytitle= '!17Latitude'

; franky FFT value bfitfgrid
fac = bfitfmax          ;6.88
levfac=1.
fctplot=-bfitfgrid/fac
levv=levgrey*levfac

iposplot=iposplot+1
CONTOUR, fctplot, b_ygrid, xxan, $
   /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LEVELS=levv, C_COLOR=cingrey, C_LINESTYLE=clstgrey, /FILL, $
;  /NOERASE, POS=pos(*,iposplot), LEVELS=levv, C_COLOR=cingrey, C_LINESTYLE=clstgrey, /FILL, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=gros, $                            ;, XTICKS=4
   YRANGE=yr, YTICKLEN=0.01, YMINOR=3, YTICKS=N_ELEMENTS(yname)-1, YTICKNAME=yname, YTITLE=ytitle, YCHARSIZE=gros

;---- Color bar -----

;ytbar= '!18F!DB!N!17(!7h!17,!18t!17) (!9X!17'+STRING(fac,FORMAT='(f5.2)')+')'
ytbar= '!18F!DB!N!17(!7h!17,!18t!17)/'+STRING(fac,FORMAT='(f5.2)')
;yrbar= [-1,1]
yrbar= [0,levfac]

cb = REPLICATE(1.,2) # levv
cbx = FINDGEN(2)

CONTOUR, cb, cbx, levv, $
   /NOERASE, POS = pos0(*,iposplot), $
   LEVELS = levv, C_COLOR = cingrey, /FILL, $
   XSTYLE=1, XTICKS=1, XCHARSIZE=rikiki, $
   YRANGE=yrbar, YTICKS=4, YMINOR=0, YTICKLEN=rikiki, YTITLE=ytbar, YCHARSIZE = gros

;---- Restore colored color table -----
red(0:nlev)=rgbvec(0,*)
gre(0:nlev)=rgbvec(1,*)
blu(0:nlev)=rgbvec(2,*)
TVLCT,red,gre,blu

;;---- Weighting function -----
;
;PLOT, ABS(bfit)/bfitmax, ABS(bfitt)/bfittmax, $
;   /NOERASE, POS = [pos(2,iposplot),pos(3,iposplot),pos0(0,iposplot-1),pos0(1,iposplot-1)], PSYM=3

;---- Color-filled contours for Bfit -----------------------------------

title3   = '!18F!DB!N!17[' + STRING(bcrit,FORMAT='(i5)') + 'G, ' + STRING(dbcrit,FORMAT='(i4)') + 'G,' + STRING(cc,FORMAT='(f5.2)') + ',' + STRING(x0crit,FORMAT='(f5.2)') + ',' + STRING(xcrit,FORMAT='(f5.2)') + ',' + STRING(dxcrit,FORMAT='(f5.2)') + ',' + STRING(bfitfac,FORMAT='(f5.2)') + '], max =' + STRING(bfitfmax,FORMAT='(f6.2)')
title3a = '!17C ='   + STRING(cbfitbfly,FORMAT='(f6.3)')
title3b = '!7v!17 =' + STRING(xbfitbfly,FORMAT='(f6.3)')

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr    = [-90.,90.]
yname = ['!1790!9%!17S','45!9%!17S','EQ','45!9%!17N','90!9%!17N']
ytitle= '!17Latitude'

fac = 1.98        ;bfitfmax    ;bflyfitmax        ;10.      ;5.5            ;
levfac=1.
fctplot=bfitf/fac
levv=lev*levfac

iposplot=iposplot+1
CONTOUR, fctplot, time-timei, xxan, $
   /NOERASE, POS=pos(*,iposplot), LEVELS=levv, C_COLOR=cin, C_LINESTYLE=clst, /FILL, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, YTICKLEN=0.01, YMINOR=3, YTICKS=N_ELEMENTS(yname)-1, YTICKNAME=yname, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

XYOUTS, xr(0)+0.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title3, CHARSIZE=gros
XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+0.90*(yr(1)-yr(0)), title3a, CHARSIZE=gros, ALIGNMENT=1.0
XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+0.80*(yr(1)-yr(0)), title3b, CHARSIZE=gros, ALIGNMENT=1.0

;---- Color bar -----

;ytbar= '!18F!DB!N!17(!7h!17,!18t!17) (!9X!17'+STRING(fac,FORMAT='(f5.2)')+')'
ytbar= '!18F!DB!N!17(!7h!17,!18t!17)/'+STRING(fac,FORMAT='(f5.2)')
;yrbar= [-1,1]
yrbar= [-levfac,levfac]

cb = REPLICATE(1.,2) # levv
cbx = FINDGEN(2)

CONTOUR, cb, cbx, levv, $
   /NOERASE, POS = pos0(*,iposplot), $
   LEVELS = levv, C_COLOR = cin, /FILL, $
   XSTYLE=1, XTICKS=1, XCHARSIZE=rikiki, $
   YRANGE=yrbar, YTICKS=4, YMINOR=0, YTICKLEN=rikiki, YTITLE=ytbar, YCHARSIZE = gros

;---- Plot butterfly diagram for emerged BMRs --------------------------

title6  = '!17[!18N!17!DBMR!N]!S!U' + STRING(expfly,FORMAT='(f5.2)') + '!R!DSmoothed!9X!17' + STRING(nsmth,FORMAT='(i2)') + '!N, max=' + STRING(bflyfitmax,format='(f6.2)')

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr    = [-90.,90.]
yname = ['!1790!9%!17S','45!9%!17S','EQ','45!9%!17N','90!9%!17N']
ytitle= '!17Latitude'

fac = 2.63        ;bflyfitmax        ;10.         ;4.
levfac=1.0
fctplot=bfly0fit/fac
levv=lev*levfac

iposplot=iposplot+1
CONTOUR, fctplot, time-timei, xxan,$
   /NOERASE, POS=pos(*,iposplot), LEVELS=levv, C_COLOR=cin, C_LINESTYLE=clst, /FILL, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, YTICKLEN=0.01, YMINOR=3, YTICKS=N_ELEMENTS(yname)-1, YTICKNAME=yname, YTITLE=ytitle, YCHARSIZE=gros

;CONTOUR, bfitf(itfit[0]:itfit[1],*)/bfitfmax, time(itfit[0]:itfit[1]), xxan, $
;   /OVERPLOT, LEVELS=[-1.,1.]*bfitcrit*levfac, C_COLOR=[cin(1),cin(nlev-3)], C_LINESTYLE=[0,0], C_THICK=[3,3], $

;OPLOT, tday0/365., 90.-(tht10+tht20)/2., $
;   PSYM=3

;OPLOT, time-timei, latmin, $
;   LINESTYLE=0, THICK=1
;OPLOT, time-timei, latmax, $
;   LINESTYLE=0, THICK=1

;OPLOT, xr, [xxan(ixfit[0]),xxan(ixfit[0])], LINESTYLE=2
;OPLOT, xr, [xxan(ixfit[1]),xxan(ixfit[1])], LINESTYLE=2
OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

;XYOUTS, xr(0)+0.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title6, CHARSIZE=gros
XYOUTS, xr[0]+1.005*(xr[1]-xr[0]), yr[0]+0.00*(yr[1]-yr[0]), '!17(h)', CHARSIZE=gros, ALIGNMENT=0.0

;---- Color bar -----

;ytbar= '!18N!17!DBMR!N(!7h!17,!18t!17) (!9X!17'+STRING(fac,FORMAT='(f5.2)')+')'
ytbar= '!18N!17!DBMR!N(!7h!17,!18t!17)/'+STRING(fac,FORMAT='(f5.2)')
;yrbar= [-1,1]
yrbar= [-levfac,levfac]

cb = REPLICATE(1.,2) # levv
cbx = FINDGEN(2)

CONTOUR, cb, cbx, levv, $
   /NOERASE, POS = pos0(*,iposplot), $
   LEVELS = levv, C_COLOR = cin, /FILL, $
   XSTYLE=1, XTICKS=1, XCHARSIZE=rikiki, $
   YRANGE=yrbar, YTICKS=4, YMINOR=0, YTICKLEN=rikiki, YTITLE=ytbar, YCHARSIZE = gros

;---- Plot pseudo-SSN (and SSN) ----------------------------------------

title7  = '!7R!Dh!N!18N!17!DBMR!N, max =' + STRING(bfly0cumulmax,format='(f6.0)')
;title7a = '!17C =' + STRING(cebssn,FORMAT='(f5.2)')

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr = [0.,200.]
;yr = [0,1.]*MAX([MAX(bflycumul),MAX(bfitcumul)])/bflycumulmax
ytitle='!7R!Dh!N!18N!17!DBMR!N'

iposplot=iposplot+1
PLOT, xr, [0.,0.], $
   /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, yTICKLEN=0.01, YMINOR=2, YTITLE=ytitle, YCHARSIZE=gros

;OPLOT, xr, [bflycumulmax,bflycumulmax]/bflycumulmax, $
;   LINESTYLE=0, THICK=3, COLOR=cnoir

OPLOT, ssntime-ssntime[0], ssnsmth, $                                   ;/ssnsmthmax
   LINESTYLE=0, THICK=3, COLOR=cbleu

;OPLOT, time-timei, ssne, $                                              ;/ssnemax
;   LINESTYLE=0, THICK=3, COLOR=cbleu

;OPLOT, time-timei, bflycumulsmth, $                                     ;/bflycumulmax, $
;   LINESTYLE=0, THICK=6, COLOR=cgrey

OPLOT, time[       0:itfit[0]]-timei, bfly0cumul[       0:itfit[0]], $   ;/bflycumulmax, $
   LINESTYLE=2, THICK=3, COLOR=cgrey
OPLOT, time[itfit[0]:itfit[1]]-timei, bfly0cumul[itfit[0]:itfit[1]], $   ;/bflycumulmax, $
   LINESTYLE=0, THICK=3, COLOR=cgrey
OPLOT, time[itfit[1]:      nt]-timei, bfly0cumul[itfit[1]:      nt], $   ;/bflycumulmax, $
   LINESTYLE=2, THICK=3, COLOR=cgrey

OPLOT, time[       0:itfit[0]]-timei, bflycumul[       0:itfit[0]], $   ;/bflycumulmax, $
   LINESTYLE=2, THICK=3, COLOR=cnoir
OPLOT, time[itfit[0]:itfit[1]]-timei, bflycumul[itfit[0]:itfit[1]], $   ;/bflycumulmax, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, time[itfit[1]:      nt]-timei, bflycumul[itfit[1]:      nt], $   ;/bflycumulmax, $
   LINESTYLE=2, THICK=3, COLOR=cnoir

;OPLOT, time[       0:itfit[0]]-timei, bfitcumul[       0:itfit[0]]/bflycumulmax, $
;   LINESTYLE=2, THICK=3, COLOR=cbleu
;OPLOT, time[itfit[0]:itfit[1]]-timei, bfitcumul[itfit[0]:itfit[1]]/bflycumulmax, $
;   LINESTYLE=0, THICK=3, COLOR=cbleu
;OPLOT, time[itfit[1]:      nt]-timei, bfitcumul[itfit[1]:      nt]/bflycumulmax, $
;   LINESTYLE=2, THICK=3, COLOR=cbleu

OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6
;franky TFITF2 is equivalent to itfit[1]
;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

IF (nfmaxcyc GE 1) THEN BEGIN
   OPLOT, time[itfmaxcyc]-timei, bflymaxcyc, $                              ;/bflycumulmax, $
      psym=8, SYMSIZE=2.0, THICK=5, COLOR=cnoir
   OPLOT, time[itfmincyc]-timei, bflymincyc, $                              ;/bflycumulmax, $
      psym=6, SYMSIZE=0.8, THICK=5, COLOR=cnoir
ENDIF
FOR icyc=1,nfmaxcyc-1 DO BEGIN
   XYOUTS, time[itfmincyc[icyc-1]]-timei+0.60*(time[itfmincyc[icyc]]-time[itfmincyc[icyc-1]]), yr[0]+0.05*(yr[1]-yr[0]), STRING(bflycountcyc[icyc],FORMAT='(i4)'), CHARSIZE=gros, ALIGNMENT=0., ORIENTATION=90
ENDFOR

XYOUTS, xr[0]+1.00*(xr[1]-xr[0]), yr[1]+youts0*(yr[1]-yr[0]), STRING(nfmaxcyc,FORMAT='(i5)')+' cycles', CHARSIZE=gros, ALIGNMENT=1.0

;XYOUTS, xr(0)+0.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title7 , CHARSIZE=gros
;
;IF (nfmaxcyc GE 2) THEN BEGIN
;;  OPLOT, time-timei, bflymaxfit/bflycumulmax, $
;;     LINESTYLE=0, THICK=3, COLOR=cbleu
;   
;   title7a = '!7s!17 =' + STRING(bflymaxpar[1],FORMAT='(f8.0)') + ', !7v!17 =' + STRING(bflymaxchi/bfitcumulmax,FORMAT='(e9.2)')
;   XYOUTS, xr(0)+1.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title7a, CHARSIZE=gros, ALIGNMENT=1.0
;ENDIF

;---- Plot FFT on pseudo-SSN (and SSN) ---------------------------------

xr     = [0.,0.2]
;xtitle = '!17Frequency (yr!U-1!N)'
xtitle = '!17Frequency/yr!U-1!N'
yr     = [1.e-4,1.e0]
;yr     = [0.,snfft0ampmax]
ytitle = '!17FT power (signed SSN)'

IF ((ibfitfac EQ nbfitfac-1) AND (ibldseed EQ nbldseed-1)) THEN BEGIN

iposplot=iposplot+1
PLOT, xr, [yr[0],yr[0]], $
;  /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=gros, $           ;, XTICKS=4
   /YLOG, YRANGE=yr, YTICKLEN=0.01, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, ssnfreq, ssnfft0amp, $
   LINESTYLE=0, THICK=3, COLOR=cbleu

OPLOT, freq, unfft0amp, $
;  PSYM=8, SYMSIZE=1.0, COLOR=cnoir
   LINESTYLE=0, THICK=3, COLOR=cnoir

OPLOT, [freq[iunfft0max],freq[iunfft0max]], yr, $
   LINESTYLE=2, THICK=3, COLOR=cnoir

XYOUTS, freq[iunfft0max], yr[0]+0.90*(yr[1]-yr[0]), STRING(freq[iunfft0max],FORMAT='(f6.3)')+'yr', CHARSIZE=gros, ALIGNMENT=0.0

OPLOT, freqq, unfft0ampfitt, $
   LINESTYLE=0, THICK=3, COLOR=cbleu

XYOUTS, xr[0]+0.00*(xr[1]-xr[0]), yr[0]+0.90*(yr[1]-yr[0]), '!18P!17 ='+STRING(unfft0amppar[0],FORMAT='(f9.6)')+'exp(-(!18f!17-'+STRING(unfft0amppar[1],FORMAT='(f9.6)')+')!U2!N/(2*'+STRING(unfft0amppar[2],FORMAT='(f9.6)')+'!U2!N))', CHARSIZE=gros, ALIGNMENT=0.0

ENDIF

;---- Plot Pseudo-SSN maxima distribution ------------------------------

xr    = [minbflymaxcyc,maxbflymaxcyc]
xtitle= '!7R!Dh!N!18N!17!DBMR!N'
yr    = [0.0,0.3]
ytitle= '!18f!D!7R!Ih!D!18N!17!IBMR!N'

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros

order=[]
FOR ibin=0,nbflymaxcycbin-1 DO order=[order,ibin,ibin+nbflymaxcycbin]
bflymaxcycbinplot = [bflymaxcycbinn(0:nbflymaxcycbin-1),bflymaxcycbinn(1:nbflymaxcycbin)]
bflymaxcycbinplot = bflymaxcycbinplot[order]
histbflymaxcycplot= [histbflymaxcyc,histbflymaxcyc]
histbflymaxcycplot= histbflymaxcycplot[order]

OPLOT, bflymaxcycbinplot, histbflymaxcycplot, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

order=[]
FOR ibin=0,nbflysmaxcycbin-1 DO order=[order,ibin,ibin+nbflysmaxcycbin]
bflysmaxcycbinplot = [bflysmaxcycbinn(0:nbflysmaxcycbin-1),bflysmaxcycbinn(1:nbflysmaxcycbin)]
bflysmaxcycbinplot = bflysmaxcycbinplot[order]
histbflysmaxcycplot= [histbflysmaxcyc,histbflysmaxcyc]
histbflysmaxcycplot= histbflysmaxcycplot[order]

OPLOT, bflysmaxcycbinplot, histbflysmaxcycplot, $
   LINESTYLE=0, THICK=3, COLOR=cbleu

order=[]
FOR ibin=0,nbflynmaxcycbin-1 DO order=[order,ibin,ibin+nbflynmaxcycbin]
bflynmaxcycbinplot = [bflynmaxcycbinn(0:nbflynmaxcycbin-1),bflynmaxcycbinn(1:nbflynmaxcycbin)]
bflynmaxcycbinplot = bflynmaxcycbinplot[order]
histbflynmaxcycplot= [histbflynmaxcyc,histbflynmaxcyc]
histbflynmaxcycplot= histbflynmaxcycplot[order]

OPLOT, bflynmaxcycbinplot, histbflynmaxcycplot, $
   LINESTYLE=0, THICK=3, COLOR=cvert

;histbflymaxcycpar=[0.17,21.35,0.54]
;histbflymaxcycfit=histbflymaxcycpar[0]*EXP(-0.5*(bflymaxcycbin-histbflymaxcycpar[1])^2/histbflymaxcycpar[2]^2)

;OPLOT, bflymaxcycbin, histbflymaxcycfit, $
;   LINESTYLE=0, THICK=3, COLOR=cbleu

;---- Plot sunspot number hemispheric asymmetries ----------------------

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr = [-20.,100.]
ytitle='!7R!Dh!N!18N!17!DBMR!N'

iposplot=iposplot+1
PLOT, xr, [0.,0.], $
   /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, yTICKLEN=0.01, YMINOR=2, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, time-timei, bflyscumulsmth, $                                    ;/bflycumulmax, $
   LINESTYLE=0, THICK=3, COLOR=cbleu
OPLOT, time-timei, bflyncumulsmth, $                                    ;/bflycumulmax, $
   LINESTYLE=0, THICK=3, COLOR=cvert
OPLOT, time-timei, bflyncumulsmth-bflyscumulsmth, $                     ;/bflycumulmax, $
   LINESTYLE=0, THICK=3, COLOR=croug

OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

IF (nfsmaxcyc GE 1) THEN BEGIN
   OPLOT, timefsmaxcyc-timei, bflysmaxcyc, $
      psym=8, SYMSIZE=2.0, THICK=5, COLOR=cbleu
   OPLOT, timefsmincyc-timei, bflysmincyc, $
      psym=6, SYMSIZE=0.8, THICK=5, COLOR=cbleu
ENDIF
IF (nfnmaxcyc GE 1) THEN BEGIN
   OPLOT, timefnmaxcyc-timei, bflynmaxcyc, $
      psym=8, SYMSIZE=2.0, THICK=5, COLOR=cvert
   OPLOT, timefnmincyc-timei, bflynmincyc, $
      psym=6, SYMSIZE=0.8, THICK=5, COLOR=cvert
ENDIF

;---- Plot cycle periods -----------------------------------------------

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr = [8.,14.]
;ytitle='!17Period (yr)'
ytitle='!17Period/years'

iposplot=iposplot+1
PLOT, xr, [0.,0.], $
   /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, yTICKLEN=0.01, YMINOR=2, YTITLE=ytitle, YCHARSIZE=gros

IF (nfmaxcyc GE 2) THEN BEGIN
   
   OPLOT, (time[itfmincyc[1:nfmaxcyc-1]]+time[itfmincyc[0:nfmaxcyc-2]])/2.-timei, perfmaxcyc, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
   OPLOT, (time[itfmincyc[1:nfmaxcyc-1]]+time[itfmincyc[0:nfmaxcyc-2]])/2.-timei, perfmaxcyc, $
      psym=6, SYMSIZE=0.8, THICK=5, COLOR=cnoir
   
   OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
   OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6
   ;FOR icycle=0,ncycles DO BEGIN
   ;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
   ;ENDFOR
   FOR icyc=0,nfmaxcyc-1 DO BEGIN
      OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
         LINESTYLE=1, THICK=3, COLOR=cnoir
   ENDFOR
   OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
      LINESTYLE=0, THICK=3, COLOR=cnoir
   OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
      LINESTYLE=0, THICK=3, COLOR=cnoir
   
   title8  = '!17Cycle periods, min =' + STRING(MIN(perfmaxcyc),format='(f6.2)') + 'years, max =' + STRING(MAX(perfmaxcyc),format='(f6.2)') + 'years'
   
   XYOUTS, xr(0)+0.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title8 , CHARSIZE=gros
   
END

;---- Plot hemispheric delay -------------------------------------------

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr = [-2.,2.]
;ytitle='!17Delay (yr)'
ytitle='!17Delay/years'

iposplot=iposplot+1
PLOT, xr, [0.,0.], $
   /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, yTICKLEN=0.01, YMINOR=2, YTITLE=ytitle, YCHARSIZE=gros

IF (nfsmaxcyc GE 1) THEN BEGIN
   
   OPLOT, time[itfmincyc]-timei, delfmincyc, $
      LINESTYLE=1, THICK=3, COLOR=croug
   OPLOT, time[itfmincyc]-timei, delfmincyc, $
      psym=6, SYMSIZE=0.8, THICK=5, COLOR=croug
   
   OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
   OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6
   ;FOR icycle=0,ncycles DO BEGIN
   ;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
   ;ENDFOR
   FOR icyc=0,nfmaxcyc-1 DO BEGIN
      OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
         LINESTYLE=1, THICK=3, COLOR=cnoir
   ENDFOR
   OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
      LINESTYLE=0, THICK=3, COLOR=cnoir
   OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
      LINESTYLE=0, THICK=3, COLOR=cnoir
   
ENDIF

;---- Simulation time -----

;PLOT, xrsim, [0.,0.], $
;   /NOERASE, POS=pos(*,iposplot), $
;   XRANGE=xrsim, XTITLE='!17t/!7s!17', $
;   YCHARSIZE=rikiki, YTICKLEN=rikiki, CHARSIZE = gros

;---- Emergence statistics ---------------------------------------------

iposplot=iposplot+1

;---- Log(flux) histogram ----- franky

title8a = '!7U!17!UMAX!N =' + STRING(flux0max, FORMAT='(f6.1)') + '!9X!17 10!U21!N Mx'

nfluxbin   = 40
minlogflux = 19.                             ;FLOAT(FLOOR(MIN(logflux0)))
maxlogflux = 23.                             ;FLOAT(CEIL (MAX(logflux0)))

logfluxbinn= FINDGEN(nfluxbin+1)/nfluxbin*(maxlogflux-minlogflux) + minlogflux
logfluxbin = (logfluxbinn(0:nfluxbin-1)+logfluxbinn(1:nfluxbin))/2.

histlogflux=HISTOGRAM(logflux0,MIN=minlogflux,MAX=maxlogflux,NBINS=nfluxbin+1,LOCATIONS=loclogflux) & histlogflux=histlogflux[0:nfluxbin-1]

;histlogflux(WHERE(histlogflux EQ 0.)) = 1.e-2                          ; Éviter les valeurs de 0
;histlogflux=histlogflux/(10^(logfluxbin-minlogflux))                   ; Ponderation contre l'augmentation de la largeur des bins en logflux
 histlogflux=histlogflux/TOTAL(histlogflux)                             ; Normalisation
 histlogflux=histlogflux*nfluxbin/16 ; Normalisation to nbin=20
;histlogflux=ALOG10(histlogflux)                                        ; Échelle verticale log

xr    = [minlogflux,maxlogflux]
;xtitle= '!17Log !7U!17 (Mx)'
xtitle= '!17Log(!7U!17/Mx)'
yr    = [0.0,0.3]
ytitle= ''

order=[]
FOR ibin=0,nfluxbin-1 DO order=[order,ibin,ibin+nfluxbin]
logfluxbinplot = [logfluxbinn(0:nfluxbin-1),logfluxbinn(1:nfluxbin)]
logfluxbinplot = logfluxbinplot[order]
histlogfluxplot= [histlogflux,histlogflux]
histlogfluxplot= histlogfluxplot[order]

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.28*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, logfluxbinplot, histlogfluxplot, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

histlogfluxpar=[0.17,21.35,0.54]
histlogfluxfit=histlogfluxpar[0]*EXP(-0.5*(logfluxbin-histlogfluxpar[1])^2/histlogfluxpar[2]^2)

OPLOT, logfluxbin, histlogfluxfit, $
   LINESTYLE=0, THICK=3, COLOR=cbleu

XYOUTS, xr[0]+0.98*(xr[1]-xr[0]), yr[0]+0.87*(yr[1]-yr[0]), title8a, CHARSIZE=gros, ALIGNMENT=1.0

;;---- Tilt vs Log(flux) -----
;
;xr    = [19.,24.]
;;xtitle= '!17Log !7U!17 (Mx)'
;xtitle= '!17Log(!7U!17/Mx)'
;yr    = [-90.,90.]
;;ytitle= '!7a!17 (deg)'
;ytitle= '!7a!17/deg'
;
;PLOT, xr, [0.,0.], $
;   /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.30*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
;   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
;
;OPLOT, logflux0, abstilt0, $
;   PSYM=3, THICK=1, SYMSIZE=0.5, COLOR=cnoir

;---- DTilt histogram -----

tiltlinpar = 0.50
tiltlinfit = tiltlinpar*abslat00
dtilt0= abstilt0-tiltlinfit

ntiltbin = 89
tiltr    = [-89,89]
tiltbinn = FINDGEN(ntiltbin+1)/ntiltbin*(tiltr[1]-tiltr[0]) + tiltr[0]
tiltbin  = (tiltbinn[0:ntiltbin-1]+tiltbinn[1:ntiltbin])/2.

histtilt = HISTOGRAM(dtilt0,MIN=tiltr[0],MAX=tiltr[1],NBINS=ntiltbin+1,LOCATIONS=loctilt) & histtilt=histtilt[0:ntiltbin-1]

;histtilt(WHERE(histtilt EQ 0.)) = 1.e-2                                ; Eviter les valeurs de 0
;histtilt = histtilt/(10^(tiltbin-tiltr[0]))                            ; Ponderation contre l'augmentation de la largeur des bins en logflux
 histtilt = histtilt/TOTAL(histtilt)                                    ; Normalisation
;histtilt = histtilt*ntiltbin/16                                        ; Normalisation to nbin=20
;histtilt = ALOG10(histtilt)                                            ; Echelle verticale log

xr    = [-90.,90.]
;xtitle= '!7Da!17 (deg)'
xtitle= '!7Da!17/deg'
yr    = [0.,0.1]
ytitle= ''

order=[]
FOR ibin=0,ntiltbin-1 DO order=[order,ibin,ibin+ntiltbin]
tiltbinplot = [tiltbinn(0:ntiltbin-1),tiltbinn(1:ntiltbin)]
tiltbinplot = tiltbinplot[order]
histtiltplot= [histtilt,histtilt]
histtiltplot= histtiltplot[order]

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.36*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=rikiki

OPLOT, tiltbinplot, histtiltplot, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

;---- DSeparation histogram -----

title8c = '!7d!17!UMAX!N =' + STRING(delta0max, FORMAT='(f6.1)') + '!9%!17'

deltalinpar = [0.46,0.42]
deltalinfit = deltalinpar[0] + deltalinpar[1]*(logflux0-21.)
ddelta0 = logdelta0-deltalinfit

ndeltabin = 49
deltar    = [-0.975,1.475]
deltabinn = FINDGEN(ndeltabin+1)/ndeltabin*(deltar[1]-deltar[0]) + deltar[0]
deltabin  = (deltabinn[0:ndeltabin-1]+deltabinn[1:ndeltabin])/2.

histdelta = HISTOGRAM(ddelta0,MIN=deltar[0],MAX=deltar[1],NBINS=ndeltabin+1,LOCATIONS=locdelta) & histdelta=histdelta[0:ndeltabin-1]

;histdelta(WHERE(histdelta EQ 0.)) = 1.e-2                              ; Eviter les valeurs de 0
;histdelta = histdelta/(10^(deltabin-deltar[0]))                        ; Ponderation contre l'augmentation de la largeur des bins en logflux
 histdelta = histdelta/TOTAL(histdelta)                                 ; Normalisation
;histdelta = histdelta*ndeltabin/16                                     ; Normalisation to nbin=20
;histdelta = ALOG10(histdelta)                                          ; Echelle verticale log

xr    = [-1.0,1.5]
;xtitle= '!7D!17Log!7d!17 (deg)'
xtitle= '!7D!17Log(!7d!17/deg)'
yr    = [0.,0.2]
ytitle= ''

order=[]
FOR ibin=0,ndeltabin-1 DO order=[order,ibin,ibin+ndeltabin]
deltabinplot  = [deltabinn(0:ndeltabin-1),deltabinn(1:ndeltabin)]
deltabinplot  = deltabinplot[order]
histdeltaplot = [histdelta,histdelta]
histdeltaplot = histdeltaplot[order]

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=rikiki

OPLOT, deltabinplot, histdeltaplot, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

histdeltapar = [0.12,0.00,0.16]
histdeltafit = histdeltapar[0]*EXP(-0.5*(deltabin-histdeltapar[1])^2/histdeltapar[2]^2)

OPLOT, deltabin, histdeltafit, $
   LINESTYLE=0, THICK=3, COLOR=cbleu

XYOUTS, xr[0]+0.98*(xr[1]-xr[0]), yr[0]+0.87*(yr[1]-yr[0]), title8c, CHARSIZE=gros, ALIGNMENT=1.0

;---- Color-filled contours for surface Br -----------------------------

title1   = '!18B!Dr!N!17('+string(yy(jbr),format='(f4.2)') + ',!7h!17,!18t!17), max =' + STRING(brmax,format='(e8.2)') + 'G'

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr    = [-90.,90.]
yname = ['!1790!9%!17S','45!9%!17S','EQ','45!9%!17N','90!9%!17N']
ytitle= '!17Latitude'

; franky Br surface
fac = 217.        ;brmax       ;
levfac=0.1        ;1.0        ;0.06
fctplot=b_r/fac
levv=lev*levfac

iposplot=iposplot+1
CONTOUR, fctplot, time-timei, xxan, $
   /NOERASE, POS=pos(*,iposplot), LEVELS=levv, C_COLOR=cin, C_LINESTYLE=clst, /FILL, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=rikiki, $                            ;, XTICKS=4
   YRANGE=yr, YTICKLEN=0.01, YMINOR=3, YTICKS=N_ELEMENTS(yname)-1, YTICKNAME=yname, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

;XYOUTS, xr(0), yr(1)+youts0*(yr(1)-yr(0)), title1,   CHARSIZE=gros

;---- Color bar -----

;ytbar= '!18B!17!Dr!N(!18R!17,!7h!17,!18t!17) (!9X!17'+STRING(fac,FORMAT='(f4.0)')+'G)'
ytbar= '!18B!17!Dr!N(!18R!17,!7h!17,!18t!17)/'+STRING(fac,FORMAT='(f4.0)')+'G'
yrbar= [-1.,1.]
;yrbar= [-levfac,levfac]

cb = REPLICATE(1.,2) # levv
cbx = FINDGEN(2)

CONTOUR, cb, cbx, levv, $
   /NOERASE, POS = pos0(*,iposplot), $
   LEVELS = levv, C_COLOR = cin, /FILL, $
   XSTYLE=1, XTICKS=1, XCHARSIZE=rikiki, $
   YRANGE=yrbar, YTICKS=4, YMINOR=0, YTICKLEN=rikiki, YTITLE=ytbar, YCHARSIZE = gros

;---- Temporal evolution of the axial dipole moment --------------------

title2  = '!17Axial dipole moment, max =' + STRING(daxialmax,format='(f7.2)') + 'G'

;xrsim = [ti,to]
xr    = [timei,timeo]-timei
;xtitle= ''
;xtitle= '!17Time (yr)'
xtitle= '!17Time/years'
yr = [-20.,20.]                                                         ;[-1.,1.]*MAX(ABS(daxial))/daxialmax
;ytitle = '!18D!17/!18R!17!U2!N (G)'
ytitle = '(!18D!17/!18R!17!U2!N)/G'

iposplot=iposplot+1
PLOT, xr, [0.,0.], $
   /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YTICKLEN=0.01, YMINOR=2, YTITLE=ytitle, YCHARSIZE=gros

;OPLOT, time-timei, daxialsmth, $
;   LINESTYLE=0, THICK=5, COLOR=cgrey

OPLOT, time[       0:itfit[0]]-timei, daxial[       0:itfit[0]], $
   LINESTYLE=2, THICK=3, COLOR=cnoir
OPLOT, time[itfit[0]:itfit[1]]-timei, daxial[itfit[0]:itfit[1]], $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, time[itfit[1]:      nt]-timei, daxial[itfit[1]:      nt], $
   LINESTYLE=2, THICK=3, COLOR=cnoir

OPLOT, [time(itfit[0]),time(itfit[0])]-timei, yr, LINESTYLE=0, THICK=6
OPLOT, [time(itfit[1]),time(itfit[1])]-timei, yr, LINESTYLE=0, THICK=6

;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
FOR icyc=0,nfmaxcyc-1 DO BEGIN
   OPLOT, [time[itfmincyc[icyc]],time[itfmincyc[icyc]]]-timei, yr, $
      LINESTYLE=1, THICK=3, COLOR=cnoir
ENDFOR
OPLOT, [time[itsnaprange[0]],time[itsnaprange[0]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir
OPLOT, [time[itsnaprange[1]],time[itsnaprange[1]]]-timei, yr, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

;XYOUTS, xr(0)+0.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title2 , CHARSIZE=gros

IF (ndmaxcyc GE 1) THEN BEGIN
   OPLOT, time[itdmaxcyc]-timei, daxialmaxcyc, $
      psym=8, SYMSIZE=2.0, THICK=5, COLOR=cnoir
   OPLOT, time[itdmincyc]-timei, daxialmincyc, $
      psym=6, SYMSIZE=0.8, THICK=5, COLOR=cnoir
ENDIF

XYOUTS, xr[0]+1.00*(xr[1]-xr[0]), yr[1]+youts0*(yr[1]-yr[0]), STRING(ndmaxcyc,FORMAT='(i5)')+' cycles', CHARSIZE=gros, ALIGNMENT=1.0

;IF (ndmaxcyc GE 2) THEN BEGIN
;   OPLOT, time-timei, daxialmaxfit, $
;      LINESTYLE=0, THICK=3, COLOR=cbleu
;   
;   title2a = '!7s!17 =' + STRING(daxialmaxpar[1],FORMAT='(f7.0)') + ', !7v!17 =' + STRING(daxialmaxchi/daxialmax,FORMAT='(e9.2)')
;   XYOUTS, xr(0)+1.00*(xr[1]-xr[0]), yr(1)+youts0*(yr(1)-yr(0)), title2a, CHARSIZE=gros, ALIGNMENT=1.0
;ENDIF

;---- Plot FFT on axial dipole moment ----------------------------------

xr     = [0,0.1]
;xtitle = '!17Frequency (yr!U-1!N)'
xtitle = '!17Frequency/yr!U-1!N'
yr     = [1.e-4,1.e0]
;yr     = [0.,dafft0ampmax]
ytitle = '!17FT power (D axial)'

IF ((ibfitfac EQ nbfitfac-1) AND (ibldseed EQ nbldseed-1)) THEN BEGIN

iposplot=iposplot+1
PLOT, xr, [yr[0],yr[0]], $
;  /NOERASE, POS=pos(*,iposplot), LINESTYLE=0, THICK=3, COLOR=cnoir, $
   /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XTICKLEN=0.04, XTITLE=xtitle, XCHARSIZE=gros, $           ;, XTICKS=4
   /YLOG, YRANGE=yr, YTICKLEN=0.01, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, freq, dafft0amp, $
;  PSYM=8, SYMSIZE=1.0, COLOR=cnoir
   LINESTYLE=0, THICK=3, COLOR=cnoir

OPLOT, [freq[idafft0max],freq[idafft0max]], yr, $
   LINESTYLE=2, THICK=3, COLOR=cnoir

XYOUTS, freq[idafft0max], yr[0]+0.90*(yr[1]-yr[0]), STRING(freq[idafft0max],FORMAT='(f6.3)')+'yr', CHARSIZE=gros, ALIGNMENT=0.0

OPLOT, freqq, dafft0ampfitt, $
   LINESTYLE=0, THICK=3, COLOR=cbleu

XYOUTS, xr[0]+0.00*(xr[1]-xr[0]), yr[0]+0.90*(yr[1]-yr[0]), '!18P!17 ='+STRING(dafft0amppar[0],FORMAT='(f9.6)')+'exp(-(!18f!17-'+STRING(dafft0amppar[1],FORMAT='(f9.6)')+')!U2!N/(2*'+STRING(dafft0amppar[2],FORMAT='(f9.6)')+'!U2!N))', CHARSIZE=gros, ALIGNMENT=0.0

ENDIF

;---- Plot Axial dipole moment maxima distribution ---------------------

xr    = [mindaxialmaxcyc,maxdaxialmaxcyc]
;xtitle= '!18D!17/!18R!17!U2!N (G)'
xtitle= '(!18D!17/!18R!17!U2!N)/G'
yr    = [0.0,0.3]
;ytitle= '!18f!DD!17/!18R!17!E2!N (G!U-1!N)'
ytitle= '!18f!D!17(D!17/!18R!17!E2!D)/G!N'

order=[]
FOR ibin=0,ndaxialmaxcycbin-1 DO order=[order,ibin,ibin+ndaxialmaxcycbin]
daxialmaxcycbinplot = [daxialmaxcycbinn(0:ndaxialmaxcycbin-1),daxialmaxcycbinn(1:ndaxialmaxcycbin)]
daxialmaxcycbinplot = daxialmaxcycbinplot[order]
histdaxialmaxcycplot= [histdaxialmaxcyc,histdaxialmaxcyc]
histdaxialmaxcycplot= histdaxialmaxcycplot[order]

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, daxialmaxcycbinplot, histdaxialmaxcycplot, $
   LINESTYLE=0, THICK=3, COLOR=cnoir

;histdaxialmaxcycpar=[0.17,21.35,0.54]
;histdaxialmaxcycfit=histdaxialmaxcycpar[0]*EXP(-0.5*(daxialmaxcycbin-histdaxialmaxcycpar[1])^2/histdaxialmaxcycpar[2]^2)

;OPLOT, daxialmaxcycbin, histdaxialmaxcycfit, $
;   LINESTYLE=0, THICK=3, COLOR=cbleu

;;---- Temporal evolution of B_r at north and south poles ---------------
;
;title12  = '!17South (thick) & north (thin) polar caps (max =' + STRING(bcapmax,format='(f7.3)') + 'G)' + ' (- - at poles)'
;
;;xrsim = [ti,to]
;xr    = [timei,timeo]-timei
;;xtitle= ''
;;xtitle= '!17Time (yr)'
;xtitle= '!17Time/years'
;;roundmaxbplot=bcapmax
;roundmaxbplot=60.
;yr = [-roundmaxbplot,roundmaxbplot]
;;ytitle = '!17B!Dr!N (G)'
;ytitle = '!17B!Dr!N/G'
;
;;---- South pole -----
;PLOT, time, bsouth, $
;   /NOERASE, POS=pos(*,iposplot), $
;   LINESTYLE=0, THICK=3, CHARSIZE=gros, $
;   XRANGE=xr, XCHARSIZE=rikiki, $
;   YRANGE=yr, YTITLE=ytitle, YTICKS=2, YMINOR=4
;
;OPLOT, time, b_r(*,0), LINESTYLE=2, THICK=3
;
;;---- North pole -----
;OPLOT, time, bnorth, LINESTYLE=0, THICK=1
;
;OPLOT, time, b_r(*,nelx), LINESTYLE=2, THICK=1
;
;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR
;OPLOT, xr, [0.,0.], LINESTYLE=0
;
;XYOUTS, xr(0), yr(1)+youts0*(yr(1)-yr(0)), title12, CHARSIZE=gros

;---- Correlations -----------------------------------------------------

;---- Magnetic energy as a function of signed pseudo-SSN -----

iposplot=iposplot+1

xr    = [-200.,200.]
xtitle= '!7R!Dh!N!18N!17!DBMR!N'
yr    = [36.6,37.8]
;ytitle= '!17Log !18E!DB!N!17 (erg)'
ytitle= '!17Log(!18E!DB!N!17/erg)'

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.28*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;  /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, bflycumul[itfit[0]:itfit[1]], ALOG10(rsun^3*ebtot [itfit[0]:itfit[1]]), $
   psym=8, SYMSIZE=0.5, THICK=5, COLOR=cnoir

;---- Magnetic energy as a function of axial dipole moment -----

;iposplot=iposplot+1

xr    = [-20.,20.]
;xtitle= '!18D!17/!18R!17!U2!N (G)'
xtitle= '!17(!18D!17/!18R!17!U2!N)/G'
yr    = [36.6,37.8]
;ytitle= '!17Log !18E!DB!N!17 (erg)'
ytitle= '!17Log(!18E!DB!N!17/erg)'

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.36*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;  /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, daxial[itfit[0]:itfit[1]], ALOG10(rsun^3*ebtot [itfit[0]:itfit[1]]), $
   psym=8, SYMSIZE=0.5, THICK=5, COLOR=cnoir

;---- Signed pseudo-SSN as a function of axial dipole moment -----

;iposplot=iposplot+1

xr    = [-20.,20.]
;xtitle= '!18D!17/!18R!17!U2!N (G)'
xtitle= '!17(!18D!17/!18R!17!U2!N)/G'
yr    = [-200.,200.]
ytitle= '!7R!Dh!N!18N!17!DBMR!N'

PLOT, xr, [0.,0.], $
   /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;  /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
   XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
   YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros

OPLOT, daxial[itfit[0]:itfit[1]], bflycumul[itfit[0]:itfit[1]], $
   psym=8, SYMSIZE=0.5, THICK=5, COLOR=cnoir

;---- Cycle amplitude cycle n vs axial dipole cycle n-3 -----

iposplot=iposplot+1

IF ((ndmaxcyc GE 4) AND (nfmaxcyc GE 4)) THEN BEGIN
   
   idaxialmaxcycnm3 = WHERE(time[itdmaxcyc] LT time[itfmaxcyc[nfmaxcyc-3]]) & PRINT,'idaxialmaxcycnm3=',idaxialmaxcycnm3
   ibflymaxcycn     = WHERE(time[itfmaxcyc] GT time[itdmaxcyc[2]])          & PRINT,'ibflymaxcycn=',ibflymaxcycn
   daxbflymaxnm3cor = CORRELATE(daxialmaxcyc[idaxialmaxcycnm3],bflymaxcyc[ibflymaxcycn])
   
   title9c = '!17' + STRING(daxbflymaxnm3cor,FORMAT='(f6.3)')
   
   xr    = [0.,1.01]*daxialmax
   xtitle= '!17(!18D!17!S!D!18n!17-3!R!UMAX!N/!18R!17!U2!N)/G'
   yr    = [0.,1.01]*bflycumulmax
   ytitle= '!18N!17!S!D!18n!17!R!UMAX!N'
   
   PLOT, xr, [0.,0.], $
      /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.28*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
   
   OPLOT, daxialmaxcyc[idaxialmaxcycnm3], bflymaxcyc[ibflymaxcycn], $
      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
   
   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9c, CHARSIZE=gros, ALIGNMENT=1.0
;  XYOUTS, xr[0]-0.35*(xr[1]-xr[0]), yr[0]+0.92*(yr[1]-yr[0]), '!17(a)', CHARSIZE=gros, ALIGNMENT=0.0
   
ENDIF

;---- Cycle amplitude cycle n vs axial dipole cycle n-2 -----

;iposplot=iposplot+1

IF ((ndmaxcyc GE 3) AND (nfmaxcyc GE 3)) THEN BEGIN
   
   idaxialmaxcycnm2 = WHERE(time[itdmaxcyc] LT time[itfmaxcyc[nfmaxcyc-2]]) & PRINT,'idaxialmaxcycnm2=',idaxialmaxcycnm2
   ibflymaxcycn     = WHERE(time[itfmaxcyc] GT time[itdmaxcyc[1]])          & PRINT,'ibflymaxcycn=',ibflymaxcycn
   daxbflymaxnm2cor = CORRELATE(daxialmaxcyc[idaxialmaxcycnm2],bflymaxcyc[ibflymaxcycn])
   
   title9c = '!17' + STRING(daxbflymaxnm2cor,FORMAT='(f6.3)')
   
   xr    = [0.,1.01]*daxialmax
   xtitle= '!17(!18D!17!S!D!18n!17-2!R!UMAX!N/!18R!17!U2!N)/G'
   yr    = [0.,1.01]*bflycumulmax
   ytitle= '!18N!17!S!D!18n!17!R!UMAX!N'
   
   PLOT, xr, [0.,0.], $
      /NOERASE, POS=[pos[0,iposplot]+0.36*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
   
   OPLOT, daxialmaxcyc[idaxialmaxcycnm2], bflymaxcyc[ibflymaxcycn], $
      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
   
   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9c, CHARSIZE=gros, ALIGNMENT=1.0
;  XYOUTS, xr[0]-0.35*(xr[1]-xr[0]), yr[0]+0.92*(yr[1]-yr[0]), '!17(b)', CHARSIZE=gros, ALIGNMENT=0.0
   
ENDIF

;---- Cycle amplitude cycle n vs axial dipole cycle n-1 -----

;iposplot=iposplot+1

IF ((ndmaxcyc GE 2) AND (nfmaxcyc GE 2)) THEN BEGIN
   
   idaxialmaxcycnm1 = WHERE(time[itdmaxcyc] LT time[itfmaxcyc[nfmaxcyc-1]]) & PRINT,'idaxialmaxcycnm1=',idaxialmaxcycnm1
   ibflymaxcycn     = WHERE(time[itfmaxcyc] GT time[itdmaxcyc[0]])          & PRINT,'ibflymaxcycn=',ibflymaxcycn
   daxbflymaxnm1cor = CORRELATE(daxialmaxcyc[idaxialmaxcycnm1],bflymaxcyc[ibflymaxcycn])
   
   title9c = '!17' + STRING(daxbflymaxnm1cor,FORMAT='(f6.3)')
   
   xr    = [0.,1.01]*daxialmax
   xtitle= '!17(!18D!17!S!D!18n!17-1!R!UMAX!N/!18R!17!U2!N)/G'
   yr    = [0.,1.01]*bflycumulmax
   ytitle= '!18N!17!S!D!18n!17!R!UMAX!N'
   
   PLOT, xr, [0.,0.], $
      /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
   
   OPLOT, daxialmaxcyc[idaxialmaxcycnm1], bflymaxcyc[ibflymaxcycn], $
      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
   
   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9c, CHARSIZE=gros, ALIGNMENT=1.0
;  XYOUTS, xr[0]-0.35*(xr[1]-xr[0]), yr[0]+0.92*(yr[1]-yr[0]), '!17(c)', CHARSIZE=gros, ALIGNMENT=0.0
   
ENDIF

;---- Cycle amplitude cycle n vs axial dipole (end of) cycle n -----

iposplot=iposplot+1

IF ((ndmaxcyc GE 2) AND (nfmaxcyc GE 2)) THEN BEGIN
   
   idaxialmaxcycn   = WHERE(time[itdmaxcyc] GT time[itfmaxcyc[0]]         ) & PRINT,'idaxialmaxcycn=',idaxialmaxcycn
   ibflymaxcycn     = WHERE(time[itfmaxcyc] LT time[itdmaxcyc[ndmaxcyc-1]]) & PRINT,'ibflymaxcycn=',ibflymaxcycn
   daxbflymaxncor = CORRELATE(daxialmaxcyc[idaxialmaxcycn],bflymaxcyc[ibflymaxcycn])
   
   title9c = '!17' + STRING(daxbflymaxncor,FORMAT='(f6.3)')
   
   xr    = [0.,1.01]*daxialmax
   xtitle= '!17(!18D!17!S!D!18n!17!R!UMAX!N/!18R!17!U2!N)/G'
   yr    = [0.,1.01]*bflycumulmax
   ytitle= '!18N!17!S!D!18n!17!R!UMAX!N'
   
   PLOT, xr, [0.,0.], $
      /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.28*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
   
   OPLOT, daxialmaxcyc[idaxialmaxcycn], bflymaxcyc[ibflymaxcycn], $
      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
   
   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9c, CHARSIZE=gros, ALIGNMENT=1.0
;  XYOUTS, xr[0]-0.35*(xr[1]-xr[0]), yr[0]+0.92*(yr[1]-yr[0]), '!17(d)', CHARSIZE=gros, ALIGNMENT=0.0
   
ENDIF

;---- Axial dipole cycle n+1 vs axial dipole cycle n -----

;iposplot=iposplot+1

IF (ndmaxcyc GE 2) THEN BEGIN
   
   idaxialmaxcycn  = INDGEN(ndmaxcyc-1)
   idaxialmaxcycnp1= INDGEN(ndmaxcyc-1)+1
   axialmaxcor = CORRELATE(daxialmaxcyc[idaxialmaxcycn],daxialmaxcyc[idaxialmaxcycnp1])
   
   title9a = '!17' + STRING(axialmaxcor,FORMAT='(f6.3)')
   
   xr    = [0.,1.01]*daxialmax
   xtitle= '!17(!18D!17!S!D!18n!17!R!UMAX!N/!18R!17!U2!N)/G'
   yr    = [0.,1.01]*daxialmax
   xtitle= '!17(!18D!17!S!D!18n!17+1!R!UMAX!N/!18R!17!U2!N)/G'
   
   PLOT, xr, [0.,0.], $
      /NOERASE, POS=[pos[0,iposplot]+0.36*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
   
   OPLOT, daxialmaxcyc[idaxialmaxcycn], daxialmaxcyc[idaxialmaxcycnp1], $
      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
   
   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9a, CHARSIZE=gros, ALIGNMENT=1.0
   
ENDIF

;;---- Cycle amplitude n+1 vs cycle amplitude n -----
;
;iposplot=iposplot+1
;
;IF (nfmaxcyc GE 2) THEN BEGIN
;   
;   ibflymaxcycn  = INDGEN(nfmaxcyc-1)
;   ibflymaxcycnp1= INDGEN(nfmaxcyc-1)+1
;   bflymaxcor = CORRELATE(bflymaxcyc[ibflymaxcycn],bflymaxcyc[ibflymaxcycnp1])
;   
;   title9b = '!17' + STRING(bflymaxcor,FORMAT='(f6.3)')
;   
;   xr    = [0.,1.01]*bflycumulmax
;   xtitle= '!17[!7R!Dh!N!18N!17!DBMR!N]!UMAX!Dn!N'
;   yr    = [0.,1.01]*bflycumulmax
;   ytitle= '!17[!7R!Dh!N!18N!17!DBMR!N]!UMAX!Dn+1!N'
;   
;   PLOT, xr, [0.,0.], $
;      /NOERASE, POS=[pos[0,iposplot]+0.36*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.64*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
;      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
;   
;   OPLOT, bflymaxcyc[ibflymaxcycn], bflymaxcyc[ibflymaxcycnp1], $
;      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
;   
;   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9b, CHARSIZE=gros, ALIGNMENT=1.0
;   
;ENDIF

;---- Cycle amplitude vs cycle period -----

;iposplot = iposplot+1

IF (nfmaxcyc GE 2) THEN BEGIN
   
   amppercor    = CORRELATE(bflymaxcyc,perfmaxcyc)
   
   title9e = '!17' + STRING(amppercor,FORMAT='(f6.3)')
   
   xr    = [8.,14.]
;  xtitle= '!17Period!D!18n!17!N (yr)'
   xtitle= '!17Period!D!18n!17!N/years'
   yr    = [0.,1.01]*bflycumulmax
   ytitle= '!18N!17!S!D!18n!17!R!UMAX!N'
   
   PLOT, xr, [0.,0.], $
      /NOERASE, POS=[pos[0,iposplot]+0.72*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+1.00*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
   
   OPLOT, perfmaxcyc, bflymaxcyc, $
      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
   
   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9e, CHARSIZE=gros, ALIGNMENT=1.0
;  XYOUTS, xr[0]-0.35*(xr[1]-xr[0]), yr[0]+0.92*(yr[1]-yr[0]), '!17(e)', CHARSIZE=gros, ALIGNMENT=0.0
   
ENDIF

;---- Cycle amplitude north vs south -----

iposplot=iposplot+1

IF (nfsmaxcyc GE 2) THEN BEGIN
   
   ampnorthsouthcor    = CORRELATE(bflysmaxcyc,bflynmaxcyc)
   
   title9f = '!17' + STRING(ampnorthsouthcor,FORMAT='(f6.3)')
   
   xr    = [0.,1.01]*bflyscumulmax
   xtitle= '!18N!17!S!D!18n!17!R!UMAX!N!DSouth'
   yr    = [0.,1.01]*bflyncumulmax
   ytitle= '!18N!17!S!D!18n!17!R!UMAX!N!DNorth'
   
   PLOT, xr, [0.,0.], $
      /NOERASE, POS=[pos[0,iposplot]+0.00*(pos[2,iposplot]-pos[0,iposplot]),pos[1,iposplot],pos[0,iposplot]+0.28*(pos[2,iposplot]-pos[0,iposplot]),pos[3,iposplot]], LINESTYLE=0, THICK=3, COLOR=cnoir, $
;     /NOERASE, POS=pos[*,iposplot], LINESTYLE=0, THICK=3, COLOR=cnoir, $
      XRANGE=xr, XSTYLE=1, XTITLE=xtitle, XCHARSIZE=gros, $
      YRANGE=yr, YSTYLE=1, YTITLE=ytitle, YCHARSIZE=gros
   
   OPLOT, bflysmaxcyc, bflynmaxcyc, $
      psym=8, SYMSIZE=1.0, THICK=5, COLOR=cnoir
   
   XYOUTS, xr(0)+0.98*(xr[1]-xr[0]), yr(0)+  0.10*(yr(1)-yr(0)), title9f, CHARSIZE=gros, ALIGNMENT=1.0
;  XYOUTS, xr[0]-0.35*(xr[1]-xr[0]), yr[0]+0.92*(yr[1]-yr[0]), '!17(f)', CHARSIZE=gros, ALIGNMENT=0.0
   
ENDIF

;-----------------------------------------------------------------------

closeans='n'
IF ((ibfitfac EQ nbfitfac-1) AND (ibldseed EQ nbldseed-1)) THEN BEGIN
;  PRINT,'Close output file ? (y/n)'
;  READ,closeans
   closeans='y'
ENDIF
IF (closeans EQ 'y') THEN BEGIN
   DEVICE, /CLOSE
   SET_PLOT,'x'

   SETENV,'FILE='+fileplot
   SPAWN,'echo "ps2pdf -dEPSCrop $FILE.eps"'
   SPAWN,'ps2pdf -dEPSCrop "$FILE.eps" "$FILE.pdf"'
   SPAWN,'rm "$FILE.eps"'
ENDIF

ENDFOREACH
ENDFOREACH

;-----------------------------------------------------------------------
;---- 4. Proxies for emergences ----------------------------------------

;SET_PLOT,'ps'
;DEVICE, FILENAME = pathout+file+'.bis'+'.eps'
;DEVICE, /PORTRAIT, YOFFSET=5., XSIZE = 21.59, YSIZE = 27.94, /ENCAPSULATED
;DEVICE, SET_FONT='Times', FONT_SIZE=4, /TT_FONT, /COLOR, BITS_PER_PIXEL=8     ;, SCALE_FACTOR=0.5
;SET_PLOT,'ps'

;xpos  = [0.15,0.85]
;ymin  = 0.05

;pos1  = [xpos(0),ymin+0.72,xpos(1),ymin+0.90]
;pos2  = [xpos(0),ymin+0.54,xpos(1),ymin+0.72]
;pos3  = [xpos(0),ymin+0.36,xpos(1),ymin+0.54]
;pos4  = [xpos(0),ymin+0.18,xpos(1),ymin+0.36]
;pos5  = [xpos(0),ymin+0.00,xpos(1),ymin+0.18]

;yr=[-90.,90.]
;levv=lev

;f=bfly
;CONTOUR, f/MAX(ABS(f)), time, xxan,$
;   /NOERASE, POS=pos1, $
;   LEVELS=levv, C_COLOR = cin, C_LINESTYLE=clst, /FILL, $
;   XRANGE=xr, XTICKLEN=0.04, $
;   YRANGE=yr, YTITLE='!17Latitude (deg)', YTICKS=6, YMINOR=3, CHARSIZE=gros
;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR

;f=bfit
;CONTOUR, f/MAX(ABS(f)), time, xxan, $
;   /NOERASE, POS=pos2, $
;   LEVELS=levv, C_COLOR = cin, C_LINESTYLE=clst, /FILL, $
;   XRANGE=xr, XCHARSIZE=rikiki, XTICKLEN=0.04, $
;   YRANGE=yr, YTITLE='!17Latitude (deg)', YTICKS=6, YMINOR=3, CHARSIZE=gros
;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR

;bcrit=0.2
;dbcrit=0.1
;cc=0.
;expsin=0.
;f=bfitmax*0.5*(erf((bfit/bfitmax-bcrit)/dbcrit)+erf((bfit/bfitmax+bcrit)/dbcrit))*ABS(bfit/bfitmax)^cc*SIN(thtt)^expsin
;CONTOUR, f/MAX(ABS(f)), time, xxan, $
;   /NOERASE, POS=pos3, $
;   LEVELS=levv, C_COLOR = cin, C_LINESTYLE=clst, /FILL, $
;   XRANGE=xr, XCHARSIZE=rikiki, XTICKLEN=0.04, $
;   YRANGE=yr, YTITLE='!17Latitude (deg)', YTICKS=6, YMINOR=3, CHARSIZE=gros
;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR

;bcrit=0.2
;dbcrit=0.1
;cc=1.
;expsin=0.
;f=bfitmax*0.5*(erf((bfit/bfitmax-bcrit)/dbcrit)+erf((bfit/bfitmax+bcrit)/dbcrit))*ABS(bfit/bfitmax)^cc*SIN(thtt)^expsin
;CONTOUR, f/MAX(ABS(f)), time, xxan, $
;   /NOERASE, POS=pos4, $
;   LEVELS=levv, C_COLOR = cin, C_LINESTYLE=clst, /FILL, $
;   XRANGE=xr, XCHARSIZE=rikiki, XTICKLEN=0.04, $
;   YRANGE=yr, YTITLE='!17Laebtottitude (deg)', YTICKS=6, YMINOR=3, CHARSIZE=gros
;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR

;bcrit=0.2
;dbcrit=0.1
;cc=0.
;expsin=1.
;f=bfitmax*0.5*(erf((bfit/bfitmax-bcrit)/dbcrit)+erf((bfit/bfitmax+bcrit)/dbcrit))*ABS(bfit/bfitmax)^cc*SIN(thtt)^expsin
;CONTOUR, f/MAX(ABS(f)), time, xxan, $
;   /NOERASE, POS=pos5, $
;   LEVELS=levv, C_COLOR = cin, C_LINESTYLE=clst, /FILL, $
;   XRANGE=xr, XCHARSIZE=rikiki, XTICKLEN=0.04, $
;   YRANGE=yr, YTITLE='!17Latitude (deg)', YTICKS=6, YMINOR=3, CHARSIZE=gros
;FOR icycle=0,ncycles DO BEGIN
;   OPLOT, [anmincyc(icycle),anmincyc(icycle)], yr, LINESTYLE=2, THICK=3
;ENDFOR

;DEVICE, /CLOSE
;SET_PLOT,'x'

;SETENV,'FILE='+fileplot
;SPAWN,'echo "ps2pdf -dEPSCrop $FILE.eps"'
;SPAWN,'ps2pdf -dEPSCrop "$FILE.eps" "$FILE.pdf"'
;SPAWN,'rm "$FILE.eps"'

;---- 5. Correlation between bfit and bfly ----------------------------
;
;pos5  = [0.15,0.77,0.85,0.97]
;pos6  = [0.15,0.53,0.85,0.73]
;pos7  = [0.15,0.29,0.85,0.49]
;pos8  = [0.15,0.05,0.85,0.25]
;
;xr=[-1.0,1.0]
;yr=[-bflymax,bflymax]
;
;PLOT, bfit(itfit[0]:itfit[1],*), bfly(itfit[0]:itfit[1],*), $
;   POS=pos5, PSYM=1, CHARSIZE=gros, XCHARSIZE=rikiki, $
;   XRANGE=xr, YRANGE=yr, YTITLE='Number of BMR'
;
;xyouts, -1., 1.04*bflymax, '!17Linear Pearson correlation coefficient=' + STRING(fit00,FORMAT='(f6.3)'), CHARSIZE=gros
   
;-----------------------------------------------------------------------

END
