c***********************************************************************
      MODULE bldyn_param
c***********************************************************************
      REAL,PARAMETER ::    pi=3.1415926536
      
c---------- Spatial mesh and time-stepping parameters ----------------
c
c     These parameter statements can only be used for a 2-D, 2-eq problem,
c     spatially discretized using quadrilateral bilinear elements
c     
c     nelx  : number of element in latitudinal direction
c     nely  : number of element in radial direction
c             (storage space is smallest if nelx<=nely)
c     ityp  : element type (=23 for bilinear, =24 for 8-node quadratic)
c     ngp   : number of gauss point per dimension, for FEM element integration
c             (=2 for bilinear elements, =3 for 8-node quadratic)
c     nd    : number of node per element (=4 for bilinear)
c     
c     nt    : number of main time steps
c     nstep : number of sub-time steps (=1 for nonlinear problems)
c     theta : time-stepping parameter (0.5<theta<1.0; 2/3 works well)
c-----------------------------------------------------------------------
      INTEGER,PARAMETER :: nelx=64, nely=96,
     +                     nesxvrai=256, nesyvrai=128
      INTEGER,PARAMETER :: nt=100, nstep=10,
     +                     nts=1, nsteps=1000
      INTEGER,PARAMETER :: ityp=23, ngp=2, nd=4
      REAL,PARAMETER ::    theta=2./3.
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c     From these are calculated:
c  
c     nely1 : number of elements from rb to C-E interface, concentrated towards latter
c     nelybuf:number of elements in outer buffer shell, concentrated towards r/R=1
c     nely2 : number of elements in convective envelope
c     nndx  : number of mesh nodes in latitudinal direction
c     nndy  : number of mesh nodes in radial direction
c     nel   : total number of elements
c     nnd   : total number of mesh nodes
c     nm    : total number of FEM coefficients
c     nbc   : maximal possible number of Dirichlet boundary conditions
c     ng    : total number of gauss points per element
c     mhb   : half bandwidth of system matrices
c     mfb   : full bandwidth of system matrices
c-----------------------------------------------------------------------
      INTEGER,PARAMETER :: nely1=nely/3, nelybuf=nely/10-1,
     +                     nely2=nely-nely1-nelybuf,
     +                     nndx=nelx+1, nndy=nely+1,
     +                     nnsxvrai=nesxvrai+1, nnsyvrai=nesyvrai+1,
     +                     nel=nelx*nely, nnd=nndx*nndy,
     +                     ng=ngp*ngp, nm=2*nnd,
     +                     mhb=2*nelx+7, mfb=2*mhb-1,
     +                     nbc=4*(nndx+nndy)+nndx*nelybuf+nndx
cALem+                     nbc=4*(nelx+nely)+nelx*nely/10,
      INTEGER,PARAMETER :: nnt=nt*nstep,nntp1=nnt+1,
     +                     nnts=nts*nsteps,nntsp1=nnts+1
c-----------------------------------------------------------------------
      END MODULE
c=======================================================================

c***********************************************************************
      REAL FUNCTION bldynfit(nfit,xfit,
     +                                                    xfitmn,xfitmx,
     +   cyc1,ncyc,nrep,tfitf,inrfile,bmrfilei,magfile,simname,simid,vb,
     +               bmrtype,bldseed,bldseedd,datseedi,inrsurf,datetime)!(Francois)
c***********************************************************************
c                                                                      *
c                       Babcock-Leighton dynamos                       *
c                       ========================                       *
c                                                                      *
c Solves 2D Axisymmetric MHD induction equation in a meridional        *
c plane or quadrant, as nonlinear initial-boundary-value problem       *
c using galerking finite element method and one-step implicit          *
c time-stepping (theta-method).                                        *
c                                                                      *
c Nonlinearity is non-local Babcock-Leighton poloidal source term.     *
c (Possibility to include normal (local) mean-field alpha effect.)     *
c                                                                      *
c Uses solar-like differential rotation, magnetic diffusivity          *
c contrast across core-envelope interface, solar-like single-cell      *
c meridional circulation in convective envelope.                       *
c                                                                      *
c Codes solves for toroidal component (B_phi) and toroidal vector      *
c potential (A_phi such that B_p=curl A)                               *
c                                                                      *
c This version has buoyant loss term, as per Dario's model             *
c                                                                      *
c Temporal and spatial variables:                                      *
c -Time measured in units of diffusion time tau=R^2/eta_T,             *
c   where eta_T is envelope magnetic diffusivity;                      *
c -Radial variable measured in units of solar radius R;                *
c -mu=cos(theta) used as latitudinal variable (increasing order)        *
c  (regular mesh for theta in decreasing order)                        *
c                                                                      *
c***********************************************************************
c References:
c
c Burnett, D.S. 1987, Finite Element Analysis, Addison-Wesley [Burnett]
c
c Dikpati, M., & Charbonneau, P. 1999, ApJ, 518, 508          [DC99]
c
c Golub, & Van Loan, Matrix Computations, 2nd ed., Johns Hopkins
c
c***********************************************************************
c   Version 1.0,           11/7/2001      Paul Charbonneau, HAO/NCAR
c     Modified             27/11/2002     Paul Charbonneau, GRPS/UdeM
c     Nettoyage            2011-12-19     ALem. Lemerle,    GRPS/UdeM
c     Ajout de isrc        2012-02-07        ''
c     SUBROUTINE surfabc   2012-02-09        ''
c     Ajout de icnd        2012-02-16        ''
c     Profiles param.      2012-03-28        ''
c     Sub fitness...       2012-04-05        ''
c     Function bldynfit    2012-04-12        ''
c     Ajout de ivrb        2012-04-13        ''
c     Ajout de ww...       2012-06-24        ''
c     Range for rbphi      2012-11...        ''
c     Better initcnd       2012-12...        ''
c     Correct surfabc      2012-12...        ''
c   Version 2.0:
c     Variable param...    2012-12-12        ''
c     Cleaning...          2013-03-04        ''
c     Smooth data file     2013-04-18        ''
c     Montly ssn fit...    2013-05-06        ''
c     bbb,aaa,etc.         2013-10-11        ''
c     Ajout fit3           2014-03-24        ''
c     Ajout qq             2014-04-09        ''
c     Ajout vv             2014-05-01        ''
c   Version bldynsurfbr    2014-05-05        ''
c     Ajout us0, etas0     2014-09-23        ''
c     Self-generated BMRs  2015-01-01        ''
c     Published params II  2017-01-10     Lemerle & Charbonneau, 2017, ApJ
c     (ybl0.050etac 8.0etat12.0u17.0rbt0.60m1.50p  0.20q 1.0v7.0w 1.0eta 600tau10a 9n 8)
c     Seeds 4 hemispheres  2017-07-03     Alex. Lemerle & Francois Labonville
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.global.f'
      INCLUDE 'bldyn_includefiles/arrays.mesh.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
      INTEGER        nfit,cyc1,ncyc,nrep,vb,bmrtype(3,4),
     +               bldseed(4),bldseedd(4),datseedi                    !(Francois)
      REAL           tfitf(2),xfit(nfit),xfitmn(nfit),xfitmx(nfit)
      CHARACTER      datetime*14,simid*8,simname*40,datseedtext*5
      CHARACTER*250  ssnfile,bmrfilei,bmrfile,
     +               magfile,idlfile,inrfile,inrsurf
c Local
      INTEGER        nncyc
      PARAMETER      (nncyc=29)
      INTEGER        nvar,fsticyc,icyc,irep
      INTEGER        ia,it,n,its,j,i
      CHARACTER*100  fitfile,home,scratch,pathd,patho
      CHARACTER*250  filename,logfile,outfile,inwfile
      LOGICAL        bmrfile_exists
      INTEGER        strlen
c     INTEGER        bldseed,bldseedd
c     PARAMETER      (bldseed=1234,bldseedd=1234)
      REAL     ctime,
     +         uuu,etass,etatt,etac,etat,omeg0,s0,tau,uumax,
     +         mu02,cputi,cputo,rbphi(2),rbr,tfit(2),latfit(2),
     +         tbmr,                                                    !(Francois)
     +         yblr(2),dybl,yblv(ncyc),yblvmn(ncyc),yblvmx(ncyc),
     +         etacr(2),detac,etacv(ncyc),etacvmn(ncyc),etacvmx(ncyc),
     +         etatr(2),detat,etatv(ncyc),etatvmn(ncyc),etatvmx(ncyc),
     +         uur(2),duu,uuv(ncyc),uuvmn(ncyc),uuvmx(ncyc),
     +                    usv(ncyc),usvmn(ncyc),usvmx(ncyc),
     +         ppr(2),dpp,ppv(ncyc),ppvmn(ncyc),ppvmx(ncyc),
     +         rbotr(2),drbot,rbotv(ncyc),rbotvmn(ncyc),rbotvmx(ncyc),
     +         mmr(2),dmm,mmv(ncyc),mmvmn(ncyc),mmvmx(ncyc),
     +         qqr(2),dqq,qqv(ncyc),qqvmn(ncyc),qqvmx(ncyc),
     +         vvr(2),dvv,vvv(ncyc),vvvmn(ncyc),vvvmx(ncyc),
     +         wwr(2),dww,wwv(ncyc),wwvmn(ncyc),wwvmx(ncyc),
     +         etasr(2),detas,etasv(ncyc),etasvmn(ncyc),etasvmx(ncyc),
     +         taur(2),dtau,taudmn,taudmx,
     +         ampr(2),damp,ampimn,ampimx,
     +         expir(2),dexpi,
     +         datseedr(2),ddatseed,
     +         ycyc(nncyc),dcyc(ncyc+1),tcyc(ncyc*nrep+1),durd,
     +         tht,sin1,cos1,fctsin,fctcos,fct,thtmax,datawang(11)
      INTEGER  shftcyc,dmonth(12),year,month,itcyc(ncyc*nrep)
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c---------- I/O control parameters and files ---------------------------
c-----------------------------------------------------------------------

c---------- Model subroutines ------------------------------------------
c
c     The code requires various subroutines defining the model's
c     input functionals, such as the meridional flow, differential
c     rotation, etc. These are incorporated via "include" statements
c     at the end of this source code.
c
c     sub.angvel.f : angular velocity as a function of mu and r
c     sub.magdif.f : magnetic diffusivity as a function of r
c     sub.circmd.f : meridional flow components as a function of mu and r
c     sub.srcpol.f : source term as a function of mu and r
c-----------------------------------------------------------------------

c---------- Global control parameters ----------------------------------
c
c  1. ipar     : Controls imposed solution parity, or lack thereof
c
c     ipar=0   : full meridional plane solution
c              theta-domain is from pi to 0, mixed parity initial condition      (cALem: was 'theta-domain is from -pi to pi' ??)
c     ipar=1   : N-meridional quadrant solution
c              theta-domain is from pi/2 to 0, dipole-like initial condition     (cALem: was 'theta-domain is from 0 to pi' ??)
c     ipar=2   : N-meridional quadrant solution
c              theta-domain is from pi/2 to 0, quadrupole-like initial condition (cALem: was 'theta-domain is from 0 to pi' ??)
c
c  2. isrc     : Controls inclusion of source term
c
c     isrc=0   : no source term
c     isrc=1   : mean-field alpha-effect (prop.to B => in matrix K)
c     isrc=2   : local or non-local alpha-effect with quenching (=> in vector F)
c     isrc=3   : Babcock-Leighton mechanism as an evolving boundary condition from surfbr.f and surfbr.idl
c
c     ialp     : Controls inclusion of alpha-effect
c     IF(isrc=1):
c        ialp=1: mean-field alpha-effect only in poloidal equations (alpha-omega)
c        ialp=2: mean-field alpha-effect in both equations (alpha^2-omega)
c     IF(isrc=2):
c        ialp=1: local alpha-effect in poloidal equations with alpha quenching
c        ialp=2: non-local alpha-effect in poloidal equations with alpha quenching
c        iloss : (0 or 1) Toroidal flux loss
c     IF(isrc=3):
c        faca  : facteur multiplicatif pour condition variable en surface
c
c  3. icnd     : Controls the type of initial condition to start with
c
c     icnd=0   : both poloidal and toroidal components = 0.
c     icnd=1   : toroidal comp. = gaussian around rzc in radial direction
c              and ipar... in theta direction (~activity maximum)
c     icnd=2   : poloidal comp. = at the surface centered near the poles
c              (~activity minimum)
c
c  4. istc     : Controls inclusion of stochastic effects [UNDER CONSTRUCTION]
c
c     istc=0   : steady model
c     istc=1   : fluctuations in source term only             [NOT THERE YET]
c     istc=2   : fluctuations in meridional circulation only  [NOT THERE YET]
c     istc=3   : fluctuations in source and circulation       [NOT THERE YET]
c
c  5. ivrb     : Controls if output and solutions are written or not
c
c     ivrb=0   : no output written
c     ivrb=1   : no output to terminal, but solution is saved to output file (subroutine writout)
c     ivrb=2   : output to terminal (and log file) and solution saved
c--------------
      ipar     = 0
      isrc     = 3
      ialp     = 0
      iloss    = 0
      faca     = 1.
      istc     = 0
      ivrb     = vb
c-----------------------------------------------------------------------

c---------- Define problem parameters (see also DC99, sec. 2.4) --------
      
c     xfit=[                                                            !choix perso
c    + 0.99999,0.00000,0.99999,0.50000,0.99999,0.00000,0.00000,0.99999]
      
c---- Number of parameters that change with each cycle
c     (other parameters remain fixed through the cycles)
      nvar=0
c     nvar=nfit/ncyc
c     IF((FLOAT(nfit)/ncyc-nvar).NE.0.) STOP 'Wrong number of param !'
      
c--------------
c     rsun     : solar radius (R) (in cm)
c     rb       : radial (r/R) position of the base of the simulation
c--------------
      rsun     = 6.9599e10
      rb       = 0.5
      
c--------------
c     rzc      : core-envelope interface; for sun,      =0.7
c     ybl      : tachocline half-thickness; for sun,    =0.025
c--------------
      yblr     = [0.05,0.05]           !dc/R = 0.05 (Optimal W21x8-11); 0.05 (Miesch2014)
c     yblr     = [0.04,0.10]           !dc/R = [0.04,0.10] (Old: [0.01,0.08])
      dybl     = yblr(2)-yblr(1)
      DO icyc=1,ncyc
         yblv  (icyc)  = yblr(1)
         yblvmn(icyc)  = yblr(1)
         yblvmx(icyc)  = yblr(1)
c        yblv  (icyc)  = yblr(1) + dybl*xfit  (1)
c        yblvmn(icyc)  = yblr(1) + dybl*xfitmn(1)
c        yblvmx(icyc)  = yblr(1) + dybl*xfitmx(1)
c        yblv  (icyc)  = yblr(1) + dybl*xfit  (ncyc*nvar+1)
c        yblvmn(icyc)  = yblr(1) + dybl*xfitmn(ncyc*nvar+1)
c        yblvmx(icyc)  = yblr(1) + dybl*xfitmx(ncyc*nvar+1)
      ENDDO
      ybl      = yblv(1)
      rzc      = 0.7
      
c--------------
c     etac     : diffusivity in the core (eta_c) (in cm²/s) (1.e9)
c     etat     : (turbulent) diffusivity in the bulk of the envelope (eta_T) (2.e10 - 2.e12 cm²/s)
c     etadf    : envelope-to-core diffusivity ratio     =eta_T/eta_c (>=1, ~20-2000)
c--------------
      etacr    = [8.,8.]               !nc = 10^8 cm^2/s (Optimal W21x8-11); 10^9 cm^2/s (Miesch2014)
c     etacr    = [7.,11.]              !nc = 10^[ 7.,11.] cm^2/s (Old: [10^8,10^10] cm^2/s)
      detac    = etacr(2)-etacr(1)
      etatr    = [12.,12.]             !nt = 10^12. cm^2/s (Optimal W21x8-11); 10^10.7 cm^2/s (Miesch2014)
c     etatr    = [11.,13.]             !nt = 10^[11.,13.] cm^2/s
      detat    = etatr(2)-etatr(1)
      DO icyc=1,ncyc
         etacv  (icyc) = 10.**(etacr(1) )
         etacvmn(icyc) = 10.**(etacr(1) )
         etacvmx(icyc) = 10.**(etacr(1) )
c        etacv  (icyc) = 10.**(etacr(1) + detac*xfit  (2))
c        etacvmn(icyc) = 10.**(etacr(1) + detac*xfitmn(2))
c        etacvmx(icyc) = 10.**(etacr(1) + detac*xfitmx(2))
c        etacv  (icyc) = 10.**(etacr(1) + detac*xfit  (ncyc*nvar+2))
c        etacvmn(icyc) = 10.**(etacr(1) + detac*xfitmn(ncyc*nvar+2))
c        etacvmx(icyc) = 10.**(etacr(1) + detac*xfitmx(ncyc*nvar+2))
         etatv  (icyc) = 10.**(etatr(1) )
         etatvmn(icyc) = 10.**(etatr(1) )
         etatvmx(icyc) = 10.**(etatr(1) )
c        etatv  (icyc) = 10.**(etatr(1) + detat*xfit  (3))
c        etatvmn(icyc) = 10.**(etatr(1) + detat*xfitmn(3))
c        etatvmx(icyc) = 10.**(etatr(1) + detat*xfitmx(3))
c        etatv  (icyc) = 10.**(etatr(1) + detat*xfit  (ncyc*nvar+3))
c        etatvmn(icyc) = 10.**(etatr(1) + detat*xfitmn(ncyc*nvar+3))
c        etatvmx(icyc) = 10.**(etatr(1) + detat*xfitmx(ncyc*nvar+3))
      ENDDO
      etac     = etacv(1)
      etat     = etatv(1)
      
      etatt    = etatv(1)
      
c--------------
c     omeg0    : amplitude of surface differential rotation (in rad/s) (2.894e-6 with etat=2.8037e11 gives Co=5.e4)
c     a0,a2,a4 : parameters for surface diff. rot.      =1., -0.1264, -0.1591
c     mu02     : colat. corresponding to core diff.rot. =55. deg
c     omgc     ; core differential rotation speed
c--------------
      omeg0    = 2.894e-6
c     a0       = 1.
      a0       = 0.04                     ! Pour fixer le référenciel "moyen" de la rot. diff.
      a2       =-0.1264
      a4       =-0.1591
      mu02     = (COS(55./180.*pi))**2
      omgc     = a0 + a2*mu02 + a4*mu02*mu02
      
c--------------
c     uu       : amplitude of meridional circulation (in cm/s) (1.00722e3 with etat=2.8037e11 gives Rm=250.)
c     rbot     : bottom of meridional circulation       =0.61
c     mm,pp,qq : parameters for meridional circulation  =0.5, 0.25, 0.
c     vv,ww    : new parameter for meridional circulation (<~1. equivalent to sin(tht)*cos(tht))
c--------------
      uur      = 1.e3*[1.7,1.7]        !u0 = 17 m/s (Optimal W21x8-11); 18 m/s (Miesch2014)
c     uur      = 1.e3*[1.2,1.2]        !u0 = 12 m/s (Optimal W21-2); 11 m/s (Baumann2004)
c     uur      = 1.e3*[1.0,1.6]        !u0 = [10.,16.] m/s (Optimal W21-2)
c     uur      = 1.e3*[0.8,1.8]        !u0 = [ 8.,18.] m/s (Optimal W21-7)
c     uur      = 1.e3*[0.5,3.0]        !u0 = [ 5.,30.] m/s (Old: [6.,28.] m/s; [8.,30.] m/s)
      duu      = uur(2)-uur(1)
      ppr      = [-0.7,-0.7]           !p = 10^(-0.7)=0.2 (Optimal W21x8-11); 10^(2.0)=100 (Miesch2014); 10^(-0.6)=0.25 (Reference)
c     ppr      = [2.2,3.0]             !p = 10^([2.2,3.0]-0.1*uu/100)
c     ppr      = [-1.,2.]              !p = 10^[-1.,2.] = [0.1,100.] (Old: [0.1,10.]; [0.01,10.])
      dpp      = ppr(2)-ppr(1)
      rbotr    = [0.6,0.6]             !Rb/R = 0.6 (Optimal W21x8-11); 0.69 (Miesch2014)
c     rbotr    = [0.6,0.7]             !Rb/R = [0.6,0.7]
      drbot    = rbotr(2)-rbotr(1)
      mmr      = [1.5,1.5]             !m = 1.5 (Adiabatic); 0.5 (Miesch2014); 0.5 (Reference)
c     mmr      = [0.4,2.0]             !m = [0.4,2.0]
      dmm      = mmr(2)-mmr(1)
      qqr      = [ 0., 0.]             !q = 2^0.=1. (Optimal W21x8-11); 2.^1.32=2.5 (Miesch2014)
c     qqr      = [2.807,2.807]         !q = 2^2.807 = 7. (Optimal W21-7,v=2); 2.^2.585 = 6.000 (Reference Baumann2004); 2.^0 = 1.000 (Peak at 45deg lat.)
c     qqr      = [2.000,3.459]         !q = 2^[2.,3.459] = [4.,11.] (Optimal W21-7,v=2)
c     qqr      = [0.7,2.3]             !q = 2^([0.7,2.3]+1.25*log2v^2) (Optimal W21-7)
c     qqr      = [ 1., 5.]             !q = 2^[1.,5.] = [2.,32.] (Optimal W21-7)
c     qqr      = [ 0., 5.]             !q = 2^[0.,5.] = [1.,32.] (Old: [1.,8.])
      dqq      = qqr(2)-qqr(1)
      vvr      = [2.807,2.807]         !v = 2^2.807=7. (Optimal W21x8-11); 2^0.=1. (Miesch2014)
c     vvr      = [ 1., 1.]             !v = 2^1 = 2. (Optimal W21-7); 2. (Reference Baumann2004), 1. (Peak at 45deg lat.)
c     vvr      = [ 0., 1.807]          !v = 2^[0.,1.807] = [1.,3.5] (Optimal W21-7)
c     vvr      = [ 0., 3.]             !v = 2^[0.,3.] = [1.,8.] (Old: 2^[0.,2.]=[1.,4.])
      dvv      = vvr(2)-vvr(1)
      wwr      = [ 0., 0.]             !w = 2^0 = 1. (Optimal W21x8-11); 2^1.807=3.5 (Miesch2014)
c     wwr      = [ 3., 3.]             !w = 2^3 = 8. (Optimal W21-7; 1. (Reference Baumann2004, Fixed in WS8, Peaks at 45deg latitutde)
c     wwr      = [ 2., 5.]             !w = 2^[2.,5.] = [4.,32.] (Optimal W21-7)
c     wwr      = [ 0., 5.]             !w = 2^[0.,5.] = [1.,32.] (Old: 2^[-2.,4.]=[0.25,16.])
      dww      = wwr(2)-wwr(1)
c     etasr    = 1.e10*[400,400]       !nR = 500.E10 cm^2/s (Optimal WS8); 600. km^2/s (Reference)
c     etasr    = 1.e10*[200,2000]      !nR = [200.E10,2000.E10] cm^2/s (Old: [100.E10,900.E10] cm^2/s)
      etasr    = [12.778,12.778]       !nR = 10^12.778=600.E10 cm^2/s (Optimal W21x8-11); 10^12.477=300.E10 cm^2/s (Miesch2014)
c     etasr    = [12.544,12.544]       !nR = 10^12.544=350.E10 cm^2/s (Optimal W21-2); 10^12.778=600.E10 cm^2/s (Baumann2004)
c     etasr    = [12.447,12.623]       !nR = 10^[12.447,12.623]=[280.E10,420.E10] cm^2/s (Optimal W21-2,u0=12m/s)
c     etasr    = [12.447,12.623]       !nR = 10^([12.447,12.623]+0.037*(u0/100-12))=[280.E10,420.E10]*10^(0.037*(u0/100-12)) cm^2/s (Optimal W21-2)
c     etasr    = [12.380,12.820]       !nR = 10^[12.380,12.820]=[240.E10,660.E10] cm^2/s  (Optimal W21-7)
c     etasr    = [12.,14.]             !nR = 10^[12.,14.] cm^2/s
      detas    = etasr(2)-etasr(1)
      DO icyc=1,ncyc
         uuv  (icyc)  =        uur(1)
         uuvmn(icyc)  =        uur(1)
         uuvmx(icyc)  =        uur(1)
c        uuv  (icyc)  =        uur(1) + duu  *xfit  (4)
c        uuvmn(icyc)  =        uur(1) + duu  *xfitmn(4)
c        uuvmx(icyc)  =        uur(1) + duu  *xfitmx(4)
c        uuv  (icyc)  =        uur(1) + duu  *xfit  (ncyc*nvar+1)
c        uuvmn(icyc)  =        uur(1) + duu  *xfitmn(ncyc*nvar+1)
c        uuvmx(icyc)  =        uur(1) + duu  *xfitmx(ncyc*nvar+1)
c        uuv  (icyc)  =        uur(1) + duu  *xfit  ((icyc-1)*nvar+1)
c        uuvmn(icyc)  =        uur(1) + duu  *xfitmn((icyc-1)*nvar+1)
c        uuvmx(icyc)  =        uur(1) + duu  *xfitmx((icyc-1)*nvar+1)
         usv  (icyc)  =        uuv  (icyc)
         usvmn(icyc)  =        uuvmn(icyc)
         usvmx(icyc)  =        uuvmx(icyc)
         ppv  (icyc)  =10.**(  ppr(1) )
         ppvmn(icyc)  =10.**(  ppr(1) )
         ppvmx(icyc)  =10.**(  ppr(1) )
c        ppv  (icyc)  =10.**(  ppr(1) + dpp  *xfit  (5))
c        ppvmn(icyc)  =10.**(  ppr(1) + dpp  *xfitmn(5))
c        ppvmx(icyc)  =10.**(  ppr(1) + dpp  *xfitmx(5))
c        ppv  (icyc)  =10.**(  ppr(1) + dpp  *xfit  (5)
c    +                                     -0.15*(0.01*uuv  (icyc)-12.))
c        ppvmn(icyc)  =10.**(  ppr(1) + dpp  *xfitmn(5)
c    +                                     -0.15*(0.01*uuvmn(icyc)-12.))
c        ppvmx(icyc)  =10.**(  ppr(1) + dpp  *xfitmx(5)
c    +                                     -0.15*(0.01*uuvmx(icyc)-12.))
c        ppv  (icyc)  =10.**(  ppr(1) + dpp *xfit  (5)-1e-3*uuv  (icyc))
c        ppvmn(icyc)  =10.**(  ppr(1) + dpp *xfitmn(5)-1e-3*uuvmn(icyc))
c        ppvmx(icyc)  =10.**(  ppr(1) + dpp *xfitmx(5)-1e-3*uuvmx(icyc))
c        ppv  (icyc)  =10.**(  ppr(1) + dpp  *xfit  (ncyc*nvar+5))
c        ppvmn(icyc)  =10.**(  ppr(1) + dpp  *xfitmn(ncyc*nvar+5))
c        ppvmx(icyc)  =10.**(  ppr(1) + dpp  *xfitmx(ncyc*nvar+5))
c        ppv  (icyc)  =10.**(  ppr(1) + dpp  *xfit  ((icyc-1)*nvar+2))
c        ppvmn(icyc)  =10.**(  ppr(1) + dpp  *xfitmn((icyc-1)*nvar+2))
c        ppvmx(icyc)  =10.**(  ppr(1) + dpp  *xfitmx((icyc-1)*nvar+2))
         rbotv  (icyc)=      rbotr(1)
         rbotvmn(icyc)=      rbotr(1)
         rbotvmx(icyc)=      rbotr(1)
c        rbotv  (icyc)=      rbotr(1) + drbot*xfit  (6)
c        rbotvmn(icyc)=      rbotr(1) + drbot*xfitmn(6)
c        rbotvmx(icyc)=      rbotr(1) + drbot*xfitmx(6)
c        rbotv  (icyc)=      rbotr(1) + drbot*xfit  (ncyc*nvar+6)
c        rbotvmn(icyc)=      rbotr(1) + drbot*xfitmn(ncyc*nvar+6)
c        rbotvmx(icyc)=      rbotr(1) + drbot*xfitmx(ncyc*nvar+6)
         mmv  (icyc)  =        mmr(1)
         mmvmn(icyc)  =        mmr(1)
         mmvmx(icyc)  =        mmr(1)
c        mmv  (icyc)  =        mmr(1) + dmm  *xfit  (7)
c        mmvmn(icyc)  =        mmr(1) + dmm  *xfitmn(7)
c        mmvmx(icyc)  =        mmr(1) + dmm  *xfitmx(7)
c        mmv  (icyc)  =        mmr(1) + dmm  *xfit  (ncyc*nvar+1)
c        mmvmn(icyc)  =        mmr(1) + dmm  *xfitmn(ncyc*nvar+1)
c        mmvmx(icyc)  =        mmr(1) + dmm  *xfitmx(ncyc*nvar+1)
         qqv  (icyc)  = 2.**(  qqr(1) )
         qqvmn(icyc)  = 2.**(  qqr(1) )
         qqvmx(icyc)  = 2.**(  qqr(1) )
c        qqv  (icyc)  = 2.**(  qqr(1) + dqq  *xfit  (7))
c        qqvmn(icyc)  = 2.**(  qqr(1) + dqq  *xfitmn(7))
c        qqvmx(icyc)  = 2.**(  qqr(1) + dqq  *xfitmx(7))
c        qqv  (icyc)  = 2.**(  qqr(1) + dqq  *xfit  (ncyc*nvar+2))
c        qqvmn(icyc)  = 2.**(  qqr(1) + dqq  *xfitmn(ncyc*nvar+2))
c        qqvmx(icyc)  = 2.**(  qqr(1) + dqq  *xfitmx(ncyc*nvar+2))
c        vvv  (icyc)  =     (  vvr(1) + dvv  *xfit  (8))
c        vvvmn(icyc)  =     (  vvr(1) + dvv  *xfitmn(8))
c        vvvmx(icyc)  =     (  vvr(1) + dvv  *xfitmx(8))
c        qqv  (icyc)  = 2.**(  qqr(1)+dqq*xfit  (7)+1.25*vvv  (icyc)**2)
c        qqvmn(icyc)  = 2.**(  qqr(1)+dqq*xfitmn(7)+1.25*vvvmn(icyc)**2)
c        qqvmx(icyc)  = 2.**(  qqr(1)+dqq*xfitmx(7)+1.25*vvvmx(icyc)**2)
c        vvv  (icyc)  = 2.**(  vvv  (icyc))
c        vvvmn(icyc)  = 2.**(  vvvmn(icyc))
c        vvvmx(icyc)  = 2.**(  vvvmx(icyc))
         vvv  (icyc)  = 2.**(  vvr(1) )
         vvvmn(icyc)  = 2.**(  vvr(1) )
         vvvmx(icyc)  = 2.**(  vvr(1) )
c        vvv  (icyc)  = 2.**(  vvr(1) + dvv  *xfit  (8))
c        vvvmn(icyc)  = 2.**(  vvr(1) + dvv  *xfitmn(8))
c        vvvmx(icyc)  = 2.**(  vvr(1) + dvv  *xfitmx(8))
c        vvv  (icyc)  = 2.**(  vvr(1) + dvv  *xfit  (ncyc*nvar+3))
c        vvvmn(icyc)  = 2.**(  vvr(1) + dvv  *xfitmn(ncyc*nvar+3))
c        vvvmx(icyc)  = 2.**(  vvr(1) + dvv  *xfitmx(ncyc*nvar+3))
         wwv  (icyc)  = 2.**(  wwr(1) )
         wwvmn(icyc)  = 2.**(  wwr(1) )
         wwvmx(icyc)  = 2.**(  wwr(1) )
c        wwv  (icyc)  = 2.**(  wwr(1) + dww  *xfit  (9))
c        wwvmn(icyc)  = 2.**(  wwr(1) + dww  *xfitmn(9))
c        wwvmx(icyc)  = 2.**(  wwr(1) + dww  *xfitmx(9))
c        wwv  (icyc)  = 2.**(  wwr(1) + dww  *xfit  (ncyc*nvar+4))
c        wwvmn(icyc)  = 2.**(  wwr(1) + dww  *xfitmn(ncyc*nvar+4))
c        wwvmx(icyc)  = 2.**(  wwr(1) + dww  *xfitmx(ncyc*nvar+4))
c        etasv  (icyc)=      etasr(1)
c        etasvmn(icyc)=      etasr(1)
c        etasvmx(icyc)=      etasr(1)
c        etasv  (icyc)=      etasr(1) + detas*xfit  (5)
c        etasvmn(icyc)=      etasr(1) + detas*xfitmn(5)
c        etasvmx(icyc)=      etasr(1) + detas*xfitmx(5)
c        etasv  (icyc)=      etasr(1) + detas*xfit  (ncyc*nvar+5)
c        etasvmn(icyc)=      etasr(1) + detas*xfitmn(ncyc*nvar+5)
c        etasvmx(icyc)=      etasr(1) + detas*xfitmx(ncyc*nvar+5)
         etasv  (icyc)=10.**(etasr(1) )
         etasvmn(icyc)=10.**(etasr(1) )
         etasvmx(icyc)=10.**(etasr(1) )
c        etasv  (icyc)=10.**(etasr(1) + detas*xfit  (10))
c        etasvmn(icyc)=10.**(etasr(1) + detas*xfitmn(10))
c        etasvmx(icyc)=10.**(etasr(1) + detas*xfitmx(10))
c        etasv  (icyc)=10.**(etasr(1) + detas*xfit  (ncyc*nvar+1))
c        etasvmn(icyc)=10.**(etasr(1) + detas*xfitmn(ncyc*nvar+1))
c        etasvmx(icyc)=10.**(etasr(1) + detas*xfitmx(ncyc*nvar+1))
c        etasv  (icyc)=10.**(etasr(1) + detas*xfit  (8) 
c    +                                   + 0.037*(0.01*uuv  (icyc)-12.))
c        etasvmn(icyc)=10.**(etasr(1) + detas*xfitmn(8) 
c    +                                   + 0.037*(0.01*uuvmn(icyc)-12.))
c        etasvmx(icyc)=10.**(etasr(1) + detas*xfitmx(8) 
c    +                                   + 0.037*(0.01*uuvmx(icyc)-12.))
      ENDDO
      uu       = uuv(1)
      us       = usv(1)
      rbot     = rbotv(1)
      mm       = mmv(1)
      pp       = ppv(1)
      qq       = qqv(1)
      nn       = 1.
      vv       = vvv(1)
      ww       = wwv(1)
      etas     = etasv(1)
      
      uuu      = uu
      etass    = etas
      
c--------------
c     taud     : exponential decay time scale of the surface magn. field
c--------------
      taur     = [3.322,3.322]      !tR >~ 2^3.322=10. yr (Optimal W21x8-11)
c     taur     = [5.,5.]            !tR >~ 2^5.=32. yr (Optimal W21-7; Reference; Miesch2014), 7.1 yr (Final WS8 solution), 8. yr (Schrijver2002)
c     taur     = [2.807,5.]         !tR = 2^[2.807,5.] = [7.,32.] yr (Optimal W21-7)
c     taur     = [1.,5.]            !tR = 2^[1.,5.] = [2.,32.] yr
      dtau     = taur(2)-taur(1)
      taud     = 2.**(taur(1) )
      taudmn   = 2.**(taur(1) )
      taudmx   = 2.**(taur(1) )
c     taud     = 2.**(taur(1) + dtau*xfit  (11))
c     taudmn   = 2.**(taur(1) + dtau*xfitmn(11))
c     taudmx   = 2.**(taur(1) + dtau*xfitmx(11))
c     taud     = 2.**(taur(1) + dtau*xfit  (ncyc*nvar+6))
c     taudmn   = 2.**(taur(1) + dtau*xfitmn(ncyc*nvar+6))
c     taudmx   = 2.**(taur(1) + dtau*xfitmx(ncyc*nvar+6))
      
c--------------
c     ampi     : if incr=0, amplitude of initial field distribution (G)
c     expi     : if incr=0, exponent of the cos(theta)
c     faci     : if incr=1, multiplication factor for rst file
c--------------
c     ampi     =-10.        !+-8.  1.e-2                 !pour icnd=2, positif pour cycles impairs (signe ajusté ci-dessous)
c     cyc1     = 18                                      !First cycle number
c     nrep     = 9                                       !Number of repetition of the same cycle
      
      ampr     = [ 8.5, 8.5]                             !B0 = 8.5 G (Optimal W21-7); 11.5 G (Svaalgard1978); 7.2 G (final solution blds_48x200_wangal_21x01s5_uqvwetaf3x2td)
c     ampr     = [ 0. ,25. ]                             !Amplitude of initial field distribution (G)
c     ampr     = [ 2. ,40. ]                             !10 x multiplication factor for rst file
      damp     = ampr(2)-ampr(1)
      ampi     = ampr(1)
      ampimn   = ampr(1)
      ampimx   = ampr(1)
c     ampi     = ampr(1) + damp*xfit  (7)
c     ampimn   = ampr(1) + damp*xfitmn(7)
c     ampimx   = ampr(1) + damp*xfitmx(7)
c     ampi     = ampr(1) + damp*xfit  (ncyc*nvar+7)
c     ampimn   = ampr(1) + damp*xfitmn(ncyc*nvar+7)
c     ampimx   = ampr(1) + damp*xfitmx(ncyc*nvar+7)
      IF ((cyc1/2.-cyc1/2).EQ.0.) THEN                   !Negative amplitude for even cycles
         ampi  =-ampi
      ENDIF
      
c     expir    = [0.0,5.0]
c     dexpi    = expir(2)-expir(1)
c     expi     = 2.**(  expir(1) )!+ dexpi  *xfit  (ncyc*nvar+8))
      expi     = 8                                       !erf(cos^12/(lerfi=pi/8)) or cos^8
      lerfi    = 0.                                      !pi/8.                                   !
      faci     = 1.                 !franky multiplication factor
      
      icnd     = 2
      IF (ampi.EQ.0.) icnd = 0
      
c--------------
c     databseed: random seed to use for the production of the synthetic database
c--------------
      IF (datseedi.EQ.0) THEN
         datseedr = [0,0]
      ELSEIF (datseedi.EQ.1) THEN
         datseedr = [10000,99999]
      ELSE
         datseedr = [datseedi,datseedi]
      ENDIF
      ddatseed = datseedr(2)-datseedr(1)
c     datseed  = INT(datseedr(1) + ddatseed*xfit(1))
      datseed  = INT(datseedr(1))
      
c--------------
c     Parameters for the fitness itself:
c     bcrit    = lower cut-off (G) on the bfit amplitude
c     dbcrit   = width of error function for the cut-off
c     bbb      = exponent of bphi in bfit
c     aaa      = exponent of aphi in bfit
c     ccc      = exponent of bfit, such that it saturates above bcrit if ccc=0.
c     xcrit0   = equatorial intercept of latitudinal weighting
c     xcrit    = upper limit to latitudinal weighting
c     dxcrit   = width of error function for latitudinal weighting
c     expsin   = ...
c     bfitfac  = multiplication factor for bfit to generate emergences
c     nsmth    = number of diffusive smoothing iterations
c     expbfly  = exponent to bfly
c--------------
      bcrit    = 10**(2.0)                               !B* = 10.^2 (Optimal W21x8-18)
c     bcrit    = 10**(1.0 + 4.0*xfit(12))                !B* = 10.^[1.,4.]
      dbcrit   = 0.1*bcrit                               !dbcrit = 0.1*bcrit
c     dbcrit   =      0.01+ 4.0*xfit(10)                 !dbcrit = [0.01,4.0]
      bbb      = 1.5                                     !b = 1.5 (Optimal W21x8-18); 1.0 (Miesch2014)
c     bbb      =      0.5 + 2.5*xfit(13)                 !b = [0.5,3.0]
      aaa      = 0.0                                     !a = 0.0 (Optimal W21x8-18); 0.0 (Miesch2014)
c     aaa      =      0.0 + 2.0*xfit(14)                 !a = [0.0,2.0]
      ccc      = 1.0                                     !c = 1.0 (Optimal W21x8-18); 1.0 (Miesch2014)
c     ccc      =      0.0 + 1.0*xfit(15)                 !c = [0.0,1.0]
      xcrit0   = 0.5                                     !xcrit0= 0.5 (Optimal W21x8-18, fits Ferriz-Mas); 0.0 (Miesch2014)
c     xcrit0   =      0.0 + 1.0*xfit(16)                 !xcrit0= [0.,1.]
      xcrit    = 0.94                                    !xcrit = 0.94=COS(20°)=SIN(70°lat) (Optimal W21x8-18); 0.69=COS(46,4°) (Miesch2014)
c     xcrit    =      0.90+0.10*xfit(17)                 !xcrit = [0.90,1.00]=[COS(26°),COS(0°)]=[SIN(64°lat),SIN(90°lat)]
      dxcrit   = 0.02                                    !dxcrit = 0.02=COS(18.3°)-COS(21.7°)=SIN(71.7°lat)-SIN(68.3°lat) (Optimal W21x8-18, fits Ferriz-Mas); 0.06 (Miesch2014)
c     dxcrit   =      0.01+0.49*xfit(14)                 !dxcrit = [0.01,0.50]
c     expsin   = 1.
      nsmth    = 5                                       !nsmth = 5
c     nsmth    =  INT(0.  +10. *xfit(14))                !nsmth = [0,10]
      expfly   = 1.                                      !expfly= 1.0
c     expfly   =      0.5 + 2.5*xfit(15)                 !expfly= [0.5,3.0]

c---- Multiplication factor for bfit to generate emergences: the DYNAMO NUMBER 
c     (see 'bfitqch' in subroutine 'surfbr2_init' for the tilt-quenching factor)
      bfitfac  = 0.70                                    !franky

c---- Proportion of each fitness to use:
      iffit1   = 1.0                                     !Linear correlation between bfit and surface bfly
      iffit2   = 0.                                      !Linear correlation between magnetic "energy" and monthly ssn
      iffit3   = 0.0                                     !Linear correlation between surface br and surface magnetogram
      
c---- Define latitudinal regions for flux integration:
      region(1)= 0./180.                                 !Region of flux accumulation (North hemisphere)
      region(2)=20./180.                                 !Region of flux accumulation (North hemisphere)
      region(3)=40./180.                                 !Region of flux transport (North hemisphere)
      region(4)=55./180.                                 !Region of flux transport (North hemisphere)
      region(5)=65./180.                                 !Region of flux emergence (North hemisphere)
      region(6)=90./180.                                 !Region of flux emergence (North hemisphere)
      
c--------------
c     s0       : amplitude of alpha effect (in cm/s) (1.00917e2 with etat=2.8095e11 gives Ca=25.)
c--------------
c     s0       = 1.00917e2
      s0       = 0.
      
c--------------
c     calpha   : alpha/source dynamo number:            =s_0 R/eta_T
c     comega   : Differential Rotation dynamo number    =Omega_0 R^2/eta_T
c     reynolds : Reynolds number based on circulation   =u_0 R/eta_T
c     gamfl    : amplitude of loss term (if iloss=1)
c--------------
c --> ALem :
      calpha   = s0*rsun/etatt
      comega   = omeg0*rsun**2./etatt
      reynolds = uuu*rsun/etatt
      
      gamfl    = iloss * 1.e-5
      
c -- following for ref model of DC99 (Sec. 3)
c     calpha   = 25.
c     comega   = 5.e4
c     etadf    = 300.
c     reynolds = 840.
c     gamfl    = 0.
c     le nombre dynamo critique pour etadf=10 calpha = 10 comega = 1.525e4

c following for aoCM
c     calpha   = 0.5
c     comega   = 5.e5
c     etadf    = 10.
c     reynolds = 2500.
c     gamfl    = 1.e-9
c     gamfl    = 0.

c following for Dario test
c     title    ='Dario/Surya test, rbot=0.61, gamma=0'
c     calpha   = 72.
c     comega   = 3.e5
c     etadf    = 182.
c     reynolds = 5000.
c     gamfl    = 0.e-6

c------ this is the amplitude of the additive noise to vector potential A
c       at the surface
c     addnamp  = 1.0e+03
      addnamp  = 0.
      
c--------------
c     ycyc     : dates of activity minima (years)
c     shftcyc  : shift between cycle number and position in ycyc
c     dmonth   : number of days in a year, before a given month
c     dcyc     : time of activity minima (days)
c     dur      : duration of the simulation, in days
c                (anciens calculs avec: 11 y. = 4017 d., 44 y. = 16068 d., 88 y. = 32136., cycles 16-23 = 31141.)
c                (cycle19x1:  3836., cycle20x1:  4261., cycle21x1:  3744., cycle22x1:  3530., cycle23x1:  4597.)
c                (cycle18x5: 18560., cycle18x9:33408., cycle19x8: 30688., cycle20x8: 34088., cycle21x8: 29952., cycle22x8: 28240., cycle23x8: 36776.)
c                (cycles19-20x4: 32388., cycles 16-23: 31169., cycles 14-23: 39020., cycles 01-23: 92684.)
c     tau      : diffusion time = R^2/eta_T (in s)
c     tomti    : duration of simulation, in diffusion times
c--------------
      ycyc= [1701.       , 1713.       , 1724.       , 1732.       ,
     +       1744.       , 1755.+ 2./12, 1766.+ 5./12, 1775.+ 5./12,
     +       1784.+ 7./12, 1798.+ 4./12, 1810.+11./12, 1823.+ 4./12,
     +       1833.+10./12, 1843.+ 6./12, 1855.+11./12, 1867.+ 2./12,
     +       1878.+11./12, 1890.+ 2./12, 1902.+ 1./12, 1913.+ 7./12,
     +       1923.+ 7./12, 1933.+ 8./12, 1944.+ 1./12, 1954.+ 3./12,
     +       1964.+ 9./12, 1976.+ 5./12, 1986.+ 8./12, 1996.+ 4./12,
     +       2008.+11./12]
c     ycyc= [1701.       , 1713.       , 1724.       , 1732.       ,
c    +       1744.       , 1755.+ 2./12, 1766.+ 5./12, 1775.+ 5./12,
c    +       1784.+ 7./12, 1798.+ 4./12, 1810.+11./12, 1823.+ 4./12,
c    +       1833.+10./12, 1843.+ 6./12, 1855.+11./12, 1867.+ 2./12,
c    +       1878.+11./12, 1890.+ 2./12, 1902.+ 1./12, 1913.+ 7./12,
c    +       1923.+ 7./12, 1933.+ 8./12, 1944.+ 1./12, 1954.+ 3./12,
c    +       1964.+ 9./12, 1976.+ 5./12,
c    +       1976.+ 5./12+1*15.375/12, 1976.+ 5./12+2*15.375/12,
c    +       1976.+ 5./12+3*15.375/12, 1976.+ 5./12+4*15.375/12,
c    +       1976.+ 5./12+5*15.375/12, 1976.+ 5./12+6*15.375/12,
c    +       1976.+ 5./12+7*15.375/12,
c    +                                   1986.+ 8./12, 1996.+ 4./12,
c    +       2008.+11./12]
c    +       1981.+6.5/12,
c    +       1978.+11.75/12,1981.+6.5/12,1984.+1.25/12,
c    +       1978.+5.6/12, 1980.+6.2/12, 1982.+6.8/12, 1984.+7.4/12,
      
      shftcyc=5
c     dmonth = [0,31,59,90,120,151,181,212,243,273,304,334]
      
c---- Modif to ycyc vector:
c     ycyc(21+shftcyc)=1976.7
      
      fsticyc=cyc1+shftcyc
      
      yeari=ycyc(fsticyc)
      dury=(ycyc(fsticyc+ncyc)-yeari)*nrep
      
      icyc=0
      DO i=fsticyc,fsticyc+ncyc
         icyc=icyc+1
c        year = INT(ycyc(i))
c        month= INT((ycyc(i)-year)*12+1+0.5)
c        dcyc(icyc)= (year-1)*365.+INT((year-1)/4.)+dmonth(month)
         dcyc(icyc)= (ycyc(i)-ycyc(fsticyc))*365. + INT((ycyc(i)-1))/4
     +                                            - INT((ycyc(i)-1))/100
     +                                            + INT((ycyc(i)-1))/400
c        IF (((year/4.-INT(year/4.)).EQ.0.).AND.(month.GE.3)) THEN
c           dcyc(icyc) = dcyc(icyc) + 1.
         IF ((INT(ycyc(i))/4.  .EQ.INT(ycyc(i))/4  ).AND.
     +      ((INT(ycyc(i))/100..NE.INT(ycyc(i))/100).OR.
     +       (INT(ycyc(i))/400..NE.INT(ycyc(i))/100))) THEN
            dcyc(icyc) = dcyc(icyc) + (ycyc(i)-INT(ycyc(i)))
         ENDIF
      ENDDO
      
      durd=dcyc(ncyc+1)-dcyc(1)
      
      DO irep=1,nrep
         DO icyc=1,ncyc
            tcyc((irep-1)*ncyc+icyc)=(dcyc(icyc)-dcyc(1))+(irep-1)*durd
         ENDDO
      ENDDO
      tcyc(ncyc*nrep+1)=nrep*durd
      
      dur=tcyc(ncyc*nrep+1)-tcyc(1)
      
      DO i=1,ncyc*nrep
         itcyc(i)=INT((tcyc(i)-tcyc(1))/dur*nt)+1
      ENDDO
      
      tau   = rsun**2./etatt
      tomti = dur*24.*3600./tau
c     tomti = 0.1
      
c-----------------------------------------------------------------------

c----------- Fitness parameters for optimization
c--------------
c     rbphi    : depth of toroidal field cut,           = was 0.718
c     rbr      : depth of radial field cut,             =1.0
c     tfit     : part of normalized temporal mesh to use (from tfit(1) to tfit(2))
c     latfit   : part of latitudinal mesh to use (from latfit(1)*pi to latfit(2)*pi)
c
c     bycrit   : Toroidal field treshold for the fit    =0.6
c     brcrit   : Surface field treshold for the fit     =3.e-2
c--------------
c     rbphi    = [0.70,0.70]                                !rbphi = [0.70,0.70]
      rbphi    = [0.68,0.70]                                !rbphi = [0.68,0.70] (Optimal W21x8-18); [0.72,0.72] (WD...); [0.68,0.70] (ancien); [0.70,0.71] (Miesch2014)
c     rbphi(1) = 0.60+0.20*xfit(18)                         !rbphi = [[0.6,0.6],[0.8,0.8]]
c     rbphi(2) = rbphi(1)
      rbr      = 1.0
      tfit     = [tfitf(1),tfitf(2)]/100.                   ![0.10,1.00]![0.03,1.00]   ![(tcyc(nrep*ncyc+1-nfcyc)-tcyc(1))/dur,1.0]
      latfit   = [ 0./180.,90./180.]                        ![25./180.,60./180.]                        !
      
c     bycrit   = 0.6
c     brcrit   = 3.e-2
      
      itfit    = [INT(tfit(1)*nnt)+1,INT(tfit(2)*nnt)+1]
      ntfit    = itfit(2)-itfit(1)+1
      ixfits   = [INT(latfit(1)*nelx)+1,INT(latfit(2)*nelx)+1]
      ixfitn   = [     nndx-ixfits(2)+1,     nndx-ixfits(1)+1]
      IF (ixfitn(1).EQ.ixfits(2)) ixfitn(1)=ixfitn(1)+1 
      nxfits   = (ixfits(2)-ixfits(1)+1)
      nxfitn   = (ixfitn(2)-ixfitn(1)+1)
      nnfit    = ntfit*(nxfits+nxfitn)
      
      nfft     = [ 3,13]
      nnfft    = (nfft(2)-nfft(1))/2+1
      
c----------- BMR emergence parameters (Fraction of time span for bmr emergences)
      tbmr     = tfitf(2)/100.                                          !(Francois)
      itbmr    = INT(tbmr*nnt)+1                                        !(Francois)

c-----------------------------------------------------------------------

c----------- Title and output files ------------------------------------

      IF(ivrb.GE.1) THEN
         
c     WRITE(title,17) ipar,isrc,ialp,iloss,icnd,istc,nt,nstep,nelx,nely
c    +               ,calpha,comega,etadf,reynolds,rzc,ybl,etac,etat,
c    +                uu,rbot,mm,pp,qq,vv,ww,rbphi(1)
c  17 format('BLdyn ',i1,i1,i1,i1,i1,i1,'+',i4,'x'i3,'steps',i3,'x',i3,
c    +       ' Ca=',e8.2,' Co=',e8.2,' deta=',e8.2,' Rm=',e8.2,
c    +       ' rzc=',f4.2,' ybl=',f5.3,' etac=',e10.4,' etat=',e10.4,
c    +       ' uu=',e10.4,' rbot=',f5.3,' m=',f4.2
c    +       ' p=',f5.2,' q=',f5.2,' w=',f4.2,' rfit=',f5.3)            !200 caract. tout juste
         
         WRITE(title,17) ipar,isrc,ialp,iloss,icnd,istc,
     +                   nt,nstep,nelx,nely,
     +                   cyc1,cyc1+ncyc-1,nrep,tfit(1),tfit(2),INT(dur),
     +                   rzc,s0,calpha,comega,reynolds
   17    FORMAT('B-L dynamo ',i1,i1,i1,i1,i1,i1,'+: ',
     +          i3,'x',i4,'steps ',i3,'x',i3,
     +          ' cyc',i2,'-',i2,'x',i1,' fit',f3.1,'-',f3.1,'x',i5,'d',
     +      ' rzc=',f3.1,' s0=',f3.1' Ca=',e8.2,' Co=',e8.2,' Rm=',e8.2)
         
      ENDIF
      
      CALL GETENV('HOME',home)
      CALL GETENV('SCRATCH',scratch)
c     scratch = home(1:strlen(home)) // '/Documents/scratch'
      
c---- All output files go to directory [patho]
c     all    : output  file [outfile] solution at each time step
c     incr=1 : restart file [inrfile] being used as initial condition
c     incw=1 : restart file [inwfile] being written
      incw=1
c     inrfile='0'
      IF (inrfile(1:strlen(inrfile)).EQ.'0') THEN
         incr=0
      ELSE
         incr=1
      ENDIF
      
c     simname='synth0_16-23'
      
c     pathd=home(1:strlen(home))//'/Documents/1-Doctorat/0-data/arnaud/'
c     bmrfile = pathd // 'synth_wolf_bmr_cycle21x01_1234.dat'
      IF (datseed.EQ.0) THEN
         bmrfile = bmrfilei
      ELSE
         WRITE(datseedtext,'(i5)') datseed
         bmrfile = bmrfilei(1:strlen(bmrfilei)-strlen('.dat'))//
     +                                          '_'//datseedtext//'.dat'
         INQUIRE(FILE=bmrfile, EXIST=bmrfile_exists)                    ! file_exists will be TRUE if the file exists and FALSE otherwise
         IF (.NOT.bmrfile_exists)
     +     CALL synthese_wolf_bmr_alex(datseed,cyc1,cyc1+ncyc-1,bmrfile)
      ENDIF
      ssnfile=bmrfile(1:strlen(bmrfile)-strlen('.dat'))//'.ssn'
      
c     simid='1'
      
      patho=''
      
      IF(ivrb.GE.1) THEN
         
c        patho = scratch(1:strlen(scratch)) //'/5-bldsforecast/xesults/'
         patho = './xesults/'
         patho = patho(1:strlen(patho))//simname(1:strlen(simname))//'/'
         
         WRITE(filename,18) ipar,isrc,ialp,icnd,
     +      nt,nstep,nsteps,nelx,nely,nesxvrai,nesyvrai,
     +      INT(reynolds+0.5),
c    +      ybl,LOG10(etac),LOG10(etat),uu/100.,
c    +      rbot,mm,pp,qq,vv,ww,INT(etas/1.e10+0.5),INT(taud+0.5),      !rbphi(1),
c    +      INT(ABS(ampi)+0.5),INT(expi+0.5),
     +      tbmr,bmrtype,simname,incr,simid,datetime                    !(Francois)
   18    FORMAT('blds',i1,i1,i1,i1,
     +      '_',i4,'x',i2,'xs',i5,'_',i3,'x',i2,'s',i3,'x',i3,
     +      '_Rm',i3,
     +      '_paramII2017',
c    +      'ybl',f5.3,'etac',f4.1,'etat',f4.1,'u',f4.1,'rbt',f4.2,
c    +      'm',f4.2,'p',f6.2,'q',f4.1,'v',f3.1,'w',f4.1,'eta',i4,'tau',!'_rfit',f4.2,
c    +      i2,'a',i2,'n',i2,
     +      '_tbmr',f4.2,'_',12i1,'_',a12,'.',i1,'_',a4,'_',a14)        !(Francois)
         
         idlfile='1-analyse/blds_'//
     +    simname(1:strlen(simname))//'_'//simid(1:strlen(simid))//'_'//
     +                            datetime(1:strlen(datetime))//'.idlin'
         OPEN (10,FILE=idlfile)
         WRITE(10,'(a40)') simname
         WRITE(10,'(a250)') filename
         WRITE(10,'(2f9.2)') rbphi(1),rbphi(2)
         WRITE(10,'(2f9.2)') tfit(1),tfit(2)
         WRITE(10,'(2f9.2)') latfit(1),latfit(2)
         WRITE(10,'(3i9)') cyc1,ncyc,nrep
         WRITE(10,'(i9)') nfit
         WRITE(10,'(100f8.5)') xfit(1:nfit)
         WRITE(10,'(100f8.5)') xfitmn(1:nfit)
         WRITE(10,'(100f8.5)') xfitmx(1:nfit)
         WRITE(10,'(i9)') ncyc
         WRITE(10,'(100f9.2)') yblv(1:ncyc)
         WRITE(10,'(100f9.2)') yblvmn(1:ncyc)
         WRITE(10,'(100f9.2)') yblvmx(1:ncyc)
         WRITE(10,'(100e9.2)') etacv(1:ncyc)
         WRITE(10,'(100e9.2)') etacvmn(1:ncyc)
         WRITE(10,'(100e9.2)') etacvmx(1:ncyc)
         WRITE(10,'(100e9.2)') etatv(1:ncyc)
         WRITE(10,'(100e9.2)') etatvmn(1:ncyc)
         WRITE(10,'(100e9.2)') etatvmx(1:ncyc)
         WRITE(10,'(100e9.2)') uuv(1:ncyc)
         WRITE(10,'(100e9.2)') uuvmn(1:ncyc)
         WRITE(10,'(100e9.2)') uuvmx(1:ncyc)
         WRITE(10,'(100e9.2)') usv(1:ncyc)
         WRITE(10,'(100e9.2)') usvmn(1:ncyc)
         WRITE(10,'(100e9.2)') usvmx(1:ncyc)
         WRITE(10,'(100f9.2)') ppv(1:ncyc)
         WRITE(10,'(100f9.2)') ppvmn(1:ncyc)
         WRITE(10,'(100f9.2)') ppvmx(1:ncyc)
         WRITE(10,'(100f9.2)') rbotv(1:ncyc)
         WRITE(10,'(100f9.2)') rbotvmn(1:ncyc)
         WRITE(10,'(100f9.2)') rbotvmx(1:ncyc)
         WRITE(10,'(100f9.2)') mmv(1:ncyc)
         WRITE(10,'(100f9.2)') mmvmn(1:ncyc)
         WRITE(10,'(100f9.2)') mmvmx(1:ncyc)
         WRITE(10,'(100f9.2)') qqv(1:ncyc)
         WRITE(10,'(100f9.2)') qqvmn(1:ncyc)
         WRITE(10,'(100f9.2)') qqvmx(1:ncyc)
         WRITE(10,'(100f9.2)') vvv(1:ncyc)
         WRITE(10,'(100f9.2)') vvvmn(1:ncyc)
         WRITE(10,'(100f9.2)') vvvmx(1:ncyc)
         WRITE(10,'(100f9.2)') wwv(1:ncyc)
         WRITE(10,'(100f9.2)') wwvmn(1:ncyc)
         WRITE(10,'(100f9.2)') wwvmx(1:ncyc)
         WRITE(10,'(100e9.2)') etasv(1:ncyc)
         WRITE(10,'(100e9.2)') etasvmn(1:ncyc)
         WRITE(10,'(100e9.2)') etasvmx(1:ncyc)
         WRITE(10,'(f9.2)') taud
         WRITE(10,'(f9.2)') taudmn
         WRITE(10,'(f9.2)') taudmx
c        WRITE(10,'(f9.2)') amp
c        WRITE(10,'(f9.2)') ampmn
c        WRITE(10,'(f9.2)') ampmx
         WRITE(10,'(9f9.2)')
     +              bbb,aaa,bcrit,dbcrit,ccc,xcrit0,xcrit,dxcrit,bfitfac
         WRITE(10,'(i9,f9.2)') nsmth,expfly
         WRITE(10,'(a250)') bmrfile
c        CLOSE(10)
         
         idlfile='1-analyse/blds_'//
     +    simname(1:strlen(simname))//'_'//simid(1:strlen(simid))//'_'//
     +                          datetime(1:strlen(datetime))//'.s.idlin'
         OPEN (20,FILE=idlfile)
         WRITE(20,'(3i9)') nntsp1,nnsxvrai,nnsyvrai
         WRITE(20,'(3i9)') cyc1,ncyc,nrep
         WRITE(20,'(2i9)') nntp1,nndx
         WRITE(20,'(2f9.2)') tfit(1),tfit(2)
         WRITE(20,'(2f9.2)') latfit(1),latfit(2)
         WRITE(20,'(6f9.2)') (region(i),i=1,6)
         
         logfile = patho(1:strlen(patho))//filename(1:strlen(filename))
     +                                                          //'.log'
         outfile = patho(1:strlen(patho))//filename(1:strlen(filename))
     +                                                          //'.i3e'
         inwfile = patho(1:strlen(patho))//filename(1:strlen(filename))
     +                                                          //'.rst'
         
         OPEN(1,FILE=logfile)
         OPEN(8,FILE=outfile,FORM='unformatted')
         IF(incw.eq.1) OPEN(9,FILE=inwfile,FORM='UNFORMATTED')
         
      ENDIF
      
c---> Open inrfile if continuing from preceding output (incr=1)
      IF(incr.EQ.1) OPEN(7,FILE=inrfile,FORM='UNFORMATTED')
      
c-----------------------------------------------------------------------
      
      ijac=0
      it=0
      its=0
      n=1
      itt=1
      ctime=0.
      
c---> Output some header info
      IF(ivrb.GE.2) THEN
         CALL header(
     +          logfile,outfile,inrfile,inwfile,ssnfile,bmrfile,magfile,
     +    ncyc,yblv,etacv,etatv,uuv,usv,rbotv,mmv,ppv,qqv,vvv,wwv,etasv)
      ENDIF
      
c---> Initialize random number generator
      CALL urand_init(12345)
c     IF (bldseed(1).NE.0) CALL urand1_init(bldseed(1))      !franky
c     IF (bldseed(2).NE.0) CALL urand2_init(bldseed(2))      !franky
c     IF (bldseed(3).NE.0) CALL urand3_init(bldseed(3))      !franky
c     IF (bldseed(4).NE.0) CALL urand4_init(bldseed(4))      !franky previous lines 1019 to 1022

      CALL urand1_init(bldseed(1))
      CALL urand2_init(bldseed(2))
      CALL urand3_init(bldseed(3))
      CALL urand4_init(bldseed(4))
      
c---> Calcul du temps d'execution
      CALL CPU_TIME(cputi)
      
c---> Mesh generation
      CALL mesh2d
      
c---> Surface flux evolution: Initialize         
      IF(isrc.EQ.3) THEN
         CALL surfbr2_init(tbmr,              !franky removed bldseedd 
     +                  uu,us,qq,nn,vv,ww,etas,taud,dur,ampi,expi,lerfi,
     +                  patho,filename,bmrfile,magfile,simname,simid,vb,
     +                                                  bmrtype,inrsurf)
         CALL surfabc
         ntcall=nt/nts
         IF(ivrb.GE.2) THEN
            WRITE(1,*) 'ntcall    =',FLOAT(nt)/nts
            WRITE(*,*) 'ntcall    =',FLOAT(nt)/nts
         ENDIF
         IF (FLOAT(nt)/nts.NE.FLOAT(ntcall)) THEN
            WRITE(1,*)'! Wrong nt/nts ratio !'
            STOP      '! Wrong nt/nts ratio !'
         ENDIF
         nstepscall=nsteps/ntcall
         IF(ivrb.GE.2) THEN
            WRITE(1,*) 'nstepscall=',FLOAT(nsteps)/ntcall
            WRITE(*,*) 'nstepscall=',FLOAT(nsteps)/ntcall
         ENDIF
         IF (FLOAT(nsteps)/ntcall.NE.FLOAT(nstepscall)) THEN
            WRITE(1,*)'! Wrong nsteps/(nt/nts) ratio !'
            STOP      '! Wrong nsteps/(nt/nts) ratio !'
         ENDIF
      ENDIF
      
c---> Apply boundary conditions (initial state) (after mesh2d and surfabc)
      CALL aplybc(it)
      
c---> Recherche des niveaux pour l'optimisation
      iybphi(1)=0
      iybphi(2)=0
      iybr=0
      DO j=1,nely
      IF ((yrad(j).LE.rbr     ).AND.(yrad(j+1).GT.rbr     )) iybr  =j
      IF ((yrad(j).LE.rbphi(1)).AND.(yrad(j+1).GT.rbphi(1))) iybphi(1)=j
      IF ((yrad(j).LE.rbphi(2)).AND.(yrad(j+1).GT.rbphi(2))) iybphi(2)=j
      ENDDO
      IF ((iybphi(1).EQ.0).OR.(iybphi(2).EQ.0).OR.(iybr.EQ.0)) THEN
         WRITE(1,*) '! rbphi or rbr out of y domain !'
         STOP '! rbphi or rbr out of y domain !'
      ENDIF
      
c---> Some safety checks on flow fields, etc
c     CALL sfcheck
c     STOP ' *TMP*' 
      
c---> Nothing for now
c     CALL prepro(0,1,1)
      
c---> Compute energies in initial state; write initial condition
      CALL postpro(it,n,ctime)
      
      IF(ivrb.GE.1) THEN
         CALL writout(it,n,ctime)
      ENDIF
      
c**** Main time-stepping loop begins *****
      icyc=0
      irep=1
      DO it=1,nt
         ijac=it
         
c------> Change cycle
         IF(it.EQ.itcyc((irep-1)*ncyc+icyc+1)) THEN
            icyc=icyc+1
            IF(ivrb.GE.2) THEN
               WRITE(*,236) it,icyc,irep
               WRITE(1,236) it,icyc,irep
            ENDIF
            
c---------- Corresponding parameters:
            ybl   = yblv(icyc)
            etac  = etacv(icyc)
            etat  = etatv(icyc)
            uu    = uuv(icyc)
            us    = usv(icyc)
            rbot  = rbotv(icyc)
            mm    = mmv(icyc)
            pp    = ppv(icyc)
            qq    = qqv(icyc)
            vv    = vvv(icyc)
            ww    = wwv(icyc)
            etas  = etasv(icyc)
            
            etat0 = etat/etatt
            etadf = etatt/etac
            
            etas0 = etas/etass
            
            qqm1  = qq-1.
            nnm1  = nn-1.
            vvpi  = 2./SQRT(pi)*vv
            wwpi  = 2./SQRT(pi)*ww
            xib=1./rbot-1.
            c1=(2.*mm+1.)*(mm+pp)/((mm+1.)*pp)*xib**(-mm) 
            c2=(2.*mm+pp+1.)*mm/((mm+1.)*pp)*xib**(-mm-pp) 
            cc0=1./(mm+1.)
            cc1=c1/(2.*mm+1.)
            cc2=c2/(2.*mm+pp+1)
            
c           Calcul du maximum de la fonction d'écoulement méridien
cvv         IF (vv.LE.1. .AND. ww.LE.1.) THEN                           !cvv
c           Profile with sin^qq*cos^nn (analytical position of maximum)
cvv            uu0 = uu/uuu*(qq/nn)**(-0.5*qq)*(qq/nn+1.)**(0.5*(qq+nn))!cvv
cvv            thtmax = 90.-ATAN(SQRT(qq/nn))/pi*180.                   !cvv
cvv         ELSE                                                        !cvv
c           Profile with erf(sin/vv)^qq*erf(cos/ww)^nn
               uumax = 0.
               DO i=1,10001
                  tht=(pi-0.)*DBLE(i-1)/10000+0.
                  cos1 = COS(tht)
                  sin1 = SIN(tht)
cvv               IF (vv.LE.1.) THEN                                    !cvv
cvv                  fctsin = sin1                                      !cvv
cvv               ELSE                                                  !cvv
                      fctsin = ERF(sin1*vv)
cvv               ENDIF                                                 !cvv
cvv               IF (ww.LE.1.) THEN                                    !cvv
cvv                  fctcos = cos1                                      !cvv
cvv               ELSE                                                  !cvv
                     fctcos = ERF(cos1*ww)
cvv               ENDIF                                                 !cvv
                  fct = fctsin**qq * fctcos**nn
                  IF(fct.GT.uumax) THEN
                     uumax = fct
                     thtmax = 90.-tht/pi*180.
                  ENDIF
               ENDDO
               uu0 = uu/uuu/uumax
               us0 = us/uuu/uumax
cvv         ENDIF                                                       !cvv
            IF(ivrb.GE.2) THEN
               WRITE(*,237) thtmax
               WRITE(1,237) thtmax
               CALL SLEEP(1)
            ENDIF
            
            IF(icyc.EQ.ncyc) THEN
               icyc=icyc-ncyc
               irep=irep+1
            ENDIF
         ENDIF
         
cALem sousroutine pour les émergences
         
c -----> Surface flux evolution: read vector potentiel A at the surface as an evolving BC
         IF(isrc.EQ.3) THEN
c---------> REinitialize random number generator
            IF (it.EQ.itbmr+1) THEN                                     !(Francois)
               IF (bldseedd(1).NE.0) CALL urand1_init(bldseedd(1))      !(Francois)
               IF (bldseedd(2).NE.0) CALL urand2_init(bldseedd(2))      !(Francois)
               IF (bldseedd(3).NE.0) CALL urand3_init(bldseedd(3))      !(Francois)
               IF (bldseedd(4).NE.0) CALL urand4_init(bldseedd(4))      !(Francois)
            ENDIF
            CALL surfbr2_solve(
     +                  it,itbmr,bldseed,bldseedd,
     +                  its,uu0,us0,qq,nn,vv,ww,etas0)!franky added
            CALL surfabc
         ENDIF
         
c------> Assemble system matrices
         CALL formm
         CALL formf(it-1)                                               !cALem: répétitif ! Déjà dans calfun...
         
c------> Apply boundary conditions (after surfabc)
         CALL aplybc(it)
         
c------> Solver (includes postpro and writout)
         CALL calfun(it)
c------> Compute magnetic energy and such
c        CALL postpro(it)
c------> Output nodal time series
c        CALL writout(it)
c------> Additive noise
c        CALL addnoise
      ENDDO
c**** Main time-stepping loop ends *****
      
c---> Finalize surface simulation
      CALL surfbr2_end
      
c---> Produce restart file from final time-step
      IF(ivrb.GE.1) THEN
         IF(incw.eq.1) CALL stlast
      ENDIF
      
c---> Calculate fitness for optimisation with PIKAIA
      CALL fitness(bldynfit,bmrfile,ssnfile,magfile)
      
      IF(ivrb.GE.2.AND.ISNAN(bldynfit)) THEN
         WRITE(*,'(a17,e10.2,a2,100f8.5)') '!!! Fitness =',
     +                                                bldynfit,' (',xfit
      ENDIF
      
      IF(ivrb.GE.2) THEN
         WRITE(*,55) bldynfit,(xfit(i),i=1,nfit)
         WRITE(1,55) bldynfit,(xfit(i),i=1,nfit)
      ENDIF
   55 FORMAT('Fitness = ',f9.6,'      xfit = [',100f8.5)
      
c---> Final file write status report
      CALL CPU_TIME(cputo)
      
      IF(ivrb.GE.2) THEN
         WRITE(*,228) cputo-cputi
         WRITE(1,228) cputo-cputi
c     ELSE
c        WRITE(*,229) cputo-cputi
      ENDIF
  228 FORMAT(/,72('*'),/,'Total CPU time:',f8.2,' s',/,72('*'),/)
c 229 FORMAT(            'Total CPU time:',f8.2,' s')
      
      CLOSE(1)
      CLOSE(3)
      CLOSE(7)
      CLOSE(8)
      CLOSE(9)
      
c     STOP ' **END**'
c-----------------------------------------------------------------------
  236 FORMAT(/,'CHANGE CYCLE: it =',i5,', icyc=',i5,', irep=',i5)
  237 FORMAT('MERIDIONAL CIRCULATION PROFILE:',
     +       ' Peak latitude =',f6.2,' deg')
      
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE header(
     +          logfile,outfile,inrfile,inwfile,ssnfile,bmrfile,magfile,
     +    ncyc,yblv,etacv,etatv,uuv,usv,rbotv,mmv,ppv,qqv,vvv,wwv,etasv)
c***********************************************************************
c  Outputs to screen some basic header info, including run parameters
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.global.f'
      INCLUDE 'bldyn_includefiles/arrays.mesh.f'
c Input
      CHARACTER*250  logfile,outfile,inrfile,inwfile,
     +               ssnfile,bmrfile,magfile
      CHARACTER      labdate*8,labclock*10
      INTEGER        ncyc
      REAL           yblv(ncyc),etacv(ncyc),etatv(ncyc),rbotv(ncyc),
     +               uuv(ncyc),usv(ncyc),mmv(ncyc),ppv(ncyc),qqv(ncyc),
     +               vvv(ncyc),wwv(ncyc),etasv(ncyc)
c Local
      INTEGER        icyc,strlen
c-----------------------------------------------------------------------
cALem CALL date(labdate)
cALem CALL time(labclock)
      CALL DATE_AND_TIME(DATE=labdate,TIME=labclock)
      
      WRITE(*,1) title,labdate,labclock
      WRITE(1,1) title,labdate,labclock
      
      IF(incr.NE.1) THEN
         WRITE(*,2)
         WRITE(1,2)
      ELSE
         WRITE(*,3) inrfile
         WRITE(1,3) inrfile
      ENDIF
      WRITE(*,5) outfile
      WRITE(1,5) outfile
      WRITE(*,6) logfile
      WRITE(1,6) logfile
      IF(incw.EQ.1) THEN
         WRITE(*,4) inwfile
         WRITE(1,4) inwfile
      ENDIF
      WRITE(*,10) ssnfile
      WRITE(1,10) ssnfile
      WRITE(*,11) bmrfile
      WRITE(1,11) bmrfile
      WRITE(*,12) magfile
      WRITE(1,12) magfile
      
      WRITE(*,8) nelx,nely,nely1,nely2,nelybuf,ityp,ngp,
     +           nt,nstep,tomti,dur,theta
      WRITE(1,8) nelx,nely,nely1,nely2,nelybuf,ityp,ngp,
     +           nt,nstep,tomti,dur,theta
      
c     WRITE(*,7) calpha,comega,reynolds,etadf,ipar,rzc,ybl
c     WRITE(1,7) calpha,comega,reynolds,etadf,ipar,rzc,ybl
      WRITE(*,7) ( yblv(icyc),icyc=1,ncyc),
     +           (etacv(icyc),icyc=1,ncyc),
     +           (etatv(icyc),icyc=1,ncyc),
     +           (rbotv(icyc),icyc=1,ncyc),
     +           (  uuv(icyc),icyc=1,ncyc),
     +           (  usv(icyc),icyc=1,ncyc),
     +           (  ppv(icyc),icyc=1,ncyc),
     +           (  mmv(icyc),icyc=1,ncyc),
     +           (  qqv(icyc),icyc=1,ncyc),
     +           (  vvv(icyc),icyc=1,ncyc),
     +           (  wwv(icyc),icyc=1,ncyc),
     +           (etasv(icyc),icyc=1,ncyc)
      WRITE(1,7) ( yblv(icyc),icyc=1,ncyc),
     +           (etacv(icyc),icyc=1,ncyc),
     +           (etatv(icyc),icyc=1,ncyc),
     +           (rbotv(icyc),icyc=1,ncyc),
     +           (  uuv(icyc),icyc=1,ncyc),
     +           (  usv(icyc),icyc=1,ncyc),
     +           (  ppv(icyc),icyc=1,ncyc),
     +           (  mmv(icyc),icyc=1,ncyc),
     +           (  qqv(icyc),icyc=1,ncyc),
     +           (  vvv(icyc),icyc=1,ncyc),
     +           (  wwv(icyc),icyc=1,ncyc),
     +           (etasv(icyc),icyc=1,ncyc)
      
      WRITE(*,9)
      WRITE(1,9)
      
c     WRITE(*,*) '(Press enter to continue)'
c     READ (*,*)
      CALL SLEEP(2)
      
      RETURN
c-----------------------------------------------------------------------
    1 format(72('*'),/,
     +       '*',10x,'NON-LINEAR KINEMATIC BABCOCK-LEIGHTON DYNAMOS',/,
     +       72('*'),/,
     +       'Run : ',a200,/,
     +       'Date: ',a8,1x,a10)
    2 format('Starting from t=0 initial condition')
    3 format('Extending earlier run. Restart file: ',/,a220)
    5 format('Output  file: ',/,a220)
    6 FORMAT('Log file:     ',/,a220)
    4 format('Restart file: ',/,a220)
   10 FORMAT('SSN file:     ',/,a220)
   11 FORMAT('BMR file:     ',/,a220)
   12 FORMAT('Magbfly file: ',/,a220)
    8 format(/,'Mesh parameters:',/,
     +         5x,'nelx       = ',i4,/,
     +         5x,'nely       = ',i4,
     +                        ' (core:',i4,', cz:',i4,', r>R:',i4,')',/,
     +         5x,'elem. type = ',i4,/,
     +         5x,'N_GP       = ',i4,/,
     +         5x,'nt         = ',i4,/,
     +         5x,'nstep      = ',i4,/,
     +         5x,'duration   = ',e12.4,' (',f9.2,' days)',/,
     +         5x,'theta      = ',e12.4)
    7 format(/,'Input parameters:',/,
     +         5x,'ybl        = ',1f9.2,/,
     +         5x,'etac       = ',1e9.2,/,
     +         5x,'etat       = ',1e9.2,/,
     +         5x,'rbot       = ',1f9.2,/,
     +         5x,'uu_north   = ',1e9.2,/,
     +         5x,'uu_south   = ',1e9.2,/,
     +         5x,'pp         = ',1f9.2,/,
     +         5x,'mm         = ',1f9.2,/,
     +         5x,'qq         = ',1f9.2,/,
     +         5x,'vv         = ',1f9.2,/,
     +         5x,'ww         = ',1f9.2,/,
     +         5x,'etas       = ',1e9.2)
c    +         5x,'C_alpha      = ',e12.4,/,
c    +         5x,'C_omega      = ',e12.4,/,
c    +         5x,'Reynolds     = ',e12.4,/,
c    +         5x,'Delta eta    = ',e12.4,/,
c    +         5x,'Parity       = ',i3,/,
c    +         5x,'R_zc/R       = ',e12.4,/,
c    +         5x,'w/R          = ',e12.4)
    9 FORMAT(72('*'),/)
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE mesh2d
c***********************************************************************
c  Generation du maillage spatial 2-D, maillage temporel, et calcul des    
c                          conditions limites                               
c
c  Called by: program bldynfit
c  Calls    : subroutines grid1, gaussp, initcnd
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femlocal.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
c Local
      REAL     xxi,xxo,yi,yo,fct
      INTEGER  i,nndy1,ii,i1,i2,isurf,itht
c-----------------------------------------------------------------------

c---------- set up radial mesh -----------------------------------------

c---------- radial direction (variable y, --> r/R ):
c
c     nely/3  elements from base to C-E interface, concentrated towards latter;
c     nely/10-1 elements in outer buffer shell, concentrated towards r/R=1;
c     remainder of elements in convective envelope, equally spaced
c     nely1=nely/3               ! = 32 for nely=96
c     nelybuf=nely/10-1          ! = 8 for nely=96
c     nely2=nely-nely1-nelybuf   ! = 56 for nely=96
      
c --> in radiative core
      yi=rb
      yo=rzc
      nndy1=nely1+1              ! = 33 for nely=96
      DO i=1,nndy1
         yrad(i)=(yo-yi)*((float(i-1)/nely1)**0.75)+yi
c        WRITE(*,*) i,yrad(i)
c        WRITE(1,*) i,yrad(i)
      ENDDO
c     WRITE(*,'()')
c     WRITE(1,'()')
      ielnoint=nelx*(nely1-1)  !cALem ???
      
c --> in convective envelope
      yi=rzc
      yo=1.
      DO i=1,nely2               ! = 56 for nely=96
         yrad(nndy1+i)=(yo-yi)*((float(i)/(nely2))**1.)+yi
c        WRITE(*,*) nndy1+i,yrad(nndy1+i)
c        WRITE(1,*) nndy1+i,yrad(nndy1+i)
      ENDDO 
c     WRITE(*,'()')
c     WRITE(1,'()')
      
c --> from surface to top of buffer zone
      yi=yo
      yo=3.0
      DO i=1,nelybuf             ! = 8 for nely=96
         yrad(nndy1+nely2+i)=(yo-yi)*((float(i)/(nelybuf))**1.75)+yi
c        WRITE(*,*) nndy1+nndy2+i,yrad(nndy1+nndy2+i)
c        WRITE(1,*) nndy1+nndy2+i,yrad(nndy1+nndy2+i)
      ENDDO
      
c---------- angular direction (variable x, --> mu=cos(theta) ):

c           Mesh is in CONSTANT theta-increments
      IF(ipar.ne.0)THEN
c --> solving only in N-hemisphere
         xxi=pi/2.
      ELSE
c --> solving in full meridional plane
         xxi=pi
      ENDIF
      xxo=0.
      DO i=1,nndx
         xtht(i)=(xxo-xxi)*(((float(i-1)/nelx)))+xxi
         xtht(i)=COS(xtht(i))
c        xxang=xx(i)
c        WRITE(*,*) i,xtht(i)
c        WRITE(1,*) i,xtht(i)
      ENDDO
c     xtht(1)=0.
c     STOP ' *TMP*'
      
c---------- generate grid and connectivity matrix
      CALL grid1
      
c---------- compute location of gauss points + some PDE coefficients
      CALL gaussp
      
c---> Initial condition
      CALL initcnd
      
c---------- temporal mesh
      DO i=1,nt+1
         time(i)=tomti*(((float(i-1)/nt))**1.0)+timein
      ENDDO
      dt=tomti/float(nnt)
      
      IF(ivrb.GE.2) THEN
         WRITE(*,15) timein,timeout
         WRITE(1,15) timein,timeout
      ENDIF
   15 format(/,'Time in : ',e12.4,/,'Time out: ',e12.4,/)

c---------- conditions limites -----------------------------------------
c
c           1. condition de dirichlet ("essential boundary conditions")
      numebc=0

c --> conditions at r=r_b: B_phi=0, A=0
      ii=numebc
      DO i=1,nndx
         ii=ii+2
         i1=2*icon(i,1)-1
         i2=2*icon(i,1)
         iuebc(ii-1)=i1
         iuebc(ii)  =i2
c        uebc (ii-1)=(100000./rzc**2)*sin(acos(xx(i)))
         uebc (ii-1)=0.
         uebc (ii)  =0.
      ENDDO
      IF(ivrb.GE.2) THEN
       WRITE(*,*) 'numebc =',ii,' **** conditions along bottom boundary'
       WRITE(1,*) 'numebc =',ii,' **** conditions along bottom boundary'
      ENDIF

c --> conditions along top boundary: A=0
      DO i=nnd,nnd-nelx,-1
         ii=ii+1
c        i1=2*icon(i,1)-1
         i1=2*i-1
         iuebc(ii)=i1
         uebc (ii)=0.
      ENDDO
      IF(ivrb.GE.2) THEN
         WRITE(*,*) 'numebc =',ii,' **** conditions along top boundary'
         WRITE(1,*) 'numebc =',ii,' **** conditions along top boundary'
      ENDIF

c --> conditions within buffer zone: B_phi=0
      DO i=nnd,nnd-((nelybuf+1)*nndx)+1,-1
         ii=ii+1
         i2=2*i
         iuebc(ii)=i2
         uebc (ii)=0.
c        WRITE(*,*) i,x(i),y(i)
c        WRITE(1,*) i,x(i),y(i)
      ENDDO
      IF(ivrb.GE.2) THEN
         WRITE(*,*) 'numebc =',ii,' **** conditions within buffer zone'
         WRITE(1,*) 'numebc =',ii,' **** conditions within buffer zone'
      ENDIF

c --> conditions along N-pole boundary: B_phi=0, A=0
      DO i=nndx,nnd,nndx
         ii=ii+2
         i1=2*i-1
         i2=2*i
         iuebc(ii-1)=i1
         iuebc(ii)  =i2
         uebc (ii-1)=0.
         uebc (ii)  =0.
      ENDDO
      IF(ivrb.GE.2) THEN
         WRITE(*,*) 'numebc =',ii,' **** conditions along N-pole'
         WRITE(1,*) 'numebc =',ii,' **** conditions along N-pole'
      ENDIF

c --> conditions along equator/S-pole boundary: set by parity
      DO i=1,nnd,nndx
c following for full meridional plane model: A=0, B=0 along S-polar axis
         IF(ipar.eq.0)THEN
            ii=ii+2
            i1=2*i-1
            i2=2*i
            iuebc(ii-1)=i1
            iuebc(ii)  =i2
            uebc (ii-1)=0.
            uebc (ii)  =0.
         ELSE
c following for dipole-like, single quadrant: B=0, dA/dmu=0  on equator
            IF(ipar.eq.1)THEN
               ii=ii+1
               i1=2*i
               iuebc(ii)=i1
               uebc (ii)=0.
            ELSE
c following for quadrupole-like, single quadrant: A=0, dB/dmu=0 on equator
               ii=ii+1
               i1=2*i-1
               iuebc(ii)=i1
               uebc (ii)=0.
            ENDIF
         ENDIF
      ENDDO
      IF(ivrb.GE.2) THEN
        WRITE(*,*) 'numebc =',ii,' **** conditions along right boundary'
        WRITE(1,*) 'numebc =',ii,' **** conditions along right boundary'
      ENDIF
      numebc=ii
      
c --> Conditions at surface (r/R=1.0)
c     (uebc(iia(itht)) to be defined insurfa subroutine
      isurf=nnd-((nelybuf+1)*nndx)
      DO itht=1,nndx
         iia(itht)=numebc+itht
         i=isurf+itht
         i1=2*i-1
         iuebc(iia(itht))=i1
         uebc(iia(itht)) =0.
      ENDDO
      numebc=iia(nndx)
      IF(ivrb.GE.2) THEN
         WRITE(*,*) 'numebc =',numebc,' **** Variable BC at r/R=1.'
         WRITE(1,*) 'numebc =',numebc,' **** Variable BC at r/R=1.'
      ENDIF
      
c -- safety check on boundary conditions
      IF(numebc.gt.nbc)THEN
       WRITE(*,19) numebc,nbc
       WRITE(1,19) numebc,nbc
       STOP ' *MESH*'
      ENDIF
   19 format(/,'Number of boundary conditions    : ',i6,/
     +         'Size of boundary condition arrays: ',i6)
      RETURN

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE grid1
c***********************************************************************
c  genere une grid d'elements quadrilateraux bilineaires; calcule
c  coordonnees des noeuds, et matrtice de connectivite
c
c  Called by: SUBROUTINE mesh2d                                  
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
c Local
      INTEGER i,j,iel,i2

c     WRITE(*,*) ' grid: entering' 
c     WRITE(1,*) ' grid: entering' 
c---------- 1. calcul du "fond'
 
      DO i=1,nndx
         y(i)=yrad(1)
         x(i)=xtht(i)
c        WRITE(*,*) i,x(i),y(i)
c        WRITE(1,*) i,x(i),y(i)
      ENDDO
c---------- 2. nely couches d'elements
c              (nelx elements quadrilateraux bilineaires par couche)
 
      DO j=1,nely
         i2=1
         DO i=(j-1)*nndx+nndx+1,j*nndx+nndx
            y(i)=yrad(j+1)
            x(i)=xtht(i2)
            i2=i2+1
c           WRITE(*,*) i,x(i),y(i)
c           WRITE(1,*) i,x(i),y(i)
          ENDDO
      ENDDO
      DO i=1,nely
         DO j=1,nelx
            iel=(i-1)*nelx+j
            icon(iel,1)=(i-1)*nndx+j
            icon(iel,2)=(i-1)*nndx+j+1
            icon(iel,3)=i*nndx+j+1
            icon(iel,4)=i*nndx+j
c           ntype(iel)=ityp
c           ngpint(iel)=ngp
         ENDDO
      ENDDO

c     IF(ivrb.GE.2) THEN
c        WRITE(*,1) nelx,nely,nel,nnd,nm
c        WRITE(1,1) nelx,nely,nel,nnd,nm
c     ENDIF
    1 format(/,'***** GRID:',5x,i3,' X ',i3,' Bilinear Elements',/,
     +         7x,'Total Elements:     ',i6,/,
     +         7x,'Total Nodes:        ',i6,/,
     +         7x,'Total Coefficients: ',i6)

      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE gaussp
c***********************************************************************
c  calcule la position des points de gauss, ainsi que les coefficients de    
c  l'equation differentielle en ces points
c
c  Called by: SUBROUTINE mesh2d                                  
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femlocal.f'
c Local
      REAL     sqr13,sqr35,xl,yl,xmid,ymid
      INTEGER  i

      sqr13=sqrt(1./3.)
      sqr35=sqrt(3./5.)
      DO i=1,nel
         ymid=(y(icon(i,4))+y(icon(i,1)))/2.
         xmid=(x(icon(i,2))+x(icon(i,1)))/2.
         xl=(x(icon(i,2))-x(icon(i,1)))/2.
         yl=(y(icon(i,4))-y(icon(i,1)))/2.
c  1 point de gauss
         IF(ngp.eq.1)THEN
            xgp(i,1)=xmid
            ygp(i,1)=ymid
         ENDIF
c  2x2 points de gauss
         IF(ngp.eq.2)THEN
            xgp(i,1)=xmid-xl*sqr13
            xgp(i,3)=xmid+xl*sqr13
            xgp(i,2)=xmid-xl*sqr13
            xgp(i,4)=xmid+xl*sqr13
            ygp(i,1)=ymid-yl*sqr13
            ygp(i,3)=ymid-yl*sqr13
            ygp(i,2)=ymid+yl*sqr13
            ygp(i,4)=ymid+yl*sqr13
         ENDIF
      ENDDO

      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE sfcheck
c***********************************************************************
c     Safety checks on the flow fields and equation coefficients
c     Produces a lot of tabular output, basically profiles as a 
c     function of depth and latitude
c
c     Called by : program bldynfit
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
c Local
      INTEGER  i,ir1,ir2,ir3,ir4,it1,it2,it3
      REAL     mu,rr,omg,domgdr,domgdm,remag,drmfac,ur,uth,durdr,
     +         duthdth,alp0,sin1,r1sin1,qdiv,dlnrhodr
c-----------------------------------------------------------------------

c first search for nodal positions
      DO i=1,nely
         IF(yrad(i).le. (0.65) .and. yrad(i+1).gt. (0.65)) ir1=i
         IF(yrad(i).le. (0.70) .and. yrad(i+1).gt. (0.70)) ir2=i
         IF(yrad(i).le. (0.85) .and. yrad(i+1).gt. (0.85)) ir3=i
         IF(yrad(i).le. (1.00) .and. yrad(i+1).gt. (1.00)) ir4=i
      ENDDO
      it1=1
      it2=nelx/2+1
      it3=nelx+1

c -- radial profiles at equator
      IF(ivrb.GE.2) THEN
         WRITE(*,1) it1,acos(xtht(it1)),xtht(it1)
         WRITE(*,10)
         WRITE(1,1) it1,acos(xtht(it1)),xtht(it1)
         WRITE(1,10)
      ENDIF
      mu=xtht(it1)
      DO i=1,ir4+2
         rr=yrad(i)
         CALL angvel(mu,rr,omg,domgdr,domgdm)
         CALL magdif(mu,rr,remag,drmfac)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         CALL srcpol(mu,rr,alp0)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
c        alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,yrad(i),alp0,remag,drmfac,omg,domgdr,domgdm
            WRITE(1,11) i,yrad(i),alp0,remag,drmfac,omg,domgdr,domgdm
         ENDIF
      ENDDO

c -- radial profiles at mid-lat
      IF(ivrb.GE.2) THEN
         WRITE(*,2) it2,acos(xtht(it2)),xtht(it2)
         WRITE(*,10)
         WRITE(1,2) it2,acos(xtht(it2)),xtht(it2)
         WRITE(1,10)
      ENDIF
      mu=xtht(it2)
      DO i=1,ir4+2
         rr=yrad(i)
         CALL angvel(mu,rr,omg,domgdr,domgdm)
         CALL magdif(mu,rr,remag,drmfac)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         CALL srcpol(mu,rr,alp0)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
c alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,yrad(i),alp0,remag,drmfac,omg,domgdr,domgdm
            WRITE(1,11) i,yrad(i),alp0,remag,drmfac,omg,domgdr,domgdm
         ENDIF
      ENDDO

c -- radial profiles at N-pole
      IF(ivrb.GE.2) THEN
         WRITE(*,3) it3,acos(xtht(it3)),xtht(it3)
         WRITE(*,10)
         WRITE(1,3) it3,acos(xtht(it3)),xtht(it3)
         WRITE(1,10)
      ENDIF
      mu=xtht(it3)-0.0001
      DO i=1,ir4+2
         rr=yrad(i)
         CALL angvel(mu,rr,omg,domgdr,domgdm)
         CALL magdif(mu,rr,remag,drmfac)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         CALL srcpol(mu,rr,alp0)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
         alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,yrad(i),alp0,remag,drmfac,omg,domgdr,domgdm
            WRITE(1,11) i,yrad(i),alp0,remag,drmfac,omg,domgdr,domgdm
         ENDIF
      ENDDO

c -- latitudinal profiles below shear layer (r=0.65)
      IF(ivrb.GE.2) THEN
         WRITE(*,4) ir1,yrad(ir1)
         WRITE(*,12)
         WRITE(1,4) ir1,yrad(ir1)
         WRITE(1,12)
      ENDIF
      rr=yrad(ir1)
      DO i=1,nelx+1
         mu=xtht(i)
         CALL angvel(mu,rr,omg,domgdr,domgdm)
         CALL magdif(mu,rr,remag,drmfac)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         CALL srcpol(mu,rr,alp0)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
         alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
            WRITE(1,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
         ENDIF
      ENDDO

c -- latitudinal profiles at shear layer (r=0.7)
      IF(ivrb.GE.2) THEN
         WRITE(*,5) ir2,yrad(ir2)
         WRITE(*,12)
         WRITE(1,5) ir2,yrad(ir2)
         WRITE(1,12)
      ENDIF
      rr=yrad(ir2)
      DO i=1,nelx+1
         mu=xtht(i)
         CALL angvel(mu,rr,omg,domgdr,domgdm)
         CALL magdif(mu,rr,remag,drmfac)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         CALL srcpol(mu,rr,alp0)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
         alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
            WRITE(1,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
         ENDIF
      ENDDO

c -- latitudinal profiles at mid-envelope (r=0.85)
      IF(ivrb.GE.2) THEN
         WRITE(*,6) ir3,yrad(ir3)
         WRITE(*,12)
         WRITE(1,6) ir3,yrad(ir3)
         WRITE(1,12)
      ENDIF
      rr=yrad(ir3)
      DO i=1,nelx+1
         mu=xtht(i)
         CALL angvel(mu,rr,omg,domgdr,domgdm)
         CALL magdif(mu,rr,remag,drmfac)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         CALL srcpol(mu,rr,alp0)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
         alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
            WRITE(1,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
         ENDIF
      ENDDO

c -- latitudinal profiles at surface (r=0.1)
      IF(ivrb.GE.2) THEN
         WRITE(*,7) ir4,yrad(ir4)
         WRITE(*,12)
         WRITE(1,7) ir4,yrad(ir4)
         WRITE(1,12)
      ENDIF
      rr=yrad(ir4)
      DO i=1,nelx+1
         mu=xtht(i)
         CALL angvel(mu,rr,omg,domgdr,domgdm)
         CALL magdif(mu,rr,remag,drmfac)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         CALL srcpol(mu,rr,alp0)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
c        alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
            WRITE(1,11) i,xtht(i),alp0,remag,drmfac,omg,domgdr,domgdm
         ENDIF
      ENDDO

c -- radial profiles at equator
      IF(ivrb.GE.2) THEN
         WRITE(*,1) it2,acos(xtht(it2)),xtht(it2)
         WRITE(*,13)
         WRITE(1,1) it2,acos(xtht(it2)),xtht(it2)
         WRITE(1,13)
      ENDIF
      mu=xtht(it2)
      DO i=1,ir4+2
         rr=yrad(i)
         CALL circmd(mu,rr,ur,uth,durdr,duthdth)
         dlnrhodr=(-0.5/rr**2)/(1./rr-1.)
         sin1    =sin(acos(mu))
         r1sin1  =rr*sin1
         qdiv=r1sin1*dlnrhodr*ur+r1sin1*durdr+2.*sin1*ur
     +       +sin1*duthdth+uth*mu
         alp0=qdiv
         IF(ivrb.GE.2) THEN
            WRITE(*,11) i,yrad(i),ur,uth,durdr,duthdth,dlnrhodr,qdiv
            WRITE(1,11) i,yrad(i),ur,uth,durdr,duthdth,dlnrhodr,qdiv
         ENDIF
      ENDDO

      STOP ' *TMP*'
  
      RETURN
c-----------------------------------------------------------------------
    1 format(/,'Radial profiles at equator, x-node= ',i4,
     +         ' theta= ',f7.4,2x,'cos(theta)= ',f7.4)
    2 format(/,'Radial profiles at mid-lat, x-node= ',i4,
     +         ' theta= ',f7.4,2x,'cos(theta)= ',f7.4)
    3 format(/,'Radial profiles at N-pole, x-node= ',i4,
     +         ' theta= ',f7.4,2x,'cos(theta)= ',f7.4)
    4 format(/,'Latitudinal profiles below shear layer, y-node= ',i4,
     +         ' r/R= ',f7.4)
    5 format(/,'Latitudinal profiles at shear layer, y-node= ',i4,
     +         ' r/R= ',f7.4)
    6 format(/,'Latitudinal profiles mid-envelope, y-node= ',i4,
     +         ' r/R= ',f7.4)
    7 format(/,'Latitudinal profiles at surface, y-node= ',i4,
     +         ' r/R= ',f7.4)
   10 format(/,7x,
     +       'r/R',7x,'S(r)',6x,'eta(r)',4x,'deta/dr',3x,'Omega',5x,
     +       'dOmg/dr',3x,'dOmg/dmu')
   13 format(/,7x,
     +       'r/R',7x,'u_r',7x,'u_tht',5x,'dur/dr',4x,'duthdth',3x,
     +       'dlnrho/dr',2x,'div rho u')
   12 format(/,7x,
     +       'mu',8x,'S(th)',5x,'eta(th)',3x,'deta/dr',3x,'Omega',5x,
     +       'dOmg/dr',3x,'dOmg/dmu')
   11 format(i5,7e10.2)
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE initcnd
c***********************************************************************
c     Calcule ou lit une condition initiale pour les composantes
c     poloidale et toroidale du champ magnetique
c
c     Initialisation de la machinerie FEM
c
c     Called by : subroutine mesh2d
c     Calls :     subroutine initdt
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
c Local
      REAL     fr,q1,q2,tht,sintht,exp1
      INTEGER  i,i1,i2
      INTEGER  nm0,nel0,nelx0,nely0,nt0,nstep0
      REAL     comega0,calpha0,reynolds0,etadf0,ybl0,rzc0,to0
      CHARACTER*80 title0
c-----------------------------------------------------------------------

c --> Initialisations de la machinerie FEM
      CALL initdt

c --> Now initial condition on magnetic field:

c --- Starting from scratch:
      IF(incr.NE.1) THEN
         timein=0.
         exp1=8+1.                                                      !expi+1.
         DO i=1,nnd
            i1=2*i-1
            i2=2*i
            tht=acos(x(i))
            sintht=SIN(tht)
c           IF(ivrb.GE.2) THEN
c              WRITE(*,*) i,x(i),tht
c              WRITE(1,*) i,x(i),tht
c           ENDIF
            
c --------- Both poloidal and toroidal fields set to 0.
            IF(icnd.EQ.0) THEN
                a(i1)=0.
                a(i2)=0.
c --------- Initial toroidal field only
            ELSEIF(icnd.EQ.1) THEN
               a(i1)=0.
               fr=exp(-400.*(y(i)-rzc-ybl)**2)
               q1=ampi*fr *sin(2.*tht)
               q2=q1*x(i)
c following for full meridional plane
               IF(ipar.eq.0)THEN
                  a(i2)=q1+q2
c following for dipolar
               ELSEIF(ipar.eq.1)THEN
                  a(i2)=q1
c following for quadrupolar
               ELSE
                  a(i2)=q2
               ENDIF
               
c --------- Initial poloidal field only (polar cap)
            ELSEIF(icnd.EQ.2) THEN
               a(i2)=0.
c              fr=exp(-200.*(y(i)-1.)**2)
               fr=1./y(i)*EXP(-200.*(y(i)-1.)**2)                       ! *1/r to enforce Btheta=0 at the surface
c              q1=ampi*(x(i)**7)*ABS(x(i))                              ! Distribution of Br at the surface (Wang & Sheeley)
c              q1=ampi*(x(i)**9)                                        ! Distribution of Br at the surface (sans ABS)
c              q1=ampi*(3.*x(i)**5  - 2.*x(i)**3)                       ! Distribution of Br at the surface (pour un A analytique -> 0 aux pôles, le plus près de Wang & Sheeley)
               IF(sintht.LT.1.e-6) sintht=1.e-6
c              a(i1) = -ampi*ERF(costht*DABS(costht)**(expi-1.)/lerfi...! CAREFUL: The integral of erf(cos(tht)^n/l) is quite complicated...
               a(i1) = -ampi/exp1*(ABS(x(i))**exp1-1.)/sintht*fr        ! Corresponds to surface Br = ampi*(1./r**2)*EXP(-200.*(r-1.)**2)*DCOS(tht)*DABS(DCOS(tht))**(expi-1.)
c              a(i1) = -ampi/9.  *(ABS(x(i))**9   -1.)/sintht*fr        ! Corresponds to surface Br = ampi*(1./r**2)*EXP(-200.*(r-1.)**2)*DCOS(tht)*DABS(DCOS(tht))**7
c              a(i1) =  ampi/2.*sintht*x(i)**4  * fr
            ENDIF
         ENDDO
         
c --- Starting from previous run
      ELSE
         read(7) title0
         read(7) nm0,nel0,nelx0,nely0,nt0,nstep0
         read(7) comega0,calpha0,reynolds0,etadf0,ybl0,rzc0,to0
         timein=to0
c -- some safety check
         IF( nelx0.ne.nelx .or. nely0.ne.nely )THEN
            WRITE(*,1) nelx0,nelx,nely0,nely
            WRITE(1,1) nelx0,nelx,nely0,nely
            STOP ' *INIT*'
         ENDIF
c        IF( comega0  .ne.comega   .or. calpha0.ne.calpha .or. 
c    +       reynolds0.ne.reynolds .or. etadf0 .ne.etadf  .or.
c    +       ybl0     .ne.ybl                                  )THEN
c           WRITE(*,2) comega0,comega,calpha0,calpha,reynolds0,
c    +                 reynolds,etadf,etadf0,ybl0,ybl
c           WRITE(1,2) comega0,comega,calpha0,calpha,reynolds0,
c    +                 reynolds,etadf,etadf0,ybl0,ybl
c           STOP ' *INIT*'
c        ENDIF
         read(7) (xtht(i),i=1,nelx0+1)
         read(7) (yrad(i),i=1,nely0+1)
         read(7) a
         
c -- facteur multiplicatif pour condition initiale
         DO i=1,nnd
            i2=2*i
            i1=i2-1
c           IF(y(i).le.0.7)THEN
               a(i1)=faci*a(i1)
               a(i2)=faci*a(i2)    !franky faci multiplies a(i2)
c           ENDIF
         ENDDO
      ENDIF
      timeout=timein+tomti
      
      RETURN
c-----------------------------------------------------------------------
    1 format(/,'Initial condition: incompatible Spatial Meshes:',
     +       /,5x,'nelx= ',i3,'/',i3,
     +       /,5x,'nely= ',i3,'/',i3)
    2 format(/,'Initial condition: incompatible Problem parameters:',
     +       /,5x,'comega  = ',e12.4,'/',e12.4,
     +       /,5x,'calpha  = ',e12.4,'/',e12.4,
     +       /,5x,'reynolds= ',e12.4,'/',e12.4,
     +       /,5x,'etadf   = ',e12.4,'/',e12.4,
     +       /,5x,'ybl     = ',e12.4,'/',e12.4)

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE initdt
c***********************************************************************
c  reads initial data, sets limits and integration points (Internal to FEM)
c
c  Called by : SUBROUTINE initcnd
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.femlocal.f'
c Local
      INTEGER n,l
c-------------------------------------
c     DO i=1,30
c        intdef(i)=0
c        ioptau(i)=0
c     ENDDO
c     nnd(23)=4
c     nnd(24)=8
c     intdef(23)=2
c     intdef(24)=3
c     ioptau(23)=1
c     ioptau(24)=4
 
c---------- gauss points and weights for gauss-legendre quadrature
      DO n=1,5
         DO l=1,5
            gp(n,l)=0.
            wt(n,l)=0.
         ENDDO
      ENDDO
c---------- for one gauss point
      gp(1,1)=0.
      wt(1,1)=2.
c---------- for two gauss points
      gp(2,1)=-1./sqrt(3.)
      gp(2,2)=-gp(2,1)
      wt(2,1)=1.
      wt(2,2)=1.
c---------- for three gauss points
      gp(3,1)=-sqrt(3./5.)
      gp(3,2)=0.
      gp(3,3)=-gp(3,1)
      wt(3,1)=5./9.
      wt(3,2)=8./9.
      wt(3,3)=wt(3,1)
c---------- for four gauss points
      gp(4,1)=-sqrt((15.+2.*sqrt(30.))/35.)
      gp(4,2)=-sqrt((15.-2.*sqrt(30.))/35.)
      gp(4,3)=-gp(4,2)
      gp(4,4)=-gp(4,1)
      wt(4,1)=49./(6.*(18.+sqrt(30.)))
      wt(4,2)=49./(6.*(18.-sqrt(30.)))
      wt(4,3)=wt(4,2)
      wt(4,4)=wt(4,1)

      RETURN

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE prepro(it,n)
c***********************************************************************
c
c     Called by: SUBROUTINE calfun
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.global.f'
      INCLUDE 'bldyn_includefiles/arrays.mesh.f'
      INCLUDE 'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'

c Input
      INTEGER        it,n
c Local
      INTEGER        i,ii
c-----------------------------------------------------------------------
      
c---- Select toroidal and poloidal fields to be stored
c     DO i=1,nndx
c        ii=(iybr-1)*nndx+i
c        asurft(itt,i)=a(2*ii-1)
c     ENDDO
      
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE postpro(it,n,ctime)
c***********************************************************************
c     Postprocessing:
c     - Computes magnetic energy and field strength in domain  
c       and portions thereof, at timestep it
c     - Stores toroidal (at rbphi) and radial surface (at rbr) magnetic
c       fields as a function of time
c
c     ebttot: Magnetic energy based on B_phi only, in whole domain
c     ebtot : Magnetic energy based on full B, in whole domain
c     b1max : Maximal toroidal field strength in r/R<rzc+ybl
c     b2max : Maximal toroidal field strength in r/R>rzc+ybl
c
c     Called by: program bldynfit
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.global.f'
      INCLUDE 'bldyn_includefiles/arrays.mesh.f'
      INCLUDE 'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'

c Input
      INTEGER  it,n
      REAL     ctime
c Local
      INTEGER  iflag,ielno,i,ii,j
      REAL     bphi,bphitemp,aquadtemp,
     +         delmu(nelx),sin1(nndx),del,absb,bpmax,aqmax
c-----------------------------------------------------------------------
c     WRITE(*,*) ' POST: entering '
c     WRITE(1,*) ' POST: entering '
      
c********** loop over all elements to compute magnetic energy
      iflag=2
      ebtot (itt) = 0.
      ebttot(itt) = 0.
      bavrt (itt) = 0.
      volt  (itt) = 0.
      DO ielno=1,nel
         CALL elem23f(ielno,iflag)
         ebtot (itt) = ebtot (itt) +ebt
         ebttot(itt) = ebttot(itt) +ebtt
         bavrt (itt) = bavrt (itt) +bavr
         volt  (itt) = volt  (itt) +vol
      ENDDO
c --> this is total magnetic energy (erg/rsun^3)
      ebtot (itt) = ebtot (itt)/4.                                     !Added 2.*pi/(8.*pi) factor for energy in ergs/R^3 (2015-02-18, A.Lemerle)
c --> this is toroidal magnetic energy (erg/rsun^3)
      ebttot(itt) = ebttot(itt)/4.                                      !Added 2.*pi/(8.*pi) factor for energy in ergs/R^3 (2015-02-18, A.Lemerle)
c --> this is average magnetic field strength (G)
      bavrt (itt) = bavrt (itt)/volt  (itt)                             !Added division by volume for magnetic field in G (2015-02-18, A.Lemerle)
c --> this is volume (cm^3/rsun^3)
      volt  (itt) = volt  (itt)*2.*pi                                   !2015-02-18 (A.Lemerle)
      
c********** now look for maximum toroidal field strength
      b1max=-1.e10
      b2max=-1.e10
      DO i=1,nnd
c --> in core and shear layer
         IF(y(i).ge.0..and.y(i).lt.rzc+ybl)THEN
            ii=2*i
            bphi=abs(a(ii))
            IF(bphi.gt.b1max) b1max=bphi
         ENDIF
c --> in envelope
         IF(y(i).gt.(rzc+ybl).and.y(i).le.1.0)THEN
            ii=2*i
            bphi=abs(a(ii))
            IF(bphi.gt.b2max) b2max=bphi
         ENDIF
      ENDDO

c---- Select toroidal and poloidal fields to be stored
      DO i=1,nndx
         bphitemp=0.
         aquadtemp=0.
         DO j=iybphi(1),iybphi(2)
            ii=(j-1)*nndx+i
            bphitemp = bphitemp+a(2*ii)
            aquadtemp=aquadtemp+a(2*ii-1)
         ENDDO
         bphit(itt,i) = bphitemp/(iybphi(2)-iybphi(1)+1)
         aquadt(itt,i)=aquadtemp/(iybphi(2)-iybphi(1)+1)
      ENDDO
      
c---- Function bfit(Bphi,Aphi) and latitudinal weighting -----
c     bpmax=-1.e10
c     aqmax=-1.e10
c     DO itt=itfit(1),itfit(2)
c        DO j=ixfits(1),ixfits(2)
c           absb = ABS( bphit(itt,j))
c           IF(absb.GT.bpmax) bpmax=absb
c           absb = ABS(aquadt(itt,j))
c           IF(absb.GT.aqmax) aqmax=absb
c        ENDDO
c        DO j=ixfitn(1),ixfitn(2)
c           absb = ABS( bphit(itt,j))
c           IF(absb.GT.bpmax) bpmax=absb
c           absb = ABS(aquadt(itt,j))
c           IF(absb.GT.aqmax) aqmax=absb
c        ENDDO
c     ENDDO
      bpmax=1000.
      aqmax=1.
      
c     bcrit = bcrit *bpmax     !Multiply by bpmax to use a relative bcrit
c     dbcrit= dbcrit*bpmax     !Multiply by bpmax to use a relative dbcrit
      
      DO j=1,nndx
         mutht(j)=xtht(j)
         IF (bphit(itt,j).EQ.0.) THEN
         bfit(itt,j)= 0.
         ELSE
         bfit(itt,j)=-bphit(itt,j)
     +                               *ABS(bphit (itt,j)/bpmax)**(bbb-1.)
     +                               *ABS(aquadt(itt,j)/aqmax)** aaa
         bfit(itt,j)=ABS(bfit(itt,j))
     +                               *ABS(  bfit(itt,j)/bpmax)**(ccc-1.)
     +                            *0.5*(ERF((bfit(itt,j)+bcrit)/dbcrit)
     +                                 +ERF((bfit(itt,j)-bcrit)/dbcrit))
         bfit(itt,j)= bfit(itt,j)
     +                            *0.5*(ERF((xtht(j)+xcrit)/dxcrit)
     +                                 -ERF((xtht(j)-xcrit)/dxcrit))
     +                               * ((1.-xcrit0)*ABS(xtht(j))+xcrit0)
c    +                                            * ABS(sin1(j))**expsin
         bfit(itt,j)= bfit(itt,j)/100.*bfitfac
         ENDIF
      ENDDO
      
c---- Select surface radial field to be stored
       DO i=1,nndx
         ii=(iybr-1)*nndx+i
         asurft(itt,i)=a(2*ii-1)
      ENDDO
      
c---- Compute B_r = -1/r*d(A*sin(theta))/dmu   ( mu=cos(theta) in increasing order => theta in decreasing order)
      DO i=1,nelx
         delmu(i)=xtht(i+1)-xtht(i)
      ENDDO
      DO i=1,nndx
         sin1(i) = SIN(ACOS(xtht(i)))
      ENDDO
      DO i=2,nelx
         del=asurft(itt,i+1)*sin1(i+1)-asurft(itt,i-1)*sin1(i-1)
         brsurf(itt,i)= -1.*del/(delmu(i-1)+delmu(i))
      ENDDO
c---- Patch for pole
      del=asurft(itt,2)*sin1(2)-asurft(itt,1)*sin1(1)
      brsurf(itt,1) = -1.*del/delmu(1)
c---- Patch for equator / other pole
      del=asurft(itt,nndx)*sin1(nndx)-asurft(itt,nelx)*sin1(nelx)
      brsurf(itt,nndx) = -1.*del/delmu(nelx)
      
c---- Some output
      IF(ivrb.GE.2) THEN
         WRITE(*,210) it,n,itt,ctime,
     +                ebtot(itt),ebttot(itt),bavrt(itt),
     +                b2max,b1max
         WRITE(1,210) it,n,itt,ctime,
     +                ebtot(itt),ebttot(itt),bavrt(itt),
     +                b2max,b1max
      ENDIF
      
      RETURN
c-----------------------------------------------------------------------
  210 format(/,'BLDYN - SOLUTION CHARACTERISTICS AT THIS STEP:',/,
     +   5x,'Time step #                         :',i4,'.',i3,'=',i5,/,
     +   5x,'Simulation time                     :',e14.4,/,
     +   5x,'Total mag energy/R^3    (rb<r/R<3.0):',e14.4,/,
     +   5x,'Toroidal mag energy/R^3 (rb<r/R<3.0):',e14.4,/,
     +   5x,'Average B               (rb<r/R<3.0):',e14.4,/,
     +   5x,'Max B_phi                 (envelope):',e14.4,/,
     +   5x,'Max B_phi              (shear layer):',e14.4,/)

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE writout(it,n,ctime)
c***********************************************************************
c     outputs results of selected time step                            *
c         unit8 --> formatted for plotting                             *
c         unit* --> tabular form                                       *
c
c     Called by: program bldynfit
c     Calls    : SUBROUTINEs angvel, magdif, circmd, srcpol
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE  'bldyn_includefiles/arrays.bldsurf.f'
      
c Input
      INTEGER   it,n
      REAL      ctime
c Local
      INTEGER   i,i1,i2
      REAL      au,ab,omgn,urn,uthn,etan,dum1,dum2
c     REAL      au,ab,omgn,urn,uthn,etan,dum1,dum2,alp2
      DIMENSION au(nnd),ab(nnd),omgn(nnd),urn(nnd),uthn(nnd),etan(nnd)
c    +          ,alp2(nnd)
c-----------------------------------------------------------------------

c---------- on first time step, output header info
      IF(it.eq.0)THEN
         WRITE(8) title
         WRITE(8) nm,nel,nelx,nely,nt,nstep
         WRITE(8) comega,calpha,reynolds,etadf,ybl,rzc,timeout
         WRITE(8) (xtht(i),i=1,nelx+1)
         WRITE(8) (yrad(i),i=1,nely+1)
         DO i=1,nnd
c -- compute angular velocity, diffusivity, and circulation at nodes
            CALL angvel(x(i),y(i),omgn(i),        dum1,dum2)
            CALL magdif(x(i),y(i),etan(i),             dum1)
            CALL circmd(x(i),y(i), urn(i),uthn(i),dum1,dum2)
c           CALL srcpol(x(i),y(i),alp2(i))
         ENDDO
         WRITE(8) (omgn(i),i=1,nnd)
         WRITE(8) ( urn(i),i=1,nnd)
         WRITE(8) (uthn(i),i=1,nnd)
         WRITE(8) (etan(i),i=1,nnd)
c        WRITE(8) (alp2(i),i=1,nnd)
      ENDIF
      
c---------- separate poloidal and toroidal fields
      DO i=1,nnd
         i2=2*i
         i1=i2-1
         au(i)=a(i1)
         ab(i)=a(i2)
      ENDDO
      
      WRITE(8) it+1,n
      WRITE(8) ctime,ebtot(itt),ebttot(itt),b1max,b2max
      WRITE(8) (au(i),i=1,nnd)
      WRITE(8) (ab(i),i=1,nnd)
      
      RETURN
      
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE surfabc
c***********************************************************************
c     Cette sousroutine lit les valeurs du potentiel vecteur A produit
c     par la simulation d'évolution du flux de surface surfbr2.f,
c     à chaque pas de temps 'it', et les entre comme condition 'limite'
c     à r/R=1.0 dans le vecteur 'uebc'
c     
c     Called by: program bldynfit
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE 'bldyn_includefiles/arrays.global.f'
      INCLUDE 'bldyn_includefiles/arrays.mesh.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
c --- Local:
      INTEGER           ia,ipos,itht
      REAL              aa(nnsyvrai),rap,pos,frac
c-----------------------------------------------------------------------
      
c --> Read Aphi in correct order
c     (surface uses theta = 0 -> pi (regular in theta) while this code 
c     uses theta = pi -> 0 (regular in theta) (=> cos(theta) = -1 -> 1))
      DO ia=1,nnsyvrai
         aa(ia) = SNGL(aphi(nnsyvrai-ia+1))
      ENDDO
      
c --- Application comme condition limite à r/R=1.0 ---------------------
      rap=FLOAT(nesyvrai)/nelx
      DO itht=1,nndx
         IF((iia(itht).GT.numebc).OR.(iia(itht).LT.1)) STOP 'Wrong iia!'
         pos=(itht-1)*rap+1.
         ipos=INT(pos)
         frac=pos-ipos
         IF(frac.EQ.0.) THEN
            uebc(iia(itht))= faca * aa(ipos)
         ELSE
c --- Interpolation linéaire entre les deux positions latitudinales
            uebc(iia(itht))= faca * ((1.-frac)*aa(ipos)+frac*aa(ipos+1))
         ENDIF
      ENDDO
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE formm
c***********************************************************************
c     form the system matrices from element matrices contributions            
c     (See Burnett, chap. 5)                                          
c     
c     This is an adaptation of equivalent routine in Burnett's code          
c     modified to handle non-symmetric system matrices                        
c     
c     Called by: program bldynfit
c     Calls    : SUBROUTINE elem23c
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE  'bldyn_includefiles/arrays.femlocal.f'
c Local
      INTEGER  i,j,ielno,i1,i2,j1,j2,ii1,ii2,jj1,jj2,ish,ishm,ishp
      REAL     onemth
c-----------------------------------------------------------------------
c---------- initialize arrays for the system matrices
c     WRITE(*,*) ' FORM: entering '
c     WRITE(1,*) ' FORM: entering '
      onemth=1.-theta

      DO j=1,nm
         DO i=1,mfb
            k(i,j)=0.
            ck(i,j)=0.
         ENDDO
      ENDDO
c******************** loop over all elements
      DO ielno=1,nel
c     WRITE(*,*) ' FORM: working on element #',ielno
c     WRITE(1,*) ' FORM: working on element #',ielno

c---------- form element matrices
         CALL elem23c(ielno)

c---------- assemble the system matrices from the element matrices
         DO i=1,nd
            i1=2*i-1
            i2=2*i
            ii1=2*icon(ielno,i)-1
            ii2=ii1+1
            DO j=1,nd
               j1=2*j-1
               j2=2*j
               jj1=2*icon(ielno,j)-1
               jj2=jj1+1
               ish=ii1+mhb-jj1
               ishm =ish-1
               ishp =ish+1
               k(ish  ,jj1) = k(ish  ,jj1)+ce(i1,j1)/dt+theta *ke(i1,j1)
               k(ishm ,jj2) = k(ishm ,jj2)+ce(i1,j2)/dt+theta *ke(i1,j2)
               k(ishp ,jj1) = k(ishp ,jj1)+ce(i2,j1)/dt+theta *ke(i2,j1)
               k(ish  ,jj2) = k(ish  ,jj2)+ce(i2,j2)/dt+theta *ke(i2,j2)
               ck(ish  ,jj1)=ck(ish  ,jj1)+ce(i1,j1)/dt-onemth*ke(i1,j1)
               ck(ishm ,jj2)=ck(ishm ,jj2)+ce(i1,j2)/dt-onemth*ke(i1,j2)
               ck(ishp ,jj1)=ck(ishp ,jj1)+ce(i2,j1)/dt-onemth*ke(i2,j1)
               ck(ish  ,jj2)=ck(ish  ,jj2)+ce(i2,j2)/dt-onemth*ke(i2,j2)
            ENDDO
         ENDDO

      ENDDO

c     WRITE(*,*) ' FORM: exiting '
c     WRITE(1,*) ' FORM: exiting '

      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE elem23c(ielno)
c***********************************************************************
c  master routine for element 23c, a quadrilateral bilinear isoparametric    
c  element for a system of two coupled parabolic PDE                        
c
c  ielno  : numbering of element for which computation is carried out
c
c  Called by: SUBROUTINEs formm
c  Calls    : SUBROUTINEs shap23, angvel, magdif, circmd, srcpol
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femlocal.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
c Input
      INTEGER  ielno
c Local 
      INTEGER  i,j,jj,intx,inty,igp,i1,i2,j1,j2
      REAL     ybot,tdkill,wgtjac,rr,mu,r2,sin1,sin2,
     +         omg,domgdr,domgdm,remag,drmfac,
     +         ur,uth,durdr,duthdth,
     +         ckat,ckbt,ckarr,ckamm,ckar,cka,
     +         ckbmm,ckbrr,ckbr,ckb,ckb1,ckb3,ckb5,
     +         cadv1,cadv2,cadv3,cadv4,cadv5,cadv6,
     +         cka1,ckba1,ckba2,ckba3
c-----------------------------------------------------------------------

c********** form elements matrices and vectors
c---------- initialisations
      DO i=1,4
         jj=icon(ielno,i)
         xx(i)=x(jj)
         yy(i)=y(jj)
         DO j=1,8
            ke(j,2*i)  =0.
            ce(j,2*i)  =0.
            ke(j,2*i-1)=0.
            ce(j,2*i-1)=0.
         ENDDO
      ENDDO
      ybot=y(icon(ielno,1))

c---------- take care of exterior solution
      IF(ybot.ge.1.)THEN
c --> within buffer;
         tdkill=0.
c        WRITE(*,*) ielno,ybot
c        WRITE(1,*) ielno,ybot
      ELSE
         tdkill=1.
      ENDIF

c---------- integrate terms (gaussian quadrature of order ngp X ngp)
      igp=0
      DO intx=1,ngp
         xi=gp(ngp,intx)
         DO inty=1,ngp
            eta=gp(ngp,inty)
            igp=igp+1
            CALL shap23(ielno,igp)
            wgtjac=wt(ngp,intx)*wt(ngp,inty)*jac(ielno,igp)
c -- some geometric terms
            mu  =xgp(ielno,igp)
            rr  =ygp(ielno,igp)
            sin2=1.-mu*mu
            sin1=sin(acos(mu))
            r2  =rr*rr
            
c -- compute angular velocity and derivatives at (r,mu)
            CALL angvel(mu,rr,omg,domgdr,domgdm)
            
c -- compute magnetic diffusivity and radial derivative at (r,mu)
            CALL magdif(mu,rr,remag,drmfac)
            
c -- compute meridional circulation and derivatives at (r,mu)
            CALL circmd(mu,rr,ur,uth,durdr,duthdth)
            
c-------------- THESE ARE THE EQUATIONS BEING SOLVED -------------------
c
c    Poloidal field is written as B_pol= curl (0,0,A)
c    Latitudinal dependent variable is mu=cos(theta)
c
c -- Poloidal Equation: (see Tassoul 1978, eq. (15.20); here times r^2)
c
c        dA    d          dA     d        dA    
c   ckat -- = ---( ckamm ---) + --( ckarr --) + cka A   [DIFFUSION]
c        dt   dmu        dmu    dr        dr   
c
c             dA
c      + ckar --                                        [DIAMAGNETISM]
c             dr
c
c              dA                    dA
c      + cadv1 -- + cadv2 A + cadv3 ---                 [ADVECTION BY CM]
c              dr                   dmu
c
c      + cka1...   + ckas                                [SOURCE TERM]
c
c -- Toroidal Equation: (see Tassoul 1978, eq. (15.18); times r^3 sin theta)
c
c        dB    d          dB     d        dB   
c   ckbt -- = ---( ckbmm ---) + --( ckbrr --) + ckb B   [DIFFUSION]
c        dt   dmu        dmu    dr        dr  
c
c             dB
c      + ckbr --                                        [DIAMAGNETISM]
c             dr
c
c              dB                    dB
c      + cadv4 -- + cadv5 B + cadv6 ---                 [ADVECTION BY CM]
c              dr                   dmu
c
c             dA                  dA
c      + ckb3 -- + ckb5 A + ckb1 ---                    [SHEARING BY DR]
c             dr                 dmu
c
c      + ckba1... ckba2... ckba3...    + ckaf           [SOURCE/LOSS TERM]
c
c-----------------------------------------------------------------------

c -- time derivative terms
cALem 2012-01-07 ok.
            ckat= r2*tdkill
            ckbt= r2
            
c -- diffusion terms, for both equations
c     REDOUBLEDCHECKED 26/8/2002 AND FOUND CORRECT, EXCEPT FOR ckbr, CORRECTED
cALem 2012-01-07 ok.
            ckamm =  sin2    *remag
            ckarr =  r2      *remag
            ckar  = -r2      *drmfac
            cka   = -1./sin2 *remag
            
            ckbmm =  ckamm
            ckbrr =  ckarr
c     NEXT TERM FOUND MISSING 26/8/2002 AND ADDED (need to double check)
cALem 2012-02-07 ckbr=0. car inclus dans ckbrr, mais manque rr*drmfac*B !
            ckbr  =  0.                      !cALem
c           ckbr  =  rr      *drmfac
cALem       ckb   =  cka
            ckb   =  cka   +  rr * drmfac    !cALem
            
c -- Shearing terms in toroidal field (B) equation
c     REDOUBLEDCHECKED    4/1/2000 AND FOUND CORRECT
c     RE-REDOUBLEDCHECKED 26/8/2002 AND FOUND CORRECT
cALem 2012-01-07 ok.
            ckb1=-r2*sin2*domgdr                   *comega
            ckb3= r2*sin2*domgdm                   *comega
            ckb5= (r2*mu*domgdr+rr*sin2*domgdm)    *comega
            
c --> Mean-field alpha-effect:
            cka1=0.
            ckba1=0.
            ckba2=0.
            ckba3=0.
            IF (isrc.EQ.1) THEN                                   !cALem
c --> terms multiplying B in poloidal equation (alpha contribution):
               WRITE(*,*) '!! isrc = ',isrc
               WRITE(1,*) '!! isrc = ',isrc
               WRITE(1,*) '!! Alpha term not defined yet !!'
               STOP '!! Alpha term not defined yet !!'
c --> terms multiplying A in toroidal equation (alpha^2 contribution):
c     REDOUBLEDCHECKED 4/1/2000 AND FOUND CORRECT
               IF (ialp.EQ.2) THEN                                !cALem
                  WRITE(*,*) '!! isrc = ',isrc
                  WRITE(1,*) '!! isrc = ',isrc
                  WRITE(1,*) '!! Alpha^2 term not defined yet !!'
                  STOP '!! Alpha^2 term not defined yet !!'
c               ckba1=-alp0*r2                         *calpha*term*ialp
c               ckba2=-alp0*sin2                       *calpha*term*ialp
c               ckba3=(alp0/sin2-rr*dalpdr+dalpdmu*mu )*calpha*term*ialp
               ENDIF
            ENDIF
            
            
c -- advection terms due to meridional circulation, both equations
c     CORRECTED         4/1/2000 for missing r^2 factors
c     REDOUBLEDCHECKED 26/8/2002 AND FOUND CORRECT
cALem 2012-01-07 ok.
            cadv1=- r2*ur                          *reynolds
            cadv2=-(ur + uth*mu/sin1)*rr           *reynolds
            cadv3=  uth*sin1*rr                    *reynolds
            cadv4=- r2*ur                          *reynolds
            cadv5=-(ur*rr+r2*durdr+rr*duthdth)     *reynolds
            cadv6=  uth*rr*sin1                    *reynolds
            
            DO i=1,4
               i1=2*i-1
               i2=2*i
               DO j=1,4
                  j1=2*j-1
                  j2=2*j

c --> coefficients of poloidal induction equation
c
                  ce(i1,j1)=ce(i1,j1)+wgtjac*ckat*   phi(j)   *phi(i)
                  ke(i1,j1)=ke(i1,j1)+wgtjac*(
     +                                   ckamm    *dphdx(j) *dphdx(i)
     +                                  +ckarr    *dphdy(j) *dphdy(i)
     +                                  -ckar     *dphdy(j)   *phi(i)
     +                                  -cka        *phi(j)   *phi(i)
     +                                  -cadv1    *dphdy(j)   *phi(i)
     +                                  -cadv2      *phi(j)   *phi(i)
     +                                  -cadv3    *dphdx(j)   *phi(i))
                  ke(i1,j2)=ke(i1,j2)+wgtjac*(
     +                                  -cka1       *phi(j)   *phi(i))

c --> coefficients of toroidal induction equation
c
                  ce(i2,j2)=ce(i2,j2)+wgtjac*ckbt*   phi(j)   *phi(i)
                  ke(i2,j1)=ke(i2,j1)+wgtjac*(
     +                                   ckba2    *dphdx(j) *dphdx(i)
     +                                  +ckba1    *dphdy(j) *dphdy(i)
     +                                  -ckba3      *phi(j)   *phi(i)
     +                                  -ckb1     *dphdx(j)   *phi(i)
     +                                  -ckb3     *dphdy(j)   *phi(i)
     +                                  -ckb5       *phi(j)   *phi(i))
                  ke(i2,j2)=ke(i2,j2)+wgtjac*(
     +                                   ckbmm    *dphdx(j) *dphdx(i)
     +                                  +ckbrr    *dphdy(j) *dphdy(i)
     +                                  -ckbr     *dphdy(j)   *phi(i)
     +                                  -ckb        *phi(j)   *phi(i)
     +                                  -cadv4    *dphdy(j)   *phi(i)
     +                                  -cadv5      *phi(j)   *phi(i)
     +                                  -cadv6    *dphdx(j)   *phi(i))
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      RETURN

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE formf(it)
c***********************************************************************
c  form the RHS vector by assembling individual element's contributions
c  (See Burnett, chap. 5)                                          
c
c  it=0   : initilize only   (CALL from program bldynfit)
c                                                                          
c  Called by: program bldynfit, SUBROUTINE calfun
c  Calls    : SUBROUTINE elem23f
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE  'bldyn_includefiles/arrays.femlocal.f'
c Local
      INTEGER  i,ielno,i1,i2,ii1,ii2,iflag,it
      REAL     onemth
c-----------------------------------------------------------------------
c---------- initialize arrays for the system matrices
c     WRITE(*,*) ' FORM: entering '
c     WRITE(1,*) ' FORM: entering '
      onemth=1.-theta
      
      DO i=1,nm
         f(i)=0.
      ENDDO
      
c --> For LOCAL or NON-LOCAL ALPHA EFFECT in poloidal equations,
c     WITH QUENCHING => modify vector F at each time step
      IF (it.GT.0) THEN
         iflag=1
         
c ------ Loop over all elements ----------------------------------------
         DO ielno=1,nel
c           WRITE(*,*) ' FORM: working on element #',ielno
c           WRITE(1,*) ' FORM: working on element #',ielno
            
c --------- Form element matrices
            CALL elem23f(ielno,iflag)
            
c --------- Assemble the system matrices from the element matrices
            DO i=1,nd
               i1=2*i-1
               i2=2*i
               ii1=2*icon(ielno,i)-1
               ii2=ii1+1
               f(ii1)=f(ii1)+fe(i1)
               f(ii2)=f(ii2)+fe(i2)
            ENDDO
            
         ENDDO
c ----------------------------------------------------------------------

      ENDIF
      
c     WRITE(*,*) ' FORM: exiting '
c     WRITE(1,*) ' FORM: exiting '
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE elem23f(ielno,iflag)
c***********************************************************************
c  Routine to calculate the load (F) vector at each time step.
c
c  ... for element 23c, a quadrilateral bilinear isoparametric    
c  element for a system of two coupled parabolic PDE                        
c
c  ielno  : numbering of element for which computation is carried out
c  iflag=1: computes element matrices   (CALL from SUBROUTINE form)
c  iflag=2: magnetic energy calculation (CALL from SUBROUTINE postpro)
c
c  Called by: SUBROUTINEs formf, postpro
c  Calls    : SUBROUTINEs shap23, angvel, magdif, circmd, srcpol
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femlocal.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
c Input
      INTEGER  ielno,iflag
c Local 
      INTEGER  i,j,ii,jj,intx,inty,igp,i1,i2,ielno2
      REAL     sum2,term,ybot,wgtjac,rr,mu,r2,sin1,sin2,xbot,
     +         alp0,ckas,ckaf,term2,remag,drmfac,
c    +         sum1,sum3,bgpt,agpt,axgpt,aygpt,br2,bth2,bgpt2,ebt,ebtt,
     +         sum1,sum3,sum4,bgpt,agpt,axgpt,aygpt,br2,bth2,bgpt2,
     +         alpsfac0
c-----------------------------------------------------------------------

c********** form elements matrices and vectors
      IF(iflag.eq.1) THEN
c---------- initialisations
         DO i=1,4
            jj=icon(ielno,i)
            xx(i)=x(jj)
            yy(i)=y(jj)
            fe(2*i)=0.
            fe(2*i-1)=0.
         ENDDO
         ybot=y(icon(ielno,1))
         xbot=x(icon(ielno,1))
c        WRITE(*,*) ' ielno,xbot ', ielno,xbot
c        WRITE(1,*) ' ielno,xbot ', ielno,xbot
         alpsfac0=alpsfac
         IF(xbot.ge.0.) alpsfac0=alpsfac2

         IF(ybot.gt.0.5.and.ybot.le.1.0) THEN
            sum2=0.
            
c --------- IF non-local alpha-effect (Use B_phi in middle of
c           "cousin" element located beneath core-envelope interface,
c           at same latitude) (DC99, eq. 7)
            IF(ialp.EQ.2) THEN
               ielno2=ielnoint+mod(ielno,nelx)

c --------- ELSE: Local alpha-effect
            ELSE
               ielno2=ielno
            ENDIF
            
            DO i=1,4
               jj=2*icon(ielno2,i)
               sum2=sum2+a(jj)
            ENDDO
            sum2=sum2/4.
            
c------- Compute nonlinear part of source term (constant within element!!): 
c        alpha-quenching-like nonlinearity:
c           B / ( 1.+( B / B_0 )^2 ) , with B_0=10^5 G

c           Following quenching for alpha-Omega
            term=sum2/( 1.+(sum2/5.e3)**2 )
c           term=erf(sum2)*sum2/( 1.+(sum2/3.1)**2 )**2

c           Following quenching for BL
c           term=(1.+erf( (abs(sum2)-6.e4)/2.e4 ))
c    +          *(1.-erf( (abs(sum2)-1.e5)/8.e4 ))*sum2
c           IF(ielno.eq.2776)THEN
c              WRITE(*,*) ielno,sum2,term
c              WRITE(1,*) ielno,sum2,term
c           ENDIF
            
c --> For flux loss by magnetic buoyancy
            term2=sum2*sum2*sum2
         ELSE
c --> away from source region, so term=0.
            term=0.
c --> no flux loss by magnetic buoyancy
            term2=0.
         ENDIF

c---------- integrate terms (gaussian quadrature of order ngp X ngp)
         igp=0
         DO intx=1,ngp
            xi=gp(ngp,intx)
            DO inty=1,ngp
               eta=gp(ngp,inty)
               igp=igp+1
               CALL shap23(ielno,igp)
               wgtjac=wt(ngp,intx)*wt(ngp,inty)*jac(ielno,igp)
      
c -- some geometric terms
               mu  =xgp(ielno,igp)
               rr  =ygp(ielno,igp)
               r2  =rr*rr

c -- compute spatial part of poloidal source term at (r,mu)
               CALL srcpol(mu,rr,alp0)

c --> Babcock-Leighton poloidal source term 
               ckas= r2*alp0 *alpsfac0*calpha *term
               
c        -- Compute buoyant loss coefficient (using magnetic
c           diffusivity profile for now...)
               CALL magdif(mu,rr,remag,drmfac)

c --> Flux loss term (zero in basic model)
               ckaf= -r2*gamfl*remag *term2
               
               DO i=1,4
                  i1=2*i-1
                  i2=2*i
c --> source term on RHS of poloidal induction equation
                  fe(i1)=fe(i1)+wgtjac*ckas*phi(i)
c --> alpha-source term on RHS of toroidal induction equation
c(erreur)         fe(i1)=fe(i1)+wgtjac*ckas*phi(i)
cALem             fe(i2)=fe(i2)+wgtjac*ckas*phi(i)
c --> flux loss term on RHS of toroidal induction equation
                  fe(i2)=fe(i2)+wgtjac*ckaf*phi(i)
               ENDDO

            ENDDO
         ENDDO

c********** do magnetic energy calculations
      ELSE  ! (iflag.NE.1)

c---------- initialisations
         DO i=1,4
            j=icon(ielno,i)
            xx(i)=x(j)
            yy(i)=y(j)
         ENDDO
c---------- integrate terms (gaussian quadrature of order ngp X ngp)
         igp  =0
         sum1=0.
         sum2=0.
         sum3=0.
         sum4=0.
         DO intx=1,ngp
            xi=gp(ngp,intx)
            DO inty=1,ngp
               eta=gp(ngp,inty)
               igp=igp+1
               CALL shap23(ielno,igp)
               wgtjac=wt(ngp,intx)*wt(ngp,inty)*jac(ielno,igp)
               
               mu  =xgp(ielno,igp)
               rr  =ygp(ielno,igp)
               sin2=1.-mu*mu
               sin1=sin(acos(mu))
               r2  =rr*rr
               
               bgpt =0.
               agpt =0.
               axgpt=0.
               aygpt=0.
               DO i=1,4
                  ii=2*icon(ielno,i)-1
                  agpt =agpt +a(ii  )*phi(i)
                  bgpt =bgpt +a(ii+1)*phi(i)
                  axgpt=axgpt+a(ii)  *dphdx(i)
                  aygpt=aygpt+a(ii)  *dphdy(i)
               ENDDO

c --> compute field magnitude and energies
c     the forms of B_r and B_theta below suggest B=curl(A), not curl(A/r sin)
               br2 =(-axgpt*sin1/rr+agpt*mu/rr/sin1)**2                 !Double-checked (2015-02-18, A.Lemerle)
               bth2=(-aygpt-agpt/rr)**2                                 !Double-checked (2015-02-18, A.Lemerle)
               bgpt2=bgpt**2
               sum1=sum1+(bgpt2+br2+bth2)    *rr**2*wgtjac              !Added rr**2 factor (2015-02-18, A.Lemerle)
               sum2=sum2+bgpt2               *rr**2*wgtjac              !Added rr**2 factor (2015-02-18, A.Lemerle)
               sum3=sum3+sqrt(bgpt2+br2+bth2)*rr**2*wgtjac              !Added rr**2 factor (2015-02-18, A.Lemerle)
               sum4=sum4+rr**2*wgtjac                                   !2015-02-18 (A.Lemerle)
            ENDDO
         ENDDO
         
c --> this is total magnetic energy
         ebt =sum1
c --> this is toroidal magnetic energy
         ebtt=sum2
c --> this is average magnetic field strength
         bavr=sum3
c --> this is volume
         vol =sum4                                                      !2015-02-18 (A.Lemerle)
         
      ENDIF

      RETURN

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE shap23(ielno,igp)
c***********************************************************************
c  Internal to FEM.
c  Evaluates shape function and derivatives at points (xi,eta)             
c  for 4-node quadrilateral. Jacobian is only computed upon first
c  CALL (controled by pareameter ijac) and stored for future use
c
c  Called by: SUBROUTINEs elem23c, elem23f
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.global.f'
      INCLUDE 'bldyn_includefiles/arrays.femlocal.f'
c Input
      INTEGER ielno,igp
c Local
      INTEGER i
      REAL    xip,xim,etp,etm,dxdxi,dydet,dydxi,dxdet,avg
c-----------------------------------------------------------------------
c---------- constants
      xip=1.+xi
      xim=1.-xi
      etp=1.+eta
      etm=1.-eta
c******************** shape functions
      phi(1)=xim*etm/4.
      phi(2)=xip*etm/4.
      phi(3)=xip*etp/4.
      phi(4)=xim*etp/4.
c******************** derivatives of the shape function
c---------- derivatives wrt xi at (xi,eta)
      dphdxi(1)=-etm/4.
      dphdxi(2)= etm/4.
      dphdxi(3)= etp/4.
      dphdxi(4)=-etp/4.
c---------- derivatives wrt eta at (xi,eta)
      dphdet(1)=-xim/4.
      dphdet(2)=-xip/4.
      dphdet(3)= xip/4.
      dphdet(4)= xim/4.
c******************** do jacobian calculations
c---------- compute jacobian at (xi,eta)
      IF(ijac.le.0)THEN
         dxdxi=0.
         dydet=0.
         dydxi=0.
         dxdet=0.
         DO i=1,4
            dxdxi=dxdxi+dphdxi(i)*xx(i)
            dydet=dydet+dphdet(i)*yy(i)
            dydxi=dydxi+dphdxi(i)*yy(i)
            dxdet=dxdet+dphdet(i)*xx(i)
c    WRITE(*,*) ielno,xx(i),yy(i)
c    WRITE(1,*) ielno,xx(i),yy(i)
         ENDDO
         jac(ielno,igp)=dxdxi*dydet-dxdet*dydxi
c---------- check positiveness of jacobian
         avg=(abs(dxdxi)+abs(dydet))/4.
         IF(jac(ielno,igp).lt.avg*1.e-8)THEN
            WRITE(*,*) ielno,jac(ielno,igp)
            WRITE(1,*) ielno,jac(ielno,igp)
            STOP '1 shap23'
         ENDIF
c---------- compute inverse of the jacobian at (xi,theta)
         jacinv(ielno,igp,1)=dydet/jac(ielno,igp)
         jacinv(ielno,igp,2)=dxdxi/jac(ielno,igp)
         jacinv(ielno,igp,3)=-dydxi/jac(ielno,igp)
         jacinv(ielno,igp,4)=-dxdet/jac(ielno,igp)
      ENDIF
c---------- derivatives wrt x and y at (xi,eta)
      DO i=1,4
         dphdx(i)=jacinv(ielno,igp,1)*dphdxi(i)
     +           +jacinv(ielno,igp,3)*dphdet(i)
         dphdy(i)=jacinv(ielno,igp,2)*dphdet(i)
     +           +jacinv(ielno,igp,4)*dphdxi(i)
      ENDDO
      RETURN

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE aplybc(it)
c***********************************************************************
c  Modifies system matrices to incorporate Dirichlet conditions            
c  (See Burnett, chap. 7)                                                  
c                                                                          
c  This is an adaptation of equivalent routine in Burnett's code           
c  modified to handle non-symmetric system matrices                        
c
c  Called by: program bldynfit
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE  'bldyn_includefiles/arrays.femlocal.f'
c Input
      INTEGER  it
c Local
      INTEGER  i,j,jj,ibc,jpmb,jshift,ishp,ishm,jrp,jrm
      REAL     sum1,sum2
c-----------------------------------------------------------------------

c******************** apply mixed boundary conditions, by modifying
c                     diagonal of stiffness matrix
c     WRITE(*,*) ' APLYBC: entering '
c     WRITE(1,*) ' APLYBC: entering '
c     IF(nummbc.le.0) goto 400
c     DO 300 i=1,nm
c        k(mband,i)=k(mband,i)-umbc(i)
c 300 CONTINUE

c******************** apply the essential boundary conditions, if any,
c                     by imposing constraint equations
  400 IF(numebc.le.0) RETURN
      
      IF (it.EQ.0) THEN
c---> INITIAL STATE: Apply essential boundary conditions to a(iuebc) directly
         DO ibc=1,numebc
            i=iuebc(ibc)
            a(i)=uebc(ibc)
         ENDDO
         
      ELSE
c---> TIME STEPPING: Calculate step change in essential boundary conditions
c                    (for ramping in calfun)
         DO i=1,nm
            u0(i)=0.
            delu(i)=0.
            keffdu(i)=0.
            keffu0(i)=0.
         ENDDO
         DO ibc=1,numebc
            i=iuebc(ibc)
            u0(i)=a(i)
            delu(i)=(uebc(ibc)-a(i))/float(nstep)
c           WRITE(*,*) ibc,' node # ',jj, ' BC= ',uebc(ibc),u0(i)
c           WRITE(1,*) ibc,' node # ',jj, ' BC= ',uebc(ibc),u0(i)
         ENDDO
         
c******* Enforce dirichlet B.C. by applying constraint equations to system
         
c------> 1. Calculate modified load vector [see Burnett, eq.(11.116)]
         DO j=1,nm
            sum1=0.
            sum2=0.
            jpmb=j+mhb
            DO i=max(1,jpmb-nm),min(mfb,jpmb-1)
               jshift=jpmb-i
               sum1=sum1+k(i,jshift)*delu(jshift)
               sum2=sum2+k(i,jshift)*  u0(jshift)
            ENDDO
            keffdu(j)=sum1
            keffu0(j)=sum2
         ENDDO
         
c------> 2. Modify system matrix
         DO ibc=1,numebc
            jj=iuebc(ibc)
c           WRITE(*,*) ' working on BC # ',ibc,' node # ',jj
c           WRITE(1,*) ' working on BC # ',ibc,' node # ',jj
c --------> Zero matrix column
            DO i=1,mfb
               k(i,jj)=0.
            ENDDO
c --------> Zero matrix "line" (shifted version)
            DO i=2,mhb
               ishp=mhb-i+1
               ishm=mhb+i-1
               jrp=jj+i-1
               jrm=jj-i+1
               IF(jrm.ge.1)  k(ishm,jrm)=0.
               IF(jrp.le.nm) k(ishp,jrp)=0.
            ENDDO
c --------> Set corresponding diagonal element of system matrix to one
            k(mhb,jj)=1.
         ENDDO
      
      ENDIF
      
c     WRITE(*,*) ' APLYBC: exiting '
c     WRITE(1,*) ' APLYBC: exiting '
      RETURN

      END
c=======================================================================

c***********************************************************************
      SUBROUTINE calfun(it)
c***********************************************************************
c  Solve the system equations for the function values, using
c  the Theta-method, an implicit one-step time-stepping scheme
c  (See Burnett, sec. 11.3, 11.4)                                      *
c                                                                      *
c  This is an adaptation of equivalent routine in Burnett's code       *
c  modified to handle non-symmetric system matrices                    *
c
c  Called by: program bldynfit
c  Calls    : SUBROUTINEs  tribu, formf, rhsbu
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE   'bldyn_includefiles/arrays.global.f'
      INCLUDE   'bldyn_includefiles/arrays.mesh.f'
      INCLUDE   'bldyn_includefiles/arrays.femglobal.f'
      INCLUDE   'bldyn_includefiles/arrays.bldsurf.f'
      
c Input
      INTEGER   it
c Local
      INTEGER   i,j,jpmb,jshift,ieb,n
      REAL      onemth,rns,rn,cka,ctime,feff,quant
      DIMENSION feff(nm)
      REAL           urand2
      EXTERNAL       urand2
 
c-----------------------------------
c******************** implicit initial-boundary-value problem
c     WRITE(*,*) ' CALFUN: entering '
c     WRITE(1,*) ' CALFUN: entering '
c     WRITE(*,*) 'nm = ',nm
c     WRITE(1,*) 'nm = ',nm
c---------- save load vector and function at start of interval
      ctime=time(it)
      DO i=1,nm
c        fnm1(i)=f0(i)
         fnm1(i)=f(i)
         anm1(i)=a(i)
      ENDDO
      onemth=1.-theta
      rns=float(nstep)
      
c---------- forward reduction of effective stiffness matrix (keff)
      CALL tribu(k)
      
c-----------------------------------------------------------------------
c---- Solve recurrence relation over all time steps --------------------
      DO n=1,nstep
         
         itt=(it-1)*nstep+n+1
         ctime=ctime+dt
         rn=float(n)
         
c------- Nothing for now
c        CALL prepro(it,n)
         
c------> Calculate RHS vector at this sub-timestep:
         
c ------ IF local or non-local alpha-effect with quenching (=> modify F)
         IF(isrc.EQ.2)THEN                                        !cALem
            
c---------- Compute stochastic multiplicative factor for alpha-effect
c           quant=float(n)/2.-float(n/2)
            quant=float(n)/5.-float(n/5)
c           IF(quant.le.0.) alpsfac=4.*urand2()-1.
c           IF(quant.le.0.) alpsfac=2.*urand2()
c           IF(quant.le.0.) alpsfac=0.5+urand2()
c           IF(quant.le.0.) alpsfac=0.75+0.5*urand2()
            IF(quant.le.0.)THEN
               alpsfac =0.0+2.0*urand2()
               alpsfac2=0.0+2.0*urand2()
            ENDIF
c           IF(quant.le.0.) alpsfac=2.*urand2()
c           alpsfac=0.5+urand2()
c           WRITE(*,*) alpsfac,alpsfac2
c           WRITE(1,*) alpsfac,alpsfac2
            alpsfac=1.
            alpsfac2=1.
            
            CALL formf(it)
            
            DO i=1,nm
               fn(i)=f(i)
            ENDDO
            
c           IF(n.eq.1)THEN
c              DO i=1,nm
c                 fnm1(i)=f(i)
c              ENDDO
c           ENDIF
            
c------- ELSE: DO not modify F, but calculate nat+int loads at step n by ramping
         ELSE
            DO i=1,nm
               fn(i)=f0(i)+(rn/rns)*(f(i)-f0(i))
            ENDDO
         ENDIF
         
c---------- calculate effective load vector (feff) at step n
         DO j=1,nm
            cka=0.
            jpmb=j+mhb
            DO i=max(1,jpmb-nm),min(mfb,jpmb-1)
               jshift=jpmb-i
               cka=cka+ck(i,jshift)*anm1(jshift)
            ENDDO
            feff(j)=onemth*fnm1(j)+theta*fn(j)+cka
         ENDDO
         
c---------- modify effective load vector for ramped essential bc
c --> insertion du "if" pour eviter keffu0 indefini si numebc=0  7/4/88
c        IF(numebc.gt.0) THEN
            DO i=1,nm
               feff(i)=feff(i)-keffu0(i)-rn*keffdu(i)
            ENDDO
            DO ieb=1,numebc
               i=iuebc(ieb)
               feff(i)=anm1(i)+delu(i)
            ENDDO
c        END if
         
c---------- reduction of load vector: backsubstitution
         CALL rhsbu(k,feff,a)
         
c---------- compute magnetic energy and such
         CALL postpro(it,n,ctime)
         
c---------- output nodal time series
         IF(ivrb.GE.1) THEN
            CALL writout(it,n,ctime)
         ENDIF
         
c---------- additive noise
         IF(addnamp.NE.0.) CALL addnoise                                !cALem 2016-10-24
         
c---------- save values for next step
         DO i=1,nm
            anm1(i)=a (i)
            fnm1(i)=fn(i)
         ENDDO
         
      ENDDO
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------

c---------- save nat+int loads at end of interval
      DO i=1,nm
         f0(i)=f(i)
      ENDDO
c     WRITE(*,*) ' CALFUN: exiting '
c     WRITE(1,*) ' CALFUN: exiting '

      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE addnoise
c***********************************************************************
c   adds stochastic noise on poloidal field at surface
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
c Local
      INTEGER   ii,iel,nelnoise
      REAL      urand2
      EXTERNAL  urand2
c-----------------------------------------------------------------------

c---------- introduce noise at surface
      nelnoise=nelx*nely/4
      DO iel=ielnoint,ielnoint+nelnoise-1
         ii=icon(iel,2)
         IF(x(ii).gt.0.and.x(ii).lt.1.0)THEN
            a(2*ii)=a(2*ii)+addnamp*(2.*urand2()-1.)
            a(2*ii-1)=a(2*ii-1)+addnamp*(2.*urand2()-1.)/nelx
         ENDIF
c        a(2*i)=a(2*i)+addnamp*2.*(urand2()-1.)
      ENDDO

      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE tribu(gk)
c***********************************************************************
c     triangularizes shifted matrix
c     (modified alg., after Golub & Van Loan, p. 152)
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
c Local
      INTEGER i,j,k,kpj,imj
      REAL      gk,c
      DIMENSION gk(mfb,nm)

      DO k=1,nm-1
         IF(gk(mhb,k).eq.0.)THEN
            WRITE(*,100) k,gk(mhb,k)
            WRITE(1,100) k,gk(mhb,k)
            STOP ' *TRIBU*'
         ENDIF
         c=1./gk(mhb,k)
         DO i=mhb+1,mfb
            gk(i,k)=c*gk(i,k)
         ENDDO
         DO j=1,min(mhb-1,nm-k)
            kpj=k+j
            c=gk(mhb-j,kpj)
            IF(c.ne.0.)THEN
CDIR$ IVDEP
               DO i=mhb+1,min(mfb,nm-k+mhb)
                  imj=i-j
                  gk(imj,kpj)=gk(imj,kpj) -c*gk(i,k)
               ENDDO
            ENDIF
         ENDDO
      ENDDO
      RETURN
c-----------------------------------------------------------------------
  100 format(//,'  set of equations may be singular..',/,
     +'   diagonal term of equation',i5,'  at tribu is equal to',
     +1pe15.8)
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE rhsbu(gk,gf,u)
c***********************************************************************
c     forward reduction & backsubstitution of triangularized system matrix
c     (modified alg., after Golub & Van Loan, p. 152)
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
c Local
      INTEGER i,j,mhbmj,ishift
      REAL      gk,gf,u
      DIMENSION gk(mfb,nm),gf(nm),u(nm)

c---------- forward reduction
      DO j=1,nm
         IF(gk(mhb,j).eq.0.)THEN
            WRITE(*,100) j,gk(mhb,j)
            WRITE(1,100) j,gk(mhb,j)
            STOP ' *RHSBU*'
         ENDIF
         mhbmj=mhb-j
         DO i=j+1,min(nm,j+mhb-1)
            ishift=i+mhbmj
            gf(i)=gf(i)-gk(ishift,j)*gf(j)
         ENDDO
      ENDDO

c---------- backsubstitution 

      DO j=nm,1,-1
c        gf(j)=gf(j)/gk(mhb,j)
         u(j)=gf(j)/gk(mhb,j)
         mhbmj=mhb-j
         DO i=max(1,j-mhb+1),j-1
            ishift=i+mhbmj
c           gf(i)=gf(i)-gk(ishift,j)*gf(j)
            gf(i)=gf(i)-gk(ishift,j)*u(j)
         ENDDO
c        u(j)=gf(j)
      ENDDO

      RETURN
c-----------------------------------------------------------------------
  100 format(//,'  set of equations are singular.',/,
     +'   diagonal term of equation',i5,'  at tribu is equal to',
     +1pe15.8)
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE stlast
c***********************************************************************
c   stores solutions of last step, for future usage as initial condition
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
      INCLUDE  'bldyn_includefiles/arrays.mesh.f'
      INCLUDE  'bldyn_includefiles/arrays.femglobal.f'
c Local
      INTEGER i
c-----------------------------------------------------------------------
c---------- output grid values
      WRITE(9) title
      WRITE(9) nm,nel,nelx,nely,nt,nstep
      WRITE(9) comega,calpha,reynolds,etadf,ybl,rzc,timeout
      WRITE(9) (xtht(i),i=1,nelx+1)
      WRITE(9) (yrad(i),i=1,nely+1)

      WRITE(9) a

      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE angvel(xx,yy,omg,domgdr,domgdm)
c***********************************************************************
c   SUN AND LATE-TYPE STAR BABCOCK-LEIGHTON DYNAMOS:                  
c
c   computes angular velocity and derivatives at r/R=yy, cos(theta)=xx
c
c   Differential rotation is solar-like, with a "tachocline" centered
c   on the core-envelope interface (r/R=rzc), with half-width w/R=ybl
c   See 2.1 in DC99
c
c   omg is angular velocity [Omega(r,mu) ]
c   domgdr=d Omega/d r       [radial derivative of Omega ]
c   domgdm=d Omega/d mu      [latitudinal derivative of Omega ]
c***********************************************************************
c   Refs: Dikpati & Charbonneau 1999 , ApJ, 518, 508-ff
c         Charbonneau et al. 1999, ApJ, 
c
c   Last revised 10/07/2001
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
c Input
      REAL    xx,yy
c Output
      REAL    omg,domgdr,domgdm
c Local
      REAL    mu2,mu4,omge,tt,del,frac,dfrac
      REAL    erf
c-----------------------------------------------------------------------      
c---------- now angular velocity; solar-like parameterization
      IF(yy.GE.1.)THEN
c --> exterior
         omg=0.
         domgdr=0.
         domgdm=0.
         
      ELSE
c --> interior
         mu2=xx*xx
         mu4=mu2*mu2
         omge=a0 + a2*mu2 + a4*mu4
         del=omge-omgc
         tt=(yy-rzc)/ybl
         frac=0.5*( 1.+erf(tt) )
         dfrac=1./(ybl*SQRT(pi)) *EXP(-tt**2)
         
         omg    = omgc+frac*del
         domgdr = dfrac*del                    
         domgdm = frac * (2.*a2*xx + 4.*a4*mu2*xx) 
      ENDIF
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE magdif(xx,yy,eta,detadr)
c***********************************************************************
c   SUN AND LATE-TYPE STAR BABCOCK-LEIGHTON DYNAMOS:                  
c
c   computes magnetic diffusivity and its radial derivative 
c   at at r/R=yy, cos(theta)=xx
c
c   Diffusivity is unity in envelope, and falls by a factor etadf at r/R=rzc;
c   falloff is error function with half-width w/R=ybl. See sec. 2.2 in DC99
c
c   eta    is magnetic diffusivity
c   detadr is radial derivative of magnetic diffusivity
c***********************************************************************
c   Refs: Dikpati & Charbonneau 1999 , ApJ, 518, 508-ff
c
c   Last revised 10/07/2001
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
c Input
      REAL    xx,yy
c Output
      REAL    eta,detadr
c Local
      REAL    tt,g1r,dg1r,df
      REAL    erf
c-----------------------------------------------------------------------

      IF(yy.gt.1.0)THEN
c --> exterior: eta=1.
         eta=1.
         detadr=0.
      ELSE
c --> interior: error function profile
         tt=(yy-rzc)/ybl
         g1r=0.5*(1.+erf(tt))
         dg1r=1./(ybl*sqrt(pi)) *exp(-tt**2)
         df=1./etadf
         eta   =df+(etat0-df)*g1r
         detadr=   (etat0-df)*dg1r
      ENDIF
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE circmd(xx,yy,ur,uth,durdr,duthdth)
c***********************************************************************
c   SUN AND LATE-TYPE STAR BABCOCK-LEIGHTON DYNAMOS:                  
c
c   computes components of meridional circulation speed and derivatives
c   at r/R=yy, cos(theta)=xx
c
c   The meridional circulation has one cell per quadrant, poleward
c   at surface r/R=1, penetrates slightly below the interface.
c   Mathematical expression given in Dikpati & Charbonneau 1999,
c   ApJ, 518, 508, eqs. (5a)--(5f).
c   The flow has a polytropic density profile built into it,
c   so no explicit density computation is needed.
c   Original flow derivation in van Ballegooijen & Choudhuri 1988
c
c   NOTE: Eqs. (5a) and (5b) in DC99 are missing a factor of 2
c         if u_0 is to be the max flow speed at surface mid-latitudes;
c         This is corrected here.
c
c   ur     is radial component of circulation
c   uth    is colatitudinal component of circulation
c   durdr  is radial derivative of radial component of circulation
c   durdth is colatitudinal derivative of colatitudinal component of circ.
c***********************************************************************
c   Refs: Dikpati & Charbonneau 1999 , ApJ, 518, 508-ff
c         van Ballegooijen & Choudhuri 1988, ApJ, 333, 965
c
c   Last revised 10/07/2001
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
c Input
      REAL    xx,yy
c Output
      REAL    ur,uth,durdr,duthdth
c Local
      REAL    cos1,cos2,sin1,sin2,sinvv,cosww,uufac,
     +        fctsin,fctcos,dfctsin,dfctcos,fctsinqqm1,fctcosnnm1,
     +        unsr,unsr2,unsr3,unsr4,xir,xirmm,xirpp,
     +        fthur,frur,dfrur,fthuth,fruth,dfthuth
c-----------------------------------------------------------------------
      
      IF(yy.GT.1..OR.yy.LE.rbot)THEN
         
         ur =0.
         uth=0.
         durdr=0.
         duthdth=0.
         
      ELSE
         
         cos1   = xx
         cos2   = cos1*cos1
         sin2   = 1.-cos2
         sin1   = SQRT(sin2)         ! Absolute value to allow for non-integer exponent 'qq'
cvv      IF (vv.LE.1.) THEN                                             !cvv
cvv         fctsin = sin1                                               !cvv
cvv         dfctsin= cos1                                               !cvv
cvv      ELSE                                                           !cvv
            sinvv  = sin1*vv
            fctsin = ERF(sinvv)
            dfctsin= cos1*vvpi*EXP(-sinvv*sinvv)
cvv      ENDIF                                                          !cvv
         fctsinqqm1 = fctsin**qqm1
cvv      IF (ww.LE.1.) THEN                                             !cvv
cvv         fctcos = cos1                                               !cvv
cvv         dfctcos=-sin1                                               !cvv
cvv      ELSE                                                           !cvv
            cosww  = cos1*ww
            fctcos = ERF(cosww)
            dfctcos=-sin1*wwpi*EXP(-cosww*cosww)
cvv      ENDIF                                                          !cvv
         fctcosnnm1 = fctcos**nnm1
         
         unsr =1./yy
         unsr2=unsr*unsr
         unsr3=unsr*unsr2
         unsr4=unsr2*unsr2
         xir  =unsr-1.
         xirmm=xir**mm
         xirpp=xir**pp
         
         IF (cos1.GT.0.) THEN
            uufac=uu0
         ELSEIF (cos1.LT.0.) THEN
            uufac=us0
         ELSE
            uufac=0.
         ENDIF
         
         fthuth = uufac * fctsin*fctsinqqm1*fctcos*fctcosnnm1           !Double-checked 2014-05-01 (ALem)
         fruth  = unsr3 *(-1. + c1*xirmm - c2*xirmm*xirpp)              !Double-checked 2014-05-01 (ALem)
         uth    = fruth * fthuth                                        !Double-checked 2014-05-01 (ALem)
         
         dfthuth= uufac * fctsinqqm1*fctcosnnm1 *
     +                           (qq*dfctsin*fctcos + nn*fctsin*dfctcos)!Double-checked 2014-05-01 (ALem)
         duthdth= fruth * dfthuth                                       !Double-checked 2014-05-01 (ALem)
         
         IF (sin1.EQ.0.) sin1=1.e-6
         fthur  = cos1 * fthuth/sin1 + dfthuth                          !Double-checked 2014-05-01 (ALem)
         frur   = unsr2 * xir*(-cc0 + cc1*xirmm - cc2*xirmm*xirpp)      !Double-checked 2014-05-01 (ALem)
         ur     = frur * fthur                                          !Double-checked 2014-05-01 (ALem)
         
         dfrur  =-2.*unsr*frur -
     +       unsr4*(-cc0 + (mm+1)*cc1*xirmm - (mm+pp+1)*cc2*xirmm*xirpp)!Double-checked 2014-05-01 (ALem)
         durdr  = dfrur* fthur                                          !Double-checked 2014-05-01 (ALem)
         
      ENDIF
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE srcpol(xx,yy,alpxy)
c***********************************************************************
c   SUN AND LATE-TYPE STAR BABCOCK-LEIGHTON DYNAMOS:                  
c
c   computes spatial dependency of nonlinear poloidal source term
c   at r/R=yy, cos(theta)=xx;
c
c   The source term is concentrated immediately beneath surface
c   See sec 2.3 and eq. (7) in DC99. It is computed only in 0.92<r/R<1
c   to avoid underflow in the error function calculation
c***********************************************************************
c   Refs: Dikpati & Charbonneau 1999 , ApJ, 518, 508-ff
c
c   Last revised 10/07/2001
c***********************************************************************
      IMPLICIT NONE
      INCLUDE  'bldyn_includefiles/arrays.global.f'
c Input
      REAL    xx,yy
c Output
      REAL    alpxy
c Local
      REAL pi,rs2,rs3,ds2,ds3
      parameter(pi=3.1415926536)
c -- these are for the Babcock-Leighton source term
      parameter(rs2=0.95,rs3=1.0,ds2=0.01,ds3=0.01)
      REAL tt2,tt3,g2r,g3r,thta,sinth,ftheta
      REAL erf
c-----------------------------------------------------------------------

      IF(yy.gt.0.92.and.yy.le.1.)THEN
c --> below surface: Babcock-Leighton source (and avoiding IEEE underflows)
         
         tt2=(yy-rs2)/(ds2)
         g2r=0.5*(1.+erf(tt2))
         tt3=(yy-rs3)/(ds3)
c        g3r=0.5*(1.-erf(tt3))
c --> following line to be consistent with DC99
         g3r=    (1.-erf(tt3))
         
         thta=acos(xx)
         sinth=sin(thta)
         ftheta=xx*sinth
         
         alpxy=g2r*g3r *ftheta
      ELSE
c --> exterior and deep interior: no source
         alpxy=0.
         
      ENDIF
      
      RETURN
      END
c=======================================================================

c***********************************************************************
      SUBROUTINE fitness(fit,bmrfile,ssnfile,magfile)
c***********************************************************************
c     This SUBROUTINE calculates de fitness needed for optimisation via
c     genetic algorithm PIKAIA.
c***********************************************************************
      USE bldyn_param
      IMPLICIT NONE
      INCLUDE 'bldyn_includefiles/arrays.global.f'
      INCLUDE 'bldyn_includefiles/arrays.mesh.f'
      INCLUDE 'bldyn_includefiles/arrays.bldsurf.f'
      
      REAL     fit
      CHARACTER*250  bmrfile,ssnfile,magfile
c Local
      INTEGER  issn,nssn,nbmr,j,itf,i,nread,jj,jint(2,6)
      REAL     delmu(nelx),
     +         datawang(11),tht,signbfly,bfly(nntp1,nndx),
     +         smthcrit,obfly(nntp1,nndx),
     +         bflyfit(nntp1,nndx),bflymax,bflycor(nnfit),absb,
     +         bfitmax,bcor(nnfit)
      REAL     ssnread(4),ssnt(10000),ssn(10000),a,b,
     +         ssne(nntp1),ssnfit(ntfit),ssnmax,
     +         ebfit(ntfit),ebmax
      REAL     magread(nndx),mbfly(nntp1,nndx),dbfly(nntp1,nndx),
     +         mbfmx(nntp1,nndx),mbfmn(nntp1,nndx),dbflymin,
     +         mbflymax,mbflycor(nnfit),dbflycor(nnfit),
     +         brmax,brcor(nnfit),
     +         intsurf(6),
     +         bintdat(ntfit,6),bintsim(ntfit,6),
     +         bintsig(ntfit,6),bintdmx(ntfit,6),bintdmn(ntfit,6),
     +         bcordat(ntfit*6),bcorsim(ntfit*6),
     +         daxidat(ntfit,2),daxisim(ntfit,2),
     +         daxisig(ntfit,2),daxidmx(ntfit,2),daxidmn(ntfit,2),
     +         tinvdat(3),tinvsim(3)
      REAL     wsave(4*(2*ntfit-2)+15),ddatp,dsimp,
     +         fftdat1 (    2,nndx),fftsim1 (    2,nndx),
     +         fftdatp (nnfft,nndx),fftsimp (nnfft,nndx),
     +         fftdatpt(nnfft,nndx),fftsimpt(nnfft,nndx)
      COMPLEX  fftdat(2*ntfit-2),fftsim(2*ntfit-2)
      REAL     fit1,fit2,fit3,
     +         fit3cor2,fit3cor(6),fit3cor6,fit3cordd(2),fit3cord,
     +         fit3corfft11(2),fit3corfft1,
     +         fit3corfftpp(2,nnfft),fit3corfftp(2),
     +         fit3chi2,fit3chi(6),fit3chi6,fit3chidd(2),fit3chid,
     +         fit3chifft11(2),fit3chifft1,
     +         fit3chifftpp(2,nnfft),fit3chifftp(2)
c-----------------------------------------------------------------------
      
      DO j=1,nelx
         delmu(j)=xtht(j+1)-xtht(j)
      ENDDO
      
c---- Linear correlation between bfit and surface bfly -----------------
      fit1=0.
c     IF (iffit1.GT.0.) THEN
         IF(ivrb.GE.2) THEN
            WRITE(*,*) 'Calculating fit1'
            WRITE(1,*) 'Calculating fit1'
         ENDIF
         
c---- Read database -----
         DO j=1,nndx
            DO itt=1,nntp1
               bfly(itt,j) = 0.
            ENDDO
         ENDDO
         nbmr=0
         
         IF(ivrb.GE.2) THEN
            WRITE(*,*) 'Opening file: ',bmrfile
            WRITE(1,*) 'Opening file: ',bmrfile
         ENDIF
         OPEN(3,FILE=bmrfile)
         READ(3,*) ! Skip header (3 lines)
         READ(3,*) ! Skip header (3 lines)
         READ(3,*) ! Skip header (3 lines)
         DO WHILE(.TRUE.)
            READ(3,*,END=27) datawang
            tht= pi/180.*(datawang(8)+datawang(10))/2.
            j  = INT((1.-tht/pi)*nelx+0.5)+1                            !regular mesh for theta in decreasing order (mu=cos(theta) in increasing order)
            itt = INT(datawang(5)/dur*nnt+0.5)+1
            signbfly = datawang(3)
            IF (tht.LE.pi/2.) signbfly =-signbfly
            bfly(itt,j) = bfly(itt,j) + 1.*signbfly
            nbmr=nbmr+1
         ENDDO
   27    CLOSE(3)
         
         IF(ivrb.GE.2) THEN
            WRITE(*,*) nbmr,' BMR read'
            WRITE(1,*) nbmr,' BMR read'
         ENDIF
         
c---- Smooth bfly -----
         smthcrit = 3/16.     ! delta_t = smthcrit * delta_x, used for smoothing... (must be < 1/4)
         
         DO i=1,nsmth
            DO itt=1,nntp1
               DO j=1,nndx
                  obfly(itt,j)  = bfly(itt,j)
               ENDDO
            ENDDO
            DO itt=2,nnt
               DO j=2,nelx
                  bfly(itt,j) = obfly(itt,j) +smthcrit*(-4*obfly(itt,j)+
     +                                obfly(itt-1,j  )+obfly(itt+1,j  )+
     +                                obfly(itt  ,j-1)+obfly(itt  ,j+1))
               ENDDO
            ENDDO
            DO itt=2,nnt
               bfly(itt,1)    = bfly(itt,2)
               bfly(itt,nndx) = bfly(itt,nelx)
            ENDDO
            DO j=1,nndx
               bfly(1,j)      = bfly(2,j)
               bfly(nntp1,j)  = bfly(nnt,j)
            ENDDO
         ENDDO
         
c        meanbfly=REAL(nbmr)/(nntp1*nndx)
         
c---- Function of bfly (fitting region only) -----
         DO itt=1,nntp1
         DO j=1,nndx
            bflyfit(itt,j) =  bfly(itt,j)*ABS(bfly(itt,j))**(expfly-1)
         ENDDO
         ENDDO
         
c---- Normalization of bflyfit (fitting region only) -----
         bflymax=-1.e10
         DO itt=itfit(1),itfit(2)
         DO j=ixfits(1),ixfits(2)
            absb = ABS(bflyfit(itt,j))
            IF(absb.GT.bflymax) bflymax=absb
         ENDDO
         DO j=ixfitn(1),ixfitn(2)
            absb = ABS(bflyfit(itt,j))
            IF(absb.GT.bflymax) bflymax=absb
         ENDDO
         ENDDO
         DO itt=1,nntp1
         DO j=1,nndx
            bflyfit(itt,j) = bflyfit(itt,j)/bflymax
         ENDDO
         ENDDO
         
c---- Normalization of bfit (fitting region only) -----
         bfitmax=-1.e10
         DO itt=itfit(1),itfit(2)
         DO j=ixfits(1),ixfits(2)
            absb = ABS(bfit(itt,j))
            IF(absb.GT.bfitmax) bfitmax=absb
         ENDDO
         DO j=ixfitn(1),ixfitn(2)
            absb = ABS(bfit(itt,j))
            IF(absb.GT.bfitmax) bfitmax=absb
         ENDDO
         ENDDO
         DO itt=1,nntp1
         DO j=1,nndx
            bfit(itt,j)    =    bfit(itt,j)/bfitmax
         ENDDO
         ENDDO
         
c---- 1D vector for correlation (fitting region only) -----
         i=0
         DO itt=itfit(1),itfit(2)
         DO j=ixfits(1),ixfits(2)
            i=i+1
            bflycor(i) = bflyfit(itt,j)
            bcor   (i) = bfit   (itt,j)
         ENDDO
         DO j=ixfitn(1),ixfitn(2)
            i=i+1
            bflycor(i) = bflyfit(itt,j)
            bcor   (i) = bfit   (itt,j)
         ENDDO
         ENDDO
         
c---- Fitness 1 -----
         CALL pearsn(bcor,bflycor,nnfit,fit1)
         
c     ENDIF
      
c---- Linear correlation between magnetic "energy" and monthly ssn -----
      fit2=0.
      IF (iffit2.GT.0.) THEN
         IF(ivrb.GE.2) THEN
            WRITE(*,*) 'Calculating fit2'
            WRITE(1,*) 'Calculating fit2'
         ENDIF
         
c---- Read monthly ssn -----
         IF(ivrb.GE.2) THEN
            WRITE(*,*) 'Opening file: ',ssnfile
            WRITE(1,*) 'Opening file: ',ssnfile
         ENDIF
         OPEN(4,FILE=ssnfile)
         issn=0
         DO WHILE(.TRUE.)
            READ(4,*,END=26) ssnread
            issn=issn+1
            ssnt(issn) =(ssnread(2)-yeari)/dury*nnt
            ssn (issn) = ssnread(4)
         ENDDO
   26    CLOSE(4)
         nssn=issn
         IF(ivrb.GE.2) THEN
            WRITE(*,*) nssn,' ssn read'
            WRITE(1,*) nssn,' ssn read'
         ENDIF
         
c---- Interpolation on temporal grid -----
         issn=1
         DO itt=1,nntp1
            DO WHILE (((issn+1).LT.nssn).AND.((itt-1).GT.ssnt(issn+1)))
               issn=issn+1
            ENDDO
            a = (ssn(issn+1)-ssn(issn))/(ssnt(issn+1)-ssnt(issn))
            b = ssn(issn)-a*ssnt(issn)
            ssne(itt) = b + a*(itt-1)
         ENDDO
         
c---- 1D vector for correlation (fitting region only) -----
         DO itf=1,ntfit
            itt=itf+itfit(1)-1
            ssnfit(itf)= ssne(itt)
         ENDDO
         
c---- Normalization of ssnfit -----
         ssnmax =-1.e10
         DO itf=1,ntfit
            IF(ssnfit(itf).GT.ssnmax) ssnmax=ssnfit(itf)
         ENDDO
         DO itf=1,ntfit
            ssnfit(itf)= ssnfit(itf)/ssnmax
         ENDDO
         
c---- Calculation of magnetic "energy" and 1D vector -----
         DO itf=1,ntfit
            itt=itf+itfit(1)-1
            ebfit(itf) = 0.
            DO j=ixfits(1),ixfits(2)-1
               ebfit(itf) = ebfit(itf) + delmu(j)
     +                            * (0.5*(bfit(itt,j)+bfit(itt,j+1)))**2
            ENDDO
            DO j=ixfitn(1),ixfitn(2)-1
               ebfit(itf) = ebfit(itf) + delmu(j)
     +                            * (0.5*(bfit(itt,j)+bfit(itt,j+1)))**2
            ENDDO
         ENDDO
         
c---- Normalization of ebfit -----
         ebmax  =-1.e10
         DO itf=1,ntfit
            IF(ebfit(itf) .GT. ebmax) ebmax = ebfit(itf)
         ENDDO
         DO itf=1,ntfit
            ebfit(itf) = ebfit(itf)/ebmax
         ENDDO
         
c---- Fitness 2 -----
         CALL pearsn(ebfit,ssnfit,ntfit,fit2)
         
      ENDIF
      
c---- Linear correlation between surface br and surface magnetogram ----
      fit3=0.
      IF (iffit3.GT.0.) THEN
         IF(ivrb.GE.2) THEN
            WRITE(*,*) 'Calculating fit3'
            WRITE(1,*) 'Calculating fit3'
         ENDIF
         
c---- Read surface magnetogram -----
         dbflymin = 1.
         
         IF(ivrb.GE.2) THEN
            WRITE(*,*) 'Opening file: ',magfile
            WRITE(1,*) 'Opening file: ',magfile
         ENDIF
         OPEN(5,FILE=magfile)
         
         READ(5,*) ! Skip header
         READ(5,*) nread
         IF(nread.NE.nntp1) STOP 'Error reading magfile: bad time steps'
         READ(5,*) ! Skip header
         READ(5,*) ! Skip header
         READ(5,*) ! Skip header
         READ(5,*) nread
         IF(nread.NE.nndx) STOP 'Error reading magfile: bad theta steps'
         READ(5,*) magread
         IF((magread(nndx).LT.magread(1)).OR.(magread(nndx).LE.1.))
     +             STOP 'Error reading magfile: theta in wrong order...'
         
         READ(5,*) ! Skip header
         DO itt=1,nntp1
            READ(5,*,END=28) (mbfly(itt,j),j=nndx,1,-1)                 !regular mesh for theta in decreasing order (mu=cos(theta) in increasing order)
         ENDDO
   28    CONTINUE
         IF (itt-1.NE.nntp1) STOP 'Error reading magfile !'
         
         READ(5,*) ! Skip header
         DO itt=1,nntp1
            READ(5,*,END=29) (dbfly(itt,j),j=nndx,1,-1)                 !regular mesh for theta in decreasing order (mu=cos(theta) in increasing order)
         ENDDO
   29    CLOSE(5)
         IF (itt-1.NE.nntp1) STOP 'Error reading magfile !'
         
         DO itt=1,nntp1
c           DO j=1,nndx
            DO j=1,nelx/2
               mbfmx(itt,j) = mbfly(itt,j)+dbfly(itt,j)
               mbfmn(itt,j) = mbfly(itt,j)-dbfly(itt,j)
               IF (dbfly(itt,j).LT.dbflymin) dbfly(itt,j) = dbflymin
            ENDDO
            j=nelx/2+1
               mbfmx(itt,j) = mbfly(itt,j)
               mbfmn(itt,j) = mbfly(itt,j)
               IF (dbfly(itt,j).LT.dbflymin) dbfly(itt,j) = dbflymin
            DO j=nelx/2+2,nndx
               mbfmx(itt,j) = mbfly(itt,j)-dbfly(itt,j)
               mbfmn(itt,j) = mbfly(itt,j)+dbfly(itt,j)
               IF (dbfly(itt,j).LT.dbflymin) dbfly(itt,j) = dbflymin
            ENDDO
         ENDDO
         
c---- Normalization of mbfly (fitting region only) -----
c        mbflymax=-1.e10
c        DO itt=itfit(1),itfit(2)
c        DO j=ixfits(1),ixfits(2)
c           absb=ABS(mbfly(itt,j))
c           IF(absb.GT.mbflymax) mbflymax=absb
c        ENDDO
c        DO j=ixfitn(1),ixfitn(2)
c           absb=ABS(mbfly(itt,j))
c           IF(absb.GT.mbflymax) mbflymax=absb
c        ENDDO
c        ENDDO
c        IF(ivrb.GE.2) THEN
c           WRITE(*,*) 'mbflymax = ',mbflymax
c           WRITE(1,*) 'mbflymax = ',mbflymax
c        ENDIF
c        DO itt=1,nntp1
c        DO j=1,nndx
c           mbfly(itt,j) = mbfly(itt,j)/mbflymax
c        ENDDO
c        ENDDO
         
c---- Normalization of br (fitting region only) -----
c        brmax=-1.e10
c        DO itt=itfit(1),itfit(2)
c        DO j=ixfits(1),ixfits(2)
c           absb=ABS(brsurf(itt,j))
c           IF(absb.GT.brmax) brmax=absb
c        ENDDO
c        DO j=ixfitn(1),ixfitn(2)
c           absb=ABS(brsurf(itt,j))
c           IF(absb.GT.brmax) brmax=absb
c        ENDDO
c        ENDDO
c        IF(ivrb.GE.2) THEN
c           WRITE(*,*) 'brmax = ',brmax
c           WRITE(1,*) 'brmax = ',brmax
c        ENDIF
c        DO itt=1,nntp1
c        DO j=1,nndx
c           brsurf(itt,j) = brsurf(itt,j)/brmax
c        ENDDO
c        ENDDO
         
c---- 1D vector for correlation (fitting region only) -----
         i=0
         DO itt=itfit(1),itfit(2)
         DO j=ixfits(1),ixfits(2)
            i=i+1
            brcor   (i) = brsurf(itt,j)
            mbflycor(i) = mbfly (itt,j)
            dbflycor(i) = dbfly (itt,j)
         ENDDO
         DO j=ixfitn(1),ixfitn(2)
            i=i+1
            brcor   (i) = brsurf(itt,j)
            mbflycor(i) = mbfly (itt,j)
            dbflycor(i) = dbfly (itt,j)
         ENDDO
         ENDDO
         
c---- Latitudinal integrals: 6 regions and axial dipole moment -----
         DO jj=1,3
            jint(1,jj)=INT(region(2*jj-1)*nelx+0.5)+1
            jint(2,jj)=INT(region(2*jj  )*nelx+0.5)+1
            jint(1,6-jj+1)=nndx-jint(2,jj)+1
            jint(2,6-jj+1)=nndx-jint(1,jj)+1
         ENDDO
         
         DO jj=1,6
            intsurf(jj) = 0.
            DO j=jint(1,jj),jint(2,jj)-1
               intsurf(jj) = intsurf(jj) + delmu(j)
            ENDDO
            intsurf(jj)=1./intsurf(jj)
         ENDDO
         
         DO itf=1,ntfit
            itt=itf+itfit(1)-1
            DO jj=1,6
               bintdat(itf,jj) = 0.
               bintdmx(itf,jj) = 0.
               bintdmn(itf,jj) = 0.
               bintsim(itf,jj) = 0.
               DO j=jint(1,jj),jint(2,jj)-1
                  bintdat(itf,jj) = bintdat(itf,jj) + delmu(j)
     +                             * 0.5*(mbfly (itt,j)+mbfly (itt,j+1))
                  bintdmx(itf,jj) = bintdmx(itf,jj) + delmu(j)
     +                             * 0.5*(mbfmx (itt,j)+mbfmx (itt,j+1))
                  bintdmn(itf,jj) = bintdmn(itf,jj) + delmu(j)
     +                             * 0.5*(mbfmn (itt,j)+mbfmn (itt,j+1))
                  bintsim(itf,jj) = bintsim(itf,jj) + delmu(j)
     +                             * 0.5*(brsurf(itt,j)+brsurf(itt,j+1))
               ENDDO
               bintdat(itf,jj) = bintdat(itf,jj)*intsurf(jj)
               bintdmx(itf,jj) = bintdmx(itf,jj)*intsurf(jj)
               bintdmn(itf,jj) = bintdmn(itf,jj)*intsurf(jj)
               bintsig(itf,jj) = ABS(bintdmx(itf,jj)-bintdmn(itf,jj))/2.
               bintsim(itf,jj) = bintsim(itf,jj)*intsurf(jj)
            ENDDO
            daxidat(itf,1) = 0.
            daxidmx(itf,1) = 0.
            daxidmn(itf,1) = 0.
            daxisim(itf,1) = 0.
            daxidat(itf,2) = 0.
            daxidmx(itf,2) = 0.
            daxidmn(itf,2) = 0.
            daxisim(itf,2) = 0.
c           DO j=ixfit(1),ixfit(2)-1
            DO j=1,nelx/2
               daxidat(itf,1) = daxidat(itf,1) + delmu(j)
     +           * 0.5*(mbfly (itt,j)*xtht(j)+mbfly (itt,j+1)*xtht(j+1))
               daxidmx(itf,1) = daxidmx(itf,1) + delmu(j)
     +           * 0.5*(mbfmx (itt,j)*xtht(j)+mbfmx (itt,j+1)*xtht(j+1))
               daxidmn(itf,1) = daxidmn(itf,1) + delmu(j)
     +           * 0.5*(mbfmn (itt,j)*xtht(j)+mbfmn (itt,j+1)*xtht(j+1))
               daxisim(itf,1) = daxisim(itf,1) + delmu(j)
     +           * 0.5*(brsurf(itt,j)*xtht(j)+brsurf(itt,j+1)*xtht(j+1))
            ENDDO
            DO j=nelx/2+1,nelx
               daxidat(itf,2) = daxidat(itf,2) + delmu(j)
     +           * 0.5*(mbfly (itt,j)*xtht(j)+mbfly (itt,j+1)*xtht(j+1))
               daxidmx(itf,2) = daxidmx(itf,2) + delmu(j)
     +           * 0.5*(mbfmx (itt,j)*xtht(j)+mbfmx (itt,j+1)*xtht(j+1))
               daxidmn(itf,2) = daxidmn(itf,2) + delmu(j)
     +           * 0.5*(mbfmn (itt,j)*xtht(j)+mbfmn (itt,j+1)*xtht(j+1))
               daxisim(itf,2) = daxisim(itf,2) + delmu(j)
     +           * 0.5*(brsurf(itt,j)*xtht(j)+brsurf(itt,j+1)*xtht(j+1))
            ENDDO
            daxidat(itf,1) = daxidat(itf,1)*1.5
            daxidmx(itf,1) = daxidmx(itf,1)*1.5
            daxidmn(itf,1) = daxidmn(itf,1)*1.5
            daxisig(itf,1) = ABS(daxidmx(itf,1)-daxidmn(itf,1))/2.
            daxisim(itf,1) = daxisim(itf,1)*1.5
            daxidat(itf,2) = daxidat(itf,2)*1.5
            daxidmx(itf,2) = daxidmx(itf,2)*1.5
            daxidmn(itf,2) = daxidmn(itf,2)*1.5
            daxisig(itf,2) = ABS(daxidmx(itf,2)-daxidmn(itf,2))/2.
            daxisim(itf,2) = daxisim(itf,2)*1.5
         ENDDO
c        WRITE(*,'(a8,/,1000f7.3)')'daxidat=',(daxidat(itf),itf=1,ntfit)
c        WRITE(1,'(a8,/,1000f7.3)')'daxidat=',(daxidat(itf),itf=1,ntfit)
c        WRITE(*,'(a8,/,1000f7.3)')'daxisig=',(daxisig(itf),itf=1,ntfit)
c        WRITE(1,'(a8,/,1000f7.3)')'daxisig=',(daxisig(itf),itf=1,ntfit)
c        WRITE(*,'(a8,/,1000f7.3)')'daxisim=',(daxisim(itf),itf=1,ntfit)
c        WRITE(1,'(a8,/,1000f7.3)')'daxisim=',(daxisim(itf),itf=1,ntfit)
         
c---- Time for polarity inversion -----
         itf=1
         DO WHILE (daxisim(itf+1,1)*daxisim(itf,1).GT.0)
            itf=itf+1
         ENDDO
         tinvsim(1) = FLOAT(itf+itfit(1)-1)/nnt*dury
         itf=ntfit
         DO WHILE (daxisim(itf-1,1)*daxisim(itf,1).GT.0)
            itf=itf-1
         ENDDO
         tinvsim(1) = (tinvsim(1) + FLOAT(itf+itfit(1)-1)/nnt*dury)/2.
         
         itf=1
         DO WHILE (daxisim(itf+1,2)*daxisim(itf,2).GT.0)
            itf=itf+1
         ENDDO
         tinvsim(2) = FLOAT(itf+itfit(1)-1)/nnt*dury
         itf=ntfit
         DO WHILE (daxisim(itf-1,2)*daxisim(itf,2).GT.0)
            itf=itf-1
         ENDDO
         tinvsim(2) = (tinvsim(2) + FLOAT(itf+itfit(1)-1)/nnt*dury)/2.
         
         itf=1
         DO WHILE ((daxisim(itf+1,1)+daxisim(itf+1,2))*
     +             (daxisim(itf  ,1)+daxisim(itf  ,2)).GT.0)
            itf=itf+1
         ENDDO
         tinvsim(3) = FLOAT(itf+itfit(1)-1)/nnt*dury
         itf=ntfit
         DO WHILE ((daxisim(itf-1,1)+daxisim(itf-1,2))*
     +             (daxisim(itf  ,1)+daxisim(itf  ,2)).GT.0)
            itf=itf-1
         ENDDO
         tinvsim(3) = (tinvsim(3) + FLOAT(itf+itfit(1)-1)/nnt*dury)/2.
         
c---- 1D vectors for correlation -----
         i=0
         DO jj=1,6
            DO itf=1,ntfit
               i=i+1
               bcordat(i)=bintdat(itf,jj)
               bcorsim(i)=bintsim(itf,jj)
            ENDDO
         ENDDO
         
c---- Spectral analysis -----
c     (cycle is copied with opposite sign to mimic periodicity)
c     (CMPLX(2R)=>Complex, REAL(C)=>Real part, AIMAG(C)=>Imaginary part)
         DO j=1,nndx
            
            DO itf=1,ntfit-1
               itt=itf+itfit(1)-1
               fftdat(        itf) = CMPLX( mbfly (itt,j),0.)
               fftdat(ntfit-1+itf) = CMPLX(-mbfly (itt,j),0.)
               fftsim(        itf) = CMPLX( brsurf(itt,j),0.)
               fftsim(ntfit-1+itf) = CMPLX(-brsurf(itt,j),0.)
            ENDDO
            CALL cffti(2*ntfit-2,wsave)
            CALL cfftf(2*ntfit-2,fftdat,wsave)
            CALL cfftf(2*ntfit-2,fftsim,wsave)
            fftdat(:) = fftdat(:)/(2*ntfit-2)
            fftsim(:) = fftsim(:)/(2*ntfit-2)
            
            fftdat1(1,j) = REAL(fftdat(2))
            fftdat1(2,j) =AIMAG(fftdat(2))
            fftsim1(1,j) = REAL(fftsim(2))
            fftsim1(2,j) =AIMAG(fftsim(2))
            
            jj=0
            DO itf=nfft(1)+1,nfft(2)+1,2
               jj=jj+1
               fftdatp(jj,j)=ATAN2(AIMAG(fftdat(itf)),REAL(fftdat(itf)))
               fftsimp(jj,j)=ATAN2(AIMAG(fftsim(itf)),REAL(fftsim(itf)))
            ENDDO
            
         ENDDO
         
         DO jj=1,nnfft
            ddatp=0
            dsimp=0
            fftdatpt(jj,1) = fftdatp(jj,1)+ddatp
            fftsimpt(jj,1) = fftsimp(jj,1)+dsimp
            DO j=2,nndx
               IF(fftdatp(jj,j)-fftdatp(jj,j-1).GT. pi) ddatp=ddatp-2*pi
               IF(fftdatp(jj,j)-fftdatp(jj,j-1).LT.-pi) ddatp=ddatp+2*pi
               IF(fftsimp(jj,j)-fftsimp(jj,j-1).GT. pi) dsimp=dsimp-2*pi
               IF(fftsimp(jj,j)-fftsimp(jj,j-1).LT.-pi) dsimp=dsimp+2*pi
               fftdatpt(jj,j) = fftdatp(jj,j)+ddatp
               fftsimpt(jj,j) = fftsimp(jj,j)+dsimp
            ENDDO
            DO j=1,nelx/2+1
               fftdatp(jj,j) = fftdatpt(jj,j)-fftdatpt(jj,ixfits(1))
               fftsimp(jj,j) = fftsimpt(jj,j)-fftsimpt(jj,ixfits(1))
            ENDDO
            DO j=nelx/2+2,nndx
               fftdatp(jj,j) = fftdatpt(jj,j)-fftdatpt(jj,ixfitn(2))
               fftsimp(jj,j) = fftsimpt(jj,j)-fftsimpt(jj,ixfitn(2))
            ENDDO
         ENDDO
         
c---- Fitness 3 -----
         CALL pearsn(brcor,mbflycor,nnfit,fit3cor2)
         
         DO jj=1,6
         CALL pearsn(bintsim(:,jj),bintdat(:,jj),ntfit,fit3cor(jj))
         ENDDO
         CALL pearsn(bcorsim,bcordat,6*ntfit,fit3cor6)
         
         DO jj=1,2
         CALL pearsn(daxisim(:,jj),daxidat(:,jj),ntfit,fit3cordd(jj))
         ENDDO
         CALL pearsn(daxisim(:,1)+daxisim(:,2),daxidat(:,1)+daxidat(:,2)
     +                                                  ,ntfit,fit3cord)
         
         DO jj=1,2
         CALL pearsn(fftsim1(jj,:),fftdat1(jj,:),nndx,fit3corfft11(jj))
         ENDDO
         fit3corfft1=(fit3corfft11(1)+fit3corfft11(2))/2.
         fit3corfftp(1)=0.
         fit3corfftp(2)=0.
         DO jj=1,nnfft
         CALL pearsn(fftsimp(jj,ixfits(1):ixfits(2)),
     +        fftdatp(jj,ixfits(1):ixfits(2)),nxfits,fit3corfftpp(1,jj))
         fit3corfftp(1) = fit3corfftp(1)+fit3corfftpp(1,jj)
         CALL pearsn(fftsimp(jj,ixfitn(1):ixfitn(2)),
     +        fftdatp(jj,ixfitn(1):ixfitn(2)),nxfitn,fit3corfftpp(2,jj))
         fit3corfftp(2) = fit3corfftp(2)+fit3corfftpp(2,jj)
         ENDDO
         fit3corfftp(1) = fit3corfftp(1)/nnfft
         fit3corfftp(2) = fit3corfftp(2)/nnfft
         
         fit3chi2 = 0.
         DO i=1,nnfit
            fit3chi2 = fit3chi2+(brcor(i)-mbflycor(i))**2
     +                                                   /dbflycor(i)**2!cfit
         ENDDO
         fit3chi2 = (fit3chi2/nnfit)**(0.5)
         
         DO jj=1,6
         fit3chi(jj) = 0.
         DO itf=1,ntfit
            fit3chi(jj)=fit3chi(jj)+(bintsim(itf,jj)-bintdat(itf,jj))**2
cfit +                                               /bintsig(itf,jj)**2!cfit
         ENDDO
         fit3chi(jj) = (fit3chi(jj)/ntfit)**(0.5)
         ENDDO
         fit3chi6 = 0.
         DO jj=1,6
            fit3chi6 = fit3chi6 + fit3chi(jj)/6.
         ENDDO
         
         DO jj=1,2
         fit3chidd(jj) = 0.
         DO itf=1,ntfit
        fit3chidd(jj)=fit3chidd(jj)+(daxisim(itf,jj)-daxidat(itf,jj))**2
cfit +                                               /daxisig(itf,jj)**2!cfit
         ENDDO
         fit3chidd(jj) = (fit3chidd(jj)/ntfit)**(0.5)
         ENDDO
         fit3chid = 0.
         DO itf=1,ntfit
            fit3chid=fit3chid+((daxisim(itf,1)+daxisim(itf,2))-
     +                               (daxidat(itf,1)+daxidat(itf,2)))**2
cfit +                               /(daxisig(itf,1)+daxisig(itf,2))**2!cfit
         ENDDO
         fit3chid = (fit3chid/ntfit)**(0.5)
         
         DO jj=1,2
            fit3chifft11(jj)=0.
            DO j=1,nndx
      fit3chifft11(jj)=fit3chifft11(jj)+(fftsim1(jj,j)-fftdat1(jj,j))**2
            ENDDO
            fit3chifft11(jj)=(fit3chifft11(jj)/nndx)**(0.5)
         ENDDO
         fit3chifft1=(fit3chifft11(1)+fit3chifft11(2))/2.
         fit3chifftp(1)=0.
         fit3chifftp(2)=0.
         DO jj=1,nnfft
            fit3chifftpp(1,jj)=0.
            fit3chifftpp(2,jj)=0.
            DO j=ixfits(1),ixfits(2)
               fit3chifftpp(1,jj)=fit3chifftpp(1,jj)+
     +                                  (fftsimp(jj,j)-fftdatp(jj,j))**2
            ENDDO
            DO j=ixfitn(1),ixfitn(2)
               fit3chifftpp(2,jj)=fit3chifftpp(2,jj)+
     +                                  (fftsimp(jj,j)-fftdatp(jj,j))**2
            ENDDO
            fit3chifftpp(1,jj)=(fit3chifftpp(1,jj)/nxfits)**(0.5)
            fit3chifftpp(2,jj)=(fit3chifftpp(2,jj)/nxfitn)**(0.5)
            fit3chifftp(1)=fit3chifftp(1)+fit3chifftpp(1,jj)
            fit3chifftp(2)=fit3chifftp(2)+fit3chifftpp(2,jj)
         ENDDO
         fit3chifftp(1) = fit3chifftp(1)/nnfft
         fit3chifftp(2) = fit3chifftp(2)/nnfft
         
cfit     fit3 = (fit3corfft1 + (fit3corfftp(1)+fit3corfftp(2))/2.)/2.
cfit     fit3 = (fit3chi2 + (fit3chi(2)+fit3chi(5))/2. + fit3chid)/3.
cfit     fit3 = ((fit3chi(2)**(-1)+fit3chi(5)**(-1))/2+fit3chid**(-1))/2
cfit     fit3 =(((fit3chi  (2)**2+fit3chi  (5)**2)/2
c    +          + fit3chid    **2                   )/2)**(-1)
cfit     fit3 =(((fit3chi  (2)**2+fit3chi  (5)**2)/2
c    +          +(fit3chidd(1)**2+fit3chidd(1)**2)/2)/2)**(-1)
cfit     fit3 =(( fit3chi2    **2                   )/1)**(-1)
cfit     fit3 =(((fit3chi  (2)**2+fit3chi  (5)**2)/2)/1)**(-1)
cfit     fit3 =(( fit3chi2    **2
c    +          + fit3chid    **2                   )/2)**(-1)
cfit     fit3 =(( fit3chi2    **2
c    +          +(fit3chi  (2)**2+fit3chi  (5)**2)/2
c    +          + fit3chid    **2                   )/3)**(-1)
cfit     fit3 =(( fit3chi2    **2
c    +          + fit3chi  (2)**2+fit3chi  (5)**2
c    +          + fit3chid    **2+fit3chid    **2   )/5)**(-1)
cfit     fit3 =(( fit3chi2    **2
c    +          +(fit3chi  (2)**2+fit3chi  (5)**2)/2
c    +          +(fit3chidd(1)**2+fit3chidd(1)**2)/2)/3)**(-1)
         fit3 =(( fit3chi2    **2
     +          + fit3chi  (2)**2+fit3chi  (5)**2
     +          + fit3chid    **2                   )/4)**(-1)
cfit     fit3 =(( fit3chid    **2                   )/1)**(-1)
         
c        IF (fit3.GE.0.42) THEN
c           fit3 = daxisim(ntfit,1)+daxisim(ntfit,2)
c           fit3 = tinvsim(2)-tinvsim(1)
c        ELSE
c           fit3 = -10.
c        ENDIF
         
      ENDIF
      
c---- FITNESS ----------------------------------------------------------
      IF(ivrb.GE.2) THEN
         WRITE(*,322)
         WRITE(1,322)
  322    FORMAT(72('*'),/,'FITNESS:',f8.2,' s',/)
         WRITE(*,*) 'itfit =',itfit(1),itfit(2)
         WRITE(1,*) 'itfit =',itfit(1),itfit(2)
         WRITE(*,*) 'ixfit =',ixfits(1),ixfits(2),ixfitn(1),ixfitn(2)
         WRITE(1,*) 'ixfit =',ixfits(1),ixfits(2),ixfitn(1),ixfitn(2)
         
         WRITE(*,*) 'daxisim(ntfit,south) =',daxisim(ntfit,1)
         WRITE(1,*) 'daxisim(ntfit,south) =',daxisim(ntfit,1)
         WRITE(*,*) 'daxisim(ntfit,north) =',daxisim(ntfit,2)
         WRITE(1,*) 'daxisim(ntfit,north) =',daxisim(ntfit,2)
         WRITE(*,*) 'daxisim(ntfit) =',daxisim(ntfit,1)+daxisim(ntfit,2)
         WRITE(1,*) 'daxisim(ntfit) =',daxisim(ntfit,1)+daxisim(ntfit,2)
         WRITE(*,*) 'tinvsim(south) =',tinvsim(1)
         WRITE(1,*) 'tinvsim(south) =',tinvsim(1)
         WRITE(*,*) 'tinvsim(north) =',tinvsim(2)
         WRITE(1,*) 'tinvsim(north) =',tinvsim(2)
         WRITE(*,*) 'tinvsim =',tinvsim(3)
         WRITE(1,*) 'tinvsim =',tinvsim(3)
         WRITE(*,*) 'tinvsim(north-south) =',tinvsim(2)-tinvsim(1)
         WRITE(1,*) 'tinvsim(north-south) =',tinvsim(2)-tinvsim(1)
         
         WRITE(*,*) 'fit1 =',fit1
         WRITE(1,*) 'fit1 =',fit1
         WRITE(*,*) 'fit2 =',fit2
         WRITE(1,*) 'fit2 =',fit2
         WRITE(*,*) 'fit3 cor 2d =',fit3cor2
         WRITE(1,*) 'fit3 cor 2d =',fit3cor2
         DO jj=1,6
            WRITE(*,*) 'fit3 cor, region',jj,' =',fit3cor(jj)
            WRITE(1,*) 'fit3 cor, region',jj,' =',fit3cor(jj)
         ENDDO
         WRITE(*,*) 'fit3 cor, all 6 regions =',fit3cor6
         WRITE(1,*) 'fit3 cor, all 6 regions =',fit3cor6
         WRITE(*,*) 'fit3 cor, axial dipole (south) =',fit3cordd(1)
         WRITE(1,*) 'fit3 cor, axial dipole (south) =',fit3cordd(1)
         WRITE(*,*) 'fit3 cor, axial dipole (north) =',fit3cordd(2)
         WRITE(1,*) 'fit3 cor, axial dipole (north) =',fit3cordd(2)
         WRITE(*,*) 'fit3 cor, axial dipole moment  =',fit3cord
         WRITE(1,*) 'fit3 cor, axial dipole moment  =',fit3cord
         WRITE(*,*) 'fit3 cor, fft n=1  =',fit3corfft1
         WRITE(1,*) 'fit3 cor, fft n=1  =',fit3corfft1
         DO jj=1,nnfft
           WRITE(*,*) 'fit3 cor, fft n=?(south) =',fit3corfftpp(1,jj)
           WRITE(1,*) 'fit3 cor, fft n=?(south) =',fit3corfftpp(1,jj)
           WRITE(*,*) 'fit3 cor, fft n=?(north) =',fit3corfftpp(2,jj)
           WRITE(1,*) 'fit3 cor, fft n=?(north) =',fit3corfftpp(2,jj)
         ENDDO
         WRITE(*,*) 'fit3 chi 2d =',fit3chi2
         WRITE(1,*) 'fit3 chi 2d =',fit3chi2
         DO jj=1,6
            WRITE(*,*) 'fit3 chi, region',jj,' =',fit3chi(jj)
            WRITE(1,*) 'fit3 chi, region',jj,' =',fit3chi(jj)
         ENDDO
         WRITE(*,*) 'fit3 chi, all 6 regions =',fit3chi6
         WRITE(1,*) 'fit3 chi, all 6 regions =',fit3chi6
         WRITE(*,*) 'fit3 chi, axial dipole (south) =',fit3chidd(1)
         WRITE(1,*) 'fit3 chi, axial dipole (south) =',fit3chidd(1)
         WRITE(*,*) 'fit3 chi, axial dipole (north) =',fit3chidd(2)
         WRITE(1,*) 'fit3 chi, axial dipole (north) =',fit3chidd(2)
         WRITE(*,*) 'fit3 chi, axial dipole moment =',fit3chid
         WRITE(1,*) 'fit3 chi, axial dipole moment =',fit3chid
         WRITE(*,*) 'fit3 chi, fft n=1  =',fit3chifft1
         WRITE(1,*) 'fit3 chi, fft n=1  =',fit3chifft1
         DO jj=1,nnfft
           WRITE(*,*) 'fit3 chi, fft n=?(south) =',fit3chifftpp(1,jj)
           WRITE(1,*) 'fit3 chi, fft n=?(south) =',fit3chifftpp(1,jj)
           WRITE(*,*) 'fit3 chi, fft n=?(north) =',fit3chifftpp(2,jj)
           WRITE(1,*) 'fit3 chi, fft n=?(north) =',fit3chifftpp(2,jj)
         ENDDO
         WRITE(*,*) 'fit3 =',fit3
         WRITE(1,*) 'fit3 =',fit3
      ENDIF
      
      fit= 1. + iffit1*fit1 + iffit2*fit2 + iffit3*fit3
      
c---- Other fitnesses -----
c     fit=0.
c     DO j=1,nndx
c        DO itt=itfit(1),itfit(2)
c           bphifit= bphit(itt,j)/bphimax
c           bflyfit= bfly (itt,j)
c           IF (bflyfit.EQ.0.) THEN
c              bflyfit = -2*meanbfly
c           ENDIF
c           fit = fit + bphifit*bflyfit
            
c           fit = TOTAL( byfit         *brfit           )
c           fit = TOTAL( byfit[indexby]*brfit  [indexby])
c           fit = TOTAL( byfit[indexby]*brfit  [indexby])/countby
c           fit = TOTAL( byfit         *brlisse         )
c           fit = TOTAL( byfit[indexby]*brlisse[indexby])/countby
c           fit = TOTAL((byfit-brlisse)^2)
c           fit = TOTAL((byfit-brfit  )^2)
c           fit = TOTAL( byabs         *bfly            )
c           fit = TOTAL( byabs  [indexby]*bfly   [indexby])
c           fit = TOTAL( byabs  [indexby]*bflyfit[indexby])
c           fit = TOTAL( bfly   [indexby])
c           fit = TOTAL( bflyfit[indexby])
c           fit = TOTAL((byabs-bfly   )^2)
c        ENDDO
c     ENDDO     
c---- Avoid negative fitness
c     fit=fit+10000.
c     IF (fit.LT.0.) fit = 0.
      
      RETURN
      
      END SUBROUTINE
c=======================================================================

c***********************************************************************
c     INCLUDE FILES
c
c     subs.random.f       --> needed for stochastically forced version
c     subs.erf.f          --> Numerical Recipes routines needed for error function
c     subs.pearsn_modif.f --> Pearson linear correlation coefficient (Numerical Recipes)
c     fct.strlen.f        --> finds true length of a string (remove blanks)
c***********************************************************************
      
      INCLUDE 'bldyn_includefiles/subs.random.f'
      INCLUDE 'bldyn_includefiles/subs.rand1om.f'
      INCLUDE 'bldyn_includefiles/subs.rand2om.f'
      INCLUDE 'bldyn_includefiles/subs.rand3om.f'
      INCLUDE 'bldyn_includefiles/subs.rand4om.f'
      INCLUDE 'bldyn_includefiles/subs.erf.f'
      INCLUDE 'bldyn_includefiles/subs.pearsn_modif.f'
c     INCLUDE 'bldyn_includefiles/subs.fftn2.f'
      INCLUDE 'bldyn_includefiles/subs.fftpack.f'
      INCLUDE 'bldyn_includefiles/fct.strlen.f'
      INCLUDE 'surfbr2.sub.f'
      
c=======================================================================
