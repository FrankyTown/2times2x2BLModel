#! /bin/bash
#
########################################################################
# Script :
#  1- compile le code fortran call_bldynsurfbr.f (avec bldynsurfbr.f)
#  2- fournit les entrées nécessaires à son exécution
########################################################################

EXEDIR='.'
#if [ "$1" ] ; then
#   DATETIME=$1
#else
DATETIME=`date +%Y%m%d%H%M%S`
#  DATETIME='20170704013110'
#fi

MAINCOD='call_bldynsurfbr.f'
FORCOD='bldynsurfbr.f'
SURFCOD='surfbr2.sub.f'

# Paramètres de la simulation ##########################################
#
#     incrfile : fichier de conditions initiales (".rst") (Entrer '0' pour ne pas utiliser de fichier)
#     incrsurf : fichier de conditions initiales (".rst") en surface (Entrer '0' pour ne pas utiliser de fichier)

#     ycyc= [1701.       , 1713.       , 1724.       , 1732.       ,
#    +       1744.       , 1755.+ 2./12, 1766.+ 5./12, 1775.+ 5./12,
#    +       1784.+ 7./12, 1798.+ 4./12, 1810.+11./12, 1823.+ 4./12,
#    +       1833.+10./12, 1843.+ 6./12, 1855.+11./12, 1867.+ 2./12,
#    +       1878.+11./12, 1890.+ 2./12, 1902.+ 1./12, 1913.+ 7./12,
#    +       1923.+ 7./12, 1933.+ 8./12, 1944.+ 1./12, 1954.+ 3./12,
#    +       1964.+ 9./12, 1976.+ 5./12, 1986.+ 8./12, 1996.+ 4./12,
#    +       2008.+11./12]
ANBEG=''
#ANBEG='_1976.7'

#Simulation duration (e.g. use FSTCYC=21, NCYC=1, NREP=32 for a ~320 years simulation (~32 cycles)):
FSTCYC=21
NCYC=1
NREP=32                                                         #Francois

#Percentage of time span for fitness calculation OR bmr emergences
TFITF='000'
TFITF2='100'                                                           #Francois End of cycle 23 058
TFITF=$TFITF' '$TFITF2                                                  #Francois

#Dynamo simulation grid:
NELX=128
NELY=96
#NT=100
NT=$((100*NCYC*NREP))
NSTEP=1
NNT=$((NT*NSTEP))

#Surface simulation grid:
NESX=128
NESY=128
NTS=1
#NSTEPS=1000
NSTEPS=$((1000*NCYC*NREP))

#XFITDEF='0.500 0.000 0.500 0.500 0.100 0.000 0.000 0.200'              #~0.82
#XFITDEF='0.520 0.130 0.430 0.510 0.180 0.000 0.000 0.200'              #~good
#XFITDEF='0.550 0.000 0.400 0.500 0.100 0.000 0.000 0.150'              #~good
#XFITDEF='0.500 0.000 0.480 0.500 0.100 0.000 0.000 0.200'              #~0.87
#XFITDEF='0.580 0.000 0.400 0.500 0.100 0.000 0.000 0.170'              #(Final WS8 solution:0.92)
#XFITDEF='0.670 0.500 0.400 0.500 0.200 0.000 0.050 0.200'              #(Test near WS8)
#XFITDEF='0.380 0.320 0.000 0.220 0.450'                                #Surface solution bld_48x200_wangal_21x08_0uqwetf3sxtd_10
#XFITDEF='0.410 0.620 0.020 0.960 0.290 0.980 0.080'                    #Surface solution blds_48x200_wangal_21x01s0f_uqvwetaf3xtd_15
#XFITDEF='0.450 0.580 0.000 0.950 0.330 0.940 0.290'                    #Surface solution blds_48x200_wangal_21x01s5_uqvwetaf3x2td_10
#XFITDEF='0.240 0.510 0.320 0.950 0.310 0.980 0.350'                    #Surface solution blds_48x500_wangal_21x01s0_uqvwetaf3xx2ttd_10
#XFITDEF='0.280 0.100 0.200 0.300 0.400 0.272'
#XFITDEF='0.190 0.060 0.470 0.830 0.420 0.010 0.130 0.950 0.000 0.970'  #Optimal
#XFITDEF='0.250 0.580 0.410 0.970 0.040 0.020 0.010 0.720 0.000 0.510 0.180 0.110 0.480 0.020 0.790 0.560 0.140 0.390' #Optimal at C=0.936 (f=0.19356277E+01), from wangal_21x08_dEeRpqvwW21uetBbac0xrf1_12
#XFITDEF='0.170 0.490 0.390 0.140 0.480 0.090 0.000 0.750 0.010 0.140 0.350 0.080 0.590 0.080 0.820 0.190 0.070 0.370' #Example at C=0.90 (f=0.18997188E+01), from wangal_21x08_dEeRpqvwW21uetBbac0xrf1_11
#XFITDEF='0.190 0.390 0.400 0.350 0.570 0.190 0.100 0.450 0.010 0.130 0.240 0.000 0.680 0.070 0.810 0.210 0.040 0.450' #Example at C=0.88 (f=0.18781960E+01), from wangal_21x08_dEeRpqvwW21uetBbac0xrf1_11

#XFITDEF='0.170 0.130 0.460 0.880 0.030 0.020 0.030 0.970 0.000 0.970 0.120' #Optimal at C=0.937 (f=0.19371686E+01), from wangal_21x08_dEeRpqvwW21uetf1_10
#XFITDEF='0.130 0.450 0.480 0.850 0.090 0.020 0.010 0.950 0.010 0.900 0.220' #Optimal at C=0.938 (f=0.19379556E+01), from wangal_21x08_dEeRpqvwW21uetf1_12
#XFITDEF='0.120 0.180 0.480 0.910 0.000 0.030 0.020 0.910 0.000 0.930 0.120' #Optimal at C=0.938 (f=0.19378800E+01), from wangal_21x08_dEeRpqvwW21uetf1_13
#XFITDEF='0.080 0.570 0.470 0.640 0.220 0.040 0.930 0.930 0.030 0.510 0.160' #Example at C=0.921 (f=0.19214967E+01), from wangal_21x08_dEeRpqvwW21uetf1_12
#XFITDEF='0.090 0.590 0.480 0.630 0.220 0.040 0.830 0.950 0.010 0.440 0.260' #Example at C=0.910 (f=0.19101726E+01), from wangal_21x08_dEeRpqvwW21uetf1_12
#XFITDEF='0.180 0.450 0.390 0.660 0.350 0.140 0.680 0.820 0.030 0.820 0.340' #Example at C=0.907 (f=0.19068506E+01), from wangal_21x08_dEeRpqvwW21uetf1_12
#XFITDEF='0.110 0.450 0.450 0.830 0.080 0.020 0.090 0.950 0.010 0.900 0.220' #Example at C=0.905 (f=0.19052051E+01), from wangal_21x08_dEeRpqvwW21uetf1_12
#XFITDEF='0.010 0.110 0.440 0.660 0.350 0.140 0.680 0.820 0.030 0.820 0.340' #Example at C=0.883 (f=0.18831812E+01), from wangal_21x08_dEeRpqvwW21uetf1_12
#XFITDEF='0.167 0.250 0.500 0.900 0.100 0.000 0.000 0.936 0.000 0.900 0.235' #Chosen optimal at C=0.934

XFITDEF='0.000'

#XFITDEF='0.500 0.000 0.625 0.200 0.200 0.000 0.000 1.000'
#XFITDEF='0.99999 0.00000 0.50000 0.50000 0.99999 0.00000 0.00000 0.62500'

# Paramètres de contrôle ###############################################

#Control parameters for Pikaia (use '1' if optimal model parameters already set in the code)
NPAR=1
#NPAR=11
NPOP=1
#NPOP=96
NGEN=1
#NGEN=500
#NDIG=1

# Types of input BMR in surfbr (0 to deactivate, 1 to activate (1st: BMRs from database, 2nd: random BMRs, 3rd: self-generated BMRs))
# (4 rows for: 1VisibleNorthern, 2HiddenNorthern, 3VisibleSouthern, 4HiddenSouthern half-hemispheres)
BMRTYPE1='0 0 1'
BMRTYPE2='0 0 1'
BMRTYPE3='0 0 1'
BMRTYPE4='0 0 1'
BMRTYPE=$BMRTYPE1' '$BMRTYPE2' '$BMRTYPE3' '$BMRTYPE4                   #Francois
# Seeds for random generator
# (4 values for: 1VisibleNorthern, 2HiddenNorthern, 3VisibleSouthern, 4HiddenSouthern half-hemispheres)
BLDSEED1='41'
BLDSEED2='41'
BLDSEED3='41'
BLDSEED4='41'
BLDSEED=$BLDSEED1' '$BLDSEED2' '$BLDSEED3' '$BLDSEED4                   #Francois
#BLDSEED='1234 1234 1234 1234'

# New seed for reinitialization of random generator after tfitf(2)
# (4 values for: Visible, Hidden, Northern, Southern hemispheres)
# (BLDSEEDD='0' to stop emergences after tfitf(2))

BLDSEEDD1='34'
BLDSEEDD2='34'
BLDSEEDD3='34'
BLDSEEDD4='34'
BLDSEEDD=$BLDSEEDD1' '$BLDSEEDD2' '$BLDSEEDD3' '$BLDSEEDD4              #Francois

# Identification de la simulation (PIKSEED) a analyser
# ('0'-'9' ou '100'-... pour valeurs par defaut, '10'-'99' pour simulations individuelles,'m' pour la moyenne sur les PIKSEEDs, 'w' pour moyenne sur toutes les databases)
#SIMID='1111'
SIMID=$BLDSEED1$BLDSEED2$BLDSEED3$BLDSEED4     #Francois
#LENSIMID=$(echo -n $SIMID | wc -m)
#if [ "$LENSIMID" -ge 5 ] ; then
#	SIMID=$BLDSEED1$BLDSEED2
#fi

#if [ "$1" ] ; then
#   SIMID=$SIMID'_'$1
#fi

# Nom de la simulation
if [ "$NREP" -ge 10 ] ; then
   SIM=$FSTCYC'x'$NREP
else
   SIM=$FSTCYC'x0'$NREP
fi
SIMPREF='bldf070q15_'$SIM
#SIMPREF='wangal_'$SIM's0'$ANBEG
#SIMPREF='wangal_'$SIM''$ANBEG
#SIMPREF='szzzz_'$SIM''$ANBEG
SIMNAM=$SIMPREF''
#SIMNAM=$SIMPREF'_optimal'
#SIMNAM=$SIMPREF'_miesch2014'
#SIMNAM=$SIMPREF'_dEeRpqvwW21uetf1'
AFORMAT='a'${#SIMPREF}

# Synthetic database seed: '0001' for random choice by Pikaia, '....' for choice imposed here
if [ `echo "$SIMNAM" | grep -c 'szzzz'` -eq 1 ] ; then
   DATSEEDV='0001'
#  DATSEEDV='0221 1234 2345 3456 4567'
else
   DATSEEDV='0'
fi

#XFORMAT="/,f13.3"
XFORMAT="/,f13.3,"$((NPAR-1))"(',',f5.3),']'"
#XFORMAT="/,f15.5,8(',',f7.5),']'"
#XFORMAT="/,f15.5,7(',',f7.5),',',/,f15.5,']'"
#XFORMAT="4(/,f15.5,','),/,f15.5,6(',',f7.5),']'"
#XFORMAT="4(/,f15.5,1(',',f7.5),','),/,f15.5,3(',',f7.5),']'"
#XFORMAT="7(/,f15.5,7(',',f7.5),','),/,f15.5,7(',',f7.5),']'"

# Noms #################################################################

#EXENAME='blds_'$SIMNAM'_'$SIMID
EXENAME='blds_'$SIMNAM'_'$SIMID'_'$DATETIME                             #(Francois)
#EXENAME='blds_'$NESX'x'$NESY'_'$SIMNAM'_'$SIMID
#EXENAME='blds_'$NT'x'$NELX'x'$NELY's'$NSTEPS'x'$NESX'x'$NESY'_'$SIMNAM'_'$SIMID
#EXENAME='blds_'$NELX'x'$NELY'x'$NESY'_'$SIMNAM
FORCODE=$EXENAME'.f'
EXENAMES=$EXENAME's'
SURFCODE=$EXENAMES'.f'
MAINCODE=$EXENAME'_call.f'
INPUT=$EXENAME'.in'

cp $FORCOD $FORCODE
sed -e "s#bldyn_param#$EXENAME#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#nelx=64, nely=96#nelx=$NELX, nely=$NELY#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#nesxvrai=256, nesyvrai=128#nesxvrai=$NESX, nesyvrai=$NESY#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#nt=100, nstep=10#nt=$NT, nstep=$NSTEP#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#nts=1, nsteps=1000#nts=$NTS, nsteps=$NSTEPS#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#'_',a12#'_',$AFORMAT#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#FORMAT(a14,64f8.5)#FORMAT(a14,$XFORMAT)#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#= ',1f9.2#= ',${NCYC}f9.2#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#= ',1e9.2#= ',${NCYC}e9.2#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#$SURFCOD#$SURFCODE#g" $FORCODE > tmp
mv tmp $FORCODE
sed -e "s#CALL GETENV('SCRATCH',scratch)#scratch = '$SCRATCH'#g" $FORCODE > tmp
mv tmp $FORCODE

cp $SURFCOD $SURFCODE
sed -e "s#surfbr2_param#$EXENAMES#g" $SURFCODE > tmp
mv tmp $SURFCODE
sed -e "s#nelx=64#nelx=$NELX#g" $SURFCODE > tmp
mv tmp $SURFCODE
sed -e "s#nesxvrai=256, nesyvrai=128#nesxvrai=$NESX, nesyvrai=$NESY#g" $SURFCODE > tmp
mv tmp $SURFCODE
sed -e "s#nt=100, nstep=10#nt=$NT, nstep=$NSTEP#g" $SURFCODE > tmp
mv tmp $SURFCODE
sed -e "s#nts=1, nsteps=1000#nts=$NTS, nsteps=$NSTEPS#g" $SURFCODE > tmp
mv tmp $SURFCODE
sed -e "s#'_',a12#'_',$AFORMAT#g" $SURFCODE > tmp
mv tmp $SURFCODE
sed -e "s#CALL GETENV('SCRATCH',scratch)#scratch = '$SCRATCH'#g" $SURFCODE > tmp
mv tmp $SURFCODE

SIMDIR=$EXEDIR'/xesults'
LOGDIR=$EXEDIR'/xesults'
DATDIR=$EXEDIR'/0-data'

#LOGFIL=$LOGDIR'/'$SIMNAM'/blds_'$NPOP'x'$NGEN'_'$SIMNAM
LOGFIL=$LOGDIR'/'$SIMNAM'/blds_128x128_'$NPOP'_'$SIMNAM
#LOGFIL=$LOGDIR'/'$SIMNAM'/bldyn0302_n'$NPAR'np'$NPOP'i'$AMPCND'_'$SIMNAM
#LOGFIL=$LOGDIR'/'$SIMNAM'/bldyn0302_'$NT'x'$NSTEP'_'$NELX'x'$NELY'_n'$NPAR'np'$NPOP'_'$SIMNAM

#if [ `echo "$SIMNAM" | grep -c 'wang'` -eq 1 ] ; then
BMRFIL=$DATDIR'wang_alex_'$SIM$ANBEG'.dat' 
#else
#if [ `echo "$SIMNAM" | grep -c 'szzzz'` -eq 1 ] ; then
#   BMRFIL=$DATDIR'/3-arnaud/synth_wolf_bmr_cycle'$SIM$ANBEG'.dat'
#else
#   BMRFIL='0'
#  echo 'BMRFIL error ! Exiting script...'
#  exit
#fi
#fi

MAGFIL=$DATDIR'/1-hathaway/magbflyData_'$SIM'_'$NNT'x'$NELX'_smth 0-0_0'$ANBEG'.dat'

#INCRFIL='0'
INCRFIL=$EXEDIR'/0blds0302_sample-initial-condition_128x96.rst'
#/manyInit _14
#INCRFIL=$EXEDIR'/xesults/0blds0302_ 800x 1xs 8000_128x96xs128_Rm 130ybl0.051etac 7.2etat11.9u16.3rbt0.60m1.50p  0.16q 1.6v7.2w 1.0eta 641tau32a 9n 8_fit0.75_wangal_21x08.0_0.rst'
#INCRFIL=$EXEDIR'/xesults/0blds0302_ 800x 1xs 4000_ 64x96xs 64_Rm 130ybl0.051etac 7.2etat11.9u16.3rbt0.60m1.50p  0.16q 1.6v7.2w 1.0eta 641tau32a 9n 8_fit0.75_wangal_21x08.0_0.rst'
#INCRFIL=$EXEDIR'/xesults/0blds0302_ 100x 1xs 500_ 64x96xs 64_Rm 130ybl0.051etac 7.2etat11.9u16.3rbt0.60m1.50p 0.16q 1.6v7.2w 1.0eta 641tau32a 9n 8_fit0.10_wangal_21x01.0_11.rst'
INCRSUR='0' #$EXEDIR'/something.s.rst'
#INCRSUR=$EXEDIR'/xesults/0surfbr2s_  1x 8000_256x128_32x0fant_u16.3q 1.6v7.2w 1.0eta 641tau32a 9n 8wdth4_wangal_21x08.0_0.rst'
#INCRSUR=$EXEDIR'/xesults/0surfbr2s_  1x 4000_128x 64_16x0fant_u16.3q 1.6v7.2w 1.0eta 641tau32a 9n 8wdth4_wangal_21x08.0_0.rst'
#INCRSUR=$EXEDIR'/xesults/0surfbr2s_  1x 2000_128x 64_16x0fant_u17.0q 1.0v1.0w 1.0eta 350tau 8a 9n 8wdth4_wang00_21x04s0.0_0.rst'
#INCRSUR=$EXEDIR'/../0-data/hathaway/magbflyData_'$SIM'_'$NNT'x'$NESY'_smth 5-0_2_c1'$ANBEG'_xs'$NESX'.rst'

# Compilation fortran ##################################################

#module load intel-compilers/12.0.4.191
WHICHIFORT=`which ifort`
IFORTINSTALLDIR="${WHICHIFORT%/bin/*}"
source $IFORTINSTALLDIR/bin/compilervars.sh intel64

cp $MAINCOD $MAINCODE
sed -e "s#FORMAT(a14,64f8.5)#FORMAT(a14,$XFORMAT)#g" $MAINCODE > tmp
mv tmp $MAINCODE
sed -e "s#$FORCOD#$FORCODE#g" $MAINCODE > tmp
mv tmp $MAINCODE

echo ''
echo 'Compiler le code fortran ? (o/n)'
#if [ "$1" ] ; then
REPONSE='o'
echo $REPONSE
#else
#   read -e REPONSE
#fi

if [ "$REPONSE" = "o" ] ; then
   
#  f95 $MAINCODE -o $EXENAME
   ifort $MAINCODE -O3 -mcmodel medium -shared-intel -o $EXENAME >$EXENAME'.out' 2>&1
   cat $EXENAME'.out'
   
   REMARK=`cat $EXENAME'.out' | grep -ci 'remark'`
   echo '*   remarks : '$REMARK
   WARNING=`cat $EXENAME'.out' | grep -ci 'warning'`
   echo '**  warnings: '$WARNING
   ERROR=`cat $EXENAME'.out' | grep -ci 'error'`
   echo '*** errors  : '$ERROR
   
   if [ $ERROR -ne 0 ] ; then
      echo 'Exiting script...'
      exit
   fi
   if [ $WARNING -ne 0 ] ; then
      echo 'Continue ? (o/n)'
      read -e REPONSE
      if [ "$REPONSE" != "o" ] ; then exit ; fi
   fi
   
   echo "$MAINCODE compile !"
   
#  for t in 3 2 1 ; do
#     echo $t
#     sleep 1
#  done
   
fi
   
# Loop over database seeds #############################################

for DATSEED in $DATSEEDV ; do
   
   SIMNAME=`echo "$SIMNAM" | sed s/zzzz/$DATSEED/`
   BMRFILE=`echo "$BMRFIL" | sed s/zzzz/$DATSEED/`
   MAGFILE=`echo "$MAGFIL" | sed s/zzzz/$DATSEED/g`
   LOGFILE=`echo "$LOGFIL" | sed s/zzzz/$DATSEED/g`
   INCRFILE=`echo "$INCRFIL" | sed s/zzzz/$DATSEED/g`
   INCRSURF=`echo "$INCRSUR" | sed s/zzzz/$DATSEED/g`
   
   mkdir -pv $SIMDIR/$SIMNAME
#  echo "Nettoyer le dossier $SIMDIR/$SIMNAME ? (o/n)"
#  read -e REPONSE
#  if [ $REPONSE = "o" ] ; then
#     rm -v $SIMDIR/$SIMNAME/*
#  fi
   
   echo ''
   echo 'Base de donnee '$DATSEED':'
   
#  echo ''
#  echo 'Quelle simulation (PIKSEED) analyser ?'
#  echo "('0' pour valeurs par defaut, '10' a '99' pour simulations individuelles,"
#  echo " 'm' pour la moyenne sur les PIKSEEDs, 'w' pour moyenne sur toutes les databases)"
#  read -e SIMID
   
   if [ $SIMID -lt 10 ] ; then
      
      XFIT=$XFITDEF
      XFITMIN=$XFITDEF
      XFITMAX=$XFITDEF
      
   else
   if [ $SIMID -ge 100 ] ; then
      
      XFIT=$XFITDEF
      XFITMIN=$XFITDEF
      XFITMAX=$XFITDEF
      
   else
      
      FLAG=0
      while read TEMP ; do
      if [ $FLAG -ne 5 ] ; then
#        echo "$TEMP"
#        if [ $FLAG -eq 5 ] ; then
#           XFITMAX="$TEMP"
#           FLAG=6
#        fi
         if [ $FLAG -eq 4 ] ; then
            XFITMID="$TEMP"
            FLAG=5
         fi
         if [ $FLAG -eq 3 ] ; then
            DFIT=$TEMP
            FLAG=4
         fi
         if [ $FLAG -eq 2 ] ; then
            XFIT="$TEMP"
            FLAG=3
	 fi
	 if [ $FLAG -eq 1 ] ; then
	    FIT=$TEMP
	    FLAG=2
	 fi
	 if [ "$TEMP" = "$SIMID" ] ; then
            FLAG=1
            echo $SIMID
         fi
      fi
      done < $LOGFILE.stat
      
      echo 'fitmax (1) ou fitmid (0) ?'
      read -e REP
      if [ $REP -eq 0 ] ; then
         XFIT="$XFITMID"
      fi
      XFITMIN="$XFIT"
      XFITMAX="$XFIT"
            
   fi
   fi
   echo ''
   echo 'Parametres :'   
   echo "$XFIT"   
#   14967

   rm -f $INPUT
   echo "$DATETIME" >> $INPUT
   echo "$FSTCYC $NCYC $NREP $TFITF" >> $INPUT
   echo "$BMRTYPE $BLDSEED $BLDSEEDD $DATSEED" >> $INPUT                #(Francois)
   echo "$INCRFILE" >> $INPUT
   echo "$INCRSURF" >> $INPUT
   echo "$BMRFILE" >> $INPUT
   echo "$MAGFILE" >> $INPUT
   echo "$SIMNAME" >> $INPUT
   echo "$SIMID" >> $INPUT
   echo "$NPAR" >> $INPUT
   echo "$XFIT" >> $INPUT
   echo "$XFITMIN" >> $INPUT
   echo "$XFITMAX" >> $INPUT
#  echo '' >> $INPUT
   
   echo ''
   echo 'Calculer la simulation associee ? (o/n)'
#   if [ "$1" ] ; then
   REPONSE='o'
   echo $REPONSE
#   else
#      read -e REPONSE
#   fi
   if [ "$REPONSE" = "o" ] ; then
      ./$EXENAME < $INPUT
   fi
   
   echo ''
   echo 'Produire les images de la dynamo ? (o/n)'
#   if [ "$1" ] ; then
   REPONSE='o'
   echo $REPONSE
#   else
#      read -e REPONSE
#   fi
   if [ "$REPONSE" = "o" ] ; then
      cd ./1-analyse
      idl -e bldyn_bfly -args 'blds_'$SIMNAM'_'$SIMID'_'$DATETIME'.idlin'
      cd ..
   fi
   
   echo ''
   echo 'Produire l animation de la dynamo ? (o/n)'
#   if [ "$1" ] ; then
   REPONSE='n'
   echo $REPONSE
#   else
#      read -e REPONSE
#   fi
   if [ "$REPONSE" = "o" ] ; then
      cd ./1-analyse
      idl -e bldyn_anim -args 'blds_'$SIMNAM'_'$SIMID'_'$DATETIME'.idlin'
      cd ..
   fi
   
   echo ''
   echo 'Produire les images de surface ? (o/n)'
#   if [ "$1" ] ; then
   REPONSE='n'
   echo $REPONSE
#   else
#      read -e REPONSE
#   fi
   if [ "$REPONSE" = "o" ] ; then
      cd ./1-analyse
      idl -e surfbr_mbflyfit -args 'blds_'$SIMNAM'_'$SIMID'_'$DATETIME'.s.idlin'
      cd ..
   fi
   
   echo ''
   echo 'Produire l animation de surface ? (o/n)'
#   if [ "$1" ] ; then
   REPONSE='n'
   echo $REPONSE
#   else
#      read -e REPONSE
#   fi
   if [ "$REPONSE" = "o" ] ; then
      cd ./1-analyse
      idl -e surfbr_anim -args 'blds_'$SIMNAM'_'$SIMID'_'$DATETIME'.s.idlin'
      cd ..
   fi
   
done

wait

echo ''
echo 'Nettoyer les fichiers de compilation et d input ? (o/n)'
#if [ "$1" ] ; then
REPONSE='o'
echo $REPONSE
#else
#   read -e REPONSE
#fi

if [ "$REPONSE" = "o" ] ; then
   rm -v $EXENAME
   rm -v $EXENAME.out
   rm -v $EXENAME.mod
   rm -v $EXENAMES.mod
   rm -v $FORCODE
   rm -v $SURFCODE
   rm -v $MAINCODE
   rm -v $INPUT
fi
########################################################################
